<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Movimientos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('movimientos_modelo');
                $this->load->model('configuracion_modelo');
                $this->data['empresa']=$this->configuracion_modelo->datos_empresa();
	}
	function index()
	{
                $this->load->library('pagination');
		
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		
		if(isset($datos['buscar'])){
			$buscar=$datos['buscar'];
		}else{
			$buscar=$this->input->post('buscar');
		}		
		if($buscar){
			$filtro="buscar/".$buscar;
		}
                if($this->session->userdata('rol')==1){
                    $vendedores=null;
                }else{
                    $vendedores=$this->session->userdata('numeros_vendedor');
                }
		$config['base_url']=site_url('movimientos/index');
		$config['total_rows']=$this->movimientos_modelo->total_listar_movimientos($vendedores,$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		
		$this->data['buscar']=$buscar;
		$this->data['movimientos']=$this->movimientos_modelo->listar_movimientos($config['per_page'],$this->uri->segment(3),$vendedores,$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
		$this->data['total_movimientos']=$config['total_rows'];
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Movimientos';
		$this->data['contenido']='movimientos/index';
		$this->load->view('template/contenido',$this->data);
	}
        function ver_movimiento($numero,$seguridad=null,$retornar=false,$imprimir=false){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->movimientos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['seguridad']=$this->config->item('encription_key');
				$this->data['encabezado']=$encabezado->row();
                                $this->data['imprimir']=$imprimir;
				$this->data['productos']=$this->movimientos_modelo->leer_productos($numero)->result();
                                if($retornar){
                                    return $this->load->view('movimientos/ver_movimiento',$this->data,true);
                                }else{
                                    $this->load->view('movimientos/ver_movimiento',$this->data);
                                }
				
			}else{
				echo "El pedido no existe";
			}
		}else{
			echo "No tiene permisos para imprimir este documento";
		}
	}
        function imprimir_movimiento($numero,$seguridad=null){
            	$verificar=sha1($numero.$this->config->item('encription_key'));
            	if($verificar==$seguridad){
                    $this->ver_movimiento($numero,$seguridad,false,true);
		}else{
			echo "No tiene permisos para imprimir este documento";
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */