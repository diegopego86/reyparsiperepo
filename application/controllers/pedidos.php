<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pedidos extends CI_Controller {
    	var $data=array();
	function __construct(){
                parent::__construct();
                date_default_timezone_set('America/Bogota');
		$this->load->model('usuarios_modelo');
		$accion=$this->uri->segment(2);
		if($accion!='imprimir_pedido' && $accion!='ver'){
			if(!$this->usuarios_modelo->verificar_sesion()){
				$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
				if(!$this->input->post('ajax')){
					redirect('sipe', 'refresh');
				}else{
					echo "error_sesion";
				}
				die();
			}
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('pedidos_modelo');
                $this->load->model('configuracion_modelo');
                $this->data['empresa']=$this->configuracion_modelo->datos_empresa();
		$this->data['configuracion']=$this->configuracion_modelo->configuracion_pedido()->row_array();
                        //array('ver_ude'=>0,'cambiar_asesor'=>0,'listar_retenciones'=>0,'listar_listas'=>0,'listar_plazos'=>0,'listar_descuentos'=>1,'modificar_precio'=>0,'modificar_dcto'=>0,'observaciones'=>1,'productos_mostrar_unidad'=>0,'productos_mostrar_lista1'=>0,'productos_mostrar_lista2'=>0,'productos_mostrar_lista3'=>0,'productos_mostrar_lista4'=>0,'productos_mostrar_tipo'=>0,'buscar_productos_mostrar_unidad'=>0,'buscar_productos_mostrar_existencia'=>0);
	}
	
	function index()
	{
		$this->data['pedidos']=$this->listar_pedidos('pedidos/index');
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/index';
		$this->load->view('template/contenido',$this->data);
	}
	function nuevo(){
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde','jquery.lightbox-0.5');
		$this->data['titulo']='Pedidos - Nuevo Pedido';
                $this->data['observaciones_predeterminadas']=$this->pedidos_modelo->listar_observaciones_pedido(1)->result();
		$this->data['contenido']='pedidos/nuevo';
		$this->load->view('template/contenido',$this->data);
	}
	function buscar_cliente(){
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		
		$buscar=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('pedidos/buscar_cliente');
		if($this->session->userdata('rol')==3){
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_nits_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}else{
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}
		$config['per_page']=7;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		if($this->session->userdata('rol')==3){
			$this->data['clientes']=$this->clientes_modelo->buscar_nits_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}else{
			$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}
		$this->data['cliente']=$buscar;
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/buscar_cliente';
		
		if($ajax){
			$json=array('estado'=>'correcto','mensaje'=>'Se listaron los clientes correctamente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
		}else{
			$json=array('estado'=>'error','mensaje'=>'Está ingresando de forma no autorizada.');
		}
                echo json_encode($json);
	}
	function datos_cliente($cliente=null){
		if(!$cliente){
			$cliente=$this->input->post('cliente');
		}
		if($cliente){
			$this->load->model('clientes_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');
			$datos_cliente=$this->clientes_modelo->leer_cliente($cliente,$this->session->userdata('numeros_vendedor'));
			if($datos_cliente->num_rows >0){
				$this->data['cliente']=$datos_cliente->row();
				$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
				$this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
				//$this->data['cartera']=$this->cartera_modelo->cartera_cliente(0,0,$cliente,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
				$this->data['cartera']=null;
				$this->data['titulo']='Pedidos';
				$this->data['contenido']='pedidos/datos_cliente';		
				$json=array('estado'=>'correcto','mensaje'=>'Se cargaron los datos del cliente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
			}else{
				$json=array('estado'=>'error','mensaje'=>'El cliente no existe.');
			}
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar la consulta');			
		}
                echo json_encode($json);
	}
	function buscar_producto(){
		$this->load->library('pagination');
		$this->load->model('productos_modelo');
		
		$buscar=strtoupper($this->input->post('producto'));
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('pedidos/buscar_producto');
		$config['total_rows']=$this->productos_modelo->resultados_buscar_producto($buscar);
		$config['per_page']=12;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		$this->data['productos']=$this->productos_modelo->buscar_producto($config['per_page'],$this->uri->segment(3),$buscar,$orden)->result();
		if($buscar){
			$this->data['producto']=$buscar;
		}else{
			$this->data['producto']='Digite el código o nombre del producto';
		}
		$this->data['estilos']=array('jquery.lightbox-0.5');
		$this->data['titulo']='Pedidos';
                $lista_cliente=$this->input->post('lista_cliente');
                if(!$lista_cliente){
                    $lista_cliente=1;
                }
                $this->data['lista_cliente']=$lista_cliente;
		$this->data['contenido']='pedidos/buscar_producto';
		if($ajax){
                    $json=array('estado'=>'correcto','mensaje'=>'Se han cargado los productos correctamente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
                    echo json_encode($json);
		}else{
                    $this->load->view('template/contenido',$this->data);
		}
	}
	function datos_producto(){
		$producto=$this->input->post('codigo');
		if($producto){
			$this->load->model('productos_modelo');
			$producto=$this->productos_modelo->leer_producto($producto);
			$ajax=$this->input->post('ajax');
			if($producto->num_rows>0){
				$producto=$producto->row();
				if($producto->precio1 > 0){					
					if($ajax){
                                            $stock=$producto->stock;
                                            $decimal=strstr($stock,'.');
                                            if($decimal=='.00' || !$decimal){
                                                $stock = number_format($stock,0,',','.');
                                            }else{
                                                $stock = number_format($stock,2,',','.');
                                            }
						$datos = array(
                                                        'estado'=>'correcto',
                                                        'mensaje'=>'Se ha agregado el producto correctamente.',
							'nombre'    =>    $producto->nombre,
                                                        'unidad'    =>    $producto->unidad,
                                                        'stock'    =>    $stock,
							'referencia'    =>    $producto->referencia,
							'precio1'    =>    number_format($producto->precio1,0),
							'precio2'    =>    $producto->precio2,
							'precio3'    =>    $producto->precio3,
                                                        'precio4'    =>    $producto->precio4,
							'iva'      =>    number_format($producto->iva,0)
						);
						echo json_encode($datos);
					}else{
						echo 'No esta accediendo de forma correcta';
					}
				}else{
					$datos = array(
                                                'estado'=>'error',
                                                'numero'=>1,
                                                'mensaje'=>'El producto se encuentra agotado.',
						'nombre'=>$producto->nombre,
						'referencia'    =>    0,
                                                'unidad'    =>    0,
                                                'stock'    =>    0,
						'precio1'    =>    0,
						'precio2'    =>    0,
						'precio3'    =>    0,
                                                'precio4'    =>    0,
						'iva'      =>    0
					);
					echo json_encode($datos);					
				}
			}else{
				if($ajax){
					$datos = array(
                                            'estado'=>'error',
                                            'numero'=>2,
                                            'mensaje'=>'El producto no existe',
                                            'nombre'    =>    'El producto no existe.'
					);
					echo json_encode($datos);
				}else{
					echo 'No esta accediendo de forma correcta';
				}
			}
		}else{
			echo 0;
		}			
	}
        function buscar_cotizacion_a_cargar(){
                $this->load->library('pagination');
		$this->load->model('cotizaciones_modelo');
                        
		$buscar=$this->input->post('buscar');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('pedidos/buscar_cotizacion_a_cargar');
		$config['total_rows']=$this->cotizaciones_modelo->total_listar_cotizaciones($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->pagination->initialize($config);
		$this->data['pedidos']=$this->cotizaciones_modelo->listar_cotizaciones($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['cliente']=$buscar;
		$this->data['seguridad']=$this->config->item('encription_key');
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/buscar_cotizacion_a_cargar';
		
		if($ajax){
			$json=array('estado'=>'correcto','mensaje'=>'Se han listado los pedidos.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
                        echo json_encode($json);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
        }
	function buscar_pedido_a_copiar(){
		$this->load->library('pagination');
		
		$buscar=$this->input->post('buscar');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('pedidos/buscar_pedido_a_copiar');
		$config['total_rows']=$this->pedidos_modelo->total_listar_pedidos($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->pagination->initialize($config);
		$this->data['pedidos']=$this->pedidos_modelo->listar_pedidos($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['cliente']=$buscar;
		$this->data['seguridad']=$this->config->item('encription_key');
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/buscar_pedido_a_copiar';
		
		if($ajax){
			$json=array('estado'=>'correcto','mensaje'=>'Se han listado los pedidos.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
                        echo json_encode($json);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
        function cargar_cotizacion($cotizacion){
            if($cotizacion){
                $this->load->model('cotizaciones_modelo');
		$productos=$this->cotizaciones_modelo->productos_cotizacion($cotizacion)->result();
		$resultados=array();
		foreach($productos as $producto):
				$datos_producto = array(
					'codigo'    =>    $producto->id_producto,
					'nombre'    =>    $producto->nombre,
                                        'referencia'    =>    $producto->referencia,
					'cantidad'    =>    $producto->cantidad,
					'unidad'    =>    $producto->unidad,
                                        'stock'    =>    $producto->stock,
					'precio'    =>    $producto->precio1,
					'precio1'    =>    $producto->precio1,
					'precio2'    =>    $producto->precio2,
					'precio3'    =>    $producto->precio3,
					'iva'      =>    $producto->iva,
					'descuento'      =>    $producto->descuento,
					'total'      =>    $producto->total
				);
				$resultados[]=$datos_producto;
		endforeach;
                $json=array('estado'=>'correcto','mensaje'=>'Se ha copiado la cotizacion.','pedido'=>$cotizacion,'productos'=>$resultados);
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar esta acción.');
            }
		echo json_encode($json);
	}
	function copiar_productos_pedido($pedido){
            if($pedido){
		$productos=$this->pedidos_modelo->productos_pedido($pedido)->result();
		$resultados=array();
		foreach($productos as $producto):
                    $stock=$producto->stock;
                    $decimal=strstr($stock,'.');
                    if($decimal=='.00' || !$decimal){
                        $stock = number_format($stock,0,',','.');
                    }else{
                        $stock = number_format($stock,2,',','.');
                    }
				$datos_producto = array(
					'codigo'    =>    $producto->id_producto,
					'nombre'    =>    $producto->nombre,
                                        'referencia'    =>    $producto->referencia,
					'cantidad'    =>    $producto->cantidad,
					'unidad'    =>    $producto->unidad,
                                        'stock'    =>    $stock,
					'precio'    =>    $producto->precio1,
					'precio1'    =>    $producto->precio1,
					'precio2'    =>    $producto->precio2,
					'precio3'    =>    $producto->precio3,
					'iva'      =>    $producto->iva,
					'descuento'      =>    $producto->descuento,
					'total'      =>    $producto->total
				);
				$resultados[]=$datos_producto;
		endforeach;
                $json=array('estado'=>'correcto','mensaje'=>'Se ha copiado el pedido.','pedido'=>$pedido,'productos'=>$resultados);
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar esta acción.');
            }
		echo json_encode($json);
	}
	function leerExcel(){
		$this->load->library('PHPExcel/IOFactory');
		$nombre=$_FILES['archivo']['name'];
		$datos=explode('.',$nombre);
		$tipo=$datos[count($datos)-1];
		$error=true;
		if($tipo=='xls'){
			$objReader =$this->iofactory->createReader('Excel5');
			$error=false;
		}else{
			if($tipo=='xlsx'){
				$objReader =$this->iofactory->createReader('Excel2007');
				$error=false;
			}
		}
		if(!$error){
			$this->load->model('clientes_modelo');
			$this->load->model('productos_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');		

			$objReader->setReadDataOnly(true);
			$objPHPExcel=$objReader->load($_FILES['archivo']['tmp_name']);
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$maxima_fila=$objWorksheet->getHighestRow();
			$nit=$objWorksheet->getCell('C2')->getCalculatedValue();
			$datos_cliente=$datos_cliente=$this->clientes_modelo->leer_cliente($nit,$this->session->userdata('numeros_vendedor'));
			
			if($datos_cliente->num_rows >0){
                            $this->data['cliente']=$datos_cliente->row();
                            $orden_cartera=null;
                            $cartera=$this->cartera_modelo->cartera_cliente(0,0,$nit,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
                        }else{
                            $this->data['cliente']='';
                            $cartera='';
                        }
				$this->data['observaciones']=$objWorksheet->getCell('B7')->getCalculatedValue();
				$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
				$this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
				$this->data['cartera']=$cartera;
				$productos=array();
				$no_existen=array();
				for($n=10;$n<=$maxima_fila;$n++){
					$producto=$this->productos_modelo->leer_producto($objWorksheet->getCellByColumnAndRow(1,$n)->getCalculatedValue());
					if($producto->num_rows>0){
						$producto=$producto->row();
						//if($producto->precio1>0){//Verificar si el producto se encuentra agotado
                                                    $stock=$producto->stock;
                                                    $decimal=strstr($stock,'.');
                                                    if($decimal=='.00' || !$decimal){
                                                        $stock = number_format($stock,0,',','.');
                                                    }else{
                                                        $stock = number_format($stock,2,',','.');
                                                    }
							$dato=array('codigo'=>$producto->id_producto,
										'nombre'=>$producto->nombre,
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>$producto->referencia,
										'iva'=>$producto->iva,
										'unidad'=>$producto->unidad,
                                                                                'stock'=>$stock,
										'precio1'=>$producto->precio1,
										'precio2'=>$producto->precio2,
										'precio3'=>$producto->precio3,
										'precio'=>$producto->precio1);
							array_push($productos,$dato);
						/*}else{
							$dato=array('codigo'=>$producto->id_producto,
										'nombre'=>  $producto->nombre,
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>'',
										'iva'=>0,
										'unidad'=>0,
                                                                                'stock'=>0,
										'precio1'=>0,
										'precio2'=>0,
										'precio3'=>0,
										'precio'=>0);
							array_push($productos,$dato);
						}*/
					}else{
							$dato=array('codigo'=>$objWorksheet->getCellByColumnAndRow(1,$n)->getCalculatedValue(),
										'nombre'=>'El producto no existe',
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>'',
                                                                                'stock'=>'',
										'iva'=>0,
										'unidad'=>0,
										'precio1'=>0,
										'precio2'=>0,
										'precio3'=>0,
										'precio'=>0);
							array_push($productos,$dato);												
					}
				}
				function compare($x, $y)
				{
				 if ( $x['codigo'] == $y['codigo'] )
				  return 0;
				 else if ( $x['codigo'] < $y['codigo'] )
				  return -1;
				 else
				  return 1;
				}			
				usort($productos, 'compare');
				
                                echo json_encode($productos);
				/*$this->data['productos']=$productos;
				$this->data['titulo']='Pedidos';
				$this->data['contenido']='pedidos/importar_pedido_formato';
				echo $this->load->view($this->data['contenido'],$this->data,true);
                                 * */
                                 			
		}else{
			echo 'El archivo debe ser de excel xls o xlsx';
		}
	}
	function subir_archivo(){
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/subir_archivo';
		$this->load->view('template/contenido',$this->data);
	}
        function prefijo(){
            $prefijo=str_split('AB');
            $letra=count($prefijo)-1;
            $abc=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $prueba=$this->pedidos_modelo->prefijo_cambio_letra($prefijo,$letra,$abc);
            print_r($prueba);
        }
	function guardar(){
		$ajax=$this->input->post('ajax');
		$mostrar='';
		if($ajax){
			$borrador=$this->input->post('numero_borrador');
			$encabezado=$this->input->post('encabezado');
			$productos=$this->input->post('productos');
			
			if($numeros=$this->pedidos_modelo->guardar_pedido($borrador,$encabezado,$productos)){
                            foreach($numeros as $numero):
                                $mensaje=$this->ver($numero,sha1($numero.$this->config->item('encription_key')),false,true,true,false,false);
                                $mostrar=$mostrar.$mensaje.'<br/>';
                            endforeach;
                            $this->data['numeros']=$numeros;
                            $this->data['mostrar']=$mostrar;
                            $this->data['envio']='';
                            $this->data['titulo']='Pedidos - Pedidos enviados correctamente.';
                            $mensaje=$this->load->view('pedidos/pedidos_enviados',$this->data,true);
                            $json=array('estado'=>'correcto','mensaje'=>'Se guardó el pedido correctamente.','vista'=>$mensaje,'numeros'=>$numeros);
                        }else{
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar el pedido');                            
                        }
                        //$envio=$this->enviar_pedidos($numeros,$vendedor,$cliente,$encabezado['email_copias']);			
                        //$envio='';
			
                        echo json_encode($json);
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Error Estas accediendo de forma no autorizada.','data'=>json_encode($this->input->post()));
                    echo json_encode($json);
		}
	}
        function guardar_test(){
                $ajax=$this->input->post('ajax');
		$mostrar='';
			$borrador=$this->input->post('numero_borrador');
			$encabezado=$this->input->post('encabezado');
			$productos=$this->input->post('productos');
			
			if($numeros=$this->pedidos_modelo->guardar_pedido_test($borrador,$encabezado,$productos)){
                            foreach($numeros as $numero):
                                $mensaje=$this->ver($numero,sha1($numero.$this->config->item('encription_key')),false,true,true,false,false);
                                $mostrar=$mostrar.$mensaje.'<br/>';
                            endforeach;
                            $this->data['numeros']=$numeros;
                            $this->data['mostrar']=$mostrar;
                            $this->data['envio']='';
                            $this->data['titulo']='Pedidos - Pedidos enviados correctamente.';
                            $mensaje=$this->load->view('pedidos/pedidos_enviados',$this->data,true);
                            $json=array('estado'=>'correcto','mensaje'=>'Se guardó el pedido correctamente.','vista'=>$mensaje,'numeros'=>$numeros);
                        }else{
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar el pedido');                            
                        }
                        //$envio=$this->enviar_pedidos($numeros,$vendedor,$cliente,$encabezado['email_copias']);			
                        //$envio='';
			
                        echo json_encode($json);
		
        }
	function enviar_pedidos($pedidos=null,$vendedor=null,$cliente=null,$otros=null){
            
            if(!$pedidos){
                $pedidos=$this->input->post('pedidos');
            }
            if($pedidos){
                if(!$vendedor){
                    $vendedor=$this->input->post('vendedor');
                }
                if(!$cliente){
                    $cliente=$this->input->post('cliente');
                }
                if(!$otros){
                    $otros=$this->input->post('otros');
                }
                $this->load->model('configuracion_modelo');
                $this->load->model('vendedores_modelo');
                $this->load->model('clientes_modelo');
                
                $enviado=false;

                $configuracion=$this->data['empresa'];

                $correos_envio=$this->configuracion_modelo->correos_envio('pedidos');
                $correo_vendedor=null;
                if($vendedor){
                    if($this->data['configuracion']['enviar_copia_vendedor']){
                        $correo_vendedor=$this->vendedores_modelo->correo($vendedor);
                    }
                }
                $correo_cliente=null;
                if($cliente){
                    if($this->data['configuracion']['enviar_copia_cliente']){
                        $correo_cliente=$this->clientes_modelo->correo($cliente);
                    }
                }

                if($configuracion->tipo_envio==1){
                    $this->load->library('phpmailer');

                    $this->phpmailer->From     = $configuracion->correo_envio;
                    $this->phpmailer->FromName = $configuracion->descripcion_pedidos;
                    $this->phpmailer->Host = $configuracion->host;
                    $this->phpmailer->Password = $configuracion->clave;
                    $this->phpmailer->Port = $configuracion->port;
                    //$this->phpmailer->Mailer='smtp';
                    $this->phpmailer->CharSet = "UTF-8";
                    foreach($correos_envio as $correo):
                        $this->phpmailer->AddAddress($correo->correo);                
                    endforeach;
                    if($correo_vendedor){
                        $this->phpmailer->AddAddress($correo_vendedor);                
                    }
                    if($correo_cliente){
                        $this->phpmailer->AddAddress($correo_cliente);                
                    }                    
                    if($otros){
                        $otros_array=explode(',',$otros);
                        foreach($otros_array as $otro){
                            if($this->es_mail(trim($otro))){
                                    $this->phpmailer->AddAddress(trim($otro));                            
                            }
                        }
                    }
                    if(is_array($pedidos)){
                        foreach($pedidos as $pedido){
                            $this->phpmailer->Subject    = 'Pedido # '.$pedido;
                            $mensaje=$this->ver($pedido,sha1($pedido.$this->config->item('encription_key')),true,false,true,false);
                            $this->phpmailer->Body=$mensaje;
                            if($this->phpmailer->Send()){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los pedidos correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los pedidos');
                            }                        
                        }
                    }else{
                        $pedido=$pedidos;
                        $this->phpmailer->Subject    = 'Pedido # '.$pedido;
                        $mensaje=$this->ver($pedido,sha1($pedido.$this->config->item('encription_key')),true,false,true,false);
                        $this->phpmailer->Body=$mensaje;
                        if($this->phpmailer->Send()){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el pedido correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el pedido');
                        }                        
                    }
                }else{

                    $config=array();
                    $config['protocol']='smtp';  
                    $config['smtp_host']=$configuracion->host;  
                    $config['smtp_port']=$configuracion->port;  
                    $config['smtp_timeout']='30';  
                    $config['smtp_user']=$configuracion->correo_envio;  
                    $config['smtp_pass']=$configuracion->clave;  
                    $config['charset']='utf-8';  
                    $config['mailtype']="html";
                    $config['newline']="\r\n";

                    $this->load->library('email',$config);
                    $para=array();
                    foreach($correos_envio as $correo):
                        $para[]=$correo->correo;
                    endforeach;
                    if($correo_vendedor){
                        $para[]=$correo_vendedor;                        
                    }
                    if($correo_cliente){
                        $para[]=$correo_cliente;
                    }
                    if($otros){
                        $otros_array=explode(',',$otros);
                        foreach($otros_array as $otro){
                            if($this->es_mail(trim($otro))){
                                    $para[]=trim($otro);                            
                            }
                        }
                    }

                    if(is_array($pedidos)){
                        foreach($pedidos as $pedido){
                            $this->email->from($configuracion->correo_envio);
                            $this->email->to($para); 
                            $this->email->subject('Pedido # '.$pedido);
                            $mensaje=$this->ver($pedido,sha1($pedido.$this->config->item('encription_key')),true,false,true,false);
                            $this->email->message($mensaje);
                            $envio=$this->email->send();
                            if($envio){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los pedidos correctamente');
                            }
                            else
                            {
                                $enviado=false; 
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los pedidos');
                            }                    
                        }
                    }else{
                        $pedido=$pedidos;

                        $this->email->from($configuracion->correo_envio,$configuracion->descripcion_pedidos);  
                        $this->email->to($para);  
                        $this->email->subject('Pedido # '.$pedido);
                        $mensaje=$this->ver($pedido,sha1($pedido.$this->config->item('encription_key')),true,false,true,false);
                        $this->email->message($mensaje);
                        $envio=$this->email->send();
                        if($envio){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el pedido correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el pedido');
                        } 
                    }
                }
                echo json_encode($json);
            }else{
                $json=array('estado'=>'error','mensaje'=>'No hay pedidos para enviar');
                echo json_encode($json);
            }
	}
        function enviar_copia($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
                    $mensaje=$this->input->post('mensaje');
                    $correos=$this->input->post('email');

                    $this->load->model('configuracion_modelo');

                    $configuracion=$this->configuracion_modelo->datos_empresa();

                    if($configuracion->tipo_envio==3){
                        $this->load->library('phpmailer');
                        $this->phpmailer->From     = $configuracion->correo_envio;
                        $this->phpmailer->FromName = $configuracion->descripcion_pedidos;
                        $this->phpmailer->IsSMTP();
                        $this->phpmailer->SMTPDebug = 1;
                        $this->phpmailer->SMTPAuth   = true;
                        $this->phpmailer->SMTPSecure = "ssl";
                        $this->phpmailer->Host = $configuracion->host;
                        $this->phpmailer->Username = $configuracion->correo_envio;
                        $this->phpmailer->Port = $configuracion->port;
                        $this->phpmailer->Password = $configuracion->clave;
                        $this->phpmailer->CharSet = "UTF-8";

                        print_r($this->phpmailer);

                        //$this->load->library('email',$config);
                        $enviado=false;
                        $para=array();
                        $otros_array=explode(',',$correos);			
                        foreach($otros_array as $email){
                                if($this->es_mail(trim($email))){
                                        $this->phpmailer->AddAddress(trim($email));
                                        //$para[]=trim($email);
                                }
                        }
                        //$this->email->to($para);
                        //$this->email->subject('Pedido # '.$numero);
                        $this->phpmailer->Subject    = 'Pedido # '.$numero;
                        $mensaje=$mensaje.'<br/><br/>'.$this->ver($numero,sha1($numero.$this->config->item('encription_key')),true,false,true,false);
                        //$this->email->message($mensaje);
                        $this->phpmailer->Body=$mensaje;
                        //if($this->email->send()){
                        if($this->phpmailer->Send()){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el pedido correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el pedido');
                        }
                    }else{
                        $config=array();
                        $config['protocol']='smtp';  
                        $config['smtp_host']=$configuracion->host;  
                        $config['smtp_port']=$configuracion->port;  
                        $config['smtp_timeout']=30; 
                        $config['smtp_user']=$configuracion->correo_envio;  
                        $config['smtp_pass']=$configuracion->clave;  
                        $config['charset']='utf-8';  
                        $config['mailtype']="html";
                        $config['newline']="\r\n";

                        $this->load->library('email', $config);
                        $para=array();
                        $otros_array=explode(',',$correos);			
                        foreach($otros_array as $email){
                                if($this->es_mail(trim($email))){
                                        $para[]=trim($email);
                                }
                        }
                        
                        $this->email->from($configuracion->correo_envio,$configuracion->descripcion_pedidos);  
                        $this->email->to($para);  
                        $this->email->subject('Pedido # '.$numero);
                        $mensaje=$mensaje.'<br/><br/>'.$this->ver($numero,sha1($numero.$this->config->item('encription_key')),true,false,true,false);
                        $this->email->message($mensaje);
                        $envio=$this->email->send();
                        if($envio){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el pedido correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el pedido');
                        } 
                    }
                    echo json_encode($json);
		}else{
			echo "No tiene permisos para imprimir este pedido";
		}
	}
        function es_mail($str){
		$match = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$/";
		if(preg_match($match, $str)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function ver($numero,$seguridad=null,$autoimprimir=false,$enlace=false,$retornar=false,$mostrar_logo=true,$mostrar_encabezado=true){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->pedidos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['seguridad']=$this->config->item('encription_key');
				$this->data['encabezado']=$encabezado->row();
                                $this->data['autoimprimir']=$autoimprimir;
                                $this->data['enlace']=$enlace;
                                $this->data['mostrar_logo']=$mostrar_logo;
                                $this->data['mostrar_encabezado']=$mostrar_encabezado;
				$this->data['productos']=$this->pedidos_modelo->leer_productos($numero)->result();
                                if($retornar){
                                    return $this->load->view('pedidos/ver',$this->data,true);
                                    
                                }else{                                    
                                    $this->load->view('pedidos/ver',$this->data);
                                }
			}else{
                            if($retornar){
                                return "El pedido no existe";
                            }else{
				echo "El pedido no existe";
                            }
			}
		}else{
			echo "No tiene permisos para imprimir este pedido";
		}
	}
	function imprimir_pedido($numero,$seguridad=null){
            $verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
                    $this->ver($numero,$seguridad,true,false,false,true);		
                }else{
			echo "No tiene permisos para imprimir este recibo";
		}
	}
	function importar_subir_archivo(){
            $ajax=$this->input->post('ajax');
            if($ajax){
		$json=array('estado'=>'correcto','mensaje'=>'Se cargó el formulario.','vista'=>$this->load->view('pedidos/subir_archivo',null,true));
            }else{
                $json=array('estado'=>'error','mensaje'=>'Está accediendo de forma no autorizada.');
            }
            echo json_encode($json);
	}
	function autoguardar(){
		$numero_borrador=$this->input->post('numero_borrador');
		$encabezado=$this->input->post('encabezado');
		$productos=$this->input->post('productos');
		$fecha=date('Y-m-d H:i:s',time());
		$resultados=array();
		//echo json_encode($encabezado);
		if($productos){
			foreach($productos as $producto){
				$resultados[]=json_encode($producto);
			}		
		}
		$estado=$this->pedidos_modelo->guardar_borrador(json_encode($encabezado),json_encode($resultados),$fecha,$numero_borrador);
		$resultados=array('id'=>$estado,'fecha'=>$fecha);
		echo json_encode($resultados);
	}
	function ver_borrador($id){
		if($id){
			$borrador=$this->pedidos_modelo->leer_borrador($id);
			if($borrador->num_rows>0){
				$borrador=$borrador->row();
				$this->data['numero_borrador']=$id;
				$this->data['encabezado']=$borrador->encabezado;
                                $this->data['observaciones_predeterminadas']=$this->pedidos_modelo->listar_observaciones_pedido(1)->result();
		
				//$this->data['productos_json']=$borrador->productos;
				$this->data['productos_json']=$this->validar_productos_cargar($borrador->productos);
				$this->data['estilos']=array('jquery-ui-1.8.custom_verde','jquery.lightbox-0.5');
				$this->data['titulo']='Pedidos - Nuevo Pedido';
				$this->data['contenido']='pedidos/nuevo';
				$this->load->view('template/contenido',$this->data);
			}else{
				echo 'No hay datos';
			}
		}else{
			echo "Faltan datos para poder acceder a la información";
		}
	}
        function cargar_borrador(){
            $id=$this->input->post('borrador');
            if($id){
			$borrador=$this->pedidos_modelo->leer_borrador($id);
			if($borrador->num_rows>0){
				$borrador=$borrador->row();
                                $productos_a_cargar=$this->validar_productos_cargar($borrador->productos);
                                if($productos_a_cargar){
                                    $json=array('estado'=>'correcto','mensaje'=>'Se obtubo la iformación del borrador','encabezado'=>json_decode($borrador->encabezado),'productos'=>json_decode($productos_a_cargar));
                                }else{
                                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al cargar el borrador');
                                }
			}else{
				$json=array('estado'=>'error','mensaje'=>'El borrador no existe o ya fué eliminado.');
			}
		}else{
			$json=array('estado'=>'error','mensaje'=>'Faltan datos para poder acceder a la información');
		}
                echo json_encode($json);
        }
	function validar_productos_cargar($productos_json){
		$productos=json_decode($productos_json);
		$productos_actualizados=array();
                if($productos){
                    foreach($productos as $producto_json):
                            $producto=json_decode($producto_json);
                            $resultado=$this->pedidos_modelo->datos_producto_borrador($producto->codigo)->row();
                            if($resultado){
                                $stock=$resultado->stock;
                                $decimal=strstr($stock,'.');
                                if($decimal=='.00' || !$decimal){
                                    $stock = number_format($stock,0,',','.');
                                }else{
                                    $stock = number_format($stock,2,',','.');
                                }
                                $producto_actualizado=array('codigo'=>(isset($producto->codigo))?$producto->codigo:'',
                                        'nombre'=>$resultado->nombre,
                                        'referencia'=>$resultado->referencia,
                                        'cantidad'=>(isset($producto->cantidad))?$producto->cantidad:'',
                                        'unidad'=>$resultado->unidad,
                                        'stock'=>$stock,
                                        'precio1'=>$resultado->precio1,
                                        'precio2'=>$resultado->precio2,
                                        'precio3'=>$resultado->precio3,
                                        'precio4'=>$resultado->precio4,
                                        'descuento'=>(isset($producto->descuento))?$producto->descuento:'',
                                        'iva'=>$resultado->iva
                                );
                            }
                            else{
                                    $producto_actualizado=array('codigo'=>$producto->codigo,
                                    'nombre'=>'',
                                    'referencia'=>$producto->referencia,
                                    'cantidad'=>$producto->cantidad,
                                    'unidad'=>'',
                                    'stock'=>'',
                                    'precio1'=>'',
                                    'precio2'=>'',
                                    'precio3'=>'',
                                    'precio4'=>'',
                                    'descuento'=>$producto->descuento,
                                    'iva'=>''
                            );
                            }
                            $producto_actualizado_json=json_encode($producto_actualizado);
                            $productos_actualizados[]=$producto_actualizado_json;
                    endforeach;
                }
		$productos_actualizados_json=json_encode($productos_actualizados);
		return $productos_actualizados_json;
	}
	function eliminar_borrador($id){
            if($id){
                if($this->pedidos_modelo->eliminar_borrador($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el borrador correctamente.');                    
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar borrador.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar la acción.');
            }
            echo json_encode($json);
	}
	function imprimir_copia($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->pedidos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->pedidos_modelo->leer_productos($numero)->result();
				$this->load->view('pedidos/imprimir_copia',$this->data);
			}else{
				echo "El pedido no existe";
			}
		}else{
			echo "No tiene permisos para imprimir este pedido";
		}
	}
	function generar_excel($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->pedidos_modelo->leer_encabezado($numero);
			
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->pedidos_modelo->leer_productos($numero)->result();
				$this->load->library('PHPExcel');
				$this->load->library('PHPExcel/IOFactory');
				
				$this->data['objPHPExcel'] = new PHPExcel();

				$this->load->view('pedidos/generar_excel',$this->data);
			}else{
				echo "El pedido no existe";
			}
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este pedido";
		}
	}
        function generar_pdf($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->pedidos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->load->library('mpdf');
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->pedidos_modelo->leer_productos($numero)->result();
				$html = $this->load->view('pedidos/ver',$this->data,true);
				$this->mpdf->debug = true;
                                $this->mpdf->WriteHTML($html);
                                //$this->mpdf->SetJs('this.print(true);');
				$this->mpdf->Output('Pedido '.$numero.'.pdf','D');  
			}else{
				echo "El pedido no existe";
			}		     
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este pedido";
		}
	}
	function productos_mas_pedidos_cliente($cliente){
		if($cliente){
			$this->load->library('pagination');
			$this->load->model('clientes_modelo');
			$buscar=$this->input->post('producto');
			$ajax=$this->input->post('ajax');
			$filtro=null;
			$orden=null;
			$datos=$this->uri->uri_to_assoc(4);
			if(isset($datos['orden'])){
				$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
			}
			if(!$buscar){
				if(isset($datos['buscar'])){
					$buscar=str_replace('_',' ',$datos['buscar']);
				}
			}
			if($buscar=='Digite el código o nombre del producto'){
				$buscar='';
			}
			if($buscar){
				$filtro='buscar/'.$buscar;
			}
				
			$config['base_url']=site_url('pedidos/productos_mas_pedidos_cliente/'.$cliente);
			$config['total_rows']=$this->clientes_modelo->resultados_productos_mas_pedidos_cliente($buscar,$cliente);
			$config['per_page']=20;
			$config['num_links']=5;
			$config['uri_segment']=4;
			$config['filtro']=$filtro;
			$config['first_link'] = '&lt;&lt;';
			$config['last_link'] = '&gt;&gt;';
			$this->pagination->initialize($config);
			$this->data['productos']=$this->clientes_modelo->productos_mas_pedidos_cliente($config['per_page'],$this->uri->segment(4),$buscar,$cliente,$orden)->result();
			$this->data['cliente']=$cliente;
			$this->data['buscar']=$buscar;
			$this->data['desde']=$this->uri->segment(3)+1;
			$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
			$this->data['total_productos']=$config['total_rows'];			
			$this->data['titulo']='Productos';
			$this->data['contenido']='pedidos/productos_mas_pedidos_cliente';
			if($ajax){
					echo $this->load->view($this->data['contenido'],$this->data,false);
			}else{
				$this->load->view('template/contenido',$this->data);
				//$this->load->view($this->data['contenido'],$this->data);
			}
		}else{
			echo "Faltan datos para realizar esta acción";
		}
	}
        function imprimir_rango(){
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Pedidos';
		$this->data['contenido']='pedidos/imprimir_rango_formulario';
		$this->load->view($this->data['contenido'],$this->data);
        }
        function imprimir_rango_pedidos(){
            $tipo=$this->input->post('tipo');
            $rango=$this->input->post('rango');
            $numeros=explode(',', $rango);
            $ids=array();
            $rangos=array();
            foreach($numeros as $numero):
                if(strpos($numero, '-')===false){
                    $ids[]=($numero*1);                    
                }else{
                    $otros=explode('-', $numero);
                    $desde=str_pad($otros[0], 6, "0", STR_PAD_LEFT);
                    $rangos[]=array('desde'=>$desde,'hasta'=>str_pad($otros[count($otros)-1], 6, "0", STR_PAD_LEFT) );                    
                }
            endforeach;
            /*$verificar=sha1($numero.$this->config->item('encription_key'));
            if($verificar==$seguridad){
                    $encabezado=$this->pedidos_modelo->leer_encabezado($numero);
                    if($encabezado->num_rows>0){
                            $this->data['encabezado']=$encabezado->row();
                            $this->data['productos']=$this->pedidos_modelo->leer_productos($numero)->result();
                            $this->load->view('pedidos/imprimir_pedido',$this->data);
                    }else{
                            echo "El pedido no existe";
                    }
            }else{
                    echo "No tiene permisos para imprimir este pedido";
            }*/
            $pedidos=$this->pedidos_modelo->datos_pedidos($ids,$rangos)->result();
            $pedidos_array=array();
            foreach($pedidos as $pedido):
                $pedidos_array[$pedido->id_maestro]['encabezado']=(object) array('id_maestro'=>$pedido->id_maestro,
                                                                                 'id_cliente'=>$pedido->id_cliente,
                                                                                 'fecha'=>$pedido->fecha,
                                                                                 'hora'=>$pedido->hora,
                                                                                 'nombre'=>$pedido->nombre,
                                                                                 'contacto'=>$pedido->contacto,
                                                                                 'direccion'=>$pedido->direccion,
                                                                                 'ciudad'=>$pedido->ciudad,
                                                                                 'telefono1'=>$pedido->telefono1,
                                                                                 'id_vendedor'=>$pedido->id_vendedor,
                                                                                 'retencion'=>$pedido->retencion,
                                                                                 'nombre_vendedor'=>$pedido->nombre_vendedor,
                                                                                 'plazo'=>$pedido->plazo,
                                                                                 'nota_1'=>$pedido->nota_1,
                                                                                 'nota_2'=>$pedido->nota_2,
                                                                                 'lista'=>$pedido->lista,
                                                                                 'subtotal_sin_dcto'=>$pedido->subtotal_sin_dcto,
                                                                                 'descuento_total'=>$pedido->descuento_total,
                                                                                 'subtotal'=>$pedido->subtotal,
                                                                                 'iva_total'=>$pedido->iva_total,
                                                                                 'total'=>$pedido->total,
                                                                            );
                $pedidos_array[$pedido->id_maestro]['productos'][]=(object) array(
                                                                                'id_producto'=>$pedido->id_producto,
                                                                                'cantidad'=>$pedido->cantidad,
                                                                                'nombre'=>$pedido->nombre_producto,
                                                                                'precio'=>$pedido->precio,
                                                                                'descuento'=>$pedido->descuento,
                                                                                'iva'=>$pedido->iva,
                                                                                'total'=>$pedido->total_item,
                                                                            
                                                                            );
            endforeach;
            $this->data['pedidos']=$pedidos_array;
            if($tipo==2){
                $this->load->view('pedidos/imprimir_rango_pedidos_tirilla',$this->data);
            }else{
                $this->load->view('pedidos/imprimir_rango_pedidos',$this->data);
            }
        }
        function condiciones($codigos){
            $codigo=explode('-',$codigos);
            $this->pedidos_modelo->grupos_productos($codigo);
        }
        function listar_pedidos($url='pedidos/listar_pedidos')
	{
		$this->load->library('pagination');
		
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		
		if(isset($datos['buscar'])){
			$buscar=$datos['buscar'];
		}else{
			$buscar=$this->input->post('buscar');
		}		
		if($buscar){
			$filtro="buscar/".$buscar;
		}		
		$config['base_url']=site_url($url);
		$config['total_rows']=$this->pedidos_modelo->total_listar_pedidos($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		
                $this->data['url']=$url;
		$this->data['buscar']=$buscar;
		$this->data['pedidos']=$this->pedidos_modelo->listar_pedidos($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
		$this->data['total_pedidos']=$config['total_rows'];
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		return $this->load->view('pedidos/listar_pedidos',$this->data,true);
	}        
        
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>