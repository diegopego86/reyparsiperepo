<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();	
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
		$this->data['configuracion']=array('cambiar_asesor'=>0,'listar_retenciones'=>0,'listar_listas'=>0,'listar_plazos'=>0,'listar_descuentos'=>1,'modificar_precio'=>0,'modificar_dcto'=>0,'observaciones'=>1,'observaciones_cartera'=>0,'observaciones_despachos'=>0,'observaciones_facturacion'=>0,'productos_mostrar_unidad'=>0,'productos_mostrar_lista1'=>0,'productos_mostrar_lista2'=>0,'productos_mostrar_lista3'=>0,'productos_mostrar_lista4'=>0,'buscar_productos_mostrar_tipo'=>0,'buscar_productos_mostrar_unidad'=>0,'buscar_productos_mostrar_existencia'=>0);
	}
	
	function index()
	{
		$this->load->library('pagination');
		$this->load->model('productos_modelo');
		
		$buscar=$this->input->post('producto');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar=='Digite el código o nombre del producto'){
			$buscar='';
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('productos/index');
		$config['total_rows']=$this->productos_modelo->resultados_buscar_producto($buscar);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		$this->data['productos']=$this->productos_modelo->buscar_producto($config['per_page'],$this->uri->segment(3),$buscar,$orden)->result();
		$this->data['buscar']=$buscar;
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
		$this->data['total_productos']=$config['total_rows'];
		$this->data['estilos']=array('jquery.lightbox-0.5');
		$this->data['titulo']='Productos';
		$this->data['contenido']='productos/index';
		if($ajax){
				echo $this->load->view($data['contenido'],$this->data,false);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */