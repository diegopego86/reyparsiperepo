<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Clientes extends CI_Controller {
    var $data = array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La p�gina que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		$this->load->model('vendedores_modelo');
		$vendedor=null;
		$buscar=$this->input->post('cliente');
		if($buscar=='Digite el nit o nombre del cliente'){
			$buscar='';
		}
		$vendedor=$this->input->post('vendedor');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if(!$vendedor){
			if(isset($datos['vendedor'])){
				$vendedor=$datos['vendedor'];
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		if($vendedor){
			$filtro=$filtro.'/vendedor/'.$vendedor;
		}
		$config['base_url']=site_url('clientes/index');
		$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'),$vendedor);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'),$vendedor)->result();
		$this->data['buscar']=$buscar;
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
                if($this->session->userdata('rol')==1){
                    $this->data['vendedores']=$this->vendedores_modelo->listar_dropdown();
                }else{
                    $this->data['vendedores']=$this->session->userdata('numeros_vendedor');
                }
		$this->data['vendedor']=$vendedor;		
		$this->data['total_clientes']=$config['total_rows'];
		$this->data['titulo']='Clientes';
		$this->data['contenido']='clientes/index';
		if($ajax){
				echo $this->load->view($this->data['contenido'],$this->data,false);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */