<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cartera extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->load->model('configuracion_modelo');
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->data['configuracion']=$this->configuracion_modelo->configuracion_cartera()->row();
	}
	function index()
	{
                $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Cartera';
		$this->data['listado_cartera']=$this->mostrar_cartera();
                $this->data['contenido']='cartera/index';
		$this->load->view('template/contenido',$this->data);
	}
        function mostrar_cartera(){
            $this->load->library('pagination');
            $this->load->model('cartera_modelo');
            $buscar=$this->input->post('buscar');
	    $ajax=$this->input->post('ajax');
            $filtro=null;
            $orden=null;
            $datos=$this->uri->uri_to_assoc(4);
            if(isset($datos['orden'])){
                    $orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
            }
            if(!$buscar){
                    if(isset($datos['buscar'])){
                            $buscar=str_replace('_',' ',$datos['buscar']);
                    }
            }
            if($buscar){
                    $filtro='buscar/'.$buscar;
            }
            $config['base_url']=site_url('cartera/mostrar_cartera');
            $config['total_rows']=$this->cartera_modelo->mostrar_cartera(0,0,$buscar);
            $config['per_page']=10;
            $config['num_links']=5;
            $config['filtro']=$filtro;
            $config['first_link'] = '&lt;&lt;';
            $config['last_link'] = '&gt;&gt;';
            $this->pagination->initialize($config);
            $this->data['cartera']=$this->cartera_modelo->mostrar_cartera($config['per_page'],$this->uri->segment(3),$buscar,$orden);
         //   print_r($this->data['cartera']);
            $this->data['cliente']=$buscar;
            $this->data['titulo']='Cartera';
            $this->data['contenido']='cartera/mostrar_cartera';
            
            if($this->input->post('ajax')){
                 echo $this->load->view('cartera/mostrar_cartera',$this->data,true);
            }else{
                 return $this->load->view('cartera/mostrar_cartera',$this->data,true);
            }            
        }
	function buscar_cliente(){
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		
		$buscar=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('cartera/buscar_cliente');
		$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		$this->data['cliente']=$buscar;
		$this->data['titulo']='Cartera';
		$this->data['contenido']='cartera/buscar_cliente';
		
		if($ajax){
			echo $this->load->view($this->data['contenido'],$this->data,false);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
	function cartera_cliente(){
		$cliente=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		if($cliente){
			$this->load->model('clientes_modelo');
			$this->load->model('cartera_modelo');
			$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
			$this->data['cliente']=$this->clientes_modelo->leer_cliente_cartera($cliente)->row();
			if($this->session->userdata('rol')==1 || $this->session->userdata('rol')==4){
				$this->data['cartera']=$this->cartera_modelo->cartera_cliente(0,0,$cliente,$orden_cartera)->result();
	            $this->data['saldo_actual']=$this->cartera_modelo->saldo_actual_cartera_cliente($cliente);
			}else{
				$this->data['cartera']=$this->cartera_modelo->cartera_cliente(0,0,$cliente,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
	            $this->data['saldo_actual']=$this->cartera_modelo->saldo_actual_cartera_cliente($cliente,$this->session->userdata('numeros_vendedor'));
			}
			$this->data['titulo']='Cartera';
			$this->data['contenido']='cartera/cartera';
			if($ajax){
				echo $this->load->view($this->data['contenido'],$this->data,false);
			}else{
				$this->load->view('template/contenido',$this->data);
			}
		}else{
			echo 'Faltan datos para realizar la operación';
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */