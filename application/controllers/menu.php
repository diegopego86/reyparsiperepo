<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller {
        var $data = array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}else{
                    $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                }
	}
	
	function index($mes=null){
		$this->load->model('pedidos_modelo');
                $this->load->model('cotizaciones_modelo');
                $this->load->model('recibos_modelo');
		if(!$mes){
			$mes=date('Y-m',time());
		}
		$this->data['fechas_pedidos']=$this->pedidos_modelo->listar_meses()->result();
                $orden=array(array('orden'=>'fecha_creacion','direccion'=>'desc'));
                $this->data['total_pedidos_mes']=$this->pedidos_vendedor_mes(date('Y-m',time()));
                $this->data['total_recibos_mes']=$this->recibos_vendedor_mes(date('Y-m',time()));
		$this->data['borradores']=$this->pedidos_modelo->listar_borradores($this->session->userdata('id_usuario'))->result();
                $this->data['borradores_cotizaciones']=$this->cotizaciones_modelo->listar_borradores($this->session->userdata('id_usuario'))->result();
                $this->data['borradores_recibos']=$this->recibos_modelo->listar_borradores($this->session->userdata('id_usuario'))->result();
		$this->data['seguridad']=$this->config->item('encription_key');
		$this->data['ultimos_pedidos']=$this->pedidos_modelo->listar_pedidos_usuario(5,0,$this->session->userdata('id_usuario'),$orden)->result();
		$this->data['estilos']=array('fullcalendar','jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Sistema de pedidos';
		$this->data['contenido']='menu/menu';
		$this->load->view('template/contenido',$this->data);
	}
        function calendario_pedidos($desde=null,$hasta=null,$actual=null){
            $desde=date('d-m-Y',$desde);
            $hasta=date('d-m-Y',$hasta);
            if(!$actual){
                $mes=date('d-m-Y',time());                
            }else{
                $mes=date('d-m-Y',$actual);                    
            }
            $this->load->model('pedidos_modelo');
            $this->load->model('usuarios_modelo');
            $vendedores=$this->usuarios_modelo->vendedores_usuario_arreglo($this->session->userdata('id_usuario'));
            $pedidos=$this->pedidos_modelo->cantidad_pedidos_vendedor_fechas($desde,$hasta,$vendedores)->result();
            $total_pedidos_mes=$this->pedidos_vendedor_mes($mes);
            $total_pedidos=array();
            $fechas=array();
            foreach($pedidos as $pedido):
                $fechas[$pedido->fecha]['fecha']=$pedido->fecha;
                $fechas[$pedido->fecha]['vendedores'][]=array(
                    'nombre'=>$pedido->nombre,
                    'cantidad'=>$pedido->cantidad,
                    'valor'=>$pedido->valor
                );
            endforeach;
            $n=1;
            foreach($fechas as $fecha):
                $texto_fecha='';
                $total_fecha=0;
                $total_pedidos_fecha=0;
                foreach($fecha['vendedores'] as $vendedor):
                    $texto_fecha=$texto_fecha.'<table class="tabla_vendedor" style="margin-top:2px;" cellpadding="0" cellspacing="0"><tr><td colspan="2">'.$vendedor['nombre'].'</td></tr><tr><td class="valores"><span style="font-size:9px;">Pedidos: '.$vendedor['cantidad'].'</span></td><td class="valores" style="text-align:right;"><b>$ '.number_format($vendedor['valor'],0,'.',',').'</b></td></tr></table>';
                    $total_fecha=$total_fecha+$vendedor['valor'];
                    $total_pedidos_fecha=$total_pedidos_fecha+$vendedor['cantidad'];
                endforeach;
                $texto_fecha=$texto_fecha.'<table style="margin-top:2px;" class="total_tabla_vendedor"><tr><td style="text-align:left;">Total:</td><td><b>$ '.number_format($total_fecha,0,'.',',').'</b></td></tr><tr><td colspan="2" style="text-align:right;font-size:11px;"><b>en '.$total_pedidos_fecha.' '.(($total_pedidos_fecha==1)?'pedido':'pedidos').'</b></td></tr></table>';
                $total_pedidos['eventos'][]=array(
                    'id'=>$n,
                    'title'=>'Evento',
                    'nombre'=>$texto_fecha,
                    'start'=>$fecha['fecha'],
                    'end'=>$fecha['fecha'],                    
                );
                $n=$n+1;
            endforeach;
            $total_pedidos['total_mes']=$total_pedidos_mes;
            echo json_encode($total_pedidos);
        }
        function calendario_recibos($desde=null,$hasta=null,$actual=null){
            $desde=date('d-m-Y',$desde);
            $hasta=date('d-m-Y',$hasta);
            if(!$actual){
                $mes=date('d-m-Y',time());                
            }else{
                $mes=date('d-m-Y',$actual);                    
            }
            $total_recibos_mes=$this->recibos_vendedor_mes($mes);
            $this->load->model('recibos_modelo');
            $vendedores=$this->usuarios_modelo->vendedores_usuario_arreglo($this->session->userdata('id_usuario'));
            $recibos=$this->recibos_modelo->cantidad_recibos_vendedor_fechas($desde,$hasta,$vendedores)->result();
            $total_recibos=array();
            $fechas=array();
            foreach($recibos as $recibo):
                $fechas[$recibo->fecha_calculada]['fecha']=$recibo->fecha_calculada;
                $fechas[$recibo->fecha_calculada]['vendedores'][]=array(
                    'nombre'=>$recibo->nombre,
                    'cantidad'=>$recibo->cantidad,
                    'valor'=>$recibo->valor
                );
            endforeach;
            $n=1;
            foreach($fechas as $fecha):
                $texto_fecha='';
                $total_fecha=0;
                $total_recibos_fecha=0;
                foreach($fecha['vendedores'] as $vendedor):
                    $texto_fecha=$texto_fecha.'<table class="tabla_vendedor" style="margin-top:2px;" cellpadding="0" cellspacing="0"><tr><td colspan="2">'.$vendedor['nombre'].'</td></tr><tr><td class="valores"><span style="font-size:9px;">Recibos: '.$vendedor['cantidad'].'</span></td><td class="valores" style="text-align:right;"><b>$ '.number_format($vendedor['valor'],0,'.',',').'</b></td></tr></table>';
                    $total_fecha=$total_fecha+$vendedor['valor'];
                    $total_recibos_fecha=$total_recibos_fecha+$vendedor['cantidad'];
                endforeach;
                $texto_fecha=$texto_fecha.'<table style="margin-top:2px;" class="total_tabla_vendedor"><tr><td style="text-align:left;">Total:</td><td><b>$ '.number_format($total_fecha,0,'.',',').'</b></td></tr><tr><td colspan="2" style="text-align:right;font-size:11px;"><b>en '.$total_recibos_fecha.' '.(($total_recibos_fecha==1)?'recibo':'recibos').'</b></td></tr></table>';
                $total_recibos['eventos'][]=array(
                    'id'=>$n,
                    'title'=>'Evento',
                    'nombre'=>$texto_fecha,
                    'start'=>$fecha['fecha'],
                    'end'=>$fecha['fecha'],                    
                );
                $n=$n+1;
            endforeach;
            $total_recibos['total_mes']=$total_recibos_mes;
            echo json_encode($total_recibos);
        }
        function pedidos_vendedor_mes($mes){
                $this->load->model('pedidos_modelo');
                $meses=array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                $data['mes']=$meses[date('n',strtotime($mes))*1-1].' '.date('Y',strtotime($mes));;
                $mes=date('Y-m',strtotime($mes));
                $data['pedidos_totales']=$this->pedidos_modelo->total_pedidos_vendedores_mes($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$mes)->result();
		$data['contenido']='pedidos/totales_pedidos_vendedor_mes';
		return $this->load->view($data['contenido'],$data,true);
	}
        function recibos_vendedor_mes($mes){
                $this->load->model('recibos_modelo');
                $meses=array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                $data['mes']=$meses[date('n',strtotime($mes))*1-1].' '.date('Y',strtotime($mes));;
                $mes=date('Y-m',strtotime($mes));
                $data['recibos_totales']=$this->recibos_modelo->total_recibos_vendedores_mes($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$mes)->result();
		$data['contenido']='recibos/totales_recibos_vendedor_mes';
		return $this->load->view($data['contenido'],$data,true);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */