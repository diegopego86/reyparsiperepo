<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cuenta extends CI_Controller {
    var $data = array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion()){
			$this->session->set_userdata('errores', 'La p�gina que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
		$this->data['titulo']='Mi Cuenta';
		$this->data['contenido']='cuenta/index';
		$this->load->view('template/contenido',$this->data);
	}
	function cambiar_clave(){
		$this->load->library('form_validation');
                $this->form_validation->set_rules('clave_actual', 'Clave actual', 'trim|required|callback_verificar_clave');
                $this->form_validation->set_rules('clave_nueva', 'Clave nueva', 'trim|required|min_length[4]');
                if ($this->form_validation->run() == false) {
                    $json=array('estado'=>'error','mensaje'=>'Verifique los datos marcados con error.');
                } else {
                    $this->load->model('usuarios_modelo');
                    $cambio=$this->usuarios_modelo->cambiar_clave($this->input->post('clave_nueva'));
                    if($cambio){
                        $json=array('estado'=>'correcto','mensaje'=>'Se ha cambiado la clave correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar cambiar la clave.');
                    }
		}
                echo json_encode($json);
	}
	function verificar_clave($clave){
                $this->load->model('usuarios_modelo');
                $es_clave=$this->usuarios_modelo->verificar_clave($clave);
                if($es_clave){
                        return true;
                }else{
                        return false;
                }
	}
        function configuracion_vista(){
            $this->load->model('usuarios_modelo');
            $datos=array(
                'tipo_vista'=>$this->input->post('tipo_vista'),
                'ancho_vista'=>$this->input->post('ancho_vista'),
                'tipo_menu'=>$this->input->post('tipo_menu')
            );
            $cambio=$this->usuarios_modelo->editar($this->session->userdata('id_usuario'),$datos);
            if($cambio){
                $json=array('estado'=>'correcto','mensaje'=>'Se guardó la configuración correctamente.');
            }else{
                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar la configuración.');
            } 
            echo json_encode($json);
        }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */