<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recibos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
                }
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('recibos_modelo');
                $this->load->model('configuracion_modelo');
	}
	
	function index()
	{
		$this->load->library('pagination');
                		

                $filtro=null;
                $orden=null;
                $datos=$this->uri->uri_to_assoc(5);
                if(isset($datos['orden'])){
                        $orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
                }

                if(isset($datos['buscar'])){
                        $buscar=$datos['buscar'];
                }else{
                        $buscar=$this->input->post('buscar');
                }		
                if($buscar){
                        $filtro="buscar/".$buscar;
                }	
                $config['base_url']=site_url('administrar/recibos/index');
                $config['total_rows']=$this->recibos_modelo->total_listar_recibos(null,$this->session->userdata('id_usuario'),$buscar);
                $config['uri_segment']=4;
                $config['per_page']=20;
                $config['num_links']=5;
                $config['filtro']=$filtro;
                $config['first_link'] = '&lt;&lt;';
                $config['last_link'] = '&gt;&gt;';
                $this->pagination->initialize($config);

                $this->data['buscar']=$buscar;
                $this->data['recibos']=$this->recibos_modelo->listar_recibos($config['per_page'],$this->uri->segment(4),null,$this->session->userdata('id_usuario'),$orden,$buscar)->result();
                $this->data['desde']=$this->uri->segment(4)+1;
                $this->data['hasta']=$this->uri->segment(4)+$config['per_page'];
                $this->data['total_recibos']=$config['total_rows'];
                $this->data['seguridad']=$this->config->item('encription_key');
                $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
                $this->data['seleccionado']='recibos';
                $this->data['titulo']='Administrar - recibos';
                $this->data['contenido']='administrar/recibos/index';
                $this->load->view('administrar/template/contenido',$this->data);
	}
        function configurar(){
            $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
            $this->data['seleccionado']='recibos';
            $this->data['correos']=$this->configuracion_modelo->correos_envio('recibos');
            $this->data['configuracion_recibo']=$this->configuracion_modelo->configuracion_recibo()->row();
            $this->data['observaciones_predeterminadas']=$this->recibos_modelo->listar_observaciones()->result();
            $this->data['titulo']='Administrar - Configurar recibos';
            $this->data['contenido']='administrar/recibos/configurar';
            $this->load->view('administrar/template/contenido',$this->data);
        }
        function modificar_configuracion($opcion,$valor){
            $ajax=$this->input->post('ajax');
            if($opcion && $ajax){
                $datos[$opcion]=$valor;
                $actualizar=$this->configuracion_modelo->modificar_configuracion_recibo($datos);
                if($actualizar){
                    $json=array('estado'=>'correcto','mensaje'=>'Se guardaron los cambios.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar los cambios.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción.');
            }
            echo json_encode($json);
        }
        function formulario_observacion($observacion=null){
            if($observacion){
                $this->data['observacion']=$this->recibos_modelo->leer_observacion($observacion)->row();
            }
            echo $this->load->view('administrar/recibos/formulario_observacion',$this->data,true);
        }
        function crear_editar_observacion(){
            $id=$this->input->post('id');
            $detalle=$this->input->post('detalle');
            $visible=$this->input->post('visible');
            if($detalle){
                $datos=array(
                      'detalle'=>$detalle,
                      'visible'=>$visible,
                );
                if(!$id || $id==''){
                    if($this->recibos_modelo->crear_observacion($datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se creó la observación correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar crear la observación.');
                    }
                }else{
                     $datos=array(
                      'detalle'=>$detalle,
                      'visible'=>$visible,
                    );
                    if($this->recibos_modelo->editar_observacion($id,$datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se editó la observación correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar editar la observación.');
                    }
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Debe colocar el detalle de la onservación');
            }
            echo json_encode($json);
        }
        function eliminar_observacion($id){
            if(!$id){
                $id=$this->input->post('id');
            }
            if($id){
                if($this->recibos_modelo->eliminar_observacion($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó la observación correctamente.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar la observación.');
                }                
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción');
            }
            echo json_encode($json);
        }
        function formulario_correo($correo=null){
            if($correo){
                $this->data['correo']=$this->configuracion_modelo->leer_correo($correo)->row();
            }
            echo $this->load->view('administrar/recibos/formulario_correo',$this->data,true);
        }
        function crear_editar_correo(){
            $id=$this->input->post('id');
            $descripcion=$this->input->post('descripcion');
            $correo=$this->input->post('correo');
            if($correo && $this->es_mail($correo)){
                $datos=array(
                      'descripcion'=>$descripcion,
                      'correo'=>$correo,
                      'tipo'=>2
                );
                if(!$id || $id==''){
                    if($this->configuracion_modelo->crear_correo($datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se agregó el correo correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar agregar el correo.');
                    }
                }else{
                     $datos=array(
                      'descripcion'=>$descripcion,
                      'correo'=>$correo,
                      'tipo'=>2
                    );
                    if($this->configuracion_modelo->editar_correo($id,$datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se editó el correo correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar editar el correo.');
                    }
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'El correo no es válido.');
            }
            echo json_encode($json);
        }
        function eliminar_correo($id){
            if(!$id){
                $id=$this->input->post('id');
            }
            if($id){
                if($this->configuracion_modelo->eliminar_correo($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el correo correctamente.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar el correo.');
                }                
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción');
            }
            echo json_encode($json);
        }
        function es_mail($str){
		$match = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$/";
		if(preg_match($match, trim($str))) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */