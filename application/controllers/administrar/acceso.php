<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Acceso extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->load->model('configuracion_modelo');
                $this->data['seleccionado']='acceso';
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	function index()
	{
		$this->data['titulo']='Administrar - Acceso.';
                $this->data['configuracion']=$this->configuracion_modelo->datos_sipe()->row();
		$this->data['contenido']='administrar/acceso/index';
		$this->load->view('administrar/template/contenido',$this->data);
	}	
        function modificar_configuracion($opcion,$valor){
            $ajax=$this->input->post('ajax');
            if($opcion && $ajax){
                $datos[$opcion]=$valor;
                $actualizar=$this->configuracion_modelo->modificar_configuracion($datos);
                if($actualizar){
                    $json=array('estado'=>'correcto','mensaje'=>'Se guardaron los cambios.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar los cambios.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción.');
            }
            echo json_encode($json);
        }
        function guardar_mensaje(){
            $ajax=$this->input->post('ajax');
            if($ajax){
                $mensaje=$this->input->post('mensaje');
                $actualizar=$this->configuracion_modelo->configuracion_guardar_mensaje($mensaje);
                if($actualizar){
                    $json=array('estado'=>'correcto','mensaje'=>'Se guardaron los cambios.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar los cambios.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción.');
            }
            echo json_encode($json);
        }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */