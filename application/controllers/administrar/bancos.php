<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bancos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
                $this->load->model('bancos_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
            $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
            $this->data['lista_cuentas']=$this->listar_cuentas();
            $this->data['lista_bancos']=$this->listar_bancos();
            $this->data['seleccionado']='bancos';
            $this->data['contenido']='administrar/bancos/index';
            if($this->input->post('ajax')){
                    echo $this->load->view($this->data['contenido'],$this->data,false);
            }else{
                    $this->load->view('administrar/template/contenido',$this->data);
            }
	}
        function listar_cuentas(){
            $this->data['tipos']=array('1'=>'Ahorros','2'=>'Corriente');
            $this->data['cuentas']=$this->bancos_modelo->listar_cuentas()->result();
            return $this->load->view('administrar/bancos/listar_cuentas',$this->data,true);
        }
        function listar_bancos(){
            $this->data['bancos']=$this->bancos_modelo->listar_bancos()->result();
            return $this->load->view('administrar/bancos/listar_bancos',$this->data,true);
        }
        function agregar_cuenta(){
            $this->data['bancos']=$this->bancos_modelo->listar_dropdown();
            echo $this->load->view('administrar/bancos/agregar_cuenta',$this->data,false);
        }
        function agregar_banco(){
            echo $this->load->view('administrar/bancos/agregar_banco',$this->data,false);
        }
        function crear_cuenta(){
            $banco=$this->input->post('banco');
            $cuenta=$this->input->post('cuenta');
            $tipo=$this->input->post('tipo');
            $datos=array(
              'banco'=>$banco,
                'cuenta'=>$cuenta,
                'tipo'=>$tipo
            );
            if($banco && $cuenta && $tipo){
                if($this->bancos_modelo->crear_cuenta($datos)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se creó la cuenta correctamente','vista'=>$this->listar_cuentas());
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar crear la cuenta');
                }
                echo json_encode($json);
            }else{
                echo "Faltan datos para realizar la acción";
            }
        }
        function eliminar_cuenta($id){
            if($id){
                if($this->bancos_modelo->eliminar_cuenta($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó la cuenta correctamente','vista'=>$this->listar_cuentas());
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar la cuenta');
                }
                echo json_encode($json);
            }else{
                echo "Faltan datos para ejecutar la acción";
            }
        }
        function crear_banco(){
            $codigo=$this->input->post('codigo');
            $nombre=$this->input->post('nombre');
            $datos=array(
              'codigo'=>$codigo,
              'nombre'=>$nombre
            );
            if($codigo && $nombre){
                if($this->bancos_modelo->crear_banco($datos)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se creó el banco correctamente','vista'=>$this->listar_bancos());
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar crear el banco');
                }
                echo json_encode($json);
            }else{
                echo "Faltan datos para realizar la acción";
            }
        }
        function eliminar_banco($id){
            if($id){
                if($this->bancos_modelo->eliminar_banco($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el banco correctamente','vista'=>$this->listar_bancos());
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar el banco');
                }
                echo json_encode($json);
            }else{
                echo "Faltan datos para ejecutar la acción";
            }
        }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */