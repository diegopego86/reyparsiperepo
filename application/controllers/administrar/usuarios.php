<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuarios extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}		
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
		if($this->session->userdata('rol')!=1){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
		$this->load->library('pagination');
		$this->load->model('usuarios_modelo');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(5);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		$buscar=$this->input->post('buscar');
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('administrar/usuarios/index');
		$config['total_rows']=$this->usuarios_modelo->total_listar_usuarios($buscar);
                $config['uri_segment']=4;
		$config['per_page']=10;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
                $this->data['buscar']=$buscar;
		$this->data['usuarios']=$this->usuarios_modelo->listar_usuarios($config['per_page'],$this->uri->segment(4),$buscar,$orden)->result();
		$this->data['desde']=$this->uri->segment(4)+1;
                $this->data['hasta']=$this->uri->segment(4)+$config['per_page'];
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
                $this->data['seleccionado']='usuarios';
		$this->data['titulo']='Administrar - Usuarios.';
		$this->data['contenido']='administrar/usuarios/index';
		$this->load->view('administrar/template/contenido',$this->data);
	}
        function crear(){
            if($this->session->userdata('rol')!=1){
                $this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
                if(!$this->input->post('ajax')){
                        redirect('sipe', 'refresh');
                }else{
                        echo "error_sesion";
                }
                die();
            }
            $ajax=$this->input->post('ajax');
            if($ajax){
                $codigo=$this->input->post('codigo');
                $nombres=$this->input->post('nombres');
                $apellidos=$this->input->post('apellidos');
                $usuario=$this->input->post('usuario');
                $clave=$this->input->post('clave');
                $email=$this->input->post('email');
                $id_vendedor=$this->input->post('id_vendedor');
                $vendedores=$this->input->post('vendedores');
                $rol=$this->input->post('rol');
                if($rol!='2'){
                    $id_vendedor=null;
                }
                $this->load->model('usuarios_modelo');
                $data=array(
                  'codigo'=>$codigo,
                  'nombres'=>$nombres,
                  'apellidos'=>$apellidos,
                  'usuario'=>$usuario,
                  'email'=>$email,
                  'id_vendedor'=>$id_vendedor,
                  'rol'=>$rol,
                  'clave'=>md5($clave),
                  'fecha_creacion'=>date('Y-m-d H:i:s',time())
                );
                if($this->usuarios_modelo->crear($data,$vendedores)){
                    $json=array('estado'=>'correcto','mensaje'=>'El usuario se creó correctamente.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar crear el usuario.');
                }
                echo json_encode($json);
            }else{
                $this->load->model('vendedores_modelo');
                $this->load->model('roles_modelo');
		$this->data['seleccionado']='usuarios';
                $this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres(array(''=>'Seleccione el vendedor'));
                $this->data['roles']=$this->roles_modelo->listar_dropdown();
                $this->data['titulo']='Administrar - Usuarios - Crear Usuario.';
                $this->data['contenido']='administrar/usuarios/crear';
                $this->load->view('administrar/template/contenido',$this->data);
            }
        }
        function editar($id){
            if($this->session->userdata('rol')!=1){
                $this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
                if(!$this->input->post('ajax')){
                        redirect('sipe', 'refresh');
                }else{
                        echo "error_sesion";
                }
                die();
            }
            if(!$id){
                $id=$this->input->post($id);
            }
            if($id){
                $ajax=$this->input->post('ajax');
                if($ajax){
                    $codigo=$this->input->post('codigo');
                    $nombres=$this->input->post('nombres');
                    $apellidos=$this->input->post('apellidos');
                    $usuario=$this->input->post('usuario');
                    $clave=$this->input->post('clave');
                    $email=$this->input->post('email');
                    $id_vendedor=$this->input->post('id_vendedor');
                    $vendedores=$this->input->post('vendedores');
                    $rol=$this->input->post('rol');
                    if($rol!='2'){
                        $id_vendedor=null;
                    }
                    $data=array(
                      'codigo'=>$codigo,
                      'nombres'=>$nombres,
                      'apellidos'=>$apellidos,
                      'usuario'=>$usuario,
                      'email'=>$email,
                      'id_vendedor'=>$id_vendedor,
                      'rol'=>$rol,                      
                      'fecha_creacion'=>date('Y-m-d H:i:s',time())
                    );
                    if($clave!=""){
                        $data['clave']=md5($clave);
                    }
                    if($this->usuarios_modelo->editar($id,$data)){
                        $datos=array();
                        if(is_array($vendedores)){
                            foreach($vendedores as $vendedor){
                                $datos[]=array('usuario'=>$id,
                                    'vendedor'=>$vendedor);
                            }
                            $this->usuarios_modelo->insertar_vendedores($id,$datos);
                        }
                        $json=array('estado'=>'correcto','mensaje'=>'El usuario se modificó correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar modifiar el usuario.');
                    }
                    echo json_encode($json);
                }else{
                    $this->load->model('usuarios_modelo');
                    $this->load->model('roles_modelo');
                    $this->data['id']=$id;
                    $this->data['usuario']=$this->usuarios_modelo->leer($id)->row();
                    $this->data['seleccionado']='usuarios';
                    $this->data['vendedores']=$this->usuarios_modelo->listar_dropdown_nombres($id,array(''=>'Seleccione el vendedor'));
                    $this->data['vendedores_usuario']=$this->usuarios_modelo->listar_vendedores($id)->result();
                    $this->data['roles']=$this->roles_modelo->listar_dropdown();
                    $this->data['titulo']='Administrar - Usuarios - Editar Usuario.';
                    $this->data['contenido']='administrar/usuarios/editar';
                    $this->load->view('administrar/template/contenido',$this->data);
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Falta información para realizar la operación.');
                echo json_encode($json);
            }
        }
        function eliminar($id){
            if($this->session->userdata('rol')!=1){
                $this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
                if(!$this->input->post('ajax')){
                        redirect('sipe', 'refresh');
                }else{
                        echo "error_sesion";
                }
                die();
            }
            if($this->session->userdata('id_usuario')!=$id){
                $ajax=$this->input->post('ajax');
                if($id && $ajax){
                    $this->load->model('usuarios_modelo');
                    if($this->usuarios_modelo->eliminar($id)){
                        echo "correcto";
                    }else{
                        echo "Se presentaron errores al intentar eliminar el usuario.";
                    }
                }else{
                    echo "Faltan datos para continuar con la operación";
                }
            }
            else{
                echo "No se puede eliminar a usted mismo";
            }
        }
        function usuario_disponible(){
            $usuario=$this->input->post('usuario');
            $this->load->model('usuarios_modelo');
            if($this->usuarios_modelo->usuario_disponible($usuario)){
                echo "correcto";
            }else{
                echo "El usuario ya está ocupado.";
            }
        }
        function asignar_nits($id){
                if($id){
                        $this->load->library('pagination');
                        $this->load->model('clientes_modelo');
                        $this->load->model('usuarios_modelo');
                        $filtro=null;
                        $orden=null;
                        $datos=$this->uri->uri_to_assoc(6);
                        if(isset($datos['orden'])){
                                $orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
                        }
                        $buscar=$this->input->post('buscar');
                        if($buscar){
                                $filtro='buscar/'.$buscar;
                        }
                        $config['base_url']=site_url('administrar/usuarios/asignar_nits');
                        $config['total_rows']=$this->clientes_modelo->total_listar_nits_cliente($buscar,$id);
                        $config['uri_segment']=5;
                        $config['per_page']=10;
                        $config['num_links']=5;
                        $config['filtro']=$filtro;
                        $config['first_link'] = '&lt;&lt;';
                        $config['last_link'] = '&gt;&gt;';
                        $this->pagination->initialize($config);
                        $this->data['clientes']=$this->clientes_modelo->listar_nits_cliente($config['per_page'],$this->uri->segment(5),$id,$buscar,$orden)->result();
                        $this->data['usuario']=$this->usuarios_modelo->leer($id)->row();
                        $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
                        $this->data['seleccionado']='usuarios';
                        $this->data['titulo']='Administrar - Usuarios - Asignar Nits';
                        $this->data['contenido']='administrar/usuarios/asignar_nits';
                        $this->load->view('administrar/template/contenido',$this->data);
                }else{
                        echo "Faltan datos para realizar la acción";
                }
        }
	function buscar_cliente(){
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		
		$buscar=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('administrar/buscar_cliente');
		if($this->session->userdata('rol')==3){
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_nits_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}else{
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		if($this->session->userdata('rol')==3){
			$this->data['clientes']=$this->clientes_modelo->buscar_nits_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}else{
			$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}
		$this->data['cliente']=$buscar;
		$this->data['titulo']='Administrar';
		$this->data['contenido']='administrar/usuarios/buscar_cliente';
		
		if($ajax){
			echo $this->load->view($this->data['contenido'],$this->data,false);
		}else{
			$this->load->view('administrar/template/contenido',$this->data);
		}
	}
	function agregar_nit_cliente($nit){
		if($this->session->userdata('rol')!=1){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
		if(!$nit){
			$nit=$this->input->post('nit');
		}
		$cliente=$this->input->post('cliente');
		if($nit && $cliente){
			$this->load->model('clientes_modelo');
			$cliente_row=$this->clientes_modelo->leer_cliente($nit);
			if($cliente_row->num_rows>0){
				$this->load->model('usuarios_modelo');
				$usuario=$this->usuarios_modelo->leer($cliente);				
				if($usuario->num_rows>0){
					$agregar=$this->usuarios_modelo->agregar_nit_cliente($nit,$cliente);
					if($agregar){
						echo "correcto";
					}else{
						echo "Se presentaron errores al intentar agregar el nit";
					}
				}else{
					echo "El cliente no está registrado como usuario";
				}
			}else{
				echo "El nit seleccionado no existe";
			}
		}else{
			echo "Faltan datos para ejecutar esta acción";
		}
	}
	function eliminar_nit_cliente($nit){
		if($this->session->userdata('rol')!=1){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
		if(!$nit){
			$nit=$this->input->post('nit');
		}
		$cliente=$this->input->post('cliente');
		if($nit && $cliente){
			$this->load->model('clientes_modelo');
			$cliente_row=$this->clientes_modelo->leer_cliente($nit);
			if($cliente_row->num_rows>0){
				$this->load->model('usuarios_modelo');
				$usuario=$this->usuarios_modelo->leer($cliente);
				if($usuario->num_rows>0){
					$agregar=$this->usuarios_modelo->eliminar_nit_cliente($nit,$cliente);
					if($agregar){
						echo "correcto";
					}else{
						echo "Se presentaron errores al intentar eliminar el nit";
					}
				}else{
					echo "El cliente no está registrado como usuario";
				}
			}else{
				echo "El nit seleccionado no existe";
			}
		}else{
			echo "Faltan datos para ejecutar esta acción";
		}		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */