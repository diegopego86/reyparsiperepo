<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contador extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
            if($this->session->userdata('rol')!=1){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
            }
            $this->load->model('pedidos_modelo');
            $desde=$this->input->post('desde');
            $hasta=$this->input->post('hasta');
            $this->data['desde']=$desde;
            $this->data['hasta']=$hasta;
            $this->data['vendedores']=$this->pedidos_modelo->cantidad_pedidos_vendedor($desde, $hasta)->result();
            $this->data['seleccionado']='contador';
            $this->data['contenido']='administrar/contador/index';
            $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
            if($this->input->post('ajax')){
                    echo $this->load->view($this->data['contenido'],$this->data,false);
            }else{
                    $this->load->view('administrar/template/contenido',$this->data);
            }
	}	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */