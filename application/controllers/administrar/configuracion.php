<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Configuracion extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
	}
	
	function index()
	{
		$this->data['titulo']='Pedidos - Pedidos enviados correctamente.';
		$this->data['contenido']='administrar/index';
		$this->load->view('administrar/template/contenido',$this->data);
	}	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */