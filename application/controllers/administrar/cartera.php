<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cartera extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('pedidos_modelo');
                $this->load->model('configuracion_modelo');                
	}
	
	function index()
	{
            $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
            $this->data['seleccionado']='cartera';
            $this->data['configuracion_cartera']=$this->configuracion_modelo->configuracion_cartera()->row();
            $this->data['titulo']='Administrar - Configurar cartera';
            $this->data['contenido']='administrar/cartera/index';
            $this->load->view('administrar/template/contenido',$this->data);
        }
        function modificar_configuracion($opcion,$valor){
            $ajax=$this->input->post('ajax');
            if($opcion && $ajax){
                $datos[$opcion]=$valor;
                $actualizar=$this->configuracion_modelo->modificar_configuracion_cartera($datos);
                if($actualizar){
                    $json=array('estado'=>'correcto','mensaje'=>'Se guardaron los cambios.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar los cambios.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción.');
            }
            echo json_encode($json);
        }        
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */