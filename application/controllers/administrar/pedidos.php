<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pedidos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		if(!$this->usuarios_modelo->verificar_sesion() || ($this->session->userdata('rol')!=1 & $this->session->userdata('rol')!=4)){
			$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
			if(!$this->input->post('ajax')){
				redirect('sipe', 'refresh');
			}else{
				echo "error_sesion";
			}
			die();
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('pedidos_modelo');
                $this->load->model('configuracion_modelo');                
	}
	
	function index()
	{
		$this->load->library('pagination');
                
                $filtro=null;
                $orden=null;
                $datos=$this->uri->uri_to_assoc(5);
                if(isset($datos['orden'])){
                        $orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
                }

                if(isset($datos['buscar'])){
                        $buscar=$datos['buscar'];
                }else{
                        $buscar=$this->input->post('buscar');
                }		
                if($buscar){
                        $filtro="buscar/".$buscar;
                }	
                $config['base_url']=site_url('administrar/pedidos/index');
                $config['total_rows']=$this->pedidos_modelo->total_listar_pedidos(null,$this->session->userdata('id_usuario'),$buscar);
                $config['uri_segment']=4;
                $config['per_page']=20;
                $config['num_links']=5;
                $config['filtro']=$filtro;
                $config['first_link'] = '&lt;&lt;';
                $config['last_link'] = '&gt;&gt;';
                $this->pagination->initialize($config);

                $this->data['buscar']=$buscar;
                $this->data['pedidos']=$this->pedidos_modelo->listar_pedidos($config['per_page'],$this->uri->segment(4),null,$this->session->userdata('id_usuario'),$orden,$buscar)->result();
                $this->data['desde']=$this->uri->segment(4)+1;
                $this->data['hasta']=$this->uri->segment(4)+$config['per_page'];
                $this->data['total_pedidos']=$config['total_rows'];
                $this->data['seguridad']=$this->config->item('encription_key');
                $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
                $this->data['seleccionado']='pedidos';
                $this->data['titulo']='Administrar - Pedidos';
                $this->data['contenido']='administrar/pedidos/index';
                $this->load->view('administrar/template/contenido',$this->data);
	}
        function configurar(){
            $this->data['estilos']=array('jquery-ui-1.8.custom_verde');
            $this->data['seleccionado']='pedidos';
            $this->data['correos']=$this->configuracion_modelo->correos_envio('pedidos');
            $this->data['configuracion_pedido']=$this->configuracion_modelo->configuracion_pedido()->row();
            $this->data['observaciones_predeterminadas']=$this->pedidos_modelo->listar_observaciones_pedido()->result();
            $this->data['titulo']='Administrar - Configurar pedidos';
            $this->data['contenido']='administrar/pedidos/configurar';
            $this->load->view('administrar/template/contenido',$this->data);
        }
        function modificar_configuracion($opcion,$valor){
            $ajax=$this->input->post('ajax');
            if($opcion && $ajax){
                $datos[$opcion]=$valor;
                $actualizar=$this->configuracion_modelo->modificar_configuracion_pedido($datos);
                if($actualizar){
                    $json=array('estado'=>'correcto','mensaje'=>'Se guardaron los cambios.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar los cambios.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción.');
            }
            echo json_encode($json);
        }
        function formulario_observacion($observacion=null){
            if($observacion){
                $this->data['observacion']=$this->pedidos_modelo->leer_observacion($observacion)->row();
            }
            echo $this->load->view('administrar/pedidos/formulario_observacion',$this->data,true);
        }
        function crear_editar_observacion(){
            $id=$this->input->post('id');
            $detalle=$this->input->post('detalle');
            $visible=$this->input->post('visible');
            if($detalle){
                $datos=array(
                      'detalle'=>$detalle,
                      'visible'=>$visible,
                );
                if(!$id || $id==''){
                    if($this->pedidos_modelo->crear_observacion($datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se creó la observación correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar crear la observación.');
                    }
                }else{
                     $datos=array(
                      'detalle'=>$detalle,
                      'visible'=>$visible,
                    );
                    if($this->pedidos_modelo->editar_observacion($id,$datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se editó la observación correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar editar la observación.');
                    }
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Debe colocar el detalle de la onservación');
            }
            echo json_encode($json);
        }
        function eliminar_observacion($id){
            if(!$id){
                $id=$this->input->post('id');
            }
            if($id){
                if($this->pedidos_modelo->eliminar_observacion($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó la observación correctamente.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar la observación.');
                }                
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción');
            }
            echo json_encode($json);
        }
        function formulario_correo($correo=null){
            if($correo){
                $this->data['correo']=$this->configuracion_modelo->leer_correo($correo)->row();
            }
            echo $this->load->view('administrar/pedidos/formulario_correo',$this->data,true);
        }
        function crear_editar_correo(){
            $id=$this->input->post('id');
            $descripcion=$this->input->post('descripcion');
            $correo=$this->input->post('correo');
            if($correo && $this->es_mail($correo)){
                $datos=array(
                      'descripcion'=>$descripcion,
                      'correo'=>$correo,
                      'tipo'=>1
                );
                if(!$id || $id==''){
                    if($this->configuracion_modelo->crear_correo($datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se agregó el correo correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar agregar el correo.');
                    }
                }else{
                     $datos=array(
                      'descripcion'=>$descripcion,
                      'correo'=>$correo,
                       'tipo'=>1
                    );
                    if($this->configuracion_modelo->editar_correo($id,$datos)){
                        $json=array('estado'=>'correcto','mensaje'=>'Se editó el correo correctamente.');
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar editar el correo.');
                    }
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'El correo no es válido.');
            }
            echo json_encode($json);
        }
        function eliminar_correo($id){
            if(!$id){
                $id=$this->input->post('id');
            }
            if($id){
                if($this->configuracion_modelo->eliminar_correo($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el correo correctamente.');
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar el correo.');
                }                
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar esta acción');
            }
            echo json_encode($json);
        }
        function es_mail($str){
		$match = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$/";
		if(preg_match($match, trim($str))) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
        function actualizar_plazos(){
            $plazos=$this->input->post('plazos');
            $datos=array(
                'plazos'=>json_encode($plazos)
            );
            if($this->configuracion_modelo->modificar_configuracion_pedido($datos)){
                $json=array('estado'=>'correcto','mensaje'=>'Se actualizaron los plazos correctamente.');
            }else{
                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar actualizar los plazos.');
            }
            echo json_encode($json);
        }
        function actualizar_descuentos(){
            $descuentos=$this->input->post('descuentos');
            $datos=array(
                'descuentos'=>json_encode($descuentos)
            );
            if($this->configuracion_modelo->modificar_configuracion_pedido($datos)){
                $json=array('estado'=>'correcto','mensaje'=>'Se actualizaron los descuentos correctamente.');
            }else{
                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar actualizar los descuentos.');
            }
            echo json_encode($json);
        }
        function anular($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->pedidos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
                                $datos=$encabezado->row();
                                if(!$datos->anulado){
                                    if($this->pedidos_modelo->anular_pedido($numero)){
                                        $json=array('estado'=>'correcto','mensaje'=>"El pedido se anuló correctamente");
                                    }else{
                                        $json=array('estado'=>'error','mensaje'=>"Se presentaron errores al intentar anular el pedido");
                                    }
                                }else{
                                    if($datos=$this->pedidos_modelo->activar_pedido($numero)){
                                        $json=array('estado'=>'correcto','mensaje'=>"El pedido se activó correctamente",'subtotal_sin_dcto'=>number_format($datos->subtotal_sin_dcto,'0',',',"."),'descuento'=>number_format($datos->descuento_total,'0',',',"."),'iva_total'=>number_format($datos->iva_total,'0',',',"."),'total'=>number_format($datos->total,'0',',',"."));
                                    }else{
                                        $json=array('estado'=>'error','mensaje'=>"Se presentaron errores al intentar activar el pedido");
                                    }
                                }
			}else{
				$json=array('estado'=>'error','mensaje'=>"El pedido no existe");
			}		     
		}else{
			$json=array('estado'=>'error','mensaje'=>"No tiene permisos para descargar en Excel la copia de este pedido");
		}
                echo json_encode($json);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */