<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cotizaciones extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
		$accion=$this->uri->segment(2);
		if($accion!='imprimir_cotizacion'){
			if(!$this->usuarios_modelo->verificar_sesion()){
				$this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
				if(!$this->input->post('ajax')){
					redirect('sipe', 'refresh');
				}else{
					echo "error_sesion";
				}
				die();
			}
		}
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('cotizaciones_modelo');
                $this->load->model('configuracion_modelo');
                $this->data['empresa']=$this->configuracion_modelo->datos_empresa();
		$this->data['configuracion']=$this->configuracion_modelo->configuracion_cotizacion()->row_array();
	}
	
	function index()
	{
		$this->load->library('pagination');
		
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		
		if(isset($datos['buscar'])){
			$buscar=$datos['buscar'];
		}else{
			$buscar=$this->input->post('buscar');
		}		
		if($buscar){
			$filtro="buscar/".$buscar;
		}		
		$config['base_url']=site_url('cotizaciones/index');
		$config['total_rows']=$this->cotizaciones_modelo->total_listar_cotizaciones($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		
		$this->data['buscar']=$buscar;
		$this->data['cotizaciones']=$this->cotizaciones_modelo->listar_cotizaciones($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
		$this->data['total_cotizaciones']=$config['total_rows'];
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Cotizaciones';
		$this->data['contenido']='cotizaciones/index';
		$this->load->view('template/contenido',$this->data);
	}
	function nuevo(){
                $this->load->model('vendedores_modelo');
                $this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
                $this->data['estilos']=array('jquery-ui-1.8.custom_verde','jquery.lightbox-0.5');
		$this->data['titulo']='Cotizaciones - Nuevo cotizacion';
                $this->data['observaciones_predeterminadas']=$this->cotizaciones_modelo->listar_observaciones_cotizacion()->result();
		$this->data['contenido']='cotizaciones/nuevo';
		$this->load->view('template/contenido',$this->data);
	}
	function buscar_cliente(){
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		
		$buscar=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('cotizaciones/buscar_cliente');
		if($this->session->userdata('rol')==3){
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_nits_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}else{
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}
		$config['per_page']=7;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		if($this->session->userdata('rol')==3){
			$this->data['clientes']=$this->clientes_modelo->buscar_nits_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}else{
			$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}
		$this->data['cliente']=$buscar;
		$this->data['titulo']='cotizaciones';
		$this->data['contenido']='cotizaciones/buscar_cliente';
		
		if($ajax){
			$json=array('estado'=>'correcto','mensaje'=>'Se listaron los clientes correctamente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
		}else{
			$json=array('estado'=>'error','mensaje'=>'Está ingresando de forma no autorizada.');
		}
                echo json_encode($json);
	}
	function datos_cliente($cliente=null){
		if(!$cliente){
			$cliente=$this->input->post('cliente');
		}
		if($cliente){
			$this->load->model('clientes_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');
			$datos_cliente=$this->clientes_modelo->leer_cliente($cliente,$this->session->userdata('numeros_vendedor'));
			if($datos_cliente->num_rows >0){
				$this->data['cliente']=$datos_cliente->row();
                                $json=array(
                                    'cliente'=>$datos_cliente->row(),
                                    'estado'=>'correcto',
                                    'mensaje'=>'Se cargaron los datos del cliente.'
                                );
			}else{
				$json=array('estado'=>'error','mensaje'=>'El cliente no existe.');
			}
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar la consulta');			
		}
                echo json_encode($json);
	}
        function datos_cliente_a($cliente=null){
		if(!$cliente){
			$cliente=$this->input->post('cliente');
		}
		if($cliente){
			$this->load->model('clientes_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');
			$datos_cliente=$this->clientes_modelo->leer_cliente($cliente,$this->session->userdata('numeros_vendedor'));
			if($datos_cliente->num_rows >0){
				$this->data['cliente']=$datos_cliente->row();
				$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
				$this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
				//$this->data['cartera']=$this->cartera_modelo->cartera_cliente(0,0,$cliente,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
				$this->data['cartera']=null;
				$this->data['titulo']='cotizaciones';
				$this->data['contenido']='cotizaciones/datos_cliente';		
				$json=array('estado'=>'correcto','mensaje'=>'Se cargaron los datos del cliente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
			}else{
				$json=array('estado'=>'error','mensaje'=>'El cliente no existe.');
			}
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar la consulta');			
		}
                echo json_encode($json);
	}
	function buscar_producto(){
		$this->load->library('pagination');
		$this->load->model('productos_modelo');
		
		$buscar=strtoupper($this->input->post('producto'));
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('cotizaciones/buscar_producto');
		$config['total_rows']=$this->productos_modelo->resultados_buscar_producto($buscar);
		$config['per_page']=12;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		$this->data['productos']=$this->productos_modelo->buscar_producto($config['per_page'],$this->uri->segment(3),$buscar,$orden)->result();
		if($buscar){
			$this->data['producto']=$buscar;
		}else{
			$this->data['producto']='Digite el código o nombre del producto';
		}
		$this->data['estilos']=array('jquery.lightbox-0.5');
		$this->data['titulo']='cotizaciones';
		$this->data['contenido']='cotizaciones/buscar_producto';
		if($ajax){
                        
			$json=array('estado'=>'correcto','mensaje'=>'Se han cargado los productos correctamente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
                        echo json_encode($json);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
	function datos_producto(){
		$producto=$this->input->post('codigo');
		if($producto){
			$this->load->model('productos_modelo');
			$producto=$this->productos_modelo->leer_producto($producto);
			$ajax=$this->input->post('ajax');
			if($producto->num_rows>0){
				$producto=$producto->row();
				if($producto->precio1 > 0){					
					if($ajax){
						$datos = array(
                                                        'estado'=>'correcto',
                                                        'mensaje'=>'Se ha agregado el producto correctamente.',
							'nombre'    =>    $producto->nombre,
							'referencia'    =>    $producto->referencia,
							'precio1'    =>    number_format($producto->precio1,0),
							'precio2'    =>    $producto->precio2,
							'precio3'    =>    $producto->precio3,
							'iva'      =>    number_format($producto->iva,0)
						);
						echo json_encode($datos);
					}else{
						echo 'No esta accediendo de forma correcta';
					}
				}else{
					$datos = array(
                                                'estado'=>'error',
                                                'numero'=>1,
                                                'mensaje'=>'El producto se encuentra agotado.',
						'nombre'    =>    'Producto agotado',
						'referencia'    =>    0,
						'precio1'    =>    0,
						'precio2'    =>    0,
						'precio3'    =>    0,
						'iva'      =>    0
					);
					echo json_encode($datos);					
				}
			}else{
				if($ajax){
					$datos = array(
                                            'estado'=>'error',
                                            'numero'=>2,
                                            'mensaje'=>'El producto no existe',
                                            'nombre'    =>    'El producto no existe.'
					);
					echo json_encode($datos);
				}else{
					echo 'No esta accediendo de forma correcta';
				}
			}
		}else{
			echo 0;
		}			
	}
	function buscar_cotizacion_a_copiar(){
		$this->load->library('pagination');
		
		$buscar=$this->input->post('buscar');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('cotizaciones/buscar_cotizacion_a_copiar');
		$config['total_rows']=$this->cotizaciones_modelo->total_listar_cotizaciones($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->pagination->initialize($config);
		$this->data['cotizaciones']=$this->cotizaciones_modelo->listar_cotizaciones($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['cliente']=$buscar;
		$this->data['seguridad']=$this->config->item('encription_key');
		$this->data['titulo']='cotizaciones';
		$this->data['contenido']='cotizaciones/buscar_cotizacion_a_copiar';
		
		if($ajax){
			$json=array('estado'=>'correcto','mensaje'=>'Se han listado los cotizaciones.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
                        echo json_encode($json);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
	function copiar_cotizacion($cotizacion){
		$productos=$this->cotizaciones_modelo->productos_cotizacion($cotizacion)->result();
		$this->data['productos']=$productos;
		$this->data['titulo']='Cotizaciones';
		$this->data['contenido']='cotizaciones/productos_cotizacion';
		$ajax=$this->input->post('ajax');
		if($ajax){
			echo $this->load->view($this->data['contenido'],$this->data,true);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
	function copiar_productos_cotizacion($cotizacion){
            if($cotizacion){
		$productos=$this->cotizaciones_modelo->productos_cotizacion($cotizacion)->result();
		$resultados=array();
		foreach($productos as $producto):
				$datos_producto = array(
					'codigo'    =>    $producto->id_producto,
					'nombre'    =>    $producto->nombre,
                                        'referencia'    =>    $producto->referencia,
					'cantidad'    =>    $producto->cantidad,
					'unidad'    =>    $producto->unidad,
					'precio'    =>    $producto->precio1,
					'precio1'    =>    $producto->precio1,
					'precio2'    =>    $producto->precio2,
					'precio3'    =>    $producto->precio3,
					'iva'      =>    $producto->iva,
					'descuento'      =>    $producto->descuento,
					'total'      =>    $producto->total
				);
				$resultados[]=$datos_producto;
		endforeach;
                $json=array('estado'=>'correcto','mensaje'=>'Se ha copiado el cotizacion.','cotizacion'=>$cotizacion,'productos'=>$resultados);
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar esta acción.');
            }
		echo json_encode($json);
	}
	function leerExcel(){
		$this->load->library('PHPExcel/IOFactory');
		$nombre=$_FILES['archivo']['name'];
		$datos=explode('.',$nombre);
		$tipo=$datos[count($datos)-1];
		$error=true;
		if($tipo=='xls'){
			$objReader =$this->iofactory->createReader('Excel5');
			$error=false;
		}else{
			if($tipo=='xlsx'){
				$objReader =$this->iofactory->createReader('Excel2007');
				$error=false;
			}
		}
		if(!$error){
			$this->load->model('clientes_modelo');
			$this->load->model('productos_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');		

			$objReader->setReadDataOnly(true);
			$objPHPExcel=$objReader->load($_FILES['archivo']['tmp_name']);
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$maxima_fila=$objWorksheet->getHighestRow();
			$nit=$objWorksheet->getCell('C2')->getCalculatedValue();
			$datos_cliente=$datos_cliente=$this->clientes_modelo->leer_cliente($nit,$this->session->userdata('numeros_vendedor'));
			
			if($datos_cliente->num_rows >0){
                            $this->data['cliente']=$datos_cliente->row();
                            $orden_cartera=null;
                            $cartera=$this->cartera_modelo->cartera_cliente(0,0,$nit,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
                        }else{
                            $this->data['cliente']='';
                            $cartera='';
                        }
				$this->data['observaciones']=$objWorksheet->getCell('B7')->getCalculatedValue();
				$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
				$this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
				$this->data['cartera']=$cartera;
				$productos=array();
				$no_existen=array();
				for($n=10;$n<=$maxima_fila;$n++){
					$producto=$this->productos_modelo->leer_producto($objWorksheet->getCellByColumnAndRow(1,$n)->getCalculatedValue());
					if($producto->num_rows>0){
						$producto=$producto->row();
						if($producto->precio1>0){//Verificar si el producto se encuentra agotado
							$dato=array('codigo'=>$producto->id_producto,
										'nombre'=>$producto->nombre,
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>$producto->referencia,
										'iva'=>$producto->iva,
										'unidad'=>$producto->unidad,
										'precio1'=>$producto->precio1,
										'precio2'=>$producto->precio2,
										'precio3'=>$producto->precio3,
										'precio'=>$producto->precio1);
							array_push($productos,$dato);
						}else{
							$dato=array('codigo'=>$producto->id_producto,
										'nombre'=>'Producto agotado',
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>'',
										'iva'=>0,
										'unidad'=>0,
										'precio1'=>0,
										'precio2'=>0,
										'precio3'=>0,
										'precio'=>0);
							array_push($productos,$dato);
						}
					}else{
							$dato=array('codigo'=>$objWorksheet->getCellByColumnAndRow(1,$n)->getCalculatedValue(),
										'nombre'=>'El producto no existe',
										'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
										'referencia'=>'',
										'iva'=>0,
										'unidad'=>0,
										'precio1'=>0,
										'precio2'=>0,
										'precio3'=>0,
										'precio'=>0);
							array_push($productos,$dato);						
						/*$dato=array('codigo'=>$objWorksheet->getCellByColumnAndRow(1,$n)->getCalculatedValue(),
									'nombre'=>$objWorksheet->getCellByColumnAndRow(2,$n)->getCalculatedValue(),
									'cantidad'=>$objWorksheet->getCellByColumnAndRow(3,$n)->getCalculatedValue(),
									'precio'=>$objWorksheet->getCellByColumnAndRow(4,$n)->getCalculatedValue());
						array_push($no_existen,$dato);*/
					}
				}
				function compare($x, $y)
				{
				 if ( $x['codigo'] == $y['codigo'] )
				  return 0;
				 else if ( $x['codigo'] < $y['codigo'] )
				  return -1;
				 else
				  return 1;
				}			
				usort($productos, 'compare');
				
                                echo json_encode($productos);
				/*$this->data['productos']=$productos;
				$this->data['titulo']='Cotizaciones';
				$this->data['contenido']='cotizaciones/importar_cotizacion_formato';
				echo $this->load->view($this->data['contenido'],$this->data,true);
                                 * */
                                 			
		}else{
			echo 'El archivo debe ser de excel xls o xlsx';
		}
	}
	function subir_archivo(){
		$this->data['titulo']='Cotizaciones';
		$this->data['contenido']='cotizaciones/subir_archivo';
		$this->load->view('template/contenido',$this->data);
	}
	function guardar(){
		$ajax=$this->input->post('ajax');
		$mostrar='';
		if($ajax){
			$borrador=$this->input->post('numero_borrador');
			$encabezado=$this->input->post('encabezado');
			$productos=$this->input->post('productos');
			
                        if($numeros=$this->cotizaciones_modelo->guardar_cotizacion($borrador,$encabezado,$productos)){
                            foreach($numeros as $numero):
                                $mensaje=$this->ver($numero,sha1($numero.$this->config->item('encription_key')),false,true,true,false,false);
                                $mostrar=$mostrar.$mensaje.'<br/>';
                            endforeach;
                            $this->data['numeros']=$numeros;
                            $this->data['mostrar']=$mostrar;
                            $this->data['envio']='';
                            $this->data['titulo']='cotizaciones - cotizaciones enviados correctamente.';
                            $mensaje=$this->load->view('cotizaciones/cotizaciones_enviados',$this->data,true);
                            $json=array('estado'=>'correcto','mensaje'=>$mensaje,'numeros'=>$numeros);
                        }else{
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar el cotizacion');                            
                        }
                        //$envio=$this->enviar_cotizaciones($numeros,$vendedor,$cliente,$encabezado['email_copias']);			
                        //$envio='';
			
                        echo json_encode($json);
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Error Estas accediendo de forma no autorizada.');
                    echo json_encode($json);
		}
	}
	function enviar_cotizaciones($cotizaciones=null,$vendedor=null,$cliente=null,$otros=null){
            
            if(!$cotizaciones){
                $cotizaciones=$this->input->post('cotizaciones');
            }
            if($cotizaciones){
                if(!$vendedor){
                    $vendedor=$this->input->post('vendedor');
                }
                if(!$cliente){
                    $cliente=$this->input->post('cliente');
                }
                if(!$otros){
                    $otros=$this->input->post('otros');
                }
                $this->load->model('configuracion_modelo');
                $this->load->model('vendedores_modelo');
                $this->load->model('clientes_modelo');
                
                $enviado=false;

                $configuracion=$this->data['empresa'];

                $correos_envio=$this->configuracion_modelo->correos_envio('cotizaciones');
                $correo_vendedor=null;
                if($vendedor){
                    if($this->data['configuracion']['enviar_copia_vendedor']){
                        $correo_vendedor=$this->vendedores_modelo->correo($vendedor);
                    }
                }
                $correo_cliente=null;
                if($cliente){
                    if($this->data['configuracion']['enviar_copia_cliente']){
                        $correo_cliente=$this->clientes_modelo->correo($cliente);
                    }
                }

                if($configuracion->tipo_envio==1){
                    $this->load->library('phpmailer');

                    $this->phpmailer->From     = $configuracion->correo_envio;
                    $this->phpmailer->FromName = $configuracion->descripcion_cotizaciones;
                    $this->phpmailer->Host = $configuracion->host;
                    $this->phpmailer->Password = $configuracion->clave;
                    $this->phpmailer->Port = $configuracion->port;
                    //$this->phpmailer->Mailer='smtp';
                    $this->phpmailer->CharSet = "UTF-8";
                    foreach($correos_envio as $correo):
                        $this->phpmailer->AddAddress($correo->correo);                
                    endforeach;
                    if($correo_vendedor){
                        $this->phpmailer->AddAddress($correo_vendedor);                
                    }
                    if($correo_cliente){
                        $this->phpmailer->AddAddress($correo_cliente);                
                    }
                    if($otros){
                        $otros_array=explode(',',$otros);
                        foreach($otros_array as $otro){
                            if($this->es_mail(trim($otro))){
                                    $this->phpmailer->AddAddress(trim($otro));                            
                            }
                        }
                    }
                    if(is_array($cotizaciones)){
                        foreach($cotizaciones as $cotizacion){
                            $this->phpmailer->Subject    = 'Cotización # '.$cotizacion;
                            $mensaje=$this->ver($cotizacion,sha1($cotizacion.$this->config->item('encription_key')),true,false,true,false);
                            $this->phpmailer->Body=$mensaje;
                            if($this->phpmailer->Send()){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los cotizaciones correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los cotizaciones');
                            }                        
                        }
                    }else{
                        $cotizacion=$cotizaciones;
                        $this->phpmailer->Subject    = 'Cotización # '.$cotizacion;
                        $mensaje=$this->ver($cotizacion,sha1($cotizacion.$this->config->item('encription_key')),true,false,true,false);
                        $this->phpmailer->Body=$mensaje;
                        if($this->phpmailer->Send()){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el cotizacion correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el cotizacion');
                        }                        
                    }
                }else{

                    $config=array();
                    $config['protocol']='smtp';  
                    $config['smtp_host']=$configuracion->host;  
                    $config['smtp_port']=$configuracion->port;  
                    $config['smtp_timeout']='30';  
                    $config['smtp_user']=$configuracion->correo_envio;  
                    $config['smtp_pass']=$configuracion->clave;  
                    $config['charset']='utf-8';  
                    $config['mailtype']="html";
                    $config['newline']="\r\n";

                    $this->load->library('email',$config);
                    $para=array();
                    foreach($correos_envio as $correo):
                        $para[]=$correo->correo;
                    endforeach;
                    if($correo_vendedor){
                        $para[]=$correo_vendedor;                        
                    }
                    if($correo_cliente){
                        $para[]=$correo_cliente;
                    }
                    if($otros){
                        $otros_array=explode(',',$otros);
                        foreach($otros_array as $otro){
                            if($this->es_mail(trim($otro))){
                                    $para[]=trim($otro);                            
                            }
                        }
                    }

                    if(is_array($cotizaciones)){
                        foreach($cotizaciones as $cotizacion){
                            $this->email->from($configuracion->correo_envio);
                            $this->email->to($para); 
                            $this->email->subject('Cotización # '.$cotizacion);
                            $mensaje=$this->ver($cotizacion,sha1($cotizacion.$this->config->item('encription_key')),true,false,true,false);
                            $this->email->message($mensaje);
                            $envio=$this->email->send();
                            if($envio){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los cotizaciones correctamente');
                            }
                            else
                            {
                                $enviado=false; 
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los cotizaciones');
                            }                    
                        }
                    }else{
                        $cotizacion=$cotizaciones;

                        $this->email->from($configuracion->correo_envio,$configuracion->descripcion_cotizaciones);  
                        $this->email->to($para);  
                        $this->email->subject('Cotización # '.$cotizacion);
                        $mensaje=$this->ver($cotizacion,sha1($cotizacion.$this->config->item('encription_key')),true,false,true,false);
                        $this->email->message($mensaje);
                        $envio=$this->email->send();
                        if($envio){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el cotizacion correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el cotizacion');
                        } 
                    }
                }
                echo json_encode($json);
            }else{
                $json=array('estado'=>'error','mensaje'=>'No hay cotizaciones para enviar');
                echo json_encode($json);
            }
	}
        function enviar_copia($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
                    $mensaje=$this->input->post('mensaje');
                    $correos=$this->input->post('email');

                    $this->load->model('configuracion_modelo');

                    $configuracion=$this->data['empresa'];

                    if($configuracion->tipo_envio==1){
                        $this->load->library('phpmailer');
                        $this->phpmailer->From     = $configuracion->correo_envio;
                        $this->phpmailer->FromName = $configuracion->descripcion_cotizaciones;
                        $this->phpmailer->Host = $configuracion->host;
                        $this->phpmailer->Password = $configuracion->clave;
                        $this->phpmailer->Port = $configuracion->port;
                        $this->phpmailer->CharSet = "UTF-8";

                        $enviado=false;
                        $para=array();
                        $otros_array=explode(',',$correos);			
                        foreach($otros_array as $email){
                                if($this->es_mail(trim($email))){
                                        $this->phpmailer->AddAddress(trim($email));
                                }
                        }
                        $this->phpmailer->Subject    = 'Cotización # '.$numero;
                        $mensaje=$mensaje.'<br/><br/>'.$this->ver($numero,sha1($numero.$this->config->item('encription_key')),true,false,true,false);
                        $this->phpmailer->Body=$mensaje;
                        if($this->phpmailer->Send()){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el cotizacion correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el cotizacion');
                        }
                    }else{
                        $config=array();
                        $config['protocol']='smtp';  
                        $config['smtp_host']=$configuracion->host;  
                        $config['smtp_port']=$configuracion->port;  
                        $config['smtp_timeout']='30';  
                        $config['smtp_user']=$configuracion->correo_envio;  
                        $config['smtp_pass']=$configuracion->clave;  
                        $config['charset']='utf-8';  
                        $config['mailtype']="html";
                        $config['newline']="\r\n";

                        $this->load->library('email',$config);
                        $para=array();
                        $otros_array=explode(',',$correos);			
                        foreach($otros_array as $email){
                                if($this->es_mail(trim($email))){
                                        $para[]=trim($email);
                                }
                        }
                        
                        $this->email->from($configuracion->correo_envio,$configuracion->descripcion_cotizaciones);  
                        $this->email->to($para);  
                        $this->email->subject('Cotización # '.$numero);
                        $mensaje=$mensaje.'<br/><br/>'.$this->ver($numero,sha1($numero.$this->config->item('encription_key')),true,false,true,false);
                        $this->email->message($mensaje);
                        $envio=$this->email->send();
                        if($envio){
                            $enviado=true;
                            $json=array('estado'=>'correcto','mensaje'=>'Se envió el cotizacion correctamente');
                        }
                        else
                        {
                            $enviado=false;
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el cotizacion');
                        } 
                    }
                    echo json_encode($json);
		}else{
			echo "No tiene permisos para imprimir este cotizacion";
		}
	}
	function es_mail($str){
		$match = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$/";
		if(preg_match($match, $str)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function ver($numero,$seguridad=null,$autoimprimir=false,$enlace=false,$retornar=false,$mostrar_logo=true,$mostrar_encabezado=true){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->cotizaciones_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['seguridad']=$this->config->item('encription_key');
				$this->data['encabezado']=$encabezado->row();
                                $this->data['autoimprimir']=$autoimprimir;
                                $this->data['enlace']=$enlace;
                                $this->data['mostrar_logo']=$mostrar_logo;
                                $this->data['mostrar_encabezado']=$mostrar_encabezado;
				$this->data['productos']=$this->cotizaciones_modelo->leer_productos($numero)->result();
                                if($retornar){
                                    return $this->load->view('cotizaciones/ver',$this->data,true);
                                    
                                }else{                                    
                                    $this->load->view('cotizaciones/ver',$this->data);
                                }
			}else{
                            if($retornar){
                                return "El cotizacion no existe";
                            }else{
				echo "El cotizacion no existe";
                            }
			}
		}else{
			echo "No tiene permisos para imprimir este cotizacion";
		}
	}
	function importar_subir_archivo(){
            $ajax=$this->input->post('ajax');
            if($ajax){
		$json=array('estado'=>'correcto','mensaje'=>'Se cargó el formulario.','vista'=>$this->load->view('cotizaciones/subir_archivo',null,true));
            }else{
                $json=array('estado'=>'error','mensaje'=>'Está accediendo de forma no autorizada.');
            }
            echo json_encode($json);
	}
	function cotizaciones_vendedor_mes(){
		$mes=$this->input->post('mes');
		$data['cotizaciones_totales']=$this->cotizaciones_modelo->total_cotizaciones_vendedores_mes($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$mes)->result();
		$data['contenido']='cotizaciones/totales_cotizaciones_vendedor_mes';
		$this->load->view($data['contenido'],$data);
	}
	function autoguardar(){
		$numero_borrador=$this->input->post('numero_borrador');
		$encabezado=$this->input->post('encabezado');
		$productos=$this->input->post('productos');
		$fecha=date('Y-m-d H:i:s',time());
		$resultados=array();
		//echo json_encode($encabezado);
		if($productos){
			foreach($productos as $producto){
				$resultados[]=json_encode($producto);
			}		
		}
		$estado=$this->cotizaciones_modelo->guardar_borrador(json_encode($encabezado),json_encode($resultados),$fecha,$numero_borrador);
		$resultados=array('id'=>$estado,'fecha'=>$fecha);
		echo json_encode($resultados);
	}
	function validar_productos_cargar($productos_json){
		$productos=json_decode($productos_json);
		$productos_actualizados=array();
		foreach($productos as $producto_json):
			$producto=json_decode($producto_json);
			$resultado=$this->cotizaciones_modelo->datos_producto_borrador($producto->codigo)->row();
			if($resultado){			
                            $producto_actualizado=array('codigo'=>$producto->codigo,
                                    'nombre'=>$resultado->nombre,
                                    'referencia'=>$resultado->referencia,
                                    'cantidad'=>$producto->cantidad,
                                    'precio'=>$resultado->precio1,
                                    'descuento'=>$producto->descuento,
                                    'iva'=>$resultado->iva
                            );
                        }
			else{
				$producto_actualizado=array('codigo'=>$producto->codigo,
				'nombre'=>'',
				'referencia'=>$producto->referencia,
				'cantidad'=>$producto->cantidad,
				'precio'=>'',
				'descuento'=>$producto->descuento,
				'iva'=>''
			);
			}
			$producto_actualizado_json=json_encode($producto_actualizado);
			$productos_actualizados[]=$producto_actualizado_json;
		endforeach;
		$productos_actualizados_json=json_encode($productos_actualizados);
		return $productos_actualizados_json;
	}
	function eliminar_borrador($id){
            if($id){
                if($this->cotizaciones_modelo->eliminar_borrador($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el borrador correctamente.');                    
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar borrador.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar la acción.');
            }
            echo json_encode($json);
	}
	function imprimir_copia($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->cotizaciones_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->cotizaciones_modelo->leer_productos($numero)->result();
				$this->load->view('cotizaciones/imprimir_copia',$this->data);
			}else{
				echo "El cotización no existe";
			}
		}else{
			echo "No tiene permisos para imprimir este cotizacion";
		}
	}
	function generar_excel($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->cotizaciones_modelo->leer_encabezado($numero);
			
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->cotizaciones_modelo->leer_productos($numero)->result();
				$this->load->library('PHPExcel');
				$this->load->library('PHPExcel/IOFactory');
				
				$this->data['objPHPExcel'] = new PHPExcel();

				$this->load->view('cotizaciones/generar_excel',$this->data);
			}else{
				echo "El cotización no existe";
			}
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este cotizacion";
		}
	}
	function generar_pdf($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->cotizaciones_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->load->library('mpdf');
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->cotizaciones_modelo->leer_productos($numero)->result();
				$html = $this->load->view('cotizaciones/cotizacion_pdf',$this->data,true);
				$this->mpdf->debug = true;
				$this->mpdf->WriteHTML($html);			
				$this->mpdf->Output('Cotización '.$numero.'.pdf','D');  
			}else{
				echo "El cotización no existe";
			}		     
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este cotizacion";
		}
	}
	function prueba($vendedor){
            $this->load->model('vendedores_modelo');
			$correo_vendedor=$this->vendedores_modelo->correo($vendedor);
			echo $correo_vendedor;
	}
	function productos_mas_cotizaciones_cliente($cliente){
		if($cliente){
			$this->load->library('pagination');
			$this->load->model('clientes_modelo');
			$buscar=$this->input->post('producto');
			$ajax=$this->input->post('ajax');
			$filtro=null;
			$orden=null;
			$datos=$this->uri->uri_to_assoc(4);
			if(isset($datos['orden'])){
				$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
			}
			if(!$buscar){
				if(isset($datos['buscar'])){
					$buscar=str_replace('_',' ',$datos['buscar']);
				}
			}
			if($buscar=='Digite el código o nombre del producto'){
				$buscar='';
			}
			if($buscar){
				$filtro='buscar/'.$buscar;
			}
				
			$config['base_url']=site_url('cotizaciones/productos_mas_cotizaciones_cliente/'.$cliente);
			$config['total_rows']=$this->clientes_modelo->resultados_productos_mas_cotizaciones_cliente($buscar,$cliente);
			$config['per_page']=20;
			$config['num_links']=5;
			$config['uri_segment']=4;
			$config['filtro']=$filtro;
			$config['first_link'] = '&lt;&lt;';
			$config['last_link'] = '&gt;&gt;';
			$this->pagination->initialize($config);
			$this->data['productos']=$this->clientes_modelo->productos_mas_cotizaciones_cliente($config['per_page'],$this->uri->segment(4),$buscar,$cliente,$orden)->result();
			$this->data['buscar']=$buscar;
			$this->data['desde']=$this->uri->segment(3)+1;
			$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
			$this->data['total_productos']=$config['total_rows'];			
			$this->data['titulo']='Productos';
			$this->data['contenido']='cotizaciones/productos_mas_cotizaciones_cliente';
			if($ajax){
					echo $this->load->view($this->data['contenido'],$this->data,false);
			}else{
				$this->load->view('template/contenido',$this->data);
				//$this->load->view($this->data['contenido'],$this->data);
			}
		}else{
			echo "Faltan datos para realizar esta acción";
		}
	}
        function imprimir_rango(){
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Cotizaciones';
		$this->data['contenido']='cotizaciones/imprimir_rango_formulario';
		$this->load->view($this->data['contenido'],$this->data);
        }
        function imprimir_rango_cotizaciones(){
            $tipo=$this->input->post('tipo');
            $rango=$this->input->post('rango');
            $numeros=explode(',', $rango);
            $ids=array();
            $rangos=array();
            foreach($numeros as $numero):
                if(strpos($numero, '-')===false){
                    $ids[]=($numero*1);                    
                }else{
                    $otros=explode('-', $numero);
                    $rangos[]=array('desde'=>($otros[0]*1),'hasta'=>($otros[count($otros)-1]*1));                    
                }
            endforeach;
            /*$verificar=sha1($numero.$this->config->item('encription_key'));
            if($verificar==$seguridad){
                    $encabezado=$this->cotizaciones_modelo->leer_encabezado($numero);
                    if($encabezado->num_rows>0){
                            $this->data['encabezado']=$encabezado->row();
                            $this->data['productos']=$this->cotizaciones_modelo->leer_productos($numero)->result();
                            $this->load->view('cotizaciones/imprimir_cotizacion',$this->data);
                    }else{
                            echo "El cotización no existe";
                    }
            }else{
                    echo "No tiene permisos para imprimir este cotizacion";
            }*/
            $cotizaciones=$this->cotizaciones_modelo->datos_cotizaciones($ids,$rangos)->result();
            $cotizaciones_array=array();
            foreach($cotizaciones as $cotizacion):
                $cotizaciones_array[$cotizacion->id_maestro]['encabezado']=(object) array('id_maestro'=>$cotizacion->id_maestro,
                                                                                 'id_cliente'=>$cotizacion->id_cliente,
                                                                                 'fecha'=>$cotizacion->fecha,
                                                                                 'hora'=>$cotizacion->hora,
                                                                                 'nombre'=>$cotizacion->nombre,
                                                                                 'contacto'=>$cotizacion->contacto,
                                                                                 'direccion'=>$cotizacion->direccion,
                                                                                 'ciudad'=>$cotizacion->ciudad,
                                                                                 'telefono1'=>$cotizacion->telefono1,
                                                                                 'id_vendedor'=>$cotizacion->id_vendedor,
                                                                                 'nombre_vendedor'=>$cotizacion->nombre_vendedor,
                                                                                 'plazo'=>$cotizacion->plazo,
                                                                                 'nota_1'=>$cotizacion->nota_1,
                                                                                 'nota_2'=>$cotizacion->nota_2,
                                                                                 'lista'=>$cotizacion->lista,
                                                                                 'subtotal_sin_dcto'=>$cotizacion->subtotal_sin_dcto,
                                                                                 'descuento_total'=>$cotizacion->descuento_total,
                                                                                 'subtotal'=>$cotizacion->subtotal,
                                                                                 'iva_total'=>$cotizacion->iva_total,
                                                                                 'total'=>$cotizacion->total,
                                                                            );
                $cotizaciones_array[$cotizacion->id_maestro]['productos'][]=(object) array(
                                                                                'id_producto'=>$cotizacion->id_producto,
                                                                                'cantidad'=>$cotizacion->cantidad,
                                                                                'nombre'=>$cotizacion->nombre_producto,
                                                                                'precio'=>$cotizacion->precio,
                                                                                'descuento'=>$cotizacion->descuento,
                                                                                'iva'=>$cotizacion->iva,
                                                                                'total'=>$cotizacion->total_item,
                                                                            
                                                                            );
            endforeach;
            $this->data['cotizaciones']=$cotizaciones_array;
            if($tipo==2){
                $this->load->view('cotizaciones/imprimir_rango_cotizaciones_tirilla',$this->data);
            }else{
                $this->load->view('cotizaciones/imprimir_rango_cotizaciones',$this->data);
            }
        }
        function cargar_borrador(){
            $id=$this->input->post('borrador');
            if($id){
			$borrador=$this->cotizaciones_modelo->leer_borrador($id);
			if($borrador->num_rows>0){
				$borrador=$borrador->row();
				$json=array('estado'=>'correcto','mensaje'=>'Se obtubo la iformación del borrador','encabezado'=>json_decode($borrador->encabezado),'productos'=>json_decode($this->validar_productos_cargar($borrador->productos)));
			}else{
				$json=array('estado'=>'error','mensaje'=>'El borrador no existe o ya fué eliminado.');
			}
		}else{
			$json=array('estado'=>'error','mensaje'=>'Faltan datos para poder acceder a la información');
		}
                echo json_encode($json);
        }
        function condiciones($codigos){
            $codigo=explode('-',$codigos);
            $this->cotizaciones_modelo->grupos_productos($codigo);
        }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>