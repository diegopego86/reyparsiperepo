<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inicio extends CI_Controller {
	function __construct(){
		parent::__construct();
		/*$this->load->model('usuarios_modelo');
		$accion=$this->uri->segment(2);
		if($accion!='cerrar_sesion'){
			if($this->usuarios_modelo->verificar_sesion()){
				redirect('menu');
			}
		}*/
	}	
	function index()
	{
		if (!$this->session->userdata('loggeado')) {
			$data['titulo']='Sistema de Pedidos - Inicio de sesión';
			$this->load->view('inicio/index',$data);
		}else{
			redirect('menu');
		}
	}
	function acceder()
	{
		$this->load->model('usuarios_modelo');
		$usuario=$this->input->post('usuario');
		$clave=$this->input->post('clave');
		$es_ajax=$this->input->post('ajax');
		$query=$this->usuarios_modelo->validar_usuario($usuario,$clave);
                if($query->num_rows==1) // Si es usuario es validado;
		{
			$usuario=$query->row();
			$vendedores=$this->usuarios_modelo->vendedores_usuario_arreglo($usuario->id);
			$data = array(
				'id_usuario' => $usuario->id,
				'codigo' => $usuario->codigo,
				'nombreusuario' => $usuario->usuario,
				'nombreCompleto' => $usuario->nombres.' '.$usuario->apellidos,
				'rol' => $usuario->rol,
				'numeros_vendedor' => $vendedores,
				'email' => $usuario->email,
				'loggeado' => true,
				'ultimo_acceso'=>time()
			);
			$this->session->set_userdata($data);
                        $json=array('estado'=>true,'mensaje'=>'Espere mientras se carga la información.');                        
		}
		else
		{
                     $this->session->set_userdata('errores', 'Existen errores de validacion.');
                     $json=array('estado'=>false,'mensaje'=>'Existen errores de validacion');
		}
                if($this->input->post('ajax')){
                    echo json_encode($json);
                }else{
                    redirect('');
                }
	}
	function cerrar_sesion()
	{
		$this->session->sess_destroy();
		redirect('');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */