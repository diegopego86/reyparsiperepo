<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sipe extends CI_Controller {
    var $data=array();
	function __construct(){
		parent::__construct();
		/*$this->load->model('usuarios_modelo');
		$accion=$this->uri->segment(2);
		if($accion!='cerrar_sesion'){
			if($this->usuarios_modelo->verificar_sesion()){
				redirect('menu');
			}
		}*/
                $this->load->model('configuracion_modelo');
                $this->data['configuracion']=$this->configuracion_modelo->datos_sipe()->row();
	}	
	function index()
	{
		if (!$this->session->userdata('loggeado')) {
			$this->data['titulo']='Sistema de Pedidos - Inicio de sesión';
                        $this->load->view('sipe/index',$this->data);
		}else{
			redirect('menu');
		}
	}
	function acceder()
	{
            $configuracion=$this->data['configuracion'];
            	$this->load->model('usuarios_modelo');
		$usuario=$this->input->post('usuario');
		$clave=$this->input->post('clave');
		$es_ajax=$this->input->post('ajax');
		$query=$this->usuarios_modelo->validar_usuario($usuario,$clave);
                if($query->num_rows==1) // Si es usuario es validado;
		{
                    $usuario=$query->row();
                    if($configuracion->activo == 1 || ( $configuracion->activo==0 && $usuario->rol==1) || ( $configuracion->activo==0 && $usuario->rol==4) ){
			$vendedores=$this->usuarios_modelo->vendedores_usuario_arreglo($usuario->id);
			$data = array(
				'id_usuario' => $usuario->id,
				'codigo' => $usuario->codigo,
				'nombreusuario' => $usuario->usuario,
				'nombreCompleto' => $usuario->nombres.' '.$usuario->apellidos,
				'rol' => $usuario->rol,
				'numeros_vendedor' => $vendedores,
				'loggeado' => true,
				'ultimo_acceso'=>time()
			);
			$this->session->set_userdata($data);
                        $json=array('estado'=>'correcto','mensaje'=>'Espere mientras se carga la información.');   
                    }else{
                        $json=array('estado'=>'error','mensaje'=>'En este momento el acceso no está permitido.');      
                    }
		}
		else
		{
                     $json=array('estado'=>'error','mensaje'=>'Existen errores de validacion');
		}
                if($this->input->post('ajax')){
                    echo json_encode($json);
                }else{
                    redirect('');
                }
	}
	function cerrar_sesion()
	{
		$this->session->sess_destroy();
		redirect('');
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */