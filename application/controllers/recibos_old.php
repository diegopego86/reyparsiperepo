<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recibos extends CI_Controller {
	var $data=array();
	function __construct(){
		parent::__construct();
		$this->load->model('usuarios_modelo');
                $accion=$this->uri->segment(2);
		if($accion!='imprimir_recibo' && $accion!='notificacion_posfechados' && $accion!='notificacion_posfechados_vendedor'){
                    if(!$this->usuarios_modelo->verificar_sesion()){
                            $this->session->set_userdata('errores', 'La página que estas tratando de acceder requiere de tu registro. Por favor valida tus datos.');
                            redirect('sipe', 'refresh');
                            die();
                    }else{
                            /*$this->load->library('permisos');
                            $this->data['items_menu']=$this->permisos->acciones_permitidas_controlador('documentacion',$this->session->userdata('rol_id'),'documentacion')->result();*/
                    }
                }
                $this->data['info_usuario']=$this->usuarios_modelo->leer($this->session->userdata('id_usuario'))->row();
                $this->load->model('recibos_modelo');
                $this->load->model('configuracion_modelo');
                $this->data['configuracion']=$this->configuracion_modelo->configuracion_recibo()->row_array();
                $this->data['empresa']=$this->configuracion_modelo->datos_empresa();
	}
	function index(){
            $this->load->library('pagination');
		$this->load->model('recibos_modelo');		
		
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		
		if(isset($datos['buscar'])){
			$buscar=$datos['buscar'];
		}else{
			$buscar=$this->input->post('buscar');
		}		
		if($buscar){
			$filtro="buscar/".$buscar;
		}		
		$config['base_url']=site_url('recibos/index');
		$config['total_rows']=$this->recibos_modelo->total_listar_recibos($this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$buscar);
		$config['per_page']=20;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		
		$this->data['buscar']=$buscar;
		$this->data['recibos']=$this->recibos_modelo->listar_recibos($config['per_page'],$this->uri->segment(3),$this->session->userdata('numeros_vendedor'),$this->session->userdata('id_usuario'),$orden,$buscar)->result();
		$this->data['desde']=$this->uri->segment(3)+1;
		$this->data['hasta']=$this->uri->segment(3)+$config['per_page'];
		$this->data['total_recibos']=$config['total_rows'];
		$this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Recibos provisionales de caja';
		$this->data['contenido']='recibos/index';
		$this->load->view('template/contenido',$this->data);
	}
	function nuevo(){
		$this->load->model('bancos_modelo');
                $this->data['cuentas']=$this->bancos_modelo->listar_cuentas_dropdown();
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde','jquery.lightbox-0.5');
                $sustituye = array("\r\n", "\n\r", "\n", "\r");
                $this->data['bancos']=str_replace($sustituye, "",form_dropdown('banco',$this->bancos_modelo->listar_dropdown()));
                $this->data['observaciones_predeterminadas']=$this->recibos_modelo->listar_observaciones(1)->result();
		$this->data['titulo']='Recibos - Nuevo Recibo provisional de caja';
		$this->data['contenido']='recibos/nuevo';
		$this->load->view('template/contenido',$this->data);
	}
	function buscar_cliente(){
		$this->load->library('pagination');
		$this->load->model('clientes_modelo');
		
		$buscar=$this->input->post('cliente');
		$ajax=$this->input->post('ajax');
		$filtro=null;
		$orden=null;
		$datos=$this->uri->uri_to_assoc(4);
		if(isset($datos['orden'])){
			$orden=array(array('orden'=>$datos['orden'],'direccion'=>$datos['direccion']));
		}
		if(!$buscar){
			if(isset($datos['buscar'])){
				$buscar=str_replace('_',' ',$datos['buscar']);
			}
		}
		if($buscar){
			$filtro='buscar/'.$buscar;
		}
		$config['base_url']=site_url('recibos/buscar_cliente');
		if($this->session->userdata('rol')==3){
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_nits_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}else{
			$config['total_rows']=$this->clientes_modelo->resultados_buscar_cliente($buscar,$this->session->userdata('numeros_vendedor'));
		}
		$config['per_page']=5;
		$config['num_links']=5;
		$config['filtro']=$filtro;
		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
		if($this->session->userdata('rol')==3){
			$this->data['clientes']=$this->clientes_modelo->buscar_nits_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}else{
			$this->data['clientes']=$this->clientes_modelo->buscar_cliente($config['per_page'],$this->uri->segment(3),$buscar,$orden,$this->session->userdata('numeros_vendedor'))->result();
		}
		$this->data['cliente']=$buscar;
		$this->data['titulo']='Recibos';
		$this->data['contenido']='recibos/buscar_cliente';
		
		if($ajax){
			echo $this->load->view($this->data['contenido'],$this->data,false);
		}else{
			$this->load->view('template/contenido',$this->data);
		}
	}
	function datos_cliente($cliente=null){
		if(!$cliente){
			$cliente=$this->input->post('cliente');
		}
		if($cliente){
			$this->load->model('clientes_modelo');
			$this->load->model('cartera_modelo');
			$this->load->model('vendedores_modelo');
			$datos_cliente=$this->clientes_modelo->leer_cliente($cliente,$this->session->userdata('numeros_vendedor'));
			if($datos_cliente->num_rows >0){
				$this->data['cliente']=$datos_cliente->row();
				$orden_cartera=array(array('orden'=>'fecha','direccion'=>'asc'),array('orden'=>'id_documento','direccion'=>'asc'));
				$this->data['vendedores']=$this->vendedores_modelo->listar_dropdown_nombres();
				$this->data['cartera']=$this->cartera_modelo->cartera_cliente(0,0,$cliente,$orden_cartera,$this->session->userdata('numeros_vendedor'))->result();
				//$this->data['cartera']=null;
				$this->data['titulo']='Recibos';
				$this->data['contenido']='recibos/datos_cliente';		
				$json=array('estado'=>'correcto','mensaje'=>'Se cargaron los datos del cliente.','vista'=>$this->load->view($this->data['contenido'],$this->data,true));
			}else{
				$json=array('estado'=>'error','mensaje'=>'El cliente no existe.');
			}
		}else{
                    $json=array('estado'=>'error','mensaje'=>'Faltan datos para realizar la consulta');			
		}
                echo json_encode($json);
	}
        function leer_borrador(){
            $id=$this->input->post('borrador');
            if($id){
                    $borrador=$this->recibos_modelo->leer_borrador($id);
                    if($borrador->num_rows>0){
                            $borrador=$borrador->row();
                            $json=array('estado'=>'correcto','mensaje'=>'Se obtubo la iformación del borrador','encabezado'=>json_decode($borrador->encabezado),'documentos'=>json_decode($borrador->documentos),'cheques'=>json_decode($borrador->cheques),'consignaciones'=>json_decode($borrador->consignaciones));
                    }else{
                            $json=array('estado'=>'error','mensaje'=>'No hay datos');
                    }
		}else{
			$json=array('estado'=>'error','mensaje'=>'Faltan datos para poder acceder a la información');
		}
                echo json_encode($json);
        }
	function guardar_borrador(){
		$this->load->model('recibos_modelo');
		$numero_borrador=$this->input->post('numero_borrador');
		$encabezado=$this->input->post('encabezado');
		$documentos=$this->input->post('documentos');
		$cheques=$this->input->post('cheques');
                $consignaciones=$this->input->post('consignaciones');
		$fecha=date('Y-m-d H:i:s',time());
		$resultados=array();
		$estado=$this->recibos_modelo->guardar_borrador(json_encode($encabezado),json_encode($documentos),json_encode($cheques),json_encode($consignaciones),$fecha,$numero_borrador);
		$resultados=array('id'=>$estado,'fecha'=>$fecha);
		echo json_encode($resultados);
	}
        function eliminar_borrador($id){
            if($id){
                if($this->recibos_modelo->eliminar_borrador($id)){
                    $json=array('estado'=>'correcto','mensaje'=>'Se eliminó el borrador correctamente.');                    
                }else{
                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar eliminar borrador.');
                }
            }else{
                $json=array('estado'=>'error','mensaje'=>'Faltan datos para ejecutar la acción.');
            }
            echo json_encode($json);
        }
        function guardar(){
		$ajax=$this->input->post('ajax');
		$mostrar='';
                
                if($ajax){
			$borrador=$this->input->post('numero_borrador');
			$encabezado=$this->input->post('encabezado');
			$documentos=$this->input->post('documentos');
                        $cheques=$this->input->post('cheques');
                        $consignaciones=$this->input->post('consignaciones');
                        $vendedor=$this->input->post('vendedor');
                        
                        if($numero=$this->recibos_modelo->guardar($borrador,$encabezado,$documentos,$cheques,$consignaciones,$vendedor)){
                            $mensaje=$this->ver($numero,sha1($numero.$this->config->item('encription_key')),false,true,true,false,false);
                            $mostrar=$mostrar.$mensaje.'<br/>';
                            
                            $this->data['numero']=$numero;
                            $this->data['mostrar']=$mostrar;
                            $this->data['envio']='';
                            $this->data['titulo']='Recibos - Recibo enviado correctamente.';
                            $mensaje=$this->load->view('recibos/recibo_enviado',$this->data,true);
                            $json=array('estado'=>'correcto','mensaje'=>$mensaje,'numero'=>$numero);
                        }else{
                            $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al intentar guardar el recibo');                            
                        }
                        echo json_encode($json);
		}else{
                    $json=array('estado'=>'Error','mensaje'=>'Error Estas accediendo de forma no autorizada.');
                    echo json_encode($json);
		}
	}
        function ver($numero,$seguridad=null,$autoimprimir=false,$enlace=false,$retornar=false,$mostrar_logo=true,$mostrar_encabezado=true){
        	$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->recibos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['seguridad']=$this->config->item('encription_key');
				$this->data['encabezado']=$encabezado->row();
                                $this->data['cheques']=$this->recibos_modelo->leer_cheques($numero)->result();
                                $this->data['consignaciones']=$this->recibos_modelo->leer_consignaciones($numero)->result();
                                $this->data['autoimprimir']=$autoimprimir;
                                $this->data['enlace']=$enlace;
                                $this->data['mostrar_logo']=$mostrar_logo;
                                $this->data['mostrar_encabezado']=$mostrar_encabezado;
				$this->data['documentos']=$this->recibos_modelo->leer_documentos($numero)->result();
                                if($retornar){
                                    return $this->load->view('recibos/ver',$this->data,true);
                                }else{
                                    $this->load->view('recibos/ver',$this->data);
                                }
			}else{
				echo "El recibo no existe";
			}
		}else{
			echo "No tiene permisos para imprimir este recibo";
		}
	}
        function imprimir_recibo($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
                    $this->ver($numero,$seguridad,true,false,false,true);
		}else{
			echo "No tiene permisos para imprimir este recibo";
		}
	}
        function enviar_recibos($recibos=null,$vendedor=null,$cliente=null,$otros=null){
            
            if(!$recibos){
                $recibos=$this->input->post('recibos');
            }
            if($recibos){
                if(!$vendedor){
                    $vendedor=$this->input->post('vendedor');
                }
                if(!$cliente){
                    $cliente=$this->input->post('cliente');
                }
                if(!$otros){
                    $otros=$this->input->post('otros');
                }
                $this->load->model('vendedores_modelo');
                $this->load->model('clientes_modelo');
                
                $enviado=false;

                $configuracion=$this->configuracion_modelo->datos_empresa();

                $correos_envio=$this->configuracion_modelo->correos_envio('recibos');
                $correo_vendedor=null;
                if($vendedor){
                    if($this->data['configuracion']['enviar_copia_vendedor']){
                        $correo_vendedor=$this->vendedores_modelo->correo($vendedor);
                    }
                }
                $correo_cliente=null;
                if($cliente){
                    if($this->data['configuracion']['enviar_copia_cliente']){
                        $correo_cliente=$this->clientes_modelo->correo($cliente);                        
                    }
                }
                if($correos_envio || $correo_vendedor || $correo_cliente){
                    if($configuracion->tipo_envio==1){
                        $this->load->library('phpmailer');

                        $this->phpmailer->From     = $configuracion->correo_envio;
                        $this->phpmailer->FromName = $configuracion->descripcion_recibos;
                        $this->phpmailer->Host = $configuracion->host;
                        $this->phpmailer->Password = $configuracion->clave;
                        $this->phpmailer->Port = $configuracion->port;
                        //$this->phpmailer->Mailer='smtp';
                        $this->phpmailer->CharSet = "UTF-8";
                        foreach($correos_envio as $correo):
                            $this->phpmailer->AddAddress($correo->correo);                
                        endforeach;
                        if($correo_vendedor){
                            $this->phpmailer->AddAddress($correo_vendedor);                
                        }
                        if($correo_cliente){
                            $this->phpmailer->AddAddress($correo_cliente);                
                        }
                        if($otros){
                            $otros_array=explode(',',$otros);
                            foreach($otros_array as $otro){
                                if($this->es_mail(trim($otro))){
                                        $this->phpmailer->AddAddress(trim($otro));                            
                                }
                            }
                        }
                        if(is_array($recibos)){
                            foreach($recibos as $recibo){
                                $this->phpmailer->Subject    = 'Recibo provisional # '.$recibo;
                                $mensaje=$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                                $this->phpmailer->Body=$mensaje;
                                if($this->phpmailer->Send()){
                                    $enviado=true;
                                    $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los recibos correctamente');
                                }
                                else
                                {
                                    $enviado=false;
                                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los recibos');
                                }                        
                            }
                        }else{
                            $recibo=$recibos;

                            $this->phpmailer->Subject    = 'Recibo provisional # '.$recibo;
                            $mensaje=$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                            $this->phpmailer->Body=$mensaje;
                            if($this->phpmailer->Send()){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se envió el recibo correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el recibo');
                            }
                        }
                    }else{

                        $config=array();
                        $config['protocol']='smtp';  
                        $config['smtp_host']=$configuracion->host;  
                        $config['smtp_port']=$configuracion->port;  
                        $config['smtp_timeout']='30';  
                        $config['smtp_user']=$configuracion->correo_envio;  
                        $config['smtp_pass']=$configuracion->clave;  
                        $config['charset']='utf-8';  
                        $config['mailtype']="html";
                        $config['newline']="\r\n";

                        $this->load->library('email',$config);
                        $para=array();
                        foreach($correos_envio as $correo):
                            $para[]=$correo->correo;
                        endforeach;
                        if($correo_vendedor){
                            $para[]=$correo_vendedor;                        
                        }
                        if($correo_cliente){
                            $para[]=$correo_cliente;
                        }
                        if($otros){
                            $otros_array=explode(',',$otros);
                            foreach($otros_array as $otro){
                                if($this->es_mail(trim($otro))){
                                        $para[]=trim($otro);                            
                                }
                            }
                        }

                        if(is_array($recibos)){
                            foreach($recibos as $recibo){
                                $this->email->from($configuracion->correo_envio);
                                $this->email->to($para); 
                                $this->email->subject('Recibo provisional # '.$recibo);
                                $mensaje=$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                                $this->email->message($mensaje);
                                $envio=$this->email->send();
                                if($envio){
                                    $enviado=true;
                                    $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los recibos correctamente');
                                }
                                else
                                {
                                    $enviado=false; 
                                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los recibos');
                                }                    
                            }
                        }else{
                            $recibo=$recibos;

                            $this->email->from($configuracion->correo_envio,$configuracion->descripcion_recibos);  
                            $this->email->to($para);  
                            $this->email->subject('Recibo provisional # '.$recibo);
                            $mensaje=$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                            $this->email->message($mensaje);
                            $envio=$this->email->send();
                            if($envio){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los recibos correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los recibos');
                            } 
                        }
                    }
                }else{
                     $json=array('estado'=>'error','mensaje'=>'No hay cuentas de correo asociadas para enviar los recibos');
                }
                //echo $enviado;
                echo json_encode($json);
            }else{
                $json=array('estado'=>'Error','mensaje'=>'No hay recibos para enviar');
                echo json_encode($json);
            }
	}
        function enviar_copia($recibo,$seguridad=null){
		$verificar=sha1($recibo.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$mensaje=$this->input->post('mensaje');
			$correos=$this->input->post('email');
			
                        
                        $configuracion=$this->configuracion_modelo->datos_empresa();
                        
                        
                        if($configuracion->tipo_envio==1){
                            $this->load->library('phpmailer');

                            $this->phpmailer->From     = $configuracion->correo_envio;
                            $this->phpmailer->FromName = $configuracion->descripcion_recibos;
                            $this->phpmailer->Host = $configuracion->host;
                            $this->phpmailer->Password = $configuracion->clave;
                            $this->phpmailer->Port = $configuracion->port;
                            //$this->phpmailer->Mailer='smtp';
                            $this->phpmailer->CharSet = "UTF-8";
                            $otros_array=explode(',',$correos);			
                            foreach($otros_array as $email){
                                    if($this->es_mail(trim($email))){
                                            $this->phpmailer->AddAddress(trim($email));
                                            //$para[]=trim($email);
                                    }
                            }
                            $this->phpmailer->Subject    = 'Recibo provisional # '.$recibo;
                            $mensaje=$mensaje.'<br/><br/>'.$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                            $this->phpmailer->Body=$mensaje;
                            if($this->phpmailer->Send()){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se envió el recibo correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar el recibo');
                            }
                            echo json_encode($json);
                        }else{
                            $config=array();
                            $config['protocol']='smtp';  
                            $config['smtp_host']=$configuracion->host;  
                            $config['smtp_port']=$configuracion->port;  
                            $config['smtp_timeout']='30';  
                            $config['smtp_user']=$configuracion->correo_envio;  
                            $config['smtp_pass']=$configuracion->clave;  
                            $config['charset']='utf-8';  
                            $config['mailtype']="html";
                            $config['newline']="\r\n";

                            $this->load->library('email',$config);
                            $para=array();
                            $otros_array=explode(',',$correos);			
                            foreach($otros_array as $email){
                                    if($this->es_mail(trim($email))){
                                            $para[]=trim($email);                                            
                                    }
                            }
                            $this->email->from($configuracion->correo_envio,$configuracion->descripcion_recibos);  
                            $this->email->to($para);  
                            $this->email->subject('Recibo provisional # '.$recibo);
                            $mensaje=$mensaje.'<br/><br/>'.$this->ver($recibo,sha1($recibo.$this->config->item('encription_key')),true,false,true,false);
                            $this->email->message($mensaje);
                            $envio=$this->email->send();
                            if($envio){
                                $enviado=true;
                                $json=array('estado'=>'correcto','mensaje'=>'Se enviaron los recibos correctamente');
                            }
                            else
                            {
                                $enviado=false;
                                $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar los recibos');
                            }
                            echo json_encode($json);
                        }
		}else{
			echo "No tiene permisos para imprimir este recibo";
		}
	}
        function generar_excel($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->recibos_modelo->leer_encabezado($numero);
			
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['productos']=$this->recibos_modelo->leer_productos($numero)->result();
				$this->load->library('PHPExcel');
				$this->load->library('PHPExcel/IOFactory');
				
				$this->data['objPHPExcel'] = new PHPExcel();

				$this->load->view('recibos/generar_excel',$this->data);
			}else{
				echo "El recibo no existe";
			}
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este recibo";
		}
	}
	function generar_pdf($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
                    $this->load->library('mpdf');
                    $html = $this->ver($numero,sha1($numero.$this->config->item('encription_key')),true,false,true,false);
                    $this->mpdf->debug = true;
                    $this->mpdf->WriteHTML($html);			
                    $this->mpdf->Output('Recibo provisional # '.$numero.'.pdf','D');  			
		}else{
			echo "No tiene permisos para descargar en Excel la copia de este recibo";
		}
	}
        function imprimir_copia($numero,$seguridad=null){
		$verificar=sha1($numero.$this->config->item('encription_key'));
		if($verificar==$seguridad){
			$encabezado=$this->recibos_modelo->leer_encabezado($numero);
			if($encabezado->num_rows>0){
				$this->data['encabezado']=$encabezado->row();
				$this->data['documentos']=$this->recibos_modelo->leer_documentos($numero)->result();
                                $this->data['consignaciones']=$this->recibos_modelo->leer_consignaciones($numero)->result();
                                $this->data['cheques']=$this->recibos_modelo->leer_cheques($numero)->result();
				$this->load->view('recibos/imprimir_tirilla',$this->data);
			}else{
				echo "El recibo no existe";
			}
		}else{
			echo "No tiene permisos para imprimir este recibo";
		}
	}
        function es_mail($str){
		$match = "/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$/";
		if(preg_match($match, $str)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
        function imprimir_rango(){
                $this->data['seguridad']=$this->config->item('encription_key');
		$this->data['estilos']=array('jquery-ui-1.8.custom_verde');
		$this->data['titulo']='Recibos';
		$this->data['contenido']='recibos/imprimir_rango_formulario';
		$this->load->view($this->data['contenido'],$this->data);
        }
        function imprimir_rango_recibos(){
            $tipo=$this->input->post('tipo');
            $rango=$this->input->post('rango');
            $numeros=explode(',', $rango);
            $ids=array();
            $rangos=array();
            foreach($numeros as $numero):
                if(strpos($numero, '-')===false){
                    $ids[]=($numero*1);                    
                }else{
                    $otros=explode('-', $numero);
                    $desde=str_pad($otros[0], 6, "0", STR_PAD_LEFT);
                    $rangos[]=array('desde'=>$desde,'hasta'=>str_pad($otros[count($otros)-1], 6, "0", STR_PAD_LEFT) );                    
                }
            endforeach;
            
            $recibos_encabezados=$this->recibos_modelo->encabezados_recibos($ids,$rangos)->result();
            $recibos_documentos=$this->recibos_modelo->documentos_recibos($ids,$rangos)->result();
            $recibos_cheques=$this->recibos_modelo->cheques_recibos($ids,$rangos)->result();
            $recibos_consignaciones=$this->recibos_modelo->consignaciones_recibos($ids,$rangos)->result();
            $recibos_array=array();
            foreach($recibos_encabezados as $recibo):
                $recibos_array[$recibo->id]['encabezado']=(object) array('id'=>$recibo->id,
                                                                                 'numero'=>$recibo->numero,
                                                                                 'fecha'=>$recibo->fecha,
                                                                                 'fecha_realizado'=>$recibo->fecha_realizado,
                                                                                 'cliente'=>$recibo->cliente,
                                                                                 'nombre'=>$recibo->nombre,
                                                                                 'contacto'=>$recibo->contacto,
                                                                                 'direccion'=>$recibo->direccion,
                                                                                 'ciudad'=>$recibo->ciudad,
                                                                                 'telefono1'=>$recibo->telefono1,
                                                                                 'vendedor'=>$recibo->vendedor,
                                                                                 'nombre_vendedor'=>$recibo->nombre_vendedor,
                                                                                 'banco_nombre'=>$recibo->banco_nombre,
																				 'numero_cuenta'=>$recibo->numero_cuenta,
                                                                                 'cuenta'=>$recibo->cuenta,
                                                                                 'consignacion'=>$recibo->consignacion,
                                                                                 'cheques'=>$recibo->cheques,
                                                                                 'total_efectivo'=>$recibo->total_efectivo,
                                                                                 'total_cheques'=>$recibo->total_cheques,
                                                                                 'es_cliente'=>$recibo->es_cliente,
                                                                                 'es_vendedor'=>$recibo->es_vendedor,
                                                                                 'observaciones'=>$recibo->observaciones,
                                                                                 'total'=>$recibo->total
                                                                            );
            endforeach;
            foreach($recibos_documentos as $recibo):
                $recibos_array[$recibo->recibo]['documentos'][]=(object) array(
                                                                                'recibo'=>$recibo->recibo,
                                                                                'documento'=>$recibo->documento,
                                                                                'valor'=>$recibo->valor,
                                                                                'descuento'=>$recibo->descuento,
                                                                                'rete_fte'=>$recibo->rete_fte,
                                                                                'nota_credito'=>$recibo->nota_credito,
                                                                                'nota_credito_detalle'=>$recibo->nota_credito_detalle,
                                                                                'nota_debito'=>$recibo->nota_debito,
                                                                                'otros'=>$recibo->otros,
                                                                                'total'=>$recibo->total,
                                                                            );
            endforeach;
            foreach($recibos_cheques as $recibo):
                $recibos_array[$recibo->recibo]['cheques'][]=(object) array(
                                                                                'cheque'=>$recibo->cheque,
                                                                                'banco'=>$recibo->banco,
                                                                                'valor'=>$recibo->valor,
                                                                                'posfechado'=>$recibo->posfechado,
                                                                                'fecha_posfechado'=>$recibo->fecha_posfechado                                                                                
                                                                            );
            endforeach;
            foreach($recibos_consignaciones as $recibo):
                $recibos_array[$recibo->recibo]['consignaciones'][]=(object) array(
                                                                                'cuenta_numero'=>$recibo->cuenta_numero,
                                                                                'banco_nombre'=>$recibo->banco_nombre,
                                                                                'numero'=>$recibo->numero,
                                                                                'valor'=>$recibo->valor,
                                                                                'fecha'=>$recibo->fecha,
                                                                                'realizado_por'=>$recibo->realizado_por
                                                                            );
            endforeach;
            $this->data['recibos']=$recibos_array;
            if($tipo==2){
                $this->load->view('recibos/imprimir_rango_recibos_tirilla',$this->data);
            }else{
                $this->load->view('recibos/imprimir_rango_recibos',$this->data);
            }
        }
        public function notificacion_posfechados_vendedor(){
            if($this->data['configuracion']['enviar_notificacion_posfechados_vendedores']){
                $cheques=$this->recibos_modelo->cheques_posfechados_dia()->result();
                $cheque_vd=array();
                foreach($cheques as $cheque):
                    $cheques_vd[$cheque->vendedor]['numero']=$cheque->vendedor;
                    $cheques_vd[$cheque->vendedor]['nombre']=$cheque->vendedor_nombre;
                    $cheques_vd[$cheque->vendedor]['correo']=$cheque->vendedor_correo;
                    $cheques_vd[$cheque->vendedor]['total_cheques']=((isset($cheques_vd[$cheque->vendedor]['total_cheques'])?$cheques_vd[$cheque->vendedor]['total_cheques']:0))+1;
                    $cheques_vd[$cheque->vendedor]['fechas'][$cheque->fecha_posfechado]['fecha']=$cheque->fecha_posfechado;
                    $cheques_vd[$cheque->vendedor]['fechas'][$cheque->fecha_posfechado]['total']=((isset($cheques_vd[$cheque->vendedor]['fechas'][$cheque->fecha_posfechado]['total'])?$cheques_vd[$cheque->vendedor]['fechas'][$cheque->fecha_posfechado]['total']:0))+$cheque->valor;
                    $cheques_vd[$cheque->vendedor]['fechas'][$cheque->fecha_posfechado]['cheques'][]=$cheque;                
                endforeach;
                foreach($cheques_vd as $vendedor):
                    $this->data['cheques']=$vendedor;
                    $cheques=$this->load->view('recibos/notificacion_posfechados_vendedor',$this->data,true);
                    $enviado=false;
                    $total_cheques=$vendedor['total_cheques'];
                    //if($this->data['configuracion']['enviar_notificacion_posfechados_vendedores']){
                        $correo_vendedor=$vendedor['correo'];
                        $configuracion=$this->data['empresa'];
                        if($correo_vendedor){
                                if($configuracion->tipo_envio==1){
                                    $this->load->library('phpmailer');

                                    $this->phpmailer->From     = $configuracion->correo_envio;
                                    $this->phpmailer->FromName = $configuracion->descripcion_recibos;
                                    $this->phpmailer->Host = $configuracion->host;
                                    $this->phpmailer->Password = $configuracion->clave;
                                    $this->phpmailer->Port = $configuracion->port;
                                    //$this->phpmailer->Mailer='smtp';
                                    $this->phpmailer->CharSet = "UTF-8";
                                    $this->phpmailer->AddAddress($correo_vendedor);                
                                    $this->phpmailer->Subject    = 'Notificacion. Tienes '.$total_cheques.' cheques posfechados por consignar';
                                    $this->phpmailer->Body=$cheques;
                                    if($this->phpmailer->Send()){
                                        $enviado=true;
                                        $json=array('estado'=>'correcto','mensaje'=>'Se envió la notificación correctamente');
                                    }
                                    else
                                    {
                                        $enviado=false;
                                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar la notificación');
                                    }                        
                                }else{

                                    $config=array();
                                    $config['protocol']='smtp';  
                                    $config['smtp_host']=$configuracion->host;  
                                    $config['smtp_port']=$configuracion->port;  
                                    $config['smtp_timeout']='30';  
                                    $config['smtp_user']=$configuracion->correo_envio;  
                                    $config['smtp_pass']=$configuracion->clave;  
                                    $config['charset']='utf-8';  
                                    $config['mailtype']="html";
                                    $config['newline']="\r\n";

                                    $this->load->library('email',$config);
                                    $para=array();
                                    foreach($correos_envio as $correo):
                                        $para[]=$correo->correo;
                                    endforeach;

                                    $this->email->from($configuracion->correo_envio,$configuracion->descripcion_recibos);  
                                    $this->email->to($para);  
                                    $this->email->subject('Notificacion. Tienes '.$total_cheques.' cheques posfechados por consignar');
                                    $this->email->message($cheques);
                                    $envio=$this->email->send();
                                    if($envio){
                                        $enviado=true;
                                        $json=array('estado'=>'correcto','mensaje'=>'Se envió la notificación correctamente');
                                    }
                                    else
                                    {
                                        $enviado=false;
                                        $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar la notificación');
                                    }                         
                                }
                            }else{
                                 $json=array('estado'=>'error','mensaje'=>'No hay cuentas de correo asociadas para enviar la notificación');
                            }
                            //echo $enviado;
                            echo json_encode($json);
                  /*  }else{
                        echo '<h1>No se envió el correo</h1>'.$vendedor['nombre'].'<br/>'.$cheques.'<br/>';
                    }*/
                endforeach;
            }else{
                $json=array('estado'=>'Error','mensaje'=>'No está activado el envío de alertas');
                 echo json_encode($json);
            }
        }
        public function notificacion_posfechados(){
                $this->data['cheques']=$this->recibos_modelo->cheques_posfechados_dia()->result();
                $total_cheques=count($this->data['cheques']);
                if($this->data['cheques']){
                    $cheques=$this->load->view('recibos/notificacion_posfechados',$this->data,true);
                    $enviado=false;
                    if($this->data['configuracion']['enviar_notificacion_posfechados']){
                        $configuracion=$this->data['empresa'];
                        $correos_envio=$this->configuracion_modelo->correos_envio('recibos');
                        if($correos_envio){
                            if($configuracion->tipo_envio==1){
                                $this->load->library('phpmailer');

                                $this->phpmailer->From     = $configuracion->correo_envio;
                                $this->phpmailer->FromName = $configuracion->descripcion_recibos;
                                $this->phpmailer->Host = $configuracion->host;
                                $this->phpmailer->Password = $configuracion->clave;
                                $this->phpmailer->Port = $configuracion->port;
                                //$this->phpmailer->Mailer='smtp';
                                $this->phpmailer->CharSet = "UTF-8";
                                foreach($correos_envio as $correo):
                                    $this->phpmailer->AddAddress($correo->correo);                
                                endforeach;

                                $this->phpmailer->Subject    = 'Notificacion. Tienes '.$total_cheques.' cheques posfechados por consignar';
                                $this->phpmailer->Body=$cheques;
                                if($this->phpmailer->Send()){
                                    $enviado=true;
                                    $json=array('estado'=>'correcto','mensaje'=>'Se envió la notificación correctamente');
                                }
                                else
                                {
                                    $enviado=false;
                                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar la notificación');
                                }                        
                            }else{

                                $config=array();
                                $config['protocol']='smtp';  
                                $config['smtp_host']=$configuracion->host;  
                                $config['smtp_port']=$configuracion->port;  
                                $config['smtp_timeout']='30';  
                                $config['smtp_user']=$configuracion->correo_envio;  
                                $config['smtp_pass']=$configuracion->clave;  
                                $config['charset']='utf-8';  
                                $config['mailtype']="html";
                                $config['newline']="\r\n";

                                $this->load->library('email',$config);
                                $para=array();
                                foreach($correos_envio as $correo):
                                    $para[]=$correo->correo;
                                endforeach;

                                $this->email->from($configuracion->correo_envio,$configuracion->descripcion_recibos);  
                                $this->email->to($para);  
                                $this->email->subject('Notificacion. Tienes '.$total_cheques.' cheques posfechados por consignar');
                                $this->email->message($cheques);
                                $envio=$this->email->send();
                                if($envio){
                                    $enviado=true;
                                    $json=array('estado'=>'correcto','mensaje'=>'Se envió la notificación correctamente');
                                }
                                else
                                {
                                    $enviado=false;
                                    $json=array('estado'=>'error','mensaje'=>'Se presentaron errores al enviar la notificación');
                                }                         
                            }
                        }else{
                             $json=array('estado'=>'error','mensaje'=>'No hay cuentas de correo asociadas para enviar la notificación');
                        }
                        //echo $enviado;
                        echo json_encode($json);
                }else{
                    $json=array('estado'=>'Error','mensaje'=>'No está activado el envío de alertas');
                    echo json_encode($json);
                    
                }
            }else{
                $json=array('estado'=>'Error','mensaje'=>'No hay posfechados');
                    echo json_encode($json);
            }
        }
        public function prueba(){
            echo "Prueba";
        }
        function actualizar_consignaciones(){
            $recibos=$this->recibos_modelo->listar_recibos(0,0)->result();
            foreach($recibos as $recibo):
                if($recibo->es_cliente){
                    $realizado_por=1;
                }else{
                    $realizado_por=2;
                }
                $consignacion=array(
                    'recibo'=>$recibo->id,
                    'cuenta'=>$recibo->cuenta,
                    'numero'=>$recibo->consignacion,
                    'fecha'=>$recibo->fecha,
                    'valor'=>$recibo->total,
                    'realizado_por'=>$realizado_por
                );
                $this->recibos_modelo->agregar_consignacion($consignacion);
            endforeach;
        }
        /*function actualizar_cheques(){
            $cheques_listado=$this->recibos_modelo->cheques_recibos_actualizar()->result();
            foreach($cheques_listado as $cheque):
                $cheques=json_decode($cheque->cheques);
                $datos_cheques=array();
                foreach($cheques as $ch):
                    $datos_cheque=array(
                        'recibo'=>$cheque->id,
                        'banco'=>$ch->banco,
                        'cheque'=>$ch->cheque,
                        'valor'=>$ch->valor,
                        'posfechado'=>0,
                        'fecha_posfechado'=>null
                    );
                    $datos_cheques[]=$datos_cheque;
                endforeach;
                $this->recibos_modelo->crear_cheques($datos_cheques);
            endforeach;
            echo "Listo";
        }*/
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */