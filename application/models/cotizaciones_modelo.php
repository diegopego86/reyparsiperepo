<?php
class Cotizaciones_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_cotizaciones($limit,$offset,$numeros_vendedor=null,$usuario=null,$orden=null,$buscar=null){
		$this->db->select('cotizaciones_maestro.*,cliente.nombre as cliente_nombre, cliente.contacto');
		if($orden){
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}else{
				$this->db->order_by('id_maestro','desc');
				$this->db->order_by('fecha_creacion','desc');
		}
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cotizaciones_maestro.nombre_cliente like '%$buscar%' or cotizaciones_maestro.cliente like '%$buscar%' or cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or cotizaciones_maestro.id_maestro like '%$buscar%') AND (cotizaciones_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or cotizaciones_maestro.id_usuario='".$usuario."')");
			}else{
				$this->db->where("(cotizaciones_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or cotizaciones_maestro.id_usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cotizaciones_maestro.nombre_cliente like '%$buscar%' or cotizaciones_maestro.cliente like '%$buscar%' or cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or cotizaciones_maestro.id_maestro like '%$buscar%')");
			}
		}
		$this->db->join('cliente','cliente.id_cliente=cotizaciones_maestro.id_cliente','left');
		if($limit){
			$query=$this->db->get('cotizaciones_maestro',$limit,$offset);
		}else{
			$query=$this->db->get('cotizaciones_maestro');
		}
                return $query;
	}
	function total_listar_cotizaciones($numeros_vendedor=null,$usuario=null,$buscar=null){
		$this->db->select('count(*) as total');
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cotizaciones_maestro.nombre_cliente like '%$buscar%' or cotizaciones_maestro.cliente like '%$buscar%' or cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or cotizaciones_maestro.id_maestro like '%$buscar%') AND (cotizaciones_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or cotizaciones_maestro.id_usuario='".$usuario."')");
			}else{
				$this->db->where("(cotizaciones_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or cotizaciones_maestro.id_usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cotizaciones_maestro.nombre_cliente like '%$buscar%' or cotizaciones_maestro.cliente like '%$buscar%' or cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or cotizaciones_maestro.id_maestro like '%$buscar%')");
			}
		}
		$this->db->join('cliente','cliente.id_cliente=cotizaciones_maestro.id_cliente','left');
		$query=$this->db->get('cotizaciones_maestro')->row();
		return $query->total;
	}
	function total_cotizaciones_vendedores_mes($vendedores,$usuario,$mes){
		$this->db->select('id_vendedor,sum(subtotal) as subtotal',false);
		if($vendedores){
			$this->db->where("(id_vendedor in ('".join("','",$vendedores)."') or id_usuario='".$usuario."')");
		}
		$this->db->like('fecha',$mes,'after');
		$this->db->order_by('id_vendedor');
		$this->db->group_by('id_vendedor');		
		$query=$this->db->get('cotizaciones_maestro');
		return $query;
	}
	function listar_meses(){
		$this->db->select("distinct(date_format(fecha,'%Y-%m')) as fecha,date_format(fecha,'%m') as mes,date_format(fecha,'%Y') as anio",false);
		$this->db->order_by('fecha','desc');
		$this->db->group_by('fecha');
		$query=$this->db->get('cotizaciones_maestro');
		return $query;
	}
	function listar_cotizaciones_usuario($limit,$offset,$usuario=null,$orden=null){
		$this->db->select('cotizaciones_maestro.*,cliente.nombre as nombre_cliente,cliente.contacto');
		if($orden){
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($usuario){
			$this->db->where("(cotizaciones_maestro.id_vendedor in ('".join("','",$this->session->userdata('numeros_vendedor'))."') or cotizaciones_maestro.id_usuario='".$usuario."')");
		}
		$this->db->join('cliente','cliente.id_cliente=cotizaciones_maestro.id_cliente','left');
		if($limit){
			$query=$this->db->get('cotizaciones_maestro',$limit,$offset);
		}else{
			$query=$this->db->get('cotizaciones_maestro');
		}
		return $query;
	}
	function productos_cotizacion($cotizacion){
		$this->db->select('cotizaciones_detalle.id_maestro,cotizaciones_detalle.id_producto,cotizaciones_detalle.cantidad,cotizaciones_detalle.descuento,cotizaciones_detalle.total, producto.nombre,producto.referencia,producto.unidad,producto.precio1,producto.precio2,producto.precio3,producto.precio4,producto.iva,producto.stock');
		$this->db->where('id_maestro',$cotizacion);
		$this->db->order_by('id_detalle');
		$this->db->join('producto','producto.id_producto=cotizaciones_detalle.id_producto','left');
		$query=$this->db->get('cotizaciones_detalle');
		return $query;
	}
	function obtener_numeros($cantidad){
                $abc=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$query=$this->db->get('cotizaciones_numero')->row();
		$numero_inicial=(int)$query->numero;
                $prefijo=$query->prefijo;
                $prefijo_tamano=strlen($prefijo);
                $ultimo_numero=substr('999999',$prefijo_tamano,6)*1;
                
                $numero_final=$numero_inicial+(int)$cantidad;
		if($numero_final>$ultimo_numero){
			//$numero_final=($ultimo_numero+1)+($cantidad-($ultimo_numero-$numero_inicial+1));
                        $numero_final=$numero_final-$ultimo_numero;
                        if($prefijo==""){
                            $nuevo_prefijo="A";
                        }else{
                            $nuevo_prefijo=$this->prefijo_cambio_letra(str_split($prefijo), $prefijo_tamano-1, $abc);
                        }
		}else{
                    $nuevo_prefijo=$prefijo;
                }
		$data=array(
                    'id'=>1,
                    'numero'=>$numero_final,
                    'prefijo'=>$nuevo_prefijo
                );
		$this->db->update('cotizaciones_numero',$data);
		$numeros=array();
		for($n=0;$n<$cantidad;$n++){
			$numero=$numero_inicial+$n;
			if($numero>$ultimo_numero){
                            $numero=$numero-$ultimo_numero;
                            $prefijo=$nuevo_prefijo;
			}
			$numeros[]= $prefijo.str_pad($numero, (6-strlen($prefijo)), "0", STR_PAD_LEFT);
		}
		return $numeros;
	}
        function prefijo_cambio_letra($prefijo,$letra,$abc){
            if($prefijo[0]<>""){
                if($letra<0){
                    for($m=$letra+1;$m<count($prefijo);$m++){
                        $prefijo[$m]='A';
                    }
                    $prefijo[count($prefijo)]='A';
                    return join($prefijo);
                }
                if($prefijo[$letra]=='Z'){
                    return $this->prefijo_cambio_letra($prefijo, $letra-1, $abc);                
                }else{
                    $nuevo=$abc[array_search($prefijo[$letra],$abc)+1];
                    $prefijo[$letra]=$nuevo;
                    return join($prefijo);
                }
            }else{
                return "A";
            }
        }
        function guardar_cotizacion($borrador,$encabezado,$productos){
            $this->db->where('id',1);
            $configuracion_cotizaciones=$this->db->get('cotizaciones_configuracion')->row();
            
            
            //Variable que dice cuantos productos por cotizacion se permiten;
            $total_por_cotizacion=$configuracion_cotizaciones->maximo_items;//Si este campo = 0 o es nulo, se generará un solo pedido
            
            //Obtener cantidad de cotizaciones a realizar
            $cantidad_cotizaciones_total=0;
            if(!$total_por_cotizacion || $total_por_cotizacion==0 || $total_por_cotizacion==null){
                $cantidad_cotizaciones_total=1;
            }else{
                $cantidad_cotizaciones_total=ceil(count($productos)/$total_por_cotizacion);
            }
                
            
            //Se comienza con las consultas para guardar los cotizaciones
            $this->db->trans_start();
               
                //Se obtienen los numeros de los cotizaciones que se van a guardar y se actualiza el contador en la base de datos
                $numeros=$this->obtener_numeros($cantidad_cotizaciones_total);
                
                //El valor cotizacion_grupo identifica los cotizaciones de un mismo grupo.
                $cotizacion_grupo=null;
                    if($cantidad_cotizaciones_total>1){
                            $cotizacion_grupo=$numeros[0];
                    }
                
                //Permite determinar si el cotizacion se debe ordenar por el campo codigo
                $se_ordenar_por_codigo=true;
                $n=0;//Contador de cotizaciones;
                $p=1;//Contador de items cotizacion;
                $datos_productos=array();
                if($se_ordenar_por_codigo){
                    usort($productos, array($this,'ordenar_por_codigo'));
                }
                $listado=$productos;
                $subtotal_sin_dcto=0;
                $descuento_total=0;
                $subtotal=0;
                $iva_total=0;
                $total=0;
                foreach($listado as $producto):

                    $datos_productos[]=array(
                            'id_maestro'=>$numeros[$n],
                            'id_detalle'=>$p,
                            'id_producto'=>$producto['codigo'],					
                            'cantidad'=>$producto['cantidad'],					
                            'precio'=>$producto['precio'],					
                            'descuento'=>$producto['descuento'],					
                            'iva'=>$producto['iva'],					
                            'total_sin_dcto'=>$producto['total_sin_dcto'],					
                            'total'=>$producto['total'],					
                            'cantidad_despachada'=>0,					
                            'estado'=>0,					
                            'id_usuario'=>$this->session->userdata('id_usuario'),					
                            'fecha_creacion'=>date('Y-m-d H:i:s',time()),					
                            'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),					
                            'afavor'=>0,					
                            'vrreal'=>0
                    );
                    $subtotal_sin_dcto=$subtotal_sin_dcto+$producto['total_sin_dcto']*1;
                    $descuento_total=$descuento_total+(($producto['precio']*$producto['descuento']/100)*$producto['cantidad']);
                    $neto_actual=($producto['total_sin_dcto']*1)-(($producto['precio']*$producto['descuento']/100)*$producto['cantidad']);
                    $iva_total=$iva_total+(($neto_actual*$producto['iva'])/100);
                    $p=$p+1;
                    if($total_por_cotizacion!=0 && $p>$total_por_cotizacion){//Se totaliza si existen mas items que el total por cotizacion
                        $subtotal=$subtotal_sin_dcto-$descuento_total;
                        $total=round($subtotal+$iva_total,0);
                        $datos_encabezado=array(
                                'id_maestro'=>$numeros[$n],
                                'fecha'=>date('Y-m-d',time()),
                                'hora'=>date('H:i:s',time()),
                                'id_concepto'=>'13',
                                'id_cliente'=>$encabezado['nit'],
                                'nombre_cliente'=>$encabezado['nombre'],
                                'cliente'=>json_encode(array(
                                   'contacto'=>$encabezado['contacto'],
                                   'email'=>$encabezado['email'],
                                   'direccion'=>$encabezado['direccion'],
                                   'ciudad'=>$encabezado['ciudad'],
                                   'telefono'=>$encabezado['telefono'],
                                   'vendedor'=>$encabezado['vendedor'],
                                   'descuento'=>$encabezado['descuento'],
                                   'plazo'=>$encabezado['plazo'],
                                   'lista'=>$encabezado['lista'],
                                   'retencion'=>$encabezado['retencion'],
                                   'direccion_envio'=>$encabezado['direccion_envio']
                                )),
                                'id_vendedor'=>$encabezado['vendedor'],
                                'plazo'=>$encabezado['plazo'],
                                'direccion_envio'=>'',
                                'descuento_pago'=>0,
                                'descuento_dias'=>0,
                                'subtotal_sin_dcto'=>$subtotal_sin_dcto,
                                'descuento_total'=>$descuento_total,
                                'subtotal'=>$subtotal,
                                'iva_total'=>$iva_total,
                                'total'=>$total,
                                'estado'=>'terminado',
                                'nota_1'=>$encabezado['observaciones'],
                                'nota_2'=>($n+1).' de '.$cantidad_cotizaciones_total,
                                'id_usuario'=>$this->session->userdata('id_usuario'),
                                'fecha_creacion'=>date('Y-m-d H:i:s',time()),
                                'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),
                                'lista'=>$encabezado['lista']                                					
                        );
                        $this->db->insert_batch('cotizaciones_detalle',$datos_productos);
                        $this->db->insert('cotizaciones_maestro',$datos_encabezado);


                        //Se inicializan las variables
                        $p=1;//Inicializo contador de items por cotizacion
                        $n=$n+1;//Contador de cotizaciones
                        $subtotal_sin_dcto=0;
                        $descuento_total=0;
                        $subtotal=0;
                        $iva_total=0;
                        $total=0;
                        $datos_productos=array();                        
                    }                    
                endforeach;
                $subtotal=$subtotal_sin_dcto-$descuento_total;
                $total=round($subtotal+$iva_total,0);
                $datos_encabezado=array(
                        'id_maestro'=>$numeros[$n],
                        'fecha'=>date('Y-m-d',time()),
                        'hora'=>date('H:i:s',time()),
                        'id_concepto'=>'13',
                        'id_cliente'=>$encabezado['nit'],
                        'nombre_cliente'=>$encabezado['nombre'],
                        'cliente'=>json_encode(array(
                               'contacto'=>$encabezado['contacto'],
                               'email'=>$encabezado['email'],
                               'direccion'=>$encabezado['direccion'],
                               'ciudad'=>$encabezado['ciudad'],
                               'telefono'=>$encabezado['telefono'],
                               'vendedor'=>$encabezado['vendedor'],
                               'descuento'=>$encabezado['descuento'],
                               'plazo'=>$encabezado['plazo'],
                               'lista'=>$encabezado['lista'],
                               'retencion'=>$encabezado['retencion'],
                               'direccion_envio'=>$encabezado['direccion_envio']
                            )),
                        'id_vendedor'=>$encabezado['vendedor'],
                        'plazo'=>$encabezado['plazo'],
                        'direccion_envio'=>'',
                        'descuento_pago'=>0,
                        'descuento_dias'=>0,
                        'subtotal_sin_dcto'=>$subtotal_sin_dcto,
                        'descuento_total'=>$descuento_total,
                        'subtotal'=>$subtotal,
                        'iva_total'=>$iva_total,
                        'total'=>$total,
                        'estado'=>'terminado',
                        'nota_1'=>$encabezado['observaciones'],
                        'nota_2'=>($n+1).' de '.$cantidad_cotizaciones_total,
                        'id_usuario'=>$this->session->userdata('id_usuario'),
                        'fecha_creacion'=>date('Y-m-d H:i:s',time()),
                        'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),
                        'lista'=>$encabezado['lista'],
                        'grupo'=>$cotizacion_grupo					
                );

                $this->db->insert_batch('cotizaciones_detalle',$datos_productos);
                $this->db->insert('cotizaciones_maestro',$datos_encabezado);
                //Por cada grupo que termine se inicializan las varialbes;
                $p=1;//Inicializo contador de items por cotizacion
                $n=$n+1;//Contador de cotizaciones
                $subtotal_sin_dcto=0;
                $descuento_total=0;
                $subtotal=0;
                $iva_total=0;
                $total=0;
                $datos_productos=array(); 
                

                //Eliminar borrador
                $this->db->where('id',$borrador);
		$this->db->delete('borradores_cotizaciones');
                
                
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $numeros;
            }
            
            //echo $this->db->last_query();
            
            
        }
        static function ordenar_por_codigo($x,$y){
             if ( $x['codigo'] == $y['codigo'] )
              return 0;
             else if ( $x['codigo'] < $y['codigo'] )
              return -1;
             else
              return 1;
        }
        function guardar_maestro($datos){
		$insertar=$this->db->insert('cotizaciones_maestro',$datos);
		return $insertar;
	}
	function guardar_detalle($datos){
		$insertar=$this->db->insert('cotizaciones_detalle',$datos);
		return $insertar;
	}
	function leer_encabezado($numero){
		$this->db->select('cotizaciones_maestro.*,cliente.nombre,cliente.contacto,cliente.direccion,cliente.telefono1,cliente.ciudad,vendedor.nombre as nombre_vendedor');
		$this->db->where('id_maestro',$numero);
		$this->db->join('cliente','cliente.id_cliente=cotizaciones_maestro.id_cliente','left');
		$this->db->join('vendedor','vendedor.id_vendedor=cotizaciones_maestro.id_vendedor','left');
		$query=$this->db->get('cotizaciones_maestro');
		return $query;
	}
	function leer_productos($numero){
		$this->db->select('cotizaciones_detalle.*, producto.nombre,producto.referencia');
		$this->db->where('id_maestro',$numero);
		$this->db->join('producto','producto.id_producto=cotizaciones_detalle.id_producto','left');
		$query=$this->db->get('cotizaciones_detalle');
		return $query;
	}
	function guardar_borrador($encabezado,$productos,$fecha,$numero=null){
		$data=array(
			'encabezado'=>$encabezado,
			'productos'=>$productos,
			'usuario'=>$this->session->userdata('id_usuario'),
			'fecha_actualizacion'=>$fecha
		);
		if($numero){
			$this->db->where('id',$numero);
			$accion=$this->db->update('borradores_cotizaciones',$data);
			if($accion){
				$accion=$numero;
			}else{
				$accion=false;
			}
		}else{
			$data['fecha']=$fecha;
			$accion=$this->db->insert('borradores_cotizaciones',$data);
			if($accion){
				$accion=$this->db->insert_id();
			}else{
				$accion=false;
			}
		}
		return $accion;
	}
	function eliminar_borrador($numero){
		$this->db->where('id',$numero);
		$eliminar=$this->db->delete('borradores_cotizaciones');
		return $eliminar;
	}
	function listar_borradores($usuario=null){
		if($usuario){
			$this->db->where('usuario',$usuario);
		}
		$this->db->order_by('fecha','desc');
		$query=$this->db->get('borradores_cotizaciones');
		return $query;
	}
	function leer_borrador($id){
		$this->db->where('id',$id);
		$this->db->where('usuario',$this->session->userdata('id_usuario'));
		$query=$this->db->get('borradores_cotizaciones');
		return $query;
	}
	function datos_producto_borrador($producto){
		$this->db->where('id_producto',$producto);
		$query=$this->db->get('producto');
		return $query;		
	}
        function datos_cotizaciones($numeros, $rangos){
            $this->db->select('cotizaciones_maestro.*,cliente.*,vendedor.nombre as nombre_vendedor, cotizaciones_detalle.id_producto, cotizaciones_detalle.cantidad, cotizaciones_detalle.precio, cotizaciones_detalle.descuento, cotizaciones_detalle.iva, cotizaciones_detalle.total_sin_dcto, cotizaciones_detalle.total as total_item,producto.nombre as nombre_producto');
            $sql='';
            if(count($numeros)>0){
                $sql='cotizaciones_maestro.id_maestro in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                    $sql=$sql.' or (cotizaciones_maestro.id_maestro >= '.$rango['desde'].' and cotizaciones_maestro.id_maestro <= '.$rango['hasta'].')';
                    }else{
                        $sql='(cotizaciones_maestro.id_maestro >= '.$rango['desde'].' and cotizaciones_maestro.id_maestro <= '.$rango['hasta'].')';
                    }
                endforeach;
            }
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1){
                $this->db->where('cotizaciones_maestro.id_usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('cliente','cliente.id_cliente=cotizaciones_maestro.id_cliente','left');
            $this->db->join('vendedor','vendedor.id_vendedor=cotizaciones_maestro.id_vendedor','left');
            $this->db->join('cotizaciones_detalle','cotizaciones_detalle.id_maestro=cotizaciones_maestro.id_maestro','left');
            $this->db->join('producto','producto.id_producto=cotizaciones_detalle.id_producto','left');
            $query=$this->db->get('cotizaciones_maestro');
            //echo $this->db->last_query();
            return $query;
        }
        function cantidad_cotizaciones_vendedor($desde,$hasta){
            if(!$desde){
                $desde=date('d-m-Y');
            }
            
            $this->db->select('vendedor.id_vendedor,vendedor.nombre, count(cotizaciones_maestro.id_maestro) as cantidad',false);
            $this->db->where("fecha>= STR_TO_DATE('$desde','%d-%m-%Y')",null,false);
            if($hasta){
               $this->db->where("fecha<= STR_TO_DATE('$hasta','%d-%m-%Y')",null,false);
            }
            $this->db->join('vendedor','vendedor.id_vendedor=cotizaciones_maestro.id_vendedor','join');
            $this->db->order_by('vendedor.nombre','asc');
            $this->db->group_by('vendedor.id_vendedor');
            $query=$this->db->get('cotizaciones_maestro');
            return $query;
        }
        function listar_observaciones_cotizacion($visible=null){
            if($visible){
                $this->db->where('visible',1);
            }
            $this->db->order_by('orden','asc');
            $query=$this->db->get('cotizaciones_observaciones');
            return $query;
        }
        function leer_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->get('cotizaciones_observaciones');
            return $query;
        }
        function crear_observacion($datos){
            $query=$this->db->insert('cotizaciones_observaciones',$datos);
            return $query;
        }
        function editar_observacion($observacion,$datos){
            $this->db->where('id',$observacion);
            $query=$this->db->update('cotizaciones_observaciones',$datos);
            return $query;
        }
        function eliminar_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->delete('cotizaciones_observaciones');
            return $query;
        }
}
?>