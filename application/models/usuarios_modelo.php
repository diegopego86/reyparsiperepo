<?php
class Usuarios_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function validar_usuario($usuario,$clave){
            $this->db->select('*');
            $this->db->where('usuario',"BINARY '$usuario'",false);
            $this->db->where('clave',md5($clave));
            $query=$this->db->get('usuarios');
            return $query;
    }
    function vendedores_usuario_arreglo($usuario){
            $this->db->select('vendedor');
            $this->db->where('usuario',$usuario);
            $query=$this->db->get('vendedores_usuario');
            $vendedores=array();
            if($query->num_rows > 0){
                    $query=$query->result();
                    foreach($query as $vendedor):
                            $vendedores[]=$vendedor->vendedor;
                    endforeach;
            }else{
                    $this->db->where('id',$usuario);
                    $vendedor=$this->db->get('usuarios')->row();
                    $vendedores[]=$vendedor->id_vendedor;
            }
            return $vendedores;
    }
    function verificar_sesion(){
            $loggeado=$this->session->userdata('loggeado');
            date_default_timezone_set('America/Bogota');
            $tiempo_actual=time();
            $ultimo_acceso=$this->session->userdata('ultimo_acceso');
            $diferencia=$tiempo_actual-$ultimo_acceso;
            /*if($diferencia>$this->config->item('tiempo_sesion_inactiva')){
                    $data = array(
                            'id_usuario' => '',
                            'codigo' => '',
                            'nombreusuario' => '',
                            'nombreCompleto' => '',
                            'rol' => '',
                            'numeros_vendedor' => '',
                            'loggeado' => '',
                            'ultimo_acceso'=>time()
                    );
                    $this->session->unset_userdata($data);
                    $this->session->sess_destroy();
                    $this->session->set_flashdata('errores', 'Se cerr� la sesi�n por inactividad.');
                    return false;
            }else{*/
                    $this->session->set_userdata('ultimo_acceso',time());
                    if(!isset($loggeado) || $loggeado != true){
                            redirect('sipe/acceder', 'refresh');
                            die();
                            return false;
                    }
                    else{
                            return true;
                    }
            //}
    }
    function listar_usuarios($limit=0,$offset=0,$nombre=null,$orden=null){
            $this->db->select('usuarios.*,roles.nombre as nombre_rol');
            if($nombre){
                    $this->db->where("(usuario LIKE '%$nombre%' OR nombres LIKE '%$nombre%' OR apellidos LIKE '%$nombre%')",null,false);			
            }
            if(!$orden){
                    $this->db->order_by('nombres','asc');
            }else{
                    foreach($orden as $od):
                            $this->db->order_by($od['orden'],$od['direccion']);
                    endforeach;
            }
            $this->db->join('roles','roles.id=usuarios.rol','left');
            if($limit==0){
                    $query=$this->db->get('usuarios');
            }else{
                    $query=$this->db->get('usuarios',$limit,$offset);
            }
            return $query;
    }
    function total_listar_usuarios($nombre=null){
            $this->db->select('count(*) as total');
            if($nombre){
                    $this->db->where("(usuario LIKE '%$nombre%' OR nombres LIKE '%$nombre%' OR apellidos LIKE '%$nombre%')",null,false);			
            }
            $query=$this->db->get('usuarios')->row();
            return $query->total;
    }
    function cambiar_clave($clave){
            $this->db->where('id',$this->session->userdata('id_usuario'));
            $datos=array('clave'=>md5($clave));
            $update=$this->db->update('usuarios',$datos);
            return $update;
    }
    function verificar_clave($clave){
            $this->db->where('id',$this->session->userdata('id_usuario'));
            $datos=$this->db->get('usuarios')->row();
            if($datos->clave==md5($clave)){
                    return true;
            }else{
                    return false;
            }		
    }
    function crear($data,$vendedores){
        $this->db->trans_start();

            $this->db->where('usuario',$data['usuario']);
            $query=$this->db->get('usuarios');
            if($query->num_rows==0){
                $insertar=$this->db->insert('usuarios',$data);
                $id=$this->db->insert_id();
                $datos=array();
                if(is_array($vendedores)){
                    foreach($vendedores as $vendedor){
                        $datos[]=array(
                            'usuario'=>$id,
                            'vendedor'=>$vendedor
                          );
                    }
                    $this->insertar_vendedores($id,$datos);
                }
            }else
            {
                $insertar=false;
            }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return false;
        }else{
            return $insertar;
        }
    }
    function editar($id,$data){
        $this->db->where('id',$id);
        $actualizar=$this->db->update('usuarios',$data);
        return $actualizar;
    }
    function leer($id){
        $this->db->where('id',$id);
        $query=$this->db->get('usuarios');
        return $query;
    }
    function eliminar($id){
        $this->db->where('id',$id);
        $delete=$this->db->delete('usuarios');
        return $delete;
    }
    function usuario_disponible($usuario){
        $this->db->where('usuario',$usuario);
        $query=$this->db->get('usuarios');
        if($query->num_rows==1){
            return false;
        }else{
            return true;
        }
    }
    function agregar_nit_cliente($nit,$cliente){
            if($nit && $cliente){
                    $data=array(
                            'nit'=>$nit,
                            'usuario'=>$cliente
                    );
                    $insertar=$this->db->insert('usuarios_nits',$data);
                    return $insertar;
            }else{
                    return false;
            }
    }
    function eliminar_nit_cliente($nit,$cliente){
            if($nit && $cliente){
                    $this->db->where('nit',$nit);
                    $this->db->where('usuario',$cliente);
                    $delete=$this->db->delete('usuarios_nits');
                    return $delete;
            }else{
                    return false;
            }
    }
    function listar_vendedores($usuario){
        $this->db->where('usuario',$usuario);
        $this->db->join('vendedor','vendedor.id_vendedor=vendedores_usuario.vendedor','left');
        return $this->db->get('vendedores_usuario');
    }
    function listar_dropdown_nombres($usuario,$inicio=null,$fin=null){
            $this->db->select('vendedor');
            $this->db->where('usuario',$usuario);
            $query=$this->db->get('vendedores_usuario')->result();
            $vendedores=array();
            if($query){
                foreach($query as $vendedor){
                    $vendedores[]=$vendedor->vendedor;
                }
            }
            
            
            $this->db->select('*');
            $this->db->order_by('id_vendedor + 0','asc');
            if($vendedores){
                $this->db->where_not_in('id_vendedor',$vendedores);
            }
            $query=$this->db->get('vendedor')->result();
            if($inicio){
                $vendedores=$inicio;
            }else{
                $vendedores=array();
            }
            foreach($query as $vendedor):
                    $vendedores[$vendedor->id_vendedor]=$vendedor->id_vendedor.' - '.$vendedor->nombre.' '.$vendedor->apellido;
            endforeach;
            return $vendedores;
    }
    function insertar_vendedores($usuario,$datos){
        $this->db->where('usuario',$usuario);
        $this->db->delete('vendedores_usuario');
        
        return $this->db->insert_batch('vendedores_usuario',$datos);
    }
}
?>