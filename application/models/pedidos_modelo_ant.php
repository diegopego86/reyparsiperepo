<?php
class Pedidos_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_pedidos($limit,$offset,$numeros_vendedor=null,$usuario=null,$orden=null,$buscar=null){
		$this->db->select('pedido_maestro.*,cliente.nombre as nombre_cliente, cliente.contacto');
		if($orden){
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}else{
				$this->db->order_by('id_maestro','desc');
				$this->db->order_by('fecha_creacion','desc');
		}
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or pedido_maestro.id_maestro like '%$buscar%') AND (pedido_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or pedido_maestro.id_usuario='".$usuario."')");
			}else{
				$this->db->where("(pedido_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or pedido_maestro.id_usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or pedido_maestro.id_maestro like '%$buscar%')");
			}
		}
		$this->db->join('cliente','cliente.id_cliente=pedido_maestro.id_cliente','left');
		if($limit){
			$query=$this->db->get('pedido_maestro',$limit,$offset);
		}else{
			$query=$this->db->get('pedido_maestro');
		}
                return $query;
	}
	function total_listar_pedidos($numeros_vendedor=null,$usuario=null,$buscar=null){
		$this->db->select('count(*) as total');
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or pedido_maestro.id_maestro like '%$buscar%') AND (pedido_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or pedido_maestro.id_usuario='".$usuario."')");
			}else{
				$this->db->where("(pedido_maestro.id_vendedor in ('".join("','",$numeros_vendedor)."') or pedido_maestro.id_usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or pedido_maestro.id_maestro like '%$buscar%')");
			}
		}
                $this->db->join('cliente','cliente.id_cliente=pedido_maestro.id_cliente','left');
		$query=$this->db->get('pedido_maestro')->row();
		return $query->total;
	}        
	function total_pedidos_vendedores_mes($vendedores,$usuario,$mes){
		$this->db->select('pedido_maestro.id_vendedor, vendedor.nombre,sum(subtotal) as subtotal',false);
		if($vendedores && $this->session->userdata('rol')!=1){
			$this->db->where("(pedido_maestro.id_vendedor in ('".join("','",$vendedores)."') or id_usuario='".$usuario."')");
		}
                $this->db->where('anulado is null',null,false);
		$this->db->like('fecha',$mes,'after');
                $this->db->join('vendedor','vendedor.id_vendedor=pedido_maestro.id_vendedor','left');
		$this->db->order_by('vendedor.nombre','asc');
		$this->db->group_by('pedido_maestro.id_vendedor');		
		$query=$this->db->get('pedido_maestro');
                //echo $this->db->last_query();
		return $query;
	}
	function listar_meses(){
		$this->db->select("distinct(date_format(fecha,'%Y-%m')) as fecha,date_format(fecha,'%m') as mes,date_format(fecha,'%Y') as anio",false);
		$this->db->order_by('fecha','desc');
		$this->db->group_by('fecha');
		$query=$this->db->get('pedido_maestro');
		return $query;
	}
	function listar_pedidos_usuario($limit,$offset,$usuario=null,$orden=null){
		$this->db->select('pedido_maestro.*,cliente.nombre as nombre_cliente,cliente.contacto');
		if($orden){
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($usuario){
			$this->db->where("(pedido_maestro.id_vendedor in ('".join("','",$this->session->userdata('numeros_vendedor'))."') or pedido_maestro.id_usuario='".$usuario."')");
		}
		$this->db->join('cliente','cliente.id_cliente=pedido_maestro.id_cliente','left');
		if($limit){
			$query=$this->db->get('pedido_maestro',$limit,$offset);
		}else{
			$query=$this->db->get('pedido_maestro');
		}
		return $query;
	}
	function productos_pedido($pedido){
		$this->db->select('pedido_detalle.id_maestro,pedido_detalle.id_producto,pedido_detalle.cantidad,pedido_detalle.descuento,pedido_detalle.total, producto.nombre,producto.referencia,producto.unidad,producto.stock,producto.precio1,producto.precio2,producto.precio3,producto.precio4,producto.iva');
		$this->db->where('id_maestro',$pedido);
		$this->db->order_by('id_detalle');
		$this->db->join('producto','producto.id_producto=pedido_detalle.id_producto','left');
		$query=$this->db->get('pedido_detalle');
		return $query;
	}
	function obtener_numeros($cantidad){
                $abc=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$query=$this->db->get('pedido_numero')->row();
		$numero_inicial=(int)$query->numero;
                $prefijo=$query->prefijo;
                $prefijo_tamano=strlen($prefijo);
                $ultimo_numero=substr('999999',$prefijo_tamano,6)*1;
                
                $numero_final=$numero_inicial+(int)$cantidad;
		if($numero_final>$ultimo_numero){
			//$numero_final=($ultimo_numero+1)+($cantidad-($ultimo_numero-$numero_inicial+1));
                        $numero_final=$numero_final-$ultimo_numero;
                        if($prefijo==""){
                            $nuevo_prefijo="A";
                        }else{
                            $nuevo_prefijo=$this->prefijo_cambio_letra(str_split($prefijo), $prefijo_tamano-1, $abc);
                        }
		}else{
                    $nuevo_prefijo=$prefijo;
                }
		$data=array(
                    'id'=>1,
                    'numero'=>$numero_final,
                    'prefijo'=>$nuevo_prefijo
                );
                
		$this->db->update('pedido_numero',$data);
		$numeros=array();
		for($n=0;$n<$cantidad;$n++){
			$numero=$numero_inicial+$n;
			if($numero>$ultimo_numero){
                            $numero=$numero-$ultimo_numero;
                            $prefijo=$nuevo_prefijo;
			}
			$numeros[]= $prefijo.str_pad($numero, (6-strlen($prefijo)), "0", STR_PAD_LEFT);
		}
		return $numeros;
	}
        function prefijo_cambio_letra($prefijo,$letra,$abc){
            if($prefijo[0]<>""){
                if($letra<0){
                    for($m=$letra+1;$m<count($prefijo);$m++){
                        $prefijo[$m]='A';
                    }
                    $prefijo[count($prefijo)]='A';
                    return join($prefijo);
                }
                if($prefijo[$letra]=='Z'){
                    return $this->prefijo_cambio_letra($prefijo, $letra-1, $abc);                
                }else{
                    $nuevo=$abc[array_search($prefijo[$letra],$abc)+1];
                    $prefijo[$letra]=$nuevo;
                    return join($prefijo);
                }
            }else{
                return "A";
            }
        }
        function recursivo($n,$total,$textos){
                if($n==$total-1){
                    return 'IFNULL(('.$textos[$n].'),0)';
                }else{
                    return 'IFNULL(('.$textos[$n].'),('.$this->recursivo($n+1,$total,$textos).'))';
                }
        }
        function guardar_pedido($borrador,$encabezado,$productos){
            $this->db->where('id',1);
            $configuracion_pedidos=$this->db->get('pedidos_configuracion')->row();
            
            //Se tienen las condiciones de la base de datos que seran reemplazadas por los datos de las condiciones de los pedidos
            $condiciones=$this->db->get('condiciones')->result();
            $cond=array();
            foreach($condiciones as $condicion):
                $cond[$condicion->id]=$condicion->consulta;
            endforeach;
            
            //Se obtienen las condiciones de los pedidos
            $this->db->order_by('grupo');
            $condiciones=$this->db->get('pedidos_condiciones');
            
            //Se obtienen los codigos de los productos en el pedido
            $codigos=array();
            foreach($productos as $producto):
                $codigos[]=$producto['codigo'];
            endforeach;
            
            $grupo='';
            $consultas=array();
            $uniones=array('1'=>'AND','2'=>'OR');
            
            //Se arman las consultas de las condiciones de los grupos de pedidos
            if($condiciones->num_rows > 0){
                $condiciones=$condiciones->result();
                foreach($condiciones as $condicion):
                    if($condicion->grupo!=$grupo){
                        $consultas[$condicion->grupo]['grupo']=$condicion->grupo;
                        $consultas[$condicion->grupo]['condicion']=str_replace('[campo]','prod2.'.$condicion->campo,$cond[$condicion->condicion]);
                        $consultas[$condicion->grupo]['condicion']=str_replace('[valor]',$condicion->valor,$consultas[$condicion->grupo]['condicion']);
                    }else{
                        $nueva_consulta='';
                        $nueva_consulta=str_replace('[campo]','prod2.'.$condicion->campo,$cond[$condicion->condicion]);
                        $consultas[$condicion->grupo]['condicion']=$consultas[$condicion->grupo]['condicion'].' '.$uniones[$condicion->union].' '.str_replace('[valor]',$condicion->valor,$nueva_consulta);                    
                    }
                    $grupo=$condicion->grupo;
                endforeach;
                $textos=array();
                foreach($consultas as $consulta):
                    $this->db->select($consulta['grupo'],false);
                    $this->db->from('producto as prod2');
                    $this->db->where('('.$consulta['condicion'].')',null,false);
                    $this->db->where('prod2.id_producto = producto.id_producto',null,false);
                    $textos[] = $this->db->_compile_select();
                    $this->db->_reset_select();
                endforeach;
                $total=count($textos);
            
                //Se arma la consulta con la cual se determina el grupo al que pertenece cada grupo de acuerdo a las condiciones dadas.
                $consulta2= $this->recursivo(0,$total,$textos);
            }else{
                $consulta2=null;
            }
            
            //Se realiza la consulta con los productos en el pedido para determinar a que grupo pertenecesn con el fin de dividir los pedidos
            if($consulta2){
                $this->db->select("producto.*, ($consulta2) as grupo",false);                
            }else{
                $this->db->select("producto.*, (0) as grupo",false);                
            }
            $this->db->from('producto',false);
            $this->db->where("id_producto in ( '".join("','",$codigos)."' )",null,false);
            $this->db->order_by('grupo','asc');
            $productos_sql= $this->db->_compile_select();
            $this->db->_reset_select();
            
            /*$this->db->select('distinct(grupo) as grupo',false);
            $this->db->from('('.$productos_sql.') as grupos',false);
            $grupos_sql=$this->db->get()->result();
            print_r($grupos_sql);
            */
            
            //Se obtienen los productos con el grupo ordenado por grupo.
            $this->db->select('*',false);
            $this->db->from('('.$productos_sql.') as grupos',false);
            $this->db->order_by('grupo','asc');
            $productos_sql=$this->db->get()->result();
            
            //Se organiza el arreglo de grupos productos para poder obtener facilmente el grupo al que pertenece cada producto, en este caso solo se organizan los codigos de la consulta sql y no se repiten codigos 
            // ejem: $grupo_producto[ELALCE12]=1; el producto con codigo ELALCE12 pertenece al grupo 1
            $grupos_producto=array();
            foreach($productos_sql as $prod):
                $grupos_producto[$prod->id_producto]=$prod->grupo;
            endforeach;
            
            //Se agrupan los productos con los datos del pedido por los grupos obtenidos anteriormente, en este caso si el pedido permite repetir codigos, estos se repetiran en este arreglo;
            //Ademas se obtienen las cantidades de productos por grupo con el fin de calcular la cantidad de pedidos que se van a generar
            $grupos=array();
            foreach($productos as $producto):
                $grupo=$grupos_producto[$producto['codigo']];
                $grupos[$grupo]['grupo']=$grupo;
                if(!isset($grupos[$grupo]['cantidad'])){
                    $grupos[$grupo]['cantidad']=1;
                }else{
                    $grupos[$grupo]['cantidad']=$grupos[$grupo]['cantidad']+1;
                }
                //Se separan los productos por grupo
                $productos_grupo[$grupo][]=$producto;
            endforeach;
            
            //Ojo en el archivo DB_active_rec.php se agrego la opcion $protect=NULL con el fin de evitar que se agregue el caracter ` a las sentencias sql.
            
            //Variable que dice cuantos productos por pedido se permiten;
            $total_por_pedido=$configuracion_pedidos->maximo_items;//Si este campo = 0 o es nulo, se generará un solo pedido
            //Obtener cantidad de pedidos a realizar
            $cantidad_pedidos_total=0;
            
            foreach($grupos as $grupo):
                if(!$total_por_pedido || $total_por_pedido==0 || $total_por_pedido==null){
                    $cantidad_pedidos_grupo=1;
                }else{
                    $cantidad_pedidos_grupo=ceil($grupo['cantidad']/$total_por_pedido);
                }
                $cantidad_pedidos_total=$cantidad_pedidos_total+$cantidad_pedidos_grupo;
                $grupos[$grupo['grupo']]['pedidos']=$cantidad_pedidos_grupo;          
            endforeach;
            
            
            
            //Se comienza con las consultas para guardar los pedidos
            $this->db->trans_start();
                
                //Se obtienen los numeros de los pedidos que se van a guardar y se actualiza el contador en la base de datos
                $numeros=$this->obtener_numeros($cantidad_pedidos_total);
                
                //El valor pedido_grupo identifica los pedidos de un mismo grupo.
                $pedido_grupo=null;
                if($cantidad_pedidos_total>1){
                        $pedido_grupo=$numeros[0];
                }
                
                //Permite determinar si el pedido se debe ordenar por el campo codigo
                $se_ordenar_por_codigo=true;
                $n=0;//Contador de pedidos;
                $p=0;//Contador de items pedido;
                $datos_productos=array();
                foreach($grupos as $grupo):
                    if($se_ordenar_por_codigo){
                        usort($productos_grupo[$grupo['grupo']], array($this,'ordenar_por_codigo'));
                    }
                    $listado=$productos_grupo[$grupo['grupo']];
                    $subtotal_sin_dcto=0;
                    $descuento_total=0;
                    $subtotal=0;
                    $iva_total=0;
                    $total=0;
                    
                    foreach($listado as $producto):

                        $datos_productos[]=array(
                                'id_maestro'=>$numeros[$n],
                                'id_detalle'=>$p+1,
                                'id_producto'=>$producto['codigo'],					
                                'cantidad'=>$producto['cantidad'],					
                                'precio'=>$producto['precio'],					
                                'descuento'=>$producto['descuento'],					
                                'iva'=>$producto['iva'],					
                                'total_sin_dcto'=>$producto['total_sin_dcto'],					
                                'total'=>$producto['total'],					
                                'cantidad_despachada'=>0,					
                                'estado'=>0,					
                                'id_usuario'=>$this->session->userdata('id_usuario'),					
                                'fecha_creacion'=>date('Y-m-d H:i:s',time()),					
                                'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),					
                                'afavor'=>0,					
                                'vrreal'=>0
                        );
                        $subtotal_sin_dcto=$subtotal_sin_dcto+$producto['total_sin_dcto']*1;
                        $descuento_total=$descuento_total+(($producto['precio']*$producto['descuento']/100)*$producto['cantidad']);
                        $neto_actual=($producto['total_sin_dcto']*1)-(($producto['precio']*$producto['descuento']/100)*$producto['cantidad']);
                        $iva_total=$iva_total+(($neto_actual*$producto['iva'])/100);
                        $p=$p+1;
                        if($total_por_pedido!=0 && $p==$total_por_pedido){//Se totaliza si existen mas items que el total por pedido
                            $subtotal=$subtotal_sin_dcto-$descuento_total;
                            $total=round($subtotal+$iva_total,0);
                            $datos_encabezado=array(
                                    'id_maestro'=>$numeros[$n],
                                    'fecha'=>date('Y-m-d',time()),
                                    'hora'=>date('H:i:s',time()),
                                    'id_concepto'=>'13',
                                    'id_cliente'=>$encabezado['nit'],
                                    'id_vendedor'=>$encabezado['asesor'],
                                    'retencion'=>$encabezado['retencion'],
                                    'plazo'=>$encabezado['plazo'],
                                    'direccion_envio'=>$encabezado['direccion_envio'],
                                    'descuento_pago'=>0,
                                    'descuento_dias'=>0,
                                    'subtotal_sin_dcto'=>$subtotal_sin_dcto,
                                    'descuento_total'=>$descuento_total,
                                    'subtotal'=>$subtotal,
                                    'iva_total'=>$iva_total,
                                    'total'=>$total,
                                    'estado'=>'terminado',
                                    'nota_1'=>$encabezado['observaciones'],
                                    'nota_2'=>($n+1).' de '.$cantidad_pedidos_total,
                                    'id_usuario'=>$this->session->userdata('id_usuario'),
                                    'fecha_creacion'=>date('Y-m-d H:i:s',time()),
                                    'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),
                                    'lista'=>$encabezado['lista'],
                                    'grupo'=>$pedido_grupo					
                            );
                            $encabezado_pedido_insertado=$this->db->insert('pedido_maestro',$datos_encabezado);
                            if($encabezado_pedido_insertado){
                                $this->db->insert_batch('pedido_detalle',$datos_productos);
                            }
                            

                            //Se inicializan las variables
                            $p=0;//Inicializo contador de items por pedido
                            $n=$n+1;//Contador de pedidos
                            $subtotal_sin_dcto=0;
                            $descuento_total=0;
                            $subtotal=0;
                            $iva_total=0;
                            $total=0;
                            $datos_productos=array();                               
                        }                    
                    endforeach;
                    if($p>0){
                        $subtotal=$subtotal_sin_dcto-$descuento_total;
                        $total=round($subtotal+$iva_total,0);
                        $datos_encabezado=array(
                                'id_maestro'=>$numeros[$n],
                                'fecha'=>date('Y-m-d',time()),
                                'hora'=>date('H:i:s',time()),
                                'id_concepto'=>'13',
                                'id_cliente'=>$encabezado['nit'],
                                'id_vendedor'=>$encabezado['asesor'],
                                'retencion'=>$encabezado['retencion'],
                                'plazo'=>$encabezado['plazo'],
                                'direccion_envio'=>$encabezado['direccion_envio'],
                                'descuento_pago'=>0,
                                'descuento_dias'=>0,
                                'subtotal_sin_dcto'=>$subtotal_sin_dcto,
                                'descuento_total'=>$descuento_total,
                                'subtotal'=>$subtotal,
                                'iva_total'=>$iva_total,
                                'total'=>$total,
                                'estado'=>'terminado',
                                'nota_1'=>$encabezado['observaciones'],
                                'nota_2'=>($n+1).' de '.$cantidad_pedidos_total,
                                'id_usuario'=>$this->session->userdata('id_usuario'),
                                'fecha_creacion'=>date('Y-m-d H:i:s',time()),
                                'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),
                                'lista'=>$encabezado['lista'],
                                'grupo'=>$pedido_grupo					
                        );

                        /*echo "Pedido detalle:<br/> ";
                        print_r($datos_productos);

                        echo "Pedido encabezado:<br/> ";
                        print_r($datos_encabezado);*/
                        //if($datos_productos){
                            $encabezado_pedido_insertado=$this->db->insert('pedido_maestro',$datos_encabezado);
                            if($encabezado_pedido_insertado){
                                $this->db->insert_batch('pedido_detalle',$datos_productos);
                            }
                            
                            $n=$n+1;//Contador de pedidos
                    }
                    //}
                    //Por cada grupo que termine se inicializan las varialbes;
                    $p=0;//Inicializo contador de items por pedido
                    $subtotal_sin_dcto=0;
                    $descuento_total=0;
                    $subtotal=0;
                    $iva_total=0;
                    $total=0;
                    $datos_productos=array(); 
                endforeach;


                //Eliminar borrador
                $this->db->where('id',$borrador);
		$this->db->delete('borradores');
                
                
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $numeros;
            }
            
            //echo $this->db->last_query();
            
            
        }
        static function ordenar_por_codigo($x,$y){
             if ( $x['codigo'] == $y['codigo'] )
              return 0;
             else if ( $x['codigo'] < $y['codigo'] )
              return -1;
             else
              return 1;
        }
        function anterior($borrador,$encabezado,$productos){
            
            
            $productos_comision=array();
            $productos_normales=array();
            foreach($productos as $prod):
                if(substr($prod['referencia'],strlen($prod['referencia'])*1-1,1)=='C'){
                        array_push($productos_comision,$prod);
                }else{
                        array_push($productos_normales,$prod);
                }
            endforeach;
            function comparar_codigos($x,$y){
                     if ( $x['referencia'] == $y['referencia'] )
                            return 0;
                     else 
                            return -1;
            }
            $productos=$productos_normales;
            $total_productos=count($productos);
            $total_productos_comision=count($productos_comision);
            //$total_por_pedido=$this->configuracion_modelo->modelo();
            $cantidad_pedidos_normal=ceil($total_productos/$total_por_pedido);
            $cantidad_pedidos_comision=ceil($total_productos_comision/$total_por_pedido);
            $cantidad_pedidos=$cantidad_pedidos_normal+$cantidad_pedidos_comision;
            function ordenar_por_codigoa($x,$y){
                     if ( $x['codigo'] == $y['codigo'] )
                      return 0;
                     else if ( $x['codigo'] < $y['codigo'] )
                      return -1;
                     else
                      return 1;
            }
            usort($productos, array($this,'ordenar_por_codigo'));
            usort($productos_comision,  array($this,'ordenar_por_codigo'));
            $numeros=$this->obtener_numeros($cantidad_pedidos);
            $grupo=null;
            if($cantidad_pedidos>1){
                    $grupo=$numeros[0];
            }
            $n=1;
            $p=0;
            $k=0;
            $cliente='';
            $vendedor='';
            foreach($numeros as $numero){
                    $subtotal_sin_dcto=0;
                    $descuento_total=0;
                    $subtotal=0;
                    $iva_total=0;
                    $total=0;
                    $item=$p+($k*$total_por_pedido);
                    if($n>$cantidad_pedidos_normal){
                            while($p < $total_por_pedido & $item < $total_productos_comision):
                                    $datos_producto=array(
                                            'id_maestro'=>$numero,
                                            'id_detalle'=>$p+1,
                                            'id_producto'=>$productos_comision[$item]['codigo'],					
                                            'cantidad'=>$productos_comision[$item]['cantidad'],					
                                            'precio'=>$productos_comision[$item]['precio'],					
                                            'descuento'=>$productos_comision[$item]['descuento'],					
                                            'iva'=>$productos_comision[$item]['iva'],					
                                            'total_sin_dcto'=>$productos_comision[$item]['total_sin_dcto'],					
                                            'total'=>$productos_comision[$item]['total'],					
                                            'cantidad_despachada'=>0,					
                                            'estado'=>0,					
                                            'id_usuario'=>$this->session->userdata('id_usuario'),					
                                            'fecha_creacion'=>date('Y-m-d H:i:s',time()),					
                                            'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),					
                                            'afavor'=>0,					
                                            'vrreal'=>0
                                    );
                                    //Totales de pedido
                                    $subtotal_sin_dcto=$subtotal_sin_dcto+$productos_comision[$item]['total_sin_dcto']*1;
                                    $descuento_total=$descuento_total+(($productos_comision[$item]['precio']*$productos_comision[$item]['descuento']/100)*$productos_comision[$item]['cantidad']);
                                    $neto_actual=($productos_comision[$item]['total_sin_dcto']*1)-(($productos_comision[$item]['precio']*$productos_comision[$item]['descuento']/100)*$productos_comision[$item]['cantidad']);
                                    $iva_total=$iva_total+(($neto_actual*$productos_comision[$item]['iva'])/100);
                                    if(!$this->pedidos_modelo->guardar_detalle($datos_producto)){
                                            echo "error";
                                            return false;
                                    }
                                    $p=$p+1;
                                    $item=$p+($k*$total_por_pedido);
                            endwhile;
                    }else{
                            while($p<$total_por_pedido & $item < $total_productos):
                                    $datos_producto=array(
                                            'id_maestro'=>$numero,
                                            'id_detalle'=>$p+1,
                                            'id_producto'=>$productos[$item]['codigo'],					
                                            'cantidad'=>$productos[$item]['cantidad'],					
                                            'precio'=>$productos[$item]['precio'],					
                                            'descuento'=>$productos[$item]['descuento'],					
                                            'iva'=>$productos[$item]['iva'],					
                                            'total_sin_dcto'=>$productos[$item]['total_sin_dcto'],					
                                            'total'=>$productos[$item]['total'],					
                                            'cantidad_despachada'=>0,					
                                            'estado'=>0,					
                                            'id_usuario'=>$this->session->userdata('id_usuario'),					
                                            'fecha_creacion'=>date('Y-m-d H:i:s',time()),					
                                            'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),					
                                            'afavor'=>0,					
                                            'vrreal'=>0
                                    );
                                    //Totales de pedido
                                    $subtotal_sin_dcto=$subtotal_sin_dcto+$productos[$item]['total_sin_dcto']*1;
                                    $descuento_total=$descuento_total+(($productos[$item]['precio']*$productos[$item]['descuento']/100)*$productos[$item]['cantidad']);
                                    $neto_actual=($productos[$item]['total_sin_dcto']*1)-(($productos[$item]['precio']*$productos[$item]['descuento']/100)*$productos[$item]['cantidad']);
                                    $iva_total=$iva_total+(($neto_actual*$productos[$item]['iva'])/100);
                                    if(!$this->pedidos_modelo->guardar_detalle($datos_producto)){
                                            echo "error";
                                            return false;
                                    }
                                    $p=$p+1;
                                    $item=$p+($k*$total_por_pedido);
                            endwhile;
                    }
                    //El resto de totales
                    $subtotal=$subtotal_sin_dcto-$descuento_total;
                    $total=round($subtotal+$iva_total,0);
                    $p=0;//Inicializo contador de items por pedido
                    $k++;//Contador de pedidos
                    $cliente=$encabezado['nit'];
                    $vendedor=$encabezado['asesor'];
                    $datos_encabezado=array(
                            'id_maestro'=>$numero,
                            'fecha'=>date('Y-m-d',time()),
                            'hora'=>date('H:i:s',time()),
                            'id_concepto'=>'13',
                            'id_cliente'=>$encabezado['nit'],
                            'id_vendedor'=>$encabezado['asesor'],
                            'plazo'=>$encabezado['plazo'],
                            'direccion_envio'=>$encabezado['direccion_envio'],
                            'descuento_pago'=>0,
                            'descuento_dias'=>0,
                            'subtotal_sin_dcto'=>$subtotal_sin_dcto,
                            'descuento_total'=>$descuento_total,
                            'subtotal'=>$subtotal,
                            'iva_total'=>$iva_total,
                            'total'=>$total,
                            'estado'=>'terminado',
                            'nota_1'=>$encabezado['observaciones'],
                            'nota_2'=>($n+1).' de '.$cantidad_pedidos,
                            'id_usuario'=>$this->session->userdata('id_usuario'),
                            'fecha_creacion'=>date('Y-m-d H:i:s',time()),
                            'fecha_actualizacion'=>date('Y-m-d H:i:s',time()),
                            'lista'=>$encabezado['lista'],
                            'grupo'=>$grupo					
                    );
                    //$this->pedidos_modelo->guardar_detalle($numero,$|productos);
                    if(!$this->pedidos_modelo->guardar_maestro($datos_encabezado)){
                            echo "error";
                            return false;
                    }else{
                            $mensaje=$this->mostrar_pedido($datos_encabezado['id_maestro']);
                            $mostrar=$mostrar.$mensaje.'<br/>';
                    }
                    if($n==$cantidad_pedidos_normal){
                            $p=0;
                            $k=0;//Inicializo contador de pedidos para los de comision
                            $item=$p+($k*$total_por_pedido);
                    }
                    $n++;
            }
            $this->db->trans_start();
                //Insertar encabezado
                $this->db->insert('pedido_maestro',$datos);
                
                
                //Eliminar borrador
                $this->db->where('id',$borrador);
		$this->db->delete('borradores');
                
                
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE)
            {
                // generate an error... or use the log_message() function to log your error
            }
        }
	function guardar_maestro($datos){
		$insertar=$this->db->insert('pedido_maestro',$datos);
		return $insertar;
	}
	function guardar_detalle($datos){
		$insertar=$this->db->insert('pedido_detalle',$datos);
		return $insertar;
	}
	function leer_encabezado($numero){
		$this->db->select('pedido_maestro.*,cliente.nombre,cliente.contacto,cliente.direccion,cliente.telefono1,cliente.ciudad,vendedor.nombre as nombre_vendedor');
		$this->db->where('id_maestro',$numero);
		$this->db->join('cliente','cliente.id_cliente=pedido_maestro.id_cliente','left');
		$this->db->join('vendedor','vendedor.id_vendedor=pedido_maestro.id_vendedor','left');
		$query=$this->db->get('pedido_maestro');
		return $query;
	}
	function leer_productos($numero){
		$this->db->select('pedido_detalle.*, producto.nombre,producto.referencia');
		$this->db->where('id_maestro',$numero);
		$this->db->join('producto','producto.id_producto=pedido_detalle.id_producto','left');
		$query=$this->db->get('pedido_detalle');
		return $query;
	}
	function guardar_borrador($encabezado,$productos,$fecha,$numero=null){
            $this->db->trans_start();
		$data=array(
			'encabezado'=>$encabezado,
			'productos'=>$productos,
			'usuario'=>$this->session->userdata('id_usuario'),
			'fecha_actualizacion'=>$fecha
		);
		if($numero){
			$this->db->where('id',$numero);
			$accion=$this->db->update('borradores',$data);
			if($accion){
				$accion=$numero;
			}else{
				$accion=false;
			}
		}else{
			$data['fecha']=$fecha;
			$accion=$this->db->insert('borradores',$data);
			if($accion){
				$accion=$this->db->insert_id();
			}else{
				$accion=false;
			}
		}
                 $this->db->trans_complete();
            
                if ($this->db->trans_status() === FALSE)
                {
                    return false;
                }else{
                    return $accion;
                }
	}
	function eliminar_borrador($numero){
		$this->db->where('id',$numero);
		$eliminar=$this->db->delete('borradores');
		return $eliminar;
	}
	function listar_borradores($usuario=null){
		if($usuario){
			$this->db->where('usuario',$usuario);
		}
		$this->db->order_by('fecha','desc');
		$query=$this->db->get('borradores');
		return $query;
	}
	function leer_borrador($id){
		$this->db->where('id',$id);
		$this->db->where('usuario',$this->session->userdata('id_usuario'));
		$query=$this->db->get('borradores');
		return $query;
	}
	function datos_producto_borrador($producto){
		$this->db->where('id_producto',$producto);
		$query=$this->db->get('producto');
		return $query;		
	}
        function datos_pedidos($numeros, $rangos){
            $this->db->select('pedido_maestro.*,cliente.*,vendedor.nombre as nombre_vendedor, pedido_detalle.id_producto, pedido_detalle.cantidad, pedido_detalle.precio, pedido_detalle.descuento, pedido_detalle.iva, pedido_detalle.total_sin_dcto, pedido_detalle.total as total_item,producto.nombre as nombre_producto');
            $sql='';
            if(count($numeros)>0){
                $sql='pedido_maestro.id_maestro in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                        $sql=$sql.' or (pedido_maestro.id_maestro >= '."'".$rango['desde']."'".' and pedido_maestro.id_maestro <= '."'".$rango['hasta']."'".')';
                    }else{
                        $sql='(pedido_maestro.id_maestro >= '."'".$rango['desde']."'".' and pedido_maestro.id_maestro <= '."'".$rango['hasta']."'".')';
                    }
                endforeach;
            }
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1){
                $this->db->where('pedido_maestro.id_usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('cliente','cliente.id_cliente=pedido_maestro.id_cliente','left');
            $this->db->join('vendedor','vendedor.id_vendedor=pedido_maestro.id_vendedor','left');
            $this->db->join('pedido_detalle','pedido_detalle.id_maestro=pedido_maestro.id_maestro','left');
            $this->db->join('producto','producto.id_producto=pedido_detalle.id_producto','left');
            $query=$this->db->get('pedido_maestro');
            //echo $this->db->last_query();
            return $query;
        }
        function cantidad_pedidos_vendedor($desde,$hasta){
            if(!$desde){
                $desde=date('d-m-Y');
            }
            
            $this->db->select('vendedor.id_vendedor,vendedor.nombre, count(pedido_maestro.id_maestro) as cantidad, sum(pedido_maestro.subtotal) as valor',false);
            $this->db->where('pedido_maestro.anulado is null',null,false);
            $this->db->where("fecha>= STR_TO_DATE('$desde','%d-%m-%Y')",null,false);
            if($hasta){
               $this->db->where("fecha<= STR_TO_DATE('$hasta','%d-%m-%Y')",null,false);
            }
            
            $this->db->join('vendedor','vendedor.id_vendedor=pedido_maestro.id_vendedor','left');
            $this->db->order_by('vendedor.nombre','asc');
            $this->db->group_by('vendedor.id_vendedor');
            $query=$this->db->get('pedido_maestro');
            return $query;
        }
        function cantidad_pedidos_vendedor_fechas($desde,$hasta,$vendedores=null){
            if(!$desde){
                $desde=date('d-m-Y');
            }
            
            $this->db->select("vendedor.id_vendedor,vendedor.nombre, count(pma.id_maestro) as cantidad_hojas, (SELECT count(pm.id_maestro) FROM pedido_maestro as pm WHERE pm.anulado is null AND pm.fecha=pma.fecha AND pm.nota_2 LIKE '1 de%' and pm.id_vendedor=pma.id_vendedor GROUP BY pm.id_vendedor ORDER BY pm.id_vendedor asc) as cantidad, sum(pma.subtotal) as valor,pma.fecha",false);
            $this->db->where('pma.anulado is null',null,false);
            $this->db->where("fecha>= STR_TO_DATE('$desde','%d-%m-%Y')",null,false);
            if($hasta){
               $this->db->where("fecha< STR_TO_DATE('$hasta','%d-%m-%Y')",null,false);
            }
            if($vendedores){
                $this->db->where_in('vendedor.id_vendedor',$vendedores);
            }
            $this->db->join('vendedor','vendedor.id_vendedor=pma.id_vendedor','left');
            $this->db->order_by('vendedor.nombre','asc');
            $this->db->group_by(array('vendedor.id_vendedor','pma.fecha'));
            $this->db->from('pedido_maestro as pma',false);
            $query=$this->db->get();
            return $query;
        }
        function listar_observaciones_pedido($visible=null){
            if($visible){
                $this->db->where('visible',1);
            }
            $this->db->order_by('orden','asc');
            $query=$this->db->get('pedido_observaciones');
            return $query;
        }
        function leer_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->get('pedido_observaciones');
            return $query;
        }
        function crear_observacion($datos){
            $query=$this->db->insert('pedido_observaciones',$datos);
            return $query;
        }
        function editar_observacion($observacion,$datos){
            $this->db->where('id',$observacion);
            $query=$this->db->update('pedido_observaciones',$datos);
            return $query;
        }
        function eliminar_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->delete('pedido_observaciones');
            return $query;
        }
        function anular_pedido($numero){
            $this->db->set('cantidad_anterior','cantidad',false);
            $this->db->set('precio_anterior','precio',false);
            $this->db->set('cantidad',0,false);
            $this->db->set('precio',0,false);
            $this->db->set('total',0,false);
            $this->db->where('id_maestro',$numero);
            $this->db->update('pedido_detalle');
            
            $this->db->set('total',0,false);
            $this->db->set('subtotal',0,false);
            $this->db->set('subtotal_sin_dcto',0,false);
            $this->db->set('descuento_total',0,false);
            $this->db->set('iva_total',0,false);
            
            $this->db->set('anulado',1);
            $this->db->where('id_maestro',$numero);
            return $this->db->update('pedido_maestro');            
        }
        function activar_pedido($numero){
            $this->db->trans_start();
            $this->db->set('cantidad','cantidad_anterior',false);
            $this->db->set('precio','precio_anterior',false);
            $this->db->set('cantidad_anterior','NULL',false);
            $this->db->set('precio_anterior','NULL',false);
            $this->db->set('total','ROUND((cantidad*precio)-(cantidad*(precio*descuento/100)))',false);            
            $this->db->where('id_maestro',$numero);
            $this->db->update('pedido_detalle');
            
            $this->db->select('id_maestro,ROUND(SUM(cantidad*precio)) as subtotal_sin_dcto, ROUND(SUM((cantidad*precio)-(cantidad*(precio * descuento/100)))) as subtotal, ROUND(SUM(cantidad*(precio * descuento/100))) as descuento_total, ROUND(SUM(((cantidad*precio)-(cantidad*(precio * descuento/100)))*(iva/100))) as iva_total');
            $this->db->group_by('id_maestro');
            $this->db->where('id_maestro',$numero);
            $totales=$this->db->get('pedido_detalle')->row();
            
            $this->db->set('total',$totales->subtotal+$totales->iva_total);
            $this->db->set('subtotal',$totales->subtotal);
            $this->db->set('subtotal_sin_dcto',$totales->subtotal_sin_dcto);
            $this->db->set('descuento_total',$totales->descuento_total);
            $this->db->set('iva_total',$totales->iva_total);            
            $this->db->set('anulado',NULL);
            $this->db->where('id_maestro',$numero);
            $this->db->update('pedido_maestro');
            $this->db->trans_complete();
            
            $this->db->where('id_maestro',$numero);
            $query=$this->db->get('pedido_maestro');
            
            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $query->row();
            }
        }
}
?>