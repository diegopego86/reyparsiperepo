<?php
class Bancos_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_dropdown(){
		$this->db->order_by('nombre','asc');
		$resultados=$this->db->get('bancos')->result();
		$lista=array(''=>'');
		foreach($resultados as $result):
			$lista[$result->id]=$result->codigo.' - '.$result->nombre;
		endforeach;
		return $lista;
	}
        function listar_cuentas_dropdown(){
		$this->db->select('cuentas.*, bancos.codigo,bancos.nombre');
                $this->db->order_by('bancos.nombre','asc');
                $this->db->order_by('cuentas.cuenta','asc');
                $this->db->join('bancos','bancos.id=cuentas.banco','left');
		$resultados=$this->db->get('cuentas')->result();
                $tipos=array('1'=>'Ahorros','2'=>'Corriente');
		$lista=array(''=>'');
		foreach($resultados as $result):
			$lista[$result->id]=$result->cuenta.' - '.$result->nombre.' ('.$tipos[$result->tipo].')';
		endforeach;
		return $lista;
	}
        function listar_bancos(){
            $this->db->order_by('bancos.nombre','asc');
            $query=$this->db->get('bancos');
            return $query;
        }
        function listar_cuentas(){
            $this->db->select('cuentas.*, bancos.codigo,bancos.nombre');
            $this->db->order_by('bancos.nombre','asc');
            $this->db->order_by('cuentas.cuenta','asc');
            $this->db->join('bancos','bancos.id=cuentas.banco','left');
            $query=$this->db->get('cuentas');
            return $query;
        }
        function crear_cuenta($datos){
            $query=$this->db->insert('cuentas',$datos);
            return $query;
        }
        function crear_banco($datos){
            $query=$this->db->insert('bancos',$datos);
            return $query;
        }
        function eliminar_banco($id){
            $this->db->trans_start();
                $this->db->where('id',$id);
                $query=$this->db->delete('bancos');
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $query;
            }
        }
        function eliminar_cuenta($id){
            $this->db->trans_start();
                $this->db->where('id',$id);
                $query=$this->db->delete('cuentas');
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $query;
            }
        }
        function editar_cuenta($id,$datos){
            $this->db->where('id',$id);
            $query=$this->db->update('cuentas',$datos);
            return $query;
        }
}
?>