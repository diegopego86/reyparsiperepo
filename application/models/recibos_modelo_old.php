<?php
class Recibos_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_recibos($limit,$offset,$numeros_vendedor=null,$usuario=null,$orden=null,$buscar=null){
		$this->db->select('recibos.*,cliente.nombre as nombre_cliente, cliente.contacto');
		if($orden){
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}else{
                    $this->db->order_by('id','desc');
                    $this->db->order_by('fecha','desc');
		}
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or recibos.id like '%$buscar%') AND (cliente.id_vendedor in ('".join("','",$numeros_vendedor)."') or recibos.usuario='".$usuario."')");
			}else{
				$this->db->where("(cliente.id_vendedor in ('".join("','",$numeros_vendedor)."') or recibos.usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or recibos.id like '%$buscar%')");
			}
		}
		$this->db->join('cliente','cliente.id_cliente=recibos.cliente','left');
		if($limit){
			$query=$this->db->get('recibos',$limit,$offset);
		}else{
			$query=$this->db->get('recibos');
		}
                //echo $this->db->last_query();
		return $query;
	}
	function total_listar_recibos($numeros_vendedor=null,$usuario=null,$buscar=null){
		$this->db->select('count(*) as total');
		if($numeros_vendedor){
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or recibos.id like '%$buscar%') AND (cliente.id_vendedor in ('".join("','",$numeros_vendedor)."') or recibos.usuario='".$usuario."')");
			}else{
				$this->db->where("(cliente.id_vendedor in ('".join("','",$numeros_vendedor)."') or recibos.usuario='".$usuario."')");
			}
		}else{
			if($buscar){
				$this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or recibos.id like '%$buscar%')");
			}
		}
		$this->db->join('cliente','cliente.id_cliente=recibos.cliente','left');
		$query=$this->db->get('recibos')->row();
		return $query->total;
	}
	function guardar_borrador($encabezado,$documentos,$cheques,$consignaciones,$fecha,$numero=null){
		$data=array(
			'encabezado'=>$encabezado,
			'documentos'=>$documentos,
			'cheques'=>$cheques,
                        'consignaciones'=>$consignaciones,
			'usuario'=>$this->session->userdata('id_usuario'),
			'fecha'=>$fecha
		);
		if($numero){
			$this->db->where('id',$numero);
			$accion=$this->db->update('borradores_recibos',$data);
			if($accion){
				$accion=$numero;
			}else{
				$accion=false;
			}
		}else{
			$data['fecha']=$fecha;
			$accion=$this->db->insert('borradores_recibos',$data);
			if($accion){
				$accion=$this->db->insert_id();
			}else{
				$accion=false;
			}
		}
		return $accion;
	}
	function eliminar_borrador($numero){
		$this->db->where('id',$numero);
		$eliminar=$this->db->delete('borradores_recibos');
		return $eliminar;
	}
	function listar_borradores($usuario=null){
		if($usuario){
			$this->db->where('usuario',$usuario);
		}
		$this->db->order_by('fecha','desc');
		$query=$this->db->get('borradores_recibos');
		return $query;
	}
	function leer_borrador($id){
		$this->db->where('id',$id);
		$this->db->where('usuario',$this->session->userdata('id_usuario'));
		$query=$this->db->get('borradores_recibos');
		return $query;
	}
        function guardar($borrador,$encabezado,$documentos,$cheques=null,$consignaciones=null,$vendedor){
            $this->db->trans_start();
                $this->db->select('max(numero) as numero');
                $this->db->where('vendedor',$vendedor);
                $numero=$this->db->get('recibos')->row();
                if(count($numero)>0){
                    //$encabezado['numero']=str_pad(($numero->numero*1)+1, 6, "0", STR_PAD_LEFT);
                    $encabezado['numero']=($numero->numero*1)+1;
                }else{
                    //$encabezado['numero']=str_pad(1, 6, "0", STR_PAD_LEFT);
                    $encabezado['numero']=1;
                }
                $encabezado['usuario']=$this->session->userdata['id_usuario'];
                $this->db->set('fecha_realizado','NOW()',false);
                $this->db->insert('recibos',$encabezado);
                $id=$this->db->insert_id();
                foreach($documentos as $documento){
                    $data=array(
                        'recibo'=>$id,
                        'documento'=>$documento['documento'],
                        'valor'=>$documento['valor'],
                        'descuento'=>$documento['descuento'],
                        'rete_fte'=>$documento['rete_fte'],
                        'nota_credito'=>$documento['nota_credito'],
                        'nota_credito_detalle'=>$documento['nota_credito_detalle'],
                        'nota_debito'=>$documento['nota_debito'],
                        'otros'=>$documento['otros'],
                        'total'=>($documento['valor']*1)-($documento['descuento']*1)-($documento['rete_fte'])-($documento['nota_credito'])+($documento['nota_debito'])-($documento['otros'])
                    );
                    $listado_documentos[]=$data;                    
                }
                $this->db->insert_batch('recibos_documentos',$listado_documentos);
                if(is_array($cheques)){
                    $n=0;
                    foreach($cheques as $cheque):
                        if($cheques[$n]['posfechado']==0){
                            $cheques[$n]['fecha_posfechado']=null;
                        }else{
                            $cheques[$n]['fecha_posfechado']=date('Y-m-d',strtotime($cheques[$n]['fecha_posfechado']));
                        }
                        $cheques[$n]['recibo']=$id;
                        $n=$n+1;
                    endforeach;
                    $this->db->insert_batch('recibos_cheques',$cheques);
                }
                if($consignaciones){
                    $n=0;
                    foreach($consignaciones as $consignacion):
                        $consignaciones[$n]['fecha']=date('Y-m-d',strtotime($consignaciones[$n]['fecha']));
                        $consignaciones[$n]['recibo']=$id;
                        $n=$n+1;
                    endforeach;
                    $this->db->insert_batch('recibos_consignaciones',$consignaciones);
                }
                //Eliminar borrador
                $this->db->where('id',$borrador);
		$this->db->delete('borradores_recibos');
                
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                return false;
            }else{
                return $id;
            }
        }
        function leer_encabezado($numero){
		$this->db->select('recibos.*,cliente.nombre,cliente.contacto,cliente.direccion,cliente.telefono1,cliente.ciudad,vendedor.nombre as nombre_vendedor, cuentas.banco, bancos.nombre as banco_nombre,cuentas.cuenta as numero_cuenta');
		$this->db->where('recibos.id',$numero);
		$this->db->join('cliente','cliente.id_cliente=recibos.cliente','left');
                $this->db->join('cuentas','cuentas.id=recibos.cuenta','left');
                $this->db->join('bancos','bancos.id=cuentas.banco','left');
		$this->db->join('vendedor','vendedor.id_vendedor=recibos.vendedor','left');
		$query=$this->db->get('recibos');
		return $query;
	}
        function leer_documentos($numero){
		$this->db->select('recibos_documentos.*');
		$this->db->where('recibo',$numero);
		$query=$this->db->get('recibos_documentos');
		return $query;
	}
        function leer_cheques($numero){
		$this->db->where('recibo',$numero);
		$query=$this->db->get('recibos_cheques');
		return $query;
	}
        function leer_consignaciones($numero){
                $this->db->select('recibos_consignaciones.*,bancos.nombre as banco_nombre, cuentas.cuenta as cuenta_numero');
		$this->db->where('recibo',$numero);
                $this->db->join('cuentas','cuentas.id=recibos_consignaciones.cuenta','left');
                $this->db->join('bancos','bancos.id=cuentas.banco','left');
		$query=$this->db->get('recibos_consignaciones');
		return $query;
	}
        function encabezados_recibos($numeros, $rangos){
            $this->db->select('recibos.*,cliente.*,vendedor.nombre as nombre_vendedor, bancos.nombre as banco_nombre, cuentas.cuenta as numero_cuenta');
            $sql='';
            if(count($numeros)>0){
                $sql='recibos.id in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                        $sql=$sql.' or (recibos.id >= '."'".$rango['desde']."'".' and recibos.id <= '."'".$rango['hasta']."'".')';
                    }else{
                        $sql='(recibos.id >= '."'".$rango['desde']."'".' and recibos.id <= '."'".$rango['hasta']."'".')';
                    }
                endforeach;
            }
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where('recibos.usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('cliente','cliente.id_cliente=recibos.cliente','left');
            $this->db->join('cuentas','cuentas.id=recibos.cuenta','left');
            $this->db->join('bancos','bancos.id=cuentas.banco','left');
            $this->db->join('vendedor','vendedor.id_vendedor=recibos.vendedor','left');
            $this->db->join('recibos_documentos','recibos_documentos.recibo=recibos_documentos.id','left');
            $query=$this->db->get('recibos');
            //echo $this->db->last_query();
            return $query;
        }
        function documentos_recibos($numeros, $rangos){
            $this->db->select('recibos_documentos.*');
            $sql='';
            if(count($numeros)>0){
                $sql='recibos_documentos.recibo in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                        $sql=$sql.' or (recibos_documentos.recibo >= '."'".$rango['desde']."'".' and recibos_documentos.recibo <= '."'".$rango['hasta']."'".')';
                    }else{
                        $sql='(recibos_documentos.recibo >= '."'".$rango['desde']."'".' and recibos_documentos.recibo <= '."'".$rango['hasta']."'".')';
                    }
                endforeach;
            }
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where('recibos.usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('recibos','recibos.id=recibos_documentos.recibo','left');
            $query=$this->db->get('recibos_documentos');
            
            return $query;
        }
        function cheques_recibos($numeros, $rangos){
            $this->db->select('recibos_cheques.*');
            $sql='';
            if(count($numeros)>0){
                $sql='recibos_cheques.recibo in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                        $sql=$sql.' or (recibos_cheques.recibo >= '."'".$rango['desde']."'".' and recibos_cheques.recibo <= '."'".$rango['hasta']."'".')';
                    }else{
                        $sql='(recibos_cheques.recibo >= '."'".$rango['desde']."'".' and recibos_cheques.recibo <= '."'".$rango['hasta']."'".')';
                    }
                endforeach;
            }
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where('recibos.usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('recibos','recibos.id=recibos_cheques.recibo','left');
            $query=$this->db->get('recibos_cheques');
            
            return $query;
        }
        function consignaciones_recibos($numeros, $rangos){
            $this->db->select('recibos_consignaciones.*,bancos.nombre as banco_nombre, cuentas.cuenta as cuenta_numero');
            $sql='';
            if(count($numeros)>0){
                $sql='recibos_consignaciones.recibo in ('.join(',',$numeros).')';                
            }
            if(count($rangos)>0){
                foreach($rangos as $rango):
                    if($sql){
                        $sql=$sql.' or (recibos_consignaciones.recibo >= '."'".$rango['desde']."'".' and recibos_consignaciones.recibo <= '."'".$rango['hasta']."'".')';
                    }else{
                        $sql='(recibos_consignaciones.recibo >= '."'".$rango['desde']."'".' and recibos_consignaciones.recibo <= '."'".$rango['hasta']."'".')';
                    }
                endforeach;
            }            
            $this->db->where($sql,null,false);
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where('recibos.usuario',$this->session->userdata('id_usuario'));
            }
            $this->db->join('cuentas','cuentas.id=recibos_consignaciones.cuenta','left');
            $this->db->join('bancos','bancos.id=cuentas.banco','left');
            $this->db->join('recibos','recibos.id=recibos_consignaciones.recibo','left');
            $query=$this->db->get('recibos_consignaciones');
            
            return $query;
        }
        function listar_observaciones($visible=null){
            if($visible){
                $this->db->where('visible',1);
            }
            $this->db->order_by('orden','asc');
            $query=$this->db->get('recibos_observaciones');
            return $query;
        }
        function leer_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->get('recibos_observaciones');
            return $query;
        }
        function crear_observacion($datos){
            $query=$this->db->insert('recibos_observaciones',$datos);
            return $query;
        }
        function editar_observacion($observacion,$datos){
            $this->db->where('id',$observacion);
            $query=$this->db->update('recibos_observaciones',$datos);
            return $query;
        }
        function eliminar_observacion($observacion){
            $this->db->where('id',$observacion);
            $query=$this->db->delete('recibos_observaciones');
            return $query;
        }
        function cantidad_recibos_vendedor_fechas($desde,$hasta,$vendedores=null){
            if(!$desde){
                $desde=date('d-m-Y');
            }            
            $this->db->select("vendedor.id_vendedor,vendedor.nombre, count(recibos.id) as cantidad, sum(recibos.total) as valor,DATE_FORMAT(recibos.fecha_realizado,'%Y-%m-%d') as fecha_calculada",false);
            $this->db->where("DATE_FORMAT(recibos.fecha_realizado,'%Y-%m-%d')>= STR_TO_DATE('$desde','%d-%m-%Y')",null,false);
            if($hasta){
               $this->db->where("DATE_FORMAT(recibos.fecha_realizado,'%Y-%m-%d')<= STR_TO_DATE('$hasta','%d-%m-%Y')",null,false);
            }
            if($vendedores){
                $this->db->where_in('vendedor.id_vendedor',$vendedores);
            }
            $this->db->join('vendedor','vendedor.id_vendedor=recibos.vendedor','left');
            $this->db->order_by('vendedor.nombre','asc');
            $this->db->group_by(array('vendedor.id_vendedor','fecha_calculada'));
            $query=$this->db->get('recibos');
            //echo $this->db->last_query();
            return $query;
        }
        function cheques_posfechados_dia(){
            $this->db->select('vendedor.nombre as vendedor_nombre,vendedores_correo.correo as vendedor_correo, recibos.vendedor,recibos.cliente,cliente.nombre,cliente.contacto,bancos.codigo as banco_codigo, bancos.nombre as banco_nombre, recibos_cheques.*');
            $this->db->where('fecha_posfechado >=',date('Y-m-d'));
            $this->db->where('fecha_posfechado <=',"INTERVAL 1 DAY + '".date('Y-m-d')."'",false);
            $this->db->join('recibos','recibos.id=recibos_cheques.recibo','left');
            $this->db->join('cliente','cliente.id_cliente=recibos.cliente','left');
            $this->db->join('vendedor','vendedor.id_vendedor=recibos.vendedor','left');
            $this->db->join('vendedores_correo','vendedores_correo.numero=recibos.vendedor','left');
            $this->db->join('bancos','bancos.id=recibos_cheques.banco','left');
            $this->db->order_by('recibos_cheques.fecha_posfechado','asc');
            $this->db->order_by('recibos.vendedor','asc');
            $this->db->order_by('recibos.numero','asc');
            $query =$this->db->get('recibos_cheques');
            return $query;
        }
        function cheques_recibos_actualizar(){
            $this->db->where('cheques <>','0');
            $this->db->where('cheques is not null');
            $result=$this->db->get('recibos');
            echo $this->db->last_query();
            return $result;
        }
        function crear_cheques($cheques){
            return $this->db->insert_batch('recibos_cheques',$cheques);
        }
        function total_recibos_vendedores_mes($vendedores,$usuario,$mes){
		$this->db->select('recibos.vendedor, vendedor.nombre,sum(total) as total',false);
		if($vendedores && $this->session->userdata('rol')!=1){
			$this->db->where("(recibos.vendedor in ('".join("','",$vendedores)."') or recibos.usuario='".$usuario."')");
		}
		$this->db->like('fecha_realizado',$mes,'after');
                $this->db->join('vendedor','vendedor.id_vendedor=recibos.vendedor','left');
		$this->db->order_by('vendedor.nombre','asc');
		$this->db->group_by('recibos.vendedor');		
		$query=$this->db->get('recibos');
                //echo $this->db->last_query();
		return $query;
	}
        function agregar_consignacion($consignacion){
            return $this->db->insert('recibos_consignaciones',$consignacion);
        }
}
?>