<?php
class Clientes_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function buscar_cliente($limit,$offset,$nombre=null,$orden=null,$vendedor=null,$filtro=null){
                $this->db->select('id_cliente, count(*) as total_documentos');
                $this->db->from('cartera');
                $this->db->group_by('cartera.id_cliente');
                $this->db->order_by('cartera.id_cliente','asc');
                $subconsulta = $this->db->_compile_select();
		$this->db->_reset_select();
                
		$this->db->select('cliente.*,cartera_clientes.total_documentos');
		if($this->session->userdata('rol')!=1 and $this->session->userdata('rol')!=4){
			if($vendedor){
				if($nombre){
					$this->db->where("(cliente.id_vendedor in ('".join("','",$vendedor)."') AND (cliente.id_cliente LIKE '%$nombre%' OR cliente.nombre LIKE '%$nombre%' OR cliente.contacto LIKE '%$nombre%' OR cliente.direccion LIKE '%$nombre%' OR cliente.ciudad LIKE '%$nombre%' OR cliente.telefono1 LIKE '%$nombre%'))",null,false);
				}else{
					$this->db->where("(cliente.id_vendedor in ('".join("','",$vendedor)."'))");
				}
			}else{
				if($nombre){
					$this->db->where("(cliente.id_cliente LIKE '%$nombre%' OR cliente.nombre LIKE '%$nombre%' OR cliente.contacto LIKE '%$nombre%' OR cliente.direccion LIKE '%$nombre%' OR cliente.ciudad LIKE '%$nombre%' OR cliente.telefono1 LIKE '%$nombre%')",null,false);
				}
			}
		}else{
                    if($nombre){
			$this->db->where("(cliente.id_cliente LIKE '%$nombre%' OR cliente.nombre LIKE '%$nombre%' OR cliente.contacto LIKE '%$nombre%' OR cliente.direccion LIKE '%$nombre%' OR cliente.ciudad LIKE '%$nombre%' OR cliente.telefono1 LIKE '%$nombre%')",null,false);
                    }
		}
                $this->db->join("($subconsulta) as cartera_clientes",'cartera_clientes.id_cliente=cliente.id_cliente','left');
		if($filtro){			
			$this->db->where("cliente.id_vendedor",$filtro);
		}
		if(!$orden){
			$this->db->order_by('cliente.nombre','asc');
		}else{
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($limit==0){
			$query=$this->db->get('cliente');
		}else{
			$query=$this->db->get('cliente',$limit,$offset);
		}
		//echo $this->db->last_query();
		return $query;
	}
	function resultados_buscar_cliente($nombre=null,$vendedor=null,$filtro=null){
		$this->db->select('count(*) as total');
		if($this->session->userdata('rol')!=1 and $this->session->userdata('rol')!=4){
			if($vendedor){
				if($nombre){
					$this->db->where("(id_vendedor in ('".join("','",$vendedor)."') AND (id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%' OR telefono1 LIKE '%$nombre%'))",null,false);
				}else{
					$this->db->where("(id_vendedor in ('".join("','",$vendedor)."'))");
				}
			}else{
				if($nombre){
					$this->db->where("(id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%' OR telefono1 LIKE '%$nombre%')",null,false);
				}
			}
		}else{
                    if($nombre){
			$this->db->where("(id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%' OR telefono1 LIKE '%$nombre%')",null,false);
                    }
		}		
		if($filtro){
			$this->db->where("id_vendedor",$filtro);
		}
		$query=$this->db->get('cliente')->row();
		return $query->total;
	}
	function leer_cliente($cliente,$vendedor=null){
		$this->db->select('*');
		$this->db->where('id_cliente',$cliente);
		if($this->session->userdata('rol')==2){
			if($vendedor){
				$this->db->where_in('id_vendedor',$vendedor);
			}
		}
		if($this->session->userdata('rol')==3){
			$this->db->join('usuarios_nits','usuarios_nits.nit=cliente.id_cliente','left');
			$this->db->where('usuarios_nits.usuario',$this->session->userdata('id_usuario'));
		}
		$query=$this->db->get('cliente');
		//echo $this->db->last_query();
		return $query;
	}
	function leer_cliente_cartera($cliente,$vendedor=null){
		$this->db->select('cartera.id_cliente,sum(cartera.saldo) as saldo');
		$this->db->from('cartera');
		$this->db->where('id_cliente',$cliente);
		$this->db->group_by('id_cliente');
		$subconsulta = $this->db->_compile_select();
		$this->db->_reset_select();
		
		$this->db->select('*');
		$this->db->where('cliente.id_cliente',$cliente);
		if($this->session->userdata('rol')==2){
			if($vendedor){
				$this->db->where_in('id_vendedor',$vendedor);
			}
		}
		if($this->session->userdata('rol')==3){
			$this->db->join('usuarios_nits','usuarios_nits.nit=cliente.id_cliente','left');
			$this->db->where('usuarios_nits.usuario',$this->session->userdata('id_usuario'));
		}
		$this->db->join("($subconsulta) as cartera_cli",'cartera_cli.id_cliente=cliente.id_cliente','left');
		$query=$this->db->get('cliente');
		//echo $this->db->last_query();
		return $query;
	}	
	function correo($cliente){
		$db2=$this->load->database('datos',true);
		$db2->where('NIT',$cliente);
		$query=$db2->get('contactos');
                $db2->close();
		if($query->num_rows > 0){
			$query=$query->row();
			return $query->email;
		}else{
			return false;
		}		
	}
	function buscar_nits_cliente($limit,$offset,$nombre=null,$orden=null,$vendedor=null,$filtro=null){
                $this->db->select('cliente.id_cliente, count(*) as total_documentos');
                $this->db->from('cartera');
                $this->db->group_by('cartera.id_cliente');
                $this->db->order_by('cartera.id_cliente','asc');
                $subconsulta = $this->db->_compile_select();
		$this->db->_reset_select();
                
		$this->db->select('cliente.*,cartera_clientes.total_documentos');
		$this->db->join('usuarios_nits','usuarios_nits.nit=cliente.id_cliente','left');
		$this->db->where('usuarios_nits.usuario',$this->session->userdata('id_usuario'));
		if($nombre){
			$this->db->where("(cliente.id_cliente LIKE '%$nombre%' OR cliente.nombre LIKE '%$nombre%' OR cliente.contacto LIKE '%$nombre%' OR cliente.direccion LIKE '%$nombre%' OR cliente.ciudad LIKE '%$nombre%')",null,false);
		}
                $this->db->join("($subconsulta) as cartera_clientes",'cartera_clientes.id_cliente=cliente.id_cliente','left');
		if($filtro){			
			$this->db->where("cliente.id_vendedor",$filtro);
		}
		if(!$orden){
			$this->db->order_by('cliente.nombre','asc');
		}else{
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($limit==0){
			$query=$this->db->get('cliente');
		}else{
			$query=$this->db->get('cliente',$limit,$offset);
		}
		//echo $this->db->last_query();
		return $query;
	}
	function resultados_buscar_nits_cliente($nombre=null,$vendedor=null,$filtro=null){
		$this->db->select('count(*) as total');
		$this->db->join('usuarios_nits','usuarios_nits.nit=cliente.id_cliente','left');
		$this->db->where('usuarios_nits.usuario',$this->session->userdata('id_usuario'));
		if($nombre){
			$this->db->where("(id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%')",null,false);
		}
		if($filtro){
			$this->db->where("id_vendedor",$filtro);
		}
		$query=$this->db->get('cliente')->row();
		return $query->total;
	}
	function listar_nits_cliente($limit,$offset,$id,$nombre=null,$orden=null){
		$this->db->select('*');
		$this->db->join('cliente','cliente.id_cliente=usuarios_nits.nit','left');
		$this->db->where('usuarios_nits.usuario',$id);
		if($nombre){
			$this->db->where("(id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%')",null,false);
		}
		if(!$orden){
			$this->db->order_by('nombre','asc');
		}else{
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($limit==0){
			$query=$this->db->get('usuarios_nits');
		}else{
			$query=$this->db->get('usuarios_nits',$limit,$offset);
		}
		//echo $this->db->last_query();
		return $query;
	}
	function total_listar_nits_cliente($nombre=null,$id){
		$this->db->select('count(*) as total');
		$this->db->join('usuarios_nits','usuarios_nits.nit=cliente.id_cliente','left');
		$this->db->where('usuarios_nits.usuario',$id);
		if($nombre){
			$this->db->where("(id_cliente LIKE '%$nombre%' OR nombre LIKE '%$nombre%' OR contacto LIKE '%$nombre%' OR direccion LIKE '%$nombre%' OR ciudad LIKE '%$nombre%')",null,false);
		}
		$query=$this->db->get('cliente')->row();
		return $query->total;
	}
	function productos_mas_pedidos_cliente($limit,$offset,$nombre=null,$cliente,$orden=null){
		$this->db->select('pedido_detalle.id_producto, COUNT(pedido_detalle.id_producto) as cantidad',false);
		$this->db->from('pedido_detalle');
		$this->db->join('pedido_maestro','pedido_maestro.id_maestro=pedido_detalle.id_maestro','left');
		$this->db->where('pedido_maestro.id_cliente',$cliente);
		$this->db->group_by('pedido_detalle.id_producto');
		$subconsulta=$this->db->_compile_select();
		$this->db->_reset_select();
		$this->db->select('producto.*,sub.*, producto_extras.imagenes');
		//$this->db->from();
		$this->db->join('producto','producto.id_producto=sub.id_producto');
		$this->db->join('producto_extras','producto_extras.producto = producto.id_producto','left');
		$this->db->order_by('cantidad','desc');
		if($nombre){
			$this->db->where("nombre LIKE '%$nombre%'",null,false);
		}
		if(!$orden){
			$this->db->order_by('nombre','asc');
		}else{
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
		if($limit==0){
			$query=$this->db->get("($subconsulta) as sub");
		}else{
			$query=$this->db->get("($subconsulta) as sub",$limit,$offset);
		}
		return $query;
	}
	function resultados_productos_mas_pedidos_cliente($nombre=null,$cliente){
		$this->db->select('pedido_detalle.id_producto,COUNT(*) as total',false);
		$this->db->join('pedido_maestro','pedido_maestro.id_maestro=pedido_detalle.id_maestro','left');
		$this->db->from('pedido_detalle');
		$this->db->where('pedido_maestro.id_cliente',$cliente);
		$this->db->group_by('pedido_detalle.id_producto');
		$subconsulta=$this->db->_compile_select();		
		$this->db->_reset_select();
		$this->db->select('count(*) as total');
		$this->db->join('producto','producto.id_producto=sub.id_producto');
		if($nombre){
			$this->db->where("nombre LIKE '%$nombre%'",null,false);
		}		
		$query=$this->db->get("($subconsulta) as sub")->row();		
		return $query->total;				
	}
}
?>