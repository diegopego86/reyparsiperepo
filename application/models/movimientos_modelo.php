<?php
class Movimientos_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function listar_movimientos($limit,$offset,$numeros_vendedor=null,$usuario=null,$orden=null,$buscar=null){
            $this->db->select('movimiento_maestro.*,cliente.id_vendedor,cliente.nombre as nombre_cliente, cliente.contacto');
            if($orden){
                    foreach($orden as $od):
                            $this->db->order_by($od['orden'],$od['direccion']);
                    endforeach;
            }else{
                            $this->db->order_by('numero','desc');
                            $this->db->order_by('fecha','desc');
            }
            if($numeros_vendedor){
                    if($buscar){
                            $this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or movimiento_maestro.numero like '%$buscar%') AND (cliente.id_vendedor in ('".join("','",$numeros_vendedor)."'))");
                    }else{
                            $this->db->where("(cliente.id_vendedor in ('".join("','",$numeros_vendedor)."'))");
                    }
            }else{
                    if($buscar){
                            $this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or movimiento_maestro.numero like '%$buscar%')");
                    }
            }
            $this->db->join('cliente','cliente.id_cliente=movimiento_maestro.codigo','left');
            if($limit){
                    $query=$this->db->get('movimiento_maestro',$limit,$offset);
            }else{
                    $query=$this->db->get('movimiento_maestro');
            }
            //echo $this->db->last_query();
            return $query;
    }
    function total_listar_movimientos($numeros_vendedor=null,$usuario=null,$buscar=null){
            $this->db->select('count(*) as total');
            if($numeros_vendedor){
                    if($buscar){
                            $this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or movimiento_maestro.numero like '%$buscar%') AND (cliente.id_vendedor in ('".join("','",$numeros_vendedor)."'))");
                    }else{
                            $this->db->where("(cliente.id_vendedor in ('".join("','",$numeros_vendedor)."'))");
                    }
            }else{
                    if($buscar){
                            $this->db->where("(cliente.nombre like '%$buscar%' or cliente.contacto like '%$buscar%' or cliente.id_cliente like '$buscar%' or movimiento_maestro.numero like '%$buscar%')");
                    }
            }
            $this->db->join('cliente','cliente.id_cliente=movimiento_maestro.codigo','left');
            $query=$this->db->get('movimiento_maestro')->row();
            return $query->total;
    }
    function total_movimientos_vendedores_mes($vendedores,$usuario,$mes){
            $this->db->select('id_vendedor,sum(subtotal) as subtotal',false);
            if($vendedores){
                    $this->db->where("(id_vendedor in ('".join("','",$vendedores)."'))");
            }
            $this->db->like('fecha',$mes,'after');
            $this->db->order_by('id_vendedor');
            $this->db->group_by('id_vendedor');		
            $query=$this->db->get('movimiento_maestro');
            return $query;
    }
    function listar_meses(){
            $this->db->select("distinct(date_format(fecha,'%Y-%m')) as fecha,date_format(fecha,'%m') as mes,date_format(fecha,'%Y') as anio",false);
            $this->db->order_by('fecha','desc');
            $this->db->group_by('fecha');
            $query=$this->db->get('movimiento_maestro');
            return $query;
    }
    function listar_movimientos_usuario($limit,$offset,$usuario=null,$orden=null){
            $this->db->select('movimiento_maestro.*,cliente.id_vendedor,cliente.nombre as nombre_cliente,cliente.contacto');
            if($orden){
                    foreach($orden as $od):
                            $this->db->order_by($od['orden'],$od['direccion']);
                    endforeach;
            }
            if($usuario){
                    $this->db->where("(cliente.id_vendedor in ('".join("','",$this->session->userdata('numeros_vendedor'))."'))");
            }
            $this->db->join('cliente','cliente.id_cliente=movimiento_maestro.codigo','left');
            if($limit){
                    $query=$this->db->get('movimiento_maestro',$limit,$offset);
            }else{
                    $query=$this->db->get('movimiento_maestro');
            }
            return $query;
    }
    function productos_movimiento($movimiento){
            $this->db->select('movimiento_detalle.numero,movimiento_detalle.cod_pcto as id_producto,movimiento_detalle.cantidad,movimiento_detalle.descuento,movimiento_detalle.total, producto.nombre,producto.referencia,producto.unidad,producto.precio1,producto.precio2,producto.precio3,producto.precio4,producto.iva');
            $this->db->where('numero',$movimiento);
            $this->db->order_by('id_detalle');
            $this->db->join('producto','producto.id_producto=cod_pcto','left');
            $query=$this->db->get('movimiento_detalle');
            return $query;
    }
    function leer_encabezado($numero){
            $this->db->select('movimiento_maestro.*,movimiento_maestro.dir_envio as direccion_envio,cliente.id_cliente,cliente.id_vendedor,cliente.nombre,cliente.contacto,cliente.direccion,cliente.telefono1,cliente.ciudad,vendedor.nombre as nombre_vendedor');
            $this->db->where('numero',$numero);
            $this->db->join('cliente','cliente.id_cliente=movimiento_maestro.codigo','left');
            $this->db->join('vendedor','vendedor.id_vendedor=cliente.id_vendedor','left');
            $query=$this->db->get('movimiento_maestro');
            return $query;
    }
    function leer_productos($numero){
            $this->db->select('movimiento_detalle.*, producto.id_producto,producto.nombre,producto.referencia');
            $this->db->where('numero',$numero);
            $this->db->join('producto','producto.id_producto=cod_pcto','left');
            $query=$this->db->get('movimiento_detalle');
            return $query;
    }
    function datos_movimientos($numeros, $rangos){
        $this->db->select('movimiento_maestro.*,cliente.*,vendedor.nombre as nombre_vendedor, cod_pcto, movimiento_detalle.cantidad, movimiento_detalle.precio, movimiento_detalle.descuento, movimiento_detalle.iva, movimiento_detalle.total_sin_dcto, movimiento_detalle.total as total_item,producto.nombre as nombre_producto');
        $sql='';
        if(count($numeros)>0){
            $sql='movimiento_maestro.numero in ('.join(',',$numeros).')';                
        }
        if(count($rangos)>0){
            foreach($rangos as $rango):
                if($sql){
                    $sql=$sql.' or (movimiento_maestro.numero >= '."'".$rango['desde']."'".' and movimiento_maestro.numero <= '."'".$rango['hasta']."'".')';
                }else{
                    $sql='(movimiento_maestro.numero >= '."'".$rango['desde']."'".' and movimiento_maestro.numero <= '."'".$rango['hasta']."'".')';
                }
            endforeach;
        }
        $this->db->where($sql,null,false);
        
        $this->db->join('cliente','cliente.id_cliente=movimiento_maestro.codigo','left');
        $this->db->join('vendedor','vendedor.id_vendedor=cliente.id_vendedor','left');
        $this->db->join('movimiento_detalle','movimiento_detalle.numero=movimiento_maestro.numero','left');
        $this->db->join('producto','producto.id_producto=cod_pcto','left');
        $query=$this->db->get('movimiento_maestro');
        //echo $this->db->last_query();
        return $query;
    }
    function cantidad_movimientos_vendedor($desde,$hasta){
        if(!$desde){
            $desde=date('d-m-Y');
        }

        $this->db->select('vendedor.id_vendedor,vendedor.nombre, count(movimiento_maestro.numero) as cantidad',false);
        $this->db->where("fecha>= STR_TO_DATE('$desde','%d-%m-%Y')",null,false);
        if($hasta){
           $this->db->where("fecha<= STR_TO_DATE('$hasta','%d-%m-%Y')",null,false);
        }
        $this->db->join('vendedor','vendedor.id_vendedor=cliente.id_vendedor','join');
        $this->db->order_by('vendedor.nombre','asc');
        $this->db->group_by('vendedor.id_vendedor');
        $query=$this->db->get('movimiento_maestro');
        return $query;
    }
}
?>