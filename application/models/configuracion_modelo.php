<?php
class Configuracion_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function datos_empresa(){
        $query=$this->db->get('empresa');
        return $query->row();
    }
    function correos_envio($tipo){
        if(is_int($tipo)){
            $this->db->where('tipo',$tipo);
        }else{
            if($tipo=='pedidos'){
                $this->db->where('tipo',1);
            }else{
                if($tipo=='recibos'){
                    $this->db->where('tipo',2);
                }else{
                    if($tipo=='cotizaciones'){
                        $this->db->where('tipo',3);
                    }else{
                        if($tipo=='contacto'){
                            $this->db->where('tipo',4);
                        }else{
                            $this->db->where('tipo',0);
                        }
                    }
                }
            }
        }
        $query=$this->db->get('correos');
        return $query->result();
    }
    function datos_sipe(){
        $this->db->where('id',1);
        $query=$this->db->get('configuraciones');
        return $query;
    }
    function configuracion_pedido(){
        $this->db->where('id',1);
        $query=$this->db->get('pedidos_configuracion');
        return $query; 
    }
    function configuracion_cotizacion(){
        $this->db->where('id',1);
        $query=$this->db->get('cotizaciones_configuracion');
        return $query; 
    }
    function configuracion_recibo(){
        $this->db->where('id',1);
        $query=$this->db->get('recibos_configuracion');
        return $query; 
    }
    function configuracion_cartera(){
        $this->db->where('id',1);
        $query=$this->db->get('cartera_configuracion');
        return $query; 
    }
    function configuracion_guardar_mensaje($mensaje){
        $this->db->where('id',1);
        $this->db->set('mensaje',$mensaje);
        $actualizar=$this->db->update('configuraciones');
        return $actualizar;
    }
    function modificar_configuracion($datos){
        $this->db->where('id',1);
        $query=$this->db->update('configuraciones',$datos);
        return $query;
    }
    function modificar_configuracion_pedido($datos){
        $this->db->where('id',1);
        $query=$this->db->update('pedidos_configuracion',$datos);
        return $query;
    }
    function modificar_configuracion_cotizacion($datos){
        $this->db->where('id',1);
        $query=$this->db->update('cotizaciones_configuracion',$datos);
        return $query;
    }
    function modificar_configuracion_recibo($datos){
        $this->db->where('id',1);
        $query=$this->db->update('recibos_configuracion',$datos);
        return $query;
    }
    function modificar_configuracion_cartera($datos){
        $this->db->where('id',1);
        $query=$this->db->update('cartera_configuracion',$datos);
        return $query;
    }
    function leer_correo($correo){
        $this->db->where('id',$correo);
        $query=$this->db->get('correos');
        return $query;
    }
    function crear_correo($datos){
        $query=$this->db->insert('correos',$datos);
        return $query;
    }
    function editar_correo($correo,$datos){
        $this->db->where('id',$correo);
        $query=$this->db->update('correos',$datos);
        return $query;
    }
    function eliminar_correo($correo){
        $this->db->where('id',$correo);
        $query=$this->db->delete('correos');
        return $query;
    }
}
?>