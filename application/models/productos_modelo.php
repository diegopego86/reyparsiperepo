<?php
class Productos_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function buscar_producto($limit,$offset,$nombre=null,$orden=null){
		$this->db->select('producto.*, producto_extras.imagenes');
		if($nombre){
			$this->db->where("((precio1 > 0) AND (tipo<>'()') AND (id_producto LIKE BINARY('$nombre%') OR nombre LIKE '%$nombre%' OR referencia LIKE BINARY('$nombre%')))",null,false);			
		}else{
			$this->db->where('precio1 >',0);
			$this->db->where('tipo <>','()');
		}
		if(!$orden){
			$this->db->order_by('nombre','asc');
		}else{
			foreach($orden as $od):
				$this->db->order_by($od['orden'],$od['direccion']);
			endforeach;
		}
                $this->db->join('producto_extras','producto_extras.producto = producto.id_producto','left');
		if($limit==0){
			$query=$this->db->get('producto');
		}else{
			$query=$this->db->get('producto',$limit,$offset);
		}
		return $query;
	}
	function resultados_buscar_producto($nombre=null){
		$this->db->select('count(*) as total');
		if($nombre){
			$this->db->where("((precio1 > 0) AND (tipo<>'()') AND (id_producto LIKE BINARY('$nombre%') OR nombre LIKE '%$nombre%' OR referencia LIKE BINARY('$nombre%')))",null,false);			
		}else{
			$this->db->where('precio1 >',0);
			$this->db->where('tipo <>','()');
		}
		$query=$this->db->get('producto')->row();
		return $query->total;
	}
	function leer_producto($producto){
		$this->db->select('*');
		$this->db->where("id_producto = BINARY('".$producto."')");
		//$this->db->where('precio1 >',0);
		$this->db->where('tipo <>','()');
		$query=$this->db->get('producto');
		return $query;
	}
}
?>