<?php
class Cartera_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function mostrar_cartera($limit,$offset,$buscar,$orden=null,$filtro=null){
        if($limit==0){
            $this->db->select('cartera.id_cliente, count(*) as total',false);
            $this->db->from('cartera',false);
            $this->db->join('cliente','cliente.id_cliente=cartera.id_cliente','left');
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where_in('cartera.id_vendedor',$this->session->userdata('numeros_vendedor'));
            }
            
            if($buscar){
                $this->db->where("((telefono1 like '%$buscar%' or ciudad like '%$buscar%' or nombre like '%$buscar%' or contacto like '%$buscar%' or cliente.id_cliente like '%$buscar%') and (cartera.id_cliente is not null) and (cartera.id_cliente <>' '))",null,false);
            }else{
                $this->db->where("cartera.id_cliente is not null and (cartera.id_cliente <>' ')",null,false);
            }
            $this->db->order_by('cartera.id_cliente');
            $this->db->group_by('cartera.id_cliente');
            
            $subconsulta = $this->db->_compile_select();
            $this->db->_reset_select();
            
            $this->db->select('count(id_cliente) as total', false);
            $this->db->from("($subconsulta) as clientes_con_cartera",false);
            $query=$this->db->get();
            $query=$query->row();
            
            return $query->total;
        }else{
            //select distinct id_cliente from carteraº
            $this->db->select('cartera.id_cliente,cliente.nombre,cliente.contacto,count(*) as total');
            $this->db->from('cartera',false);
            $this->db->join('cliente','cliente.id_cliente=cartera.id_cliente','left');
            if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                $this->db->where_in('cartera.id_vendedor',$this->session->userdata('numeros_vendedor'));
            }
            
            if($buscar){
                $this->db->where("((telefono1 like '%$buscar%' or ciudad like '%$buscar%' or nombre like '%$buscar%' or contacto like '%$buscar%' or cliente.id_cliente like '%$buscar%') and (cartera.id_cliente is not null) and (cartera.id_cliente <>' '))",null,false);
            }else{
                $this->db->where("cartera.id_cliente is not null and (cartera.id_cliente <>' ')",null,false);
            }            
            $this->db->order_by('cartera.id_cliente');
            $this->db->group_by('cartera.id_cliente');
            $subconsulta = $this->db->_compile_select();
            $this->db->_reset_select();
            
            $this->db->select('clientes_con_cartera.id_cliente', false);
            //$this->db->from();
            if(!$orden){
		$this->db->order_by('id_cliente','asc');
            }else{
                foreach($orden as $od):
                        $this->db->order_by($od['orden'],$od['direccion']);
                endforeach;
            }
            $this->db->from("($subconsulta) as clientes_con_cartera",false);
            $this->db->limit($limit,$offset);
            $query=$this->db->get();
            $query=$query->result();
            $clientes=array();
            foreach($query as $cliente):
                $clientes[]=$cliente->id_cliente;
            endforeach;
            
            
            if($clientes){
                $this->db->select('cliente.nombre, cliente.contacto, cliente.telefono1, cliente.ciudad, cliente.cupo, cliente.direccion, cartera.*, DATEDIFF(now(),fecha) as dias',false);
                $this->db->join('cliente','cliente.id_cliente=cartera.id_cliente','left');
                $this->db->where_in('cartera.id_cliente',$clientes);
                if(!$orden){
                    $this->db->order_by('cartera.id_cliente','asc');
                    $this->db->order_by('cartera.fecha','asc');
                }else{
                        foreach($orden as $od):
                                $this->db->order_by($od['orden'],$od['direccion']);
                        endforeach;
                        $this->db->order_by('cartera.fecha','asc');
                }           


                $query=$this->db->get('cartera');
                $query=$query->result();
                $datos=array();
                foreach($query as $cartera):
                    if(!isset($datos[$cartera->id_cliente])){
                        $datos[$cartera->id_cliente]=new stdClass();
                    }
                    $datos[$cartera->id_cliente]->cliente=(object) array(
                        'id_cliente'=>$cartera->id_cliente,
                        'nombre'=>$cartera->nombre,
                        'contacto'=>$cartera->contacto,
                        'telefono1'=>$cartera->telefono1,
                        'ciudad'=>$cartera->ciudad,
                        'cupo'=>$cartera->cupo,
                        'direccion'=>$cartera->direccion
                    );
                    $datos[$cartera->id_cliente]->cartera[]=(object) array(
                        'tipo'=>$cartera->tipo,
                        'id_documento'=>$cartera->id_documento,
                        'fecha'=>$cartera->fecha,
                        'fecha_vence'=>$cartera->fecha_vence,
                        'dias'=>$cartera->dias,
                        'id_vendedor'=>$cartera->id_vendedor,
                        'valor_inicial'=>$cartera->valor_inicial,
                        'saldo_ant'=>$cartera->saldo_ant,
                        'cargo'=>$cartera->cargo,
                        'abono'=>$cartera->abono,
                        'saldo'=>$cartera->saldo
                    );                
                endforeach;
                return (object) $datos;
            }else{
                return false;
            }
        }        
    }
	function cartera_cliente($limit,$offset,$nit,$orden=null,$filtro=null){
		$this->db->select(' *, DATEDIFF(now(),fecha_vence) as dias',false);
		if($nit){
                    if($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){
                        if($filtro){
                            $n=0;
                            foreach($filtro as $vd):
                                if($n==0){
                                        $this->db->where("id_vendedor = '$vd' AND (id_cliente = '$nit')",null,false);
                                }else{
                                        $this->db->or_where("id_vendedor = '$vd' AND (id_cliente ='$nit')",null,false);
                                }
                                $n++;
                            endforeach;
                        }else{
                            //$this->db->where('saldo >',0,false);			
                            $this->db->where('id_cliente',$nit);
                        }
                    }else{
                        //$this->db->where('saldo >',0,false);			
                        $this->db->where('id_cliente',$nit);
                    }
		}
		if(!$orden){
                    $this->db->order_by('fecha','asc');
		}else{
                    foreach($orden as $od):
                        $this->db->order_by($od['orden'],$od['direccion']);
                    endforeach;
		}
		if($limit==0){
                    $query=$this->db->get('cartera');
		}else{
                    $query=$this->db->get('cartera',$limit,$offset);
		}
		return $query;
	}
	function saldo_actual_cartera_cliente($nit,$filtro=null){
		$this->db->select('sum(saldo) as saldo',false);
		if($nit){
                    if($this->session->userdata('rol')!=3){
                        if($filtro){
                            $n=0;
                            foreach($filtro as $vd):
                                if($n==0){
                                        $this->db->where("id_vendedor = '$vd' AND (id_cliente = '$nit')",null,false);
                                }else{
                                        $this->db->or_where("id_vendedor = '$vd' AND (id_cliente ='$nit')",null,false);
                                }
                                $n++;
                            endforeach;
                        }else{
                            $this->db->where('id_cliente',$nit);
                        }
                    }else{
                        $this->db->where('id_cliente',$nit);
                    }
		}
                $query=$this->db->get('cartera')->row();
		return $query->saldo;
	}
}
?>