<?php
class Roles_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_dropdown(){
            $this->db->select('*');
            $this->db->order_by('nombre','asc');
            $query=$this->db->get('roles')->result();
            $tipos=array(''=>'Seleccione el rol');
            foreach($query as $tipo):
                    $tipos[$tipo->id]=$tipo->nombre;
            endforeach;
            return $tipos;
	}
}
?>