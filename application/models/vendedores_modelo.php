<?php
class Vendedores_modelo extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function listar_dropdown(){
		$this->db->select('*');
		$this->db->order_by('id_vendedor','asc');
		$query=$this->db->get('vendedor')->result();
		$vendedores=array();
		foreach($query as $vendedor):
			$vendedores[$vendedor->id_vendedor]=$vendedor->id_vendedor;
		endforeach;
		return $vendedores;
	}
	function listar_dropdown_nombres($inicio=null,$fin=null){
		$this->db->select('*');
		$this->db->order_by('id_vendedor','asc');
		$query=$this->db->get('vendedor')->result();
                if($inicio){
                    $vendedores=$inicio;
                }else{
                    $vendedores=array();
                }
		foreach($query as $vendedor):
			$vendedores[$vendedor->id_vendedor]=$vendedor->id_vendedor.' '.$vendedor->nombre;
		endforeach;
		return $vendedores;
	}
	function correo($vendedor){
		$this->db->where('id_vendedor',$vendedor);
		$query=$this->db->get('usuarios');
		if($query->num_rows>0){
			$query=$query->row();
			return $query->email;
		}else{
			return false;
		}
	}
}
?>