<div class="form" class="grid16">
<h1>Cartera</h1>
<?php 
    echo form_open('cartera/mostrar_cartera');
    echo form_label('Cliente: (Digite el nit, nombre o contacto del cliente)','cliente');
    echo form_input(array('name'=>'cliente','value'=>set_value('cliente'),'style'=>'width:400px;'));
    echo form_submit('buscar','Buscar');    
    echo form_close();
?>
</div>
<div id="cartera_cliente"><?php echo $listado_cartera;?></div>
<div class="clear"></div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#cartera_cliente a').live('click',function(){
        var url=$(this).attr('href');
        actualizar_busqueda(url);
        return false;
    });
    $('form').live('submit',function(){
        var url=$(this).attr('action');
        actualizar_busqueda(url);
        return false;
    });
    function actualizar_busqueda(url){
        var form_data={
            buscar:$('input[name=cliente]').val(),
            ajax:1
        }
        $.ajax({
           type: "POST",
           url: url,
           data: form_data,
           success: function(msg){
                   $('#cargando').hide();
                   if(msg){				   
                        $('#cartera_cliente').html(msg);					
                        var buscar=$('input[name=cliente]').val();
                        if(buscar!=''){
                                $('[title=Nombre]').highlight(buscar);
                                $('[title=Contacto]').highlight(buscar);
                                $('[title=Telefono]').highlight(buscar);
                                $('[title=Nit]').highlight(buscar);
                                $('[title=Ciudad]').highlight(buscar);
                        }
                   }else{
                           alert("error");
                   }
           },
           error: function(x,e){
                mostrar_error(x,e);
           }
        });
    }	
});
</script>