<div class="grid16">
<?php if($cartera){?>
    <div class="resultados"><?php echo $this->pagination->resultados();?></div>
    <div class="ordenar">Ordenar por: <?php echo $this->pagination->ordenar('nombre','Nombre del cliente');?> | <?php echo $this->pagination->ordenar('contacto','Nombre del contacto');?> | <?php echo $this->pagination->ordenar('id_cliente','Nit');?></div>
        <div id="paginacion_clientes" class="paginacion"><?php echo $this->pagination->create_links();?></div>
    <?php 
        //print_r($cartera);
        foreach($cartera as $cartera_individual):
        $cliente=$cartera_individual->cliente;
        ?>
    <div class="cliente">
        <div style="margin-top:20px; margin-bottom: 8px;"><h2 title="Nombre"><?php echo $cliente->nombre;?></h2><h3 title="Contacto"><?php echo $cliente->contacto;?></h3></div>
            <ul><li class="nit">Nit: <?php echo $cliente->id_cliente;?>
                </li>
                <li title="Telefono">Teléfono: <?php echo $cliente->telefono1;?></li>
                <li title="Direccion">Dirección: <?php echo $cliente->direccion;?></li>
                <li title="Ciudad">Ciudad: <?php echo $cliente->ciudad;?></li>
                <li>Cupo: <?php echo number_format($cliente->cupo,0,'.',',');?></li>                
            </ul>
            <div style="margin-top:20px" id="cartera_cliente">
            <table class="tabla">
            <thead>
            <tr><th style="height:30px;" title="Tipo de documento F: Factura NC:Nota crédito ND:Nota débito R:Recibo de caja">Tipo</th><th>Documento</th><th>Fecha</th><th>Vence</th><th title="Dias de vencido">Dias</th><th title="Número del vendedor">Vd</th><th>Valor Inicial</th><th>Saldo Anterior</th><th>Débitos Mes</th><th>Créditos Mes</th><th>Saldo Actual</th></tr>
            </thead>
            <tbody>
            <?php 
            $valores_iniciales=0;
            $saldos_anteriores=0;
            $debitos=0;
            $creditos=0;
            $saldos_actuales=0;
            foreach($cartera_individual->cartera as $dato):
            $clase='';
            $valores_iniciales=$valores_iniciales+$dato->valor_inicial;
            $saldos_anteriores=$saldos_anteriores+$dato->saldo_ant;
            $debitos=$debitos+$dato->cargo;
            $creditos=$creditos+$dato->abono;
            $saldos_actuales=$saldos_actuales+$dato->saldo;
            if($dato->dias > $configuracion->dias_vencimiento && $dato->saldo > 0){
                $clase='vencido';
            }else{
                if($dato->saldo==0){
                    $clase='cancelado';
                }else{
                    $clase='';
                }
            }
            ?>
            <tr class="<?php echo $clase;?>">
            <td><?php echo $dato->tipo;?></td>
            <td><?php echo $dato->id_documento;?></td>
            <td style="text-align:center;"><?php echo $dato->fecha;?></td>
            <td style="text-align:center;"><?php echo $dato->fecha_vence;?></td>
            <td title="Dias de vencido" style="text-align:center;"><?php echo $dato->dias;?></td>
            <td style="text-align:center;"><?php echo $dato->id_vendedor;?></td>
            <td style="text-align:right;"><?php echo number_format($dato->valor_inicial,0,'.',',');?></td>
            <td style="text-align:right;"><?php echo number_format($dato->saldo_ant,0,'.',',');?></td>
            <td style="text-align:right;"><?php echo number_format($dato->cargo,0,'.',',');?></td>
            <td style="text-align:right;"><?php echo number_format($dato->abono,0,'.',',');?></td>
            <td style="text-align:right;"><?php echo number_format($dato->saldo,0,'.',',');?></td></tr>
            <?php endforeach;?>
            </tbody>
            <tfoot style="font-weight:bold;">
            <tr style="background:#FFC"><td colspan="6">Totales</td><td style="text-align:right;"><?php echo number_format($valores_iniciales,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_anteriores,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($debitos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($creditos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_actuales,0,'.',',');?></td></tr>
            </tfoot>
            </table>
        </div>
    </div>
        <?php endforeach;?>    
        <div id="paginacion_clientes" class="paginacion"><?php echo $this->pagination->create_links();?></div>
<?php }else{
    ?><span style="display:block; margin-top:20px;" class="error">No se encontraron clientes con los datos buscados.</span>
    <?php }?>    
</div>
<div class="clear"></div>