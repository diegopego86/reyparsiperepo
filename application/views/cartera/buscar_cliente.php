<div style="margin-top:20px;">
<?php
if(!$cliente || $cliente==''){
	$cliente='Digite el nit o nombre del cliente';
}
echo form_label('Cliente: ','cliente');
echo form_input(array('name'=>'cliente','value'=>$cliente,'style'=>'width:400px;'));
echo anchor('pedidos/buscar_cliente','Buscar','class="boton" rel="filtrar_cliente"');
?>
</div>
<?php if($clientes){?>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<table id="resultados_buscar_clientes" class="tabla">
<thead>
<tr>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('id_cliente','Nit','title="Nit del cliente"');?></th>
    <th style="width:25%;"><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
    <th style="width:25%;"><?php echo $this->pagination->ordenar('contacto','Contacto');?></th>
    <th style="width:3%;"><?php echo $this->pagination->ordenar('id_vendedor','Vd');?></th>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('ciudad','Ciudad');?></th>
    <th style="width:5%;"><?php echo $this->pagination->ordenar('direccion','Dirección');?></th>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('telefono','Teléfono1');?></th>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('email','E-mail');?></th>
    <th style="width:8%;">Acción</th>
</tr>
</thead>
<tbody>
<?php foreach($clientes as $cliente):?>
<tr>
<td><?php echo $cliente->id_cliente;?></td>
<td><?php echo $cliente->nombre;?></td>
<td><?php echo $cliente->contacto;?></td>
<td><?php echo $cliente->id_vendedor;?></td>
<td><?php echo $cliente->ciudad;?></td>
<td><?php echo $cliente->direccion;?></td>
<td><?php echo $cliente->telefono1;?></td>
<td><?php echo $cliente->email;?></td>
<td style="text-align:center;"><?php echo anchor('pedidos/datos_cliente/'.$cliente->id_cliente,'Seleccionar','class="boton" id="'.$cliente->id_cliente.'" rel="boton_seleccionar_cliente"');?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
<?php }else{?>
<table class="tabla">
<thead>
<tr><th style="height:30px;">Nit</th><th>Nombre</th><th>Contacto</th></tr>
</thead>
<tbody>
<tr><td colspan="3"><span class="alerta"></span> No existen clientes con los datos de busqueda.</td></tr>
</tbody>
</table>
<?php }?>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>