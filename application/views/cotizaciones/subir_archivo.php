<div style="margin-top:30px;">
	<div style="width:50%;float:left;">
		<h2>Descargar formato</h2>
		<p style="margin:10px 0px; color:#999;"><?php echo anchor('documentos/Formato_cotizacion.xls','Descarga este formato de cotizacion, para poder importar la información.');?></p>
		<?php echo anchor('documentos/Formato_cotizacion.xls',' ','class="boton_descargar_excel"'); ?>
	</div>
	<div style="width:50%;float:left;">
		<h2>Subir el archivo</h2>
		<p style="margin-top:10px; color:#999;">La extensión del archivo debe ser .xls o .xlsx</p>
		<p style="margin-bottom:10px;color:#360;">Se recomienda importar el formato de la descarga.</p>
		<?php echo form_open_multipart('cotizaciones/leerExcel',array('target'=>'uno'));?>
		<input type="file" id="archivo_importar" name="archivo" size="20" />
		<?php 
			echo form_submit('importar','Importar'); 
			echo form_close();
		?>
	</div>
	<div class="clear"></div>
</div>