<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mostrar cotizacion</title>
<style>
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:small;
}
#contenido-cotizacion,#contenido-cotizacion table{
	font-family:Arial, Helvetica, sans-serif;
	font-size:0.9em;	
}
#contenido-cotizacion span.sub_label{
	font-size:0.9em;
	color:#999;
	display:block;
}
#contenido-cotizacion .seccion{
	margin:10px 0px;
	padding-bottom:10px;
	border-bottom:1px dotted #CCC;
}
#contenido-cotizacion .seccion h2{
	font-size:160%;
	font-weight:normal;
	margin:0 0 0.2em;	
}
#contenido-cotizacion ul,#contenido-cotizacion p{
	margin:0;
	padding:0;
}
#contenido-cotizacion ul.formulario{
	list-style:none;
}
#contenido-cotizacion ul.formulario li{
	margin:0;
	padding:6px 1% 9px;	
   -moz-box-sizing:    border-box;
   -webkit-box-sizing: border-box;
    box-sizing:        border-box;
}
#contenido-cotizacion .right{
	clear:none;
	float:right;
}
#contenido-cotizacion .left{
	float:left;
}
#contenido-cotizacion table.tabla{
	border-collapse:collapse;	
}
#contenido-cotizacion table.tabla thead tr{
	border-bottom:1px solid #000;
	padding:1px 1px;
}
#contenido-cotizacion table.tabla tbody tr{
	border-bottom:1px solid #CCC;
	padding:1px 2px;
}
#contenido-cotizacion table.tabla tbody td{
	vertical-align:text-top;
}
#contenido-cotizacion table.tabla thead th{
	padding-bottom:0px;
}
</style>
</head>
<body>
<div id="contenido-cotizacion" style="width:700px;">
	<div style="font-size:0.8em; margin-bottom:10px; text-align:right;">NIT. <?php echo $empresa->nit.' - '.$empresa->nombre;?></div>
	<div style="float:left; width:60%">
	    <table style="">
	    	<tr><td colspan="2" style="border-bottom:1px solid #777777;"><div style="font-weight:bold; font-size:1.2em; text-align:left;">CLIENTE</div></td></tr>
        	<tr><td style="width:80px;">Nit:</td><td><?php echo $encabezado->id_cliente;?></td></tr>
            <tr><td>Nombre:</td><td><?php echo $encabezado->nombre;?></td></tr>
            <tr><td>Contacto:</td><td><?php echo $encabezado->contacto;?></td></tr>
            <tr><td>Dirección:</td><td><?php echo $encabezado->direccion;?></td></tr>
            <tr><td>Ciudad:</td><td><?php echo $encabezado->ciudad;?></td></tr>
            <tr><td>Teléfono:</td><td><?php echo $encabezado->telefono1;?></td></tr>
            <tr><td>Vendedor:</td><td><?php echo $encabezado->id_vendedor.' - '.$encabezado->nombre_vendedor;?></td></tr>
            <tr><td>Plazo:</td><td><?php echo $encabezado->plazo;?></td></tr>
            <?php if($configuracion['listar_listas']){?>
            <tr><td>Lista:</td><td><?php echo $encabezado->lista;?></td></tr>
            <? }?>
			<?php if($configuracion['listar_retenciones']){?>
            <tr><td>Retención:</td><td>Retiene sobre la base</td></tr>
            <?php }?>
        </table>
    </div>
    <div style="width:40%; float:right" align="right">
    	<table align="right">
        <tr><td colspan="2" style="border-bottom:1px solid #777777;"><div style="font-weight:bold; font-size:1.2em; text-align:right;">Cotización <?php echo $encabezado->nota_2;?></div></td></tr>
        	<tr><td>Número:</td><td style="text-align:right;"><b><?php echo $encabezado->id_maestro;?></b></td></tr>
            <tr><td>Fecha:</td><td style="text-align:right;"><?php echo $encabezado->fecha;?></td></tr>
            <tr><td>Hora:</td><td style="text-align:right;"><?php echo $encabezado->hora;?></td></tr>
        </table>
    </div>
    <div style="clear:both;"></div>
    <div id="datos_productos" style="width:100%; margin-top:10px; float:left;">
        <table id="productos_cotizacion" class="tabla">
            <thead>
            <tr>
               <th height="30px;" style="width:3%; border-bottom:1px solid #777777;">-</th>
                <th style="width:12%; border-bottom:1px solid #777777;">CÓDIGO</th>
                <th style="width:8%; border-bottom:1px solid #777777;">CANTIDAD</th>
                <th style="width:42%; border-bottom:1px solid #777777;">DESCRIPCIÓN</th>
                <th style="width:10%; border-bottom:1px solid #777777;">PRECIO</th>
                <th style="width:5%; border-bottom:1px solid #777777;" title="Descuento">%DCTO</th>
                <th style="width:5%; border-bottom:1px solid #777777;" title="Iva">%IVA</th>
                <th style="width:15%; border-bottom:1px solid #777777;">VALOR NETO</th>
            </tr>
            </thead>
        <tbody>
        <?php $n=1;foreach($productos as $producto):?>
            <tr>
               <td style="border-bottom:1px solid #E4E4E4;"><?php echo $n;?></td>
               <td style="border-bottom:1px solid #E4E4E4;"><?php echo $producto->id_producto;?></td>
               <td style="text-align:right; padding-right:20px; border-bottom:1px solid #E4E4E4;"><?php echo number_format($producto->cantidad,0,'.',',');?></td>
               <td style="border-bottom:1px solid #E4E4E4;"><?php echo $producto->nombre;?></td>
               <td style="text-align:right; border-bottom:1px solid #E4E4E4;"><?php echo number_format($producto->precio,0,'.',',');?></td>
               <td style="text-align:right; border-bottom:1px solid #E4E4E4;"><?php echo number_format($producto->descuento,0,'.',',');?></td>
               <td style="text-align:right; border-bottom:1px solid #E4E4E4;"><?php echo number_format($producto->iva,0);?></td>
               <td style="text-align:right; border-bottom:1px solid #E4E4E4;"><?php echo number_format($producto->total,0,'.',',');?></td>
            </tr>
        <?php $n++; endforeach;?>
        </tbody>
        </table>
    </div>
    <div style="clear:both;margin-bottom:10px;"></div>
    <div style="margin-top:10px;float:left; width:70%;">
		<?php if($configuracion['observaciones']){?>
        <div style="padding-right:10px;"><div style="font-weight:bold;text-align:left;border-bottom:1px solid #777777;">OBSERVACIONES</div><p><?php echo $encabezado->nota_1;?></p></div>
        <?php }?>        
    </div>
    <div style="float:left; width:30%">
        <table class="subtotales" style="float:right; width:100%">
            <tr>
                  <td style="width:100px;" class="item">Subtotal:</td><td id="totales_subtotal" style="text-align:right;"><?php echo number_format($encabezado->subtotal_sin_dcto,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Descuento:</td><td id="totales_descuento" style="text-align:right;"><?php echo number_format($encabezado->descuento_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item">Neto:</td><td id="totales_neto" style="text-align:right;"><?php echo number_format($encabezado->subtotal,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Iva:</td><td id="totales_iva" style="text-align:right;"><?php echo number_format($encabezado->iva_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item"><b>Total =&gt;</b></td><td id="totales_total" style="text-align:right;"><b><?php echo number_format($encabezado->total,0,'.',',');?></b></td>
            </tr>
        </table>
    </div>
    <div style="clear:both;"></div>
</div>
</body>
</html>