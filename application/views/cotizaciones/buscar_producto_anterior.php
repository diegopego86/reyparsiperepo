<div style="margin-top:20px;">
<?php
echo form_label('Producto: ','producto');
echo form_input(array('name'=>'producto','value'=>set_value('producto',$producto),'style'=>'width:400px;'));
//echo form_submit('enviar','Guardar');
echo anchor('cotizaciones/buscar_producto','Buscar','class="boton" id="boton_buscar_producto"');
?>
</div>
<?php if($productos){?>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div id="paginacion_productos" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<table id="resultados_buscar_productos" class="tabla">
<thead>
<tr>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('id_producto','Código','title="Código del producto"');?></th>
    <th style="width:34%;"><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
    <?php if($configuracion['buscar_productos_mostrar_tipo']){?>
    <th style="width:6%;"><?php echo $this->pagination->ordenar('tipo','Tipo');?></th>
    <?php }?>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('referencia','Ref','title="Referencia"');?></th>
    <?php if($configuracion['buscar_productos_mostrar_unidad']){?>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('unidad','Und','title="Unidad de empaque"');?></th>
    <?php }?>
    <?php if($configuracion['buscar_productos_mostrar_existencia']){?>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('stock','Exis','title="Existencia en inventario"');?></th>
    <?php }?>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('precio1','Precio');?></th>
    <th style="width:10%;">Acción</th>
</tr>
</thead>
<tbody>
<?php foreach($productos as $producto):?>
<tr><td title="codigo"><?php echo $producto->id_producto;?></td><td><?php echo $producto->nombre;?></td>
<?php if($configuracion['buscar_productos_mostrar_tipo']){?>
<td style="text-align:center;"><?php echo $producto->tipo;?></td>
<?php }?>
<td><?php echo $producto->referencia;?></td>
    <?php if($configuracion['buscar_productos_mostrar_unidad']){?>
<td style="text-align:right;"><?php echo $producto->unidad;?></td>
<?php }?>
    <?php if($configuracion['buscar_productos_mostrar_existencia']){?>
<td style="text-align:right;"><?php echo $producto->stock;?></td>
<?php }?>
<td style="text-align:right;"><?php echo $producto->precio1;?></td>
<td style="text-align:center;"><?php echo anchor($producto->id_producto,'Agregar','class="boton" rel="boton_seleccionar_producto"');?></td></tr>
<?php endforeach;?>
</tbody>
</table>
<?php }else{?>
<table class="tabla">
<thead>
<tr><th style="height:30px;">Nit</th><th>Nombre</th><th>Contacto</th></tr>
</thead>
<tbody>
<tr><td colspan="3"><span class="alerta"></span> No existen productos con los datos de busqueda.</td></tr>
</tbody>
</table>
<?php }?>
<div id="paginacion_productos" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#resultados_buscar_productos th a').click(function(){
		$('#cargando').html('<span></span> Ordenando los resultados');
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
			$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});
	$('#paginacion_productos a').click(function(){
		$('#cargando').html('<span></span> Cargando datos de la busqueda');
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});
	$('a[rel=boton_cerrar_busqueda_productos]').click(function(){
		  $('#buscarProducto input[name=producto]').val('');
		  $('#buscarProducto').html('');
		  return false;
	});
	$('a[rel=boton_seleccionar_producto]').click(function(){
		$('#cargando').html('<span></span> Agregando el producto al cotizacion');
		$('#cargando').removeClass('correcto error');
		$('#cargando').show();
		var fila=$('#buscarProducto').attr('item');
		$('tr#'+fila).find('input[name=codigo]').val($(this).parent().parent().find('[title=codigo]').text());
		$('tr#'+fila).find('input[name=codigo]').change();
		$('#cargando').hide();
		$('#buscarProducto').dialog('destroy');
		$('#buscarProducto').remove();
		$('#buscarProducto').dialog('close');
		return false;
	});
	var buscar=$('#buscarProducto input[name=producto]').val();
	if(buscar!=''){
		$('#buscarProducto').highlight(buscar);
	}
	$('#buscarProducto input[name=producto]').focus(function(){
		var valor=$(this).val();
		if(valor=='Digite el código o nombre del producto'){
			$(this).val('');
		}
	});
	$('#buscarProducto input[name=producto]').focusout(function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el código o nombre del producto');
		}
	});
	$('#boton_buscar_producto').click(function(){
		$('#cargando').html('<span></span> Cargando listado de productos.');
		$('#cargando').show();
		var producto =$('#buscarProducto input[name=producto]').val();
		if(producto=='Digite el código o nombre del producto'){
			$('#buscarProducto input[name=producto]').val('');
			producto='';
		}
		var form_data={
			producto: producto,
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: "<?php echo site_url('cotizaciones/buscar_producto');?>",
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
					$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});	
	$('#buscarProducto input[name=producto]').keyup(function(event){
		if (event.keyCode == '13') {
			$('#boton_buscar_producto').click();
		}
		return false;
	});
});
</script>