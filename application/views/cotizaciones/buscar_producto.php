<div style="margin-top:20px;">
<?php
echo form_input(array('name'=>'producto','value'=>set_value('producto',$producto),'style'=>'width:400px;'));
echo anchor('pedidos/buscar_producto','Buscar','class="boton" id="boton_buscar_producto" onclick="return false;"');
?>
</div>
<?php if($productos){?>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div id="paginacion_productos" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<?php /*
<?php echo $this->pagination->ordenar('id_producto','Ordenar por Código','title="Código del producto" class="boton"');?>
<?php echo $this->pagination->ordenar('nombre','Ordenar por Nombre','class="boton"');?>
<?php echo $this->pagination->ordenar('referencia','Ordenar por Referencia','title="Referencia" class="boton"');?>
 * 
 */
?>
<ul id="listado_productos" class="container16" style="margin-left:0%; margin-right: 0%; width:100%">
<?php $n=0;foreach($productos as $producto): ?>
<li title="codigo" class="grid3" style="position:relative;">
<?php 
$ruta=$this->config->item('ruta_imagenes');
$url=@fopen($ruta.$producto->id_producto.".JPG",'r');
?>
    <div class="imagen" style="min-height: 120px;">
    <?php
if($url)
{
//$default_image =”…/directoryFolder/junal.jpg”;
?>
<?php echo anchor($ruta.$producto->id_producto.'.JPG',img(array('src'=>$ruta.$producto->id_producto.'.JPG','border'=>0,'width'=>'190px')),'title="Foto del producto '.$producto->id_producto.' - '.$producto->nombre.'" target="_blank" rel="mostrar_foto"');?>
<?php }else{
echo anchor('img/sin_foto.jpg',img(array('src'=>'img/sin_foto.jpg','border'=>0,'width'=>'100%')),'title="Foto del producto '.$producto->id_producto.' - '.$producto->nombre.'" target="_blank" rel="mostrar_foto"');?>
<?php }?>
    </div>
<div title="codigo" class="codigo"><?php echo $producto->id_producto;?></div>
<?php if($configuracion['productos_mostrar_referencia']==1){?>
<div title="Referencia" class="referencia">Ref. 
    <?php 
        echo $producto->referencia;
    ?>
</div>
<?php }?>
<?php if($configuracion['productos_mostrar_tipo']==1){?>
<div title="Tipo" class="tipo">Tipo: 
    <?php 
        echo $producto->tipo;
    ?>
</div>
<?php }?>
<div title="nombre" class="nombre"><?php echo $producto->nombre;?></div>
<?php if($configuracion['productos_mostrar_ude']==1 && $producto->unidad){?>
<div title="Unidad" class="unidad">Unidad de empaque: 
    <?php echo $producto->unidad;      
    ?>
</div>
<?php }?>
<?php if($configuracion['productos_mostrar_existencia']==1){?>
<div title="Existencia" class="existencia">Existencia: 
    <?php $existencia=$producto->stock;
        $decimal=strstr($existencia,'.');
        if($decimal=='.00'){
            echo number_format($existencia,0,',','.');
        }else{
            echo number_format($existencia,2,',','.');
        }
    ?>
</div>
<?php }?>
<?php if($configuracion['productos_mostrar_precio']==1){?>
<div title="precio" class="precio">$ 
    <?php 
        $precio=$producto->{'precio'.($configuracion['productos_lista_numero_por_defecto'])};
        $decimal=strstr($precio,'.');
        if($decimal=='.00'){
            echo number_format($precio,0,',','.');
        }else{
            echo number_format($precio,2,',','.');
        }        
    ?>
</div>
<?php }?>
<?php if($configuracion['productos_mostrar_listas']==1){?>
<div title="Listas">
    <ul>
        <li>
        <?php 
            $precio=$producto->precio1;
            $decimal=strstr($precio,'.');
            if($decimal=='.00'){
                echo 'Lista 1: $'.number_format($precio,0,',','.');
            }else{
                echo 'Lista 1: $'.number_format($precio,2,',','.');
            }
        ?>
        </li>
        <li>
        <?php
        $precio=$producto->precio2;
        $decimal=strstr($precio,'.');
        if($decimal=='.00'){
            echo 'Lista 2: $'.number_format($precio,0,',','.');
        }else{
            echo 'Lista 2: $'.number_format($precio,2,',','.');
        }
        ?>
        </li>
        <li>
        <?php
        $precio=$producto->precio3;
        $decimal=strstr($precio,'.');
        if($decimal=='.00'){
            echo 'Lista 3: $'.number_format($precio,0,',','.');
        }else{
            echo 'Lista 3: $'.number_format($precio,2,',','.');
        }
        ?>
        </li>
        <li>
        <?php
        $precio=$producto->precio4;
        $decimal=strstr($precio,'.');
        if($decimal=='.00'){
            echo 'Lista 4: $'.number_format($precio,0,',','.');
        }else{
            echo 'Lista 4: $'.number_format($precio,2,',','.');
        }
        ?>
        </li>
    </ul>
</div>
<?php }?>
<div title="agregar" class="agregar"><?php echo anchor($producto->id_producto,'Agregar','class="boton" rel="boton_seleccionar_producto"');?></div></li>
<?php 
$n++;
endforeach;
?>
</ul>
<div class="clear"></div>
<?php }else{?>
<div style="color:#900; font-style:italic;">No se encontraron productos con la búsqueda</div>
<?php }?>
<div id="paginacion_productos" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>