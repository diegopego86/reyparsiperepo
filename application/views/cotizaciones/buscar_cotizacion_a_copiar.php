<div style="margin-top:20px;">
<?php
echo form_label('Cliente: ','cliente');
if($cliente==''){$cliente='Digite el nit o nombre del cliente';}
echo form_input(array('name'=>'cliente','value'=>set_value('cliente',$cliente),'style'=>'width:400px;'));
echo anchor('cotizaciones/buscar_cotizacion_a_copiar','Buscar','class="boton" rel="buscar_cotizacion_a_copiar"');
?>
</div>
    <div id="resultados_cotizaciones">
        <div class="resultados"><?php echo $this->pagination->resultados();?></div>
        <div class="paginacion"><?php echo $this->pagination->create_links();?></div>
        <table class="tabla">
            <thead>
            <tr>
            <th style="width:3%;"></th>
                <th style="width:5%;"><?php echo $this->pagination->ordenar('id_maestro','Num','title="Número"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('id_cliente','Nit');?></th>
                <th style="width:21%;"><?php echo $this->pagination->ordenar('nombre_cliente','Nombre del cliente');?></th>   
                <th style="width:5%;"><?php echo $this->pagination->ordenar('id_vendedor','vd','title="Id Vendedor"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('subtotal_sin_dcto','Subtotal','title="Subtotal sin descuento"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('iva_total','Iva','title="Iva"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('total','Total','title="Total"');?></th>
                <th style="width:8%;"><?php echo $this->pagination->ordenar('fecha','Fecha');?></th>
                <th style="width:8%;">-</th>
                </tr>
            </thead>
            <tbody>
        <?php if($cotizaciones){
                $n=$desde;
                foreach($cotizaciones as $cotizacion):
            ?>
            <tr>
                <td class="item"><?php echo $n?></td><td><?php echo $cotizacion->id_maestro;?></td>
                <td title="nit"><?php echo $cotizacion->id_cliente;?></td>
                <td title="nombre"><?php if($cotizacion->nombre_cliente){echo $cotizacion->nombre_cliente;}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
                <td style="text-align:center;"><?php echo $cotizacion->id_vendedor;?></td>
                <td style="text-align:right;"><?php echo number_format($cotizacion->subtotal_sin_dcto,'0',',',".");?><?php if($cotizacion->descuento_total>0){ echo '<div class="descuento">-dcto '.number_format($cotizacion->descuento_total,'0',',',".").'</div>';}?></td>
                <td style="text-align:right;"><?php echo number_format($cotizacion->iva_total,'0',',',".");?></td>
                <td style="text-align:right;"><?php echo number_format($cotizacion->total,'0',',',".");?></td>
                <td style="color:#930; text-align:center; font-size:0.7em;"><?php echo $cotizacion->fecha;?></td>
                <td style="text-align:center;"><?php echo anchor_popup('cotizaciones/ver_cotizacion/'.$cotizacion->id_maestro.'/'.sha1($cotizacion->id_maestro.$seguridad), '<span class="iconos_blanco ver"></span>', array('width'=> '800','height'     => '600','scrollbars' => 'yes','status'=> 'yes','resizable'=> 'yes','directories'=>'no','location'=>'no','class'=>'boton peque')).' '.anchor('cotizaciones/copiar_productos_cotizacion/'.$cotizacion->id_maestro,'<span class="iconos_blanco copiar"></span>','class="boton peque" rel="copiar_cotizacion" title="Copiar información del cotizacion"');?></td>
            </tr>
            <?php
                $n++;
                endforeach;
            ?>
        <?php }else{?>
        	<tr><td colspan="10"><span class="alerta"></span> No tiene cotizaciones realizados para copiar.</td></tr>
        <?php }?>
            </tbody>
        </table>
        <div class="paginacion"><?php echo $this->pagination->create_links();?></div>
    </div>