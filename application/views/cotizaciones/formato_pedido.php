<style>
.ui-dialog{
	font-size:90%;
}
</style>
<div id="form">
	<div class="grid_16">    
        <h1>Nuevo cotizacion</h1>
        <div>
        <?php echo anchor('cotizaciones/index','Volver','class="boton"');?>
        <?php echo anchor('cotizaciones/index','Guardar y enviar','class="boton"');?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="grid_16" style="margin-top:20px;">
    	<div class="seccion"><h2>Datos del cliente</h2></div>
        <div><?php echo form_input(array('name'=>'nit','size'=>'15')); echo ' '.anchor('','Buscar  <span class="iconos_blancos buscar"></span>','class="boton" rel="boton_buscar_cliente"');?><span style="margin-top:5px;" class="sub_label">Nit</span></div>
        <div id="datos_cliente">
            <ul class="formulario">
                <li class="left cuadro noderecha" style="width:30%;">-<span class="sub_label">Nombre del cliente</span></li>
                <li class="left cuadro" style="width:30%;">-<span class="sub_label">Contacto</span></li>
                <li class="left cuadro noizquierda" style="width:40%;">-<span class="sub_label">E-mail</span></li>
                <div class="clear"></div>
                <li class="left cuadro noderecha noarriba" style="width:40%;">-<span class="sub_label">Dirección</span></li>
                <li class="left cuadro noarriba" style="width:30%;">-<span class="sub_label">Ciudad</span></li>
                <li class="left cuadro noizquierda noarriba" style="width:30%;">-<span class="sub_label">Teléfono</span></li>
                <div class="clear"></div>
                <li class="left cuadro noderecha noarriba" style="width:45%;">-<span class="sub_label">Asesor</span></li>
                <li class="left cuadro noarriba" style="width:10%;">-<span class="sub_label">Plazo</span></li>
                <li class="left cuadro noizquierda noarriba" style="width:10%;">-<span class="sub_label">Lista</span></li>
                <li class="left cuadro noizquierda noarriba" style="width:35%;">-<span class="sub_label">Retención</span></li>
                <div class="clear"></div>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
    <div class="grid_16" id="datos_productos">
    	<div class="seccion"><h2>Productos</h2></div>
        <div id="datosProducto">
            <table id="productos_cotizacion" class="tabla">
                <thead>
                <tr>
                   <th height="30px;" style="width:3%;"></th>
                    <th style="width:14%;">Código</th>
                    <th style="width:37%;">Nombre del producto</th>
                    <th style="width:8%;">Cantidad</th>
                    <th style="width:10%;">Precio</th>
                    <th style="width:5%;" title="Descuento">Dcto</th>
                    <th style="width:5%;" title="Iva">Iva</th>
                    <th style="width:15%;">Total</th>
                    <th style="width:3%;"></th>
                </tr>
                </thead>
            <tbody>
				<tr class="agregar"><td class="item numero">-</td><td colspan="8"><?php echo form_input(array('name'=>'items_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_items" class="boton"');?></td></tr>
            </tbody>
            <tfoot>            
 	           <tr><td id="total_productos_cotizacion" class="numero" colspan="9"><span class="alerta"></span> No ha agregado productos al cotizacion.</td></tr>
            </tfoot>
            </table>
        </div>
    </div>
    <div class="clear"></div>
        <div class="grid_10">
    		<div style="padding:10px; background:#F4F4F4;"><?php echo form_label('Observaciones despachos: ','observaciones_despacho'); echo form_textarea(array('name'=>'observaciones_despacho','style'=>'width:98%;','rows'=>4));?> </div>
           	<div style="padding:10px; background:#E4E4E4;">
			<?php echo form_label('Observaciones cartera: ','observaciones_cartera'); echo form_textarea(array('name'=>'observaciones_despacho','style'=>'width:98%;','rows'=>4));?></div>
           	<div style="padding:10px; background:#F4F4F4;">
			<?php echo form_label('Observaciones facturación: ','observaciones_facturacion'); echo form_textarea(array('name'=>'observaciones_facturacion','style'=>'width:98%;','rows'=>4));?></div>
    	</div>
        <div class="grid_6">
            <table class="tabla">
                <tr>
                    <td style="width:100px;" class="item">Subtotal</td><td id="totales_subtotal" style="text-align:right;"></td>
                </tr>
                <tr class="altrow">
                    <td style="width:100px;" class="item">Descuento</td><td id="totales_descuento" style="text-align:right;"></td>
                </tr>
                <tr>
                    <td style="width:100px;" class="item">Neto</td><td id="totales_neto" style="text-align:right;"></td>
                </tr>
                <tr class="altrow">
                    <td style="width:100px;" class="item">Iva</td><td id="totales_iva" style="text-align:right;"></td>
                </tr>
                <tr>
                    <td style="width:100px;" class="item">Total</td><td id="totales_total" style="text-align:right;"></td>
                </tr>
            </table>
        </div>
        <div class="clear"></div>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numberformatter-1.1.2.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.calculator.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form input[name=cliente]').focus(function(){
		var valor=$(this).val();
		if(valor=='Digite el nit o nombre del cliente'){
			$(this).val('');
		}
	});
	$('#form input[name=cliente]').focusout(function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el nit o nombre del cliente');
		}
	});
	/*$('#form input[name=nit]').keyup(function(event){
		if (event.keyCode == '13') {
			$('#form input[name=nit]').change();
		}
		return false;
	});*/
	$('#form input[name=nit]').change(function(event){
		$('#cargando').html('<span></span> Buscando datos del cliente. Por favor espere...');
		$('#cargando').show();
		var form_data={
			cliente: $(this).val(),
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: "<?php echo site_url('cotizaciones/datos_cliente');?>",
		   data: form_data,
		   success: function(msg){
			   if(msg){
				   var error=msg.substr(0,5);
				   if(error=='Error'){
						$('#cargando').html('<span class="icono"></span> '+msg.substr(6,msg.length));
						$('#cargando').addClass('error');
					   $('#datos_cliente').html('<ul class="formulario"><li class="left cuadro noderecha" style="width:30%;">-<span class="sub_label">Nombre del cliente</span></li><li class="left cuadro" style="width:30%;">-<span class="sub_label">Contacto</span></li><li class="left cuadro noizquierda" style="width:40%;">-<span class="sub_label">E-mail</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:40%;">-<span class="sub_label">Dirección</span></li><li class="left cuadro noarriba" style="width:30%;">-<span class="sub_label">Ciudad</span></li><li class="left cuadro noizquierda noarriba" style="width:30%;">-<span class="sub_label">Teléfono</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:45%;">-<span class="sub_label">Asesor</span></li><li class="left cuadro noarriba" style="width:10%;">-<span class="sub_label">Plazo</span></li><li class="left cuadro noizquierda noarriba" style="width:10%;">-<span class="sub_label">Lista</span></li><li class="left cuadro noizquierda noarriba" style="width:35%;">-<span class="sub_label">Retención</span></li><div class="clear"></div></ul>');
					   $('#cargando').delay(1200).fadeOut('slow',function(){ $(this).removeClass('correcto error');});
				   }else{
						$('#cargando').html('Datos encontrados.');
						$('#cargando').addClass('correcto');
						$('#datos_cliente').html(msg);
						$('#cargando').delay(200).fadeOut('slow',function(){ $(this).removeClass('correcto error');});
				   }					
			   }else{
				   alert(msg);
			   }
		   }
		});
	});
	$('a[rel=boton_buscar_cliente]').click(function(){
		$('#contenido').append('<div id="buscarCliente" title="Buscar cliente"></div>');
		$('#cargando').html('<span></span> Cargando listado de productos.');
		$('#cargando').show();
		var form_data={
			cliente: '',
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: "<?php echo site_url('cotizaciones/buscar_cliente');?>",
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
					$('#buscarCliente').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });					
		$("#buscarCliente").dialog({
				resizable: false,
				width: 800,
				height:480,
				modal: true,
				buttons: {
					'Cerrar': function() {
						$(this).dialog('destroy');
						$('#buscarCliente').remove();
						$(this).dialog('close');
					}
				}
		});
		//alert($(this).parent().parent().find('input[name=codigo]').val());
		return false;
	});
	$(document).keydown(function(event){
		//alert(event.keyCode);
		if (event.keyCode == 8 && (event.srcElement.type!= "text" && event.srcElement.type!= "textarea" && event.srcElement.type!= "password")) {
		// When backspace is pressed but not in form element
		return false;
			cancelKey(event);
		}
		else if (event.keyCode == 116) {
		// When F5 is pressed
			cancelKey(event);
		} 							   
	});
	function cancelKey(e) {
		if(!e) var e = window.event;
		if (e.preventDefault) {
			e.preventDefault();
		}
		e.cancelBubble = true;
		e.returnValue = false;
		e.keyCode = 0;

		//e.stopPropagation works in Firefox.
		if (e.stopPropagation) {
			e.stopPropagation();
			e.preventDefault();
		}
		return false;
	}
	$('input[name=items_agregar]').keyup(function(event){
		if (event.keyCode == '13') {
		  $('a[rel=boton_agregar_items]').click();
		}
		return false;
	});
	$('a[rel=boton_agregar_items]').click(function(){
		$('#cargando').html('Agregando campos. Por favor espere...');
		$('#cargando').show();
		var n=0;
		n=parseInt($('#productos_cotizacion tbody tr').length);
		var cantidad=$('input[name=items_agregar]').val();
		if(!cantidad || cantidad==0){
			cantidad=1;
		}
		var clase="";
		for(var f=0;f<cantidad;f++){
			if(n % 2){
				clase="";
			}else{
				clase="altrow";
			}
			$('<tr id="fila_'+n+'" class="'+clase+'" style="display:none;">'+'<td id="numero_item_'+n+'" class="item numero">'+n+'</td>'+
						'<td><?php echo form_input(array('name'=>'codigo','size'=>'10')); echo anchor('','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="buscar_producto"');?></td>'+
						'<td id="nombre_item_'+n+'"></td>'+
						'<td style="text-align:center;"><?php echo form_input(array('name'=>'cantidad','size'=>8,'value'=>'0'));?><div class="ude" title="Unidad de empaque"></div></td>'+
						'<td style="text-align:center;"><?php echo form_input(array('name'=>'precio','size'=>'12','style'=>'text-align:right;','value'=>'0.00'));?>'+
						'<div class="listas" title="Listas de precios"><ul></ul></div>' +
						'</td>'+
						'<td style="text-align:center;"><?php echo form_input(array('name'=>'dcto','size'=>2,'value'=>'0.00'));?></td>'+
						'<td style="text-align:center;" id="iva_item_'+n+'"><?php echo '';?></td>'+
						'<td class="total_producto" id="total_item_'+n+'">0.00</td>'+
						'<td><a href="" rel="eliminar_producto" title="Eliminar"><span class="iconos_rojo eliminar"></span></a></td></tr>').insertBefore($('#productos_cotizacion tbody tr.agregar'));
			var fila=$('tr#fila_'+n);
			n++;
				//Se formatean los campos para que acepten solo numeros y se les aplica la funcion de q al cambiar actualicen los totales.
				
				fila.find("input[name=precio]").numeric();
				fila.find("input[name=cantidad]").numeric();
				fila.find("input[name=dcto]").numeric();
				fila.find("input[name=precio]").bind('change',function(){
					recalcularTodo();
				});
				fila.find("input[name=cantidad]").bind('change',function(){
					recalcularTodo();					
				});
				fila.find("input[name=dcto]").bind('change',function(){
					recalcularTodo();					
				});
				fila.find('a[rel=buscar_producto]').click(function(){
					var campo=$(this).parent().parent().attr('id');
					$('#contenido').append('<div id="buscarProducto" title="Buscar producto" item="'+campo+'"></div>');
					$('#cargando').html('<span></span> Cargando listado de productos.');
					$('#cargando').show();
					var form_data={
						ajax:'1'
					};
					$.ajax({
					   type: "POST",
					   url: "<?php echo site_url('cotizaciones/buscar_producto');?>",
					   data: form_data,
					   success: function(msg){
							$('#cargando').hide();
						   if(msg){				   
								$('#buscarProducto').html(msg);
						   }else{
							   alert("error");
						   }
					   }
					 });					
					$("#buscarProducto").dialog({
							resizable: false,
							width: 800,
							height:480,
							modal: true,
							buttons: {
								'Cerrar': function() {
									$(this).dialog('destroy');
									$('#buscarProducto').remove();
									$(this).dialog('close');
								}
							}
					});
					//alert($(this).parent().parent().find('input[name=codigo]').val());
					return false;
				});
				var cantidad_anterior=0;
				var precio_anterior=0;
				var dcto_anterior=0;
				fila.find("input[name=codigo]").bind('change',function(){
					$('#cargando').html('Cargando datos del producto.');
					$('#cargando').show();
					var url='<?php echo site_url('cotizaciones/datos_producto');?>';
					var fila=$(this).parent().parent();
					var form_data={
						codigo: $(this).val(),
						ajax:1
					};
					$.ajax({
					   type: "POST",
					   url: url,
					   dataType: 'json',
					   data: form_data,
					   success: function(msg){
						   if(msg){
							   $('#cargando').hide();
							   //$('#cargando').delay(200).fadeOut('slow',function(){ $(this).removeClass('correcto error');});
							   if (typeof msg['precio1'] == "undefined"){
								   $('#cargando').html('El producto no existe.');							  
								   $('#cargando').addClass('error');
								   fila.find('input[name=codigo]').addClass('error');
								   fila.find('[id^=nombre_item_]').text('El producto no existe');
								   fila.find('input[name=cantidad]').val('0');
								   fila.find('input[name=dcto]').val('0.00');
								   fila.find('input[name=precio]').val('0.00');
								   fila.find('[id^=iva_item_]').text('0.00');
								   fila.find('input[name=codigo]').select();
							   }else{
								   $('#cargando').html('Producto agregado.');							  
								   $('#cargando').addClass('correcto');
								   fila.find('input[name=codigo]').removeClass('error');
								   fila.find('[id^=nombre_item_]').text(msg['nombre']);
								   fila.find('input[name=cantidad]').val('0');
								   fila.find('[name=dcto]').val('0.00');
								   fila.find('[name=precio]').val(msg['precio1']);
								   fila.find('[id^=iva_item_]').text(msg['iva']);
								   fila.find('input[name=cantidad]').select();
								   var valor_cantidad='0.00';
								   var valor_precio=msg['precio1'];
								   var valor_dcto='0.00';
								   fila.find('input[name=cantidad]').bind('blur',function(){
										if(valor_cantidad!=$(this).val()){
											recalcularTodo();
											$(this).parent().parent().find('input[name=precio]').select();
											$(this).unbind('blur');
										}
									});
								   fila.find('input[name=precio]').bind('blur',function(){
										if(valor_precio!=$(this).val()){
											recalcularTodo();
											$(this).parent().parent().find('input[name=dcto]').select();
											$(this).unbind('blur');
										}
									});
								   fila.find('input[name=dcto]').bind('blur',function(){
										if(valor_dcto!=$(this).val()){
											recalcularTodo();
											//fila.find('input[name=precio]').select();
											$(this).unbind('blur');
										}
									});
							   }							   
						   }else{
							   $('#cargando').html('Se presentaron errores al intentar cargar la información del producto. Inténtelo nuevamente.');													
							   $('#cargando').addClass('error');
								$('#cargando').delay(1200).fadeOut('slow',function(){ $(this).removeClass('correcto error');});
						   }
					   }
					});
				});
				fila.find('a[rel=eliminar_producto]').unbind('click');
				fila.find('a[rel=eliminar_producto]').bind('click',function(){
					$('#cargando').html('Eliminando item.');
					$('#cargando').show();
					var fila=$(this).parent().parent();
						  fila.css({background:"#FFD5D5",color:"#CC0000"});
						  fila.animate({
							opacity: 0.5
						  }, 200, function() {				  
							  fila.remove();
								var productos_agregados=parseInt($('#productos_cotizacion >tbody >tr').length);
								var n=1;
								$('#productos_cotizacion >tbody >tr').each(function(){
									if(n % 2){
										$(this).removeClass('altrow');
									}else{
										$(this).addClass('altrow');
									}
									$(this).find('[id^=total_item_]').attr('id','total_item_'+n);
									$(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
									$(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
									$(this).find('[id^=numero_item_]').html(n);
									n++;
								});
								$('#form input[name=productos_agregados]').val(productos_agregados);
								$('#total_productos_cotizacion').html('Total de productos en el cotizacion '+productos_agregados);
								if($('#productos_cotizacion >tbody >tr').length==0){
									$('#total_productos_cotizacion').html('<span class="alerta"></span> No ha agregado productos al cotizacion.'); 
								}
								recalcularTodo();
								$('#cargando').hide();
						  });			
						return false;
				});
				fila.fadeIn('slow');
		}
		$('input[name=items_agregar]').val(1);
		$('#cargando').hide();
		return false;
	});
	recalcularTodo();
	function formatear(){
		$("input[name=cantidad]").format({format:"#,###", locale:"us"});
		$("input[name=dcto]").format({format:"#,###.00", locale:"us"});
		$("input[name=precio]").format({format:"#,###.00", locale:"us"});
		$("[id^=total_item_]").format({format:"#,###.00", locale:"us"});
	};
	function formatearTotales(){		
		$("#totales_subtotal").format({format:"$ #,###.00", locale:"us"});
		$("#totales_descuento").format({format:"$ #,###.00", locale:"us"});
		$("#totales_neto").format({format:"$ #,###.00", locale:"us"});
		$("#totales_iva").format({format:"$ #,###.00", locale:"us"});
		$("#totales_total").format({format:"$ #,###.00", locale:"us"});
	}
	function recalcularTodo(){		
		$('#cargando').html('Recalculando totales del cotizacion.');
		$('#cargando').show();
		var totales_subtotal=0;
		var totales_dcto=0;
		var totales_iva=0;
		var subtotal=0;
		var dcto=0;
		var iva=0;
		var cantidades=$("input[name=cantidad]").parseNumber();
		var precios=$("input[name=precio]").parseNumber();
		var descuentos=$("input[name=dcto]").parseNumber();
		var ivas=$("[id^=iva_item_]").parseNumber();
		for(n=0;n<cantidades.length;n++){
			subtotal=cantidades[n]*precios[n];
			dcto=cantidades[n]*(precios[n] * descuentos[n]/100);
			iva=(subtotal-dcto)*(ivas[n]/100);
			totales_subtotal=totales_subtotal+subtotal;
			totales_dcto=totales_dcto+dcto;
			totales_iva=totales_iva+iva;
		}
		var totales_neto=totales_subtotal-totales_dcto;
		var totales_total=totales_neto+totales_iva;
		$("#totales_subtotal").text(totales_subtotal);
		$("#totales_descuento").text(totales_dcto);
		//$("#totales_neto").text(totales_neto);
		$("#totales_iva").text(totales_iva);
		$("#totales_total").text(totales_total);
		
		$("[id^=total_item_]").calc(
			// the equation to use for the calculation
			"(cantidad * precio) - (cantidad * (precio * dcto / 100))",
			// define the variables used in the equation, these can be a jQuery object
			{
				cantidad: $("input[name=cantidad]"),
				precio: $("input[name=precio]"),
				dcto: $("input[name=dcto]")
			},
			// define the formatting callback, the results of the calculation are passed to this function
			function (s){
				// return the number as a dollar amount				
				return s.toFixed(2);
			},
			// define the finish callback, this runs after the calculation has been complete
			function ($this){
				// sum the total of the $("[id^=total_item]") selector
				var neto = $this.sum();
				$("#totales_neto").text(
					// round the results to 2 digits
					neto.toFixed(2)					
				);			
			}			
		);
		$('#cargando').hide();
		formatear();
		formatearTotales();
	}
	$('input[name=items_agregar]').numeric();
   	$('a[rel=boton_agregar_items]').click();
});
</script>