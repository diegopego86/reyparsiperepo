<style>
    .ui-datepicker,.ui-dialog {
            font-size: 90%;
    }

    .alerta .ui-helper-clearfix button.ui-state-default {
            border: 1px solid #900;
            color: #FADDDE;
            background: #980C10;
            background: -webkit-gradient(linear, left top, left bottom, from(#ED1C24),to(#AA1317) );
            background: -moz-linear-gradient(center top, #ED1C24, #AA1317) repeat scroll 0 0 transparent;
            filter: progid:DXImageTransform.Microsoft.gradient (startColorstr = '#ED1C24', endColorstr ='#AA1317' );
    }

    .alerta .ui-helper-clearfix button.ui-state-default:hover {
            background: #AA1317;
            background: -webkit-gradient(linear, left top, left bottom, from(#AA1317),to(#ED1C24) );
            background: -moz-linear-gradient(center top, #AA1317, #ED1C24) repeat scroll 0 0 transparent;
            filter: progid:DXImageTransform.Microsoft.gradient ( startColorstr = '#AA1317', endColorstr = '#ED1C24');
            color: #DE898C;
    }
    .dialog_confirm button.ui-state-default{
            border:1px solid #900;
            color:#FADDDE;	
            background: #980C10;
            background: -webkit-gradient(linear, left top, left bottom, from(#ED1C24), to(#AA1317));
            background:-moz-linear-gradient(center top , #ED1C24, #AA1317) repeat scroll 0 0 transparent;
            filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ED1C24', endColorstr='#AA1317');	
    }
    .dialog_confirm button.ui-state-default:hover{
            background: #AA1317;
            background: -webkit-gradient(linear, left top, left bottom, from(#AA1317), to(#ED1C24));
            background:-moz-linear-gradient(center top , #AA1317, #ED1C24) repeat scroll 0 0 transparent;
            filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#AA1317', endColorstr='#ED1C24');	
            color:#DE898C;
    }
</style>
<div id="cotizacion_contenido" class="form">
    <div class="grid16">
        <h1>Nuevo cotizacion</h1>
        <div><?php echo anchor('','Importar datos desde archivo de excel','class="boton" rel="importar_excel" onclick="return false;"');?> 
        <?php echo anchor('','Copiar productos de otras cotizaciones','class="boton" rel="boton_copiar_desde_cotizacion" onclick="return false;"');?>
        <?php echo anchor('cotizaciones/guardar','Guardar definitivo y enviar','class="boton naranja" style="float:right;" rel="guardar_cotizacion" onclick="return false;"');?>
        <?php echo anchor('cotizaciones/guardar_borrador','Guardar Borrador','class="boton" style="float:right;margin-right:2px;" rel="guardar_borrador" onclick="return false;"');?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="grid16"><?php echo form_hidden('cambios',set_value('cambios',0));?>
        <div id="autoguardado" style="text-align: right;"></div>
    </div>
    <div class="clear"></div>
    <div class="grid16" style="margin-top: 20px;">
        <div style="text-align: right;">
            <ul style="float: right">
                    <li style="text-align: left;"><?php echo form_label('(C.C.) Enviar copias del cotizacion a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','size'=>60));?></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
    <div id="cotizacion">
        <div class="grid16" style="margin-top: 20px;">
            <div class="seccion">
                <h2>Datos del cliente</h2>
            </div>
            <div>
                <?php echo form_input(array('name'=>'nit','size'=>'15')); echo ' '.anchor('','Buscar  <span class="iconos_blancos buscar"></span>','class="boton" rel="boton_buscar_cliente"');?>
                <span style="margin-top: 5px;" class="sub_label">* Nit</span>
            </div>
            <div id="datos_cliente" style="margin-top: 10px;">
                <ul class="formulario">
                    <li class="left" style="height:60px;width:30%;">
                        <p id="nombre_cliente">
                            <?php echo form_input(array('name'=>'nombre','size'=>40,'maxlength'=>100));?>
                        </p>
                        <span class="sub_label">* Nombre del cliente</span>
                    </li>
                    <li class="left" style="height:60px;width:30%;">
                        <?php echo form_input(array('name'=>'contacto','style'=>'width:100%','maxlength'=>100));?>
                        <span class="sub_label">Contacto</span>
                    </li>
                    <li class="left" style="height:60px;width:40%;">
                        <?php echo form_input(array('name'=>'email','style'=>'width:100%','maxlength'=>100));?>
                        <span class="sub_label">E-mail</span>
                    </li>
                    <div class="clear"></div>
                    <li class="left" style="height:60px;width:40%;">
                        <?php echo form_input(array('name'=>'direccion','style'=>'width:100%','maxlength'=>100));?>
                        <span class="sub_label">* Dirección</span>
                    </li>
                    <li class="left" style="height:60px;width:30%;">
                        <?php echo form_input(array('name'=>'ciudad','style'=>'width:100%','maxlength'=>100));?>
                        <span class="sub_label">* Ciudad</span>
                    </li>
                    <li class="left" style="height:60px;width:30%;">
                        <?php echo form_input(array('name'=>'telefono','size'=>10,'maxlength'=>20));?>
                        <span class="sub_label">* Teléfono</span>
                    </li>
                    <div class="clear"></div>
                    <li class="left" style="height:60px;width:45%;">
                        <?php 
                        $adicional='';
                        if(!$configuracion['cambiar_asesor']){
                                $adicional='disabled="disabled"';
                        }
                        $vendedor=$this->session->userdata('numeros_vendedor');
                        echo form_dropdown('vendedor', $vendedores, $vendedor[0], $adicional);                        
                        ////if(!in_array($cliente->id_vendedor,$this->session->userdata('numeros_vendedor'))){
                        echo ' <div style="cursor:pointer;display:inline;color:#930;display:none;" title="Usted no aparece como el asesor comercial de este cliente. Por favor elija el número correcto y comuníquese con cartera para su corrección."><span class="alerta"></span>Alerta</div>';
                        //}
                        ?><span class="sub_label">Asesor</span>
                    </li>
                    <?php if($configuracion['listar_plazos']){?>
                    <li class="left" style="height:60px;width:10%;">
                        <?php $plazos=json_decode($configuracion['plazos']);
                            foreach($plazos as $plazo):
                                $plazos_pedido[$plazo]=$plazo;
                            endforeach;
                            echo form_dropdown('plazo',$plazos_pedido);?>
                        <span class="sub_label">Plazo</span>
                    </li>
                    <?php } if($configuracion['listar_descuentos']){?>
                    <li class="left" style="height:60px;width:10%;">
                        <?php $descuentos=json_decode($configuracion['descuentos']);
                            foreach($descuentos as $descuento):
                                $descuentos_pedido[$descuento]=$descuento;
                            endforeach;
                            echo form_dropdown('descuento_cotizacion',$descuentos_pedido);?>
                        <span class="sub_label">Descuento</span>
                    </li>
                    <?php } if($configuracion['listar_listas']){?>
                    <li class="left" style="height:60px;width:10%;">
                        <?php $precio=1; echo form_dropdown('lista',array('1'=>'1','2'=>'2','3'=>'3'),$precio);?>
                        <span class="sub_label">Lista</span>
                    </li>
                    <?php } if($configuracion['listar_retenciones']){?>
                    <li class="left" style="height:60px;width:35%;">
                        <?php echo form_dropdown('retencion',array(''=>'','1'=>'No retiene','2'=>'Retiene sobre la base','3'=>'Retiene sobre cualquier valor','3'=>'Autorretenedor y retiene sobre la base','4'=>'Autorretenedor y retiene sobre cualquier valor')); ?>
                        <span class="sub_label">Retención</span>
                    </li>
                    <?php }?>
                    <li class="left" style="height:50px;width:22%;">
                        <?php echo form_input(array('name'=>'direccion_envio','value'=>'','maxlength'=>'25','size'=>25));?>
                        <span class="sub_label">Dirección de envío</span>
                    </li>
                    <div class="clear"></div>
                </ul>
                <div style="margin-top:10px; color: #900; padding: 10px; font-style: italic;">Los datos campos con * son obligatorios.</div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="grid16" id="datos_productos">
            <div class="seccion">
                <h2>Productos</h2>
                <div id="eliminar_rango_productos" style="display:none;">
                            <?php echo anchor('','Eliminar seleccionados','rel="eliminar_productos_seleccionados" class="boton boton_rojo" onclick="return false;"');?>
                </div>
            </div>
            <div id="datosProducto">
                <table id="productos_cotizacion" class="tabla">
                        <thead>
                                <tr>
                                        <th height="30px;" style="width: 3%;"><input type="checkbox" id="seleccionar_todos" value="0"/></th>
                                        <th style="width: 138px;">Código</th>
                                        <th style="">Nombre del producto</th>
                                        <th style="width:95px;">Cantidad</th>
                                        <th style="width:115px">Precio</th>
                                        <th style="width:50px;" title="Descuento">Dcto</th>
                                        <th style="width:40px;" title="Iva">Iva</th>
                                        <th style="width:70px;">Total</th>
                                        <th style="width:30px;"></th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr class="agregar">
                                        <td class="item numero">-</td>
                                        <td colspan="8"><?php echo form_input(array('name'=>'items_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_items" class="boton" onclick="return false;"');?></td>
                                </tr>
                        </tbody>
                        <tfoot>
                                <tr>
                                        <td id="total_productos_cotizacion" class="numero" colspan="9"><span
                                                class="alerta"></span> No ha agregado productos al cotizacion.</td>
                                </tr>
                        </tfoot>
                </table>
            </div>
        </div>
        <div class="clear"></div>
        <div class="totales_cotizacion" style="margin-top:10px;">
            <div class="grid10"><?php if($configuracion['observaciones']){?>
            <div style="padding: 10px; background: #F4F4F4;"><?php echo form_label('Observaciones: ','observaciones',array('style'=>'width:200px; float:left;')); echo '<div class="faltantes">300</div>'; 
            echo '<div style="clear:both;"></div><div><ul id="observaciones_predeterminadas">';
            foreach($observaciones_predeterminadas as $observacion):
                echo '<li><a href="" rel="agregar_observacion">'.$observacion->detalle.'</a></li>';
            endforeach;
            echo '</ul></div>';
            echo form_textarea(array('name'=>'observaciones','style'=>'width:99%;','rows'=>4,'maxlength'=>300));?></div>
            <?php }?> </div>
            <div class="grid6">
            <table class="tabla">
                    <tr>
                            <td style="width: 100px;" class="item">Subtotal</td>
                            <td id="totales_subtotal" style="text-align: right;"></td>
                    </tr>
                    <tr class="altrow">
                            <td style="width: 100px;" class="item">Descuento</td>
                            <td id="totales_descuento" style="text-align: right;"></td>
                    </tr>
                    <tr>
                            <td style="width: 100px;" class="item">Neto</td>
                            <td id="totales_neto" style="text-align: right;"></td>
                    </tr>
                    <tr class="altrow">
                            <td style="width: 100px;" class="item">Iva</td>
                            <td id="totales_iva" style="text-align: right;"></td>
                    </tr>
                    <tr>
                            <td style="width: 100px;" class="item">Total</td>
                            <td id="totales_total" style="text-align: right;"></td>
                    </tr>
            </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?php if(!isset($numero_borrador)){ $numero_borrador=null;} echo form_hidden('borrador',set_value('borrador',$numero_borrador));?>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.highlight-3.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numberformatter-1.1.2.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.calculator.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    /*Declaracion de variables*/
    var tiempo_autoguardar;	
    var dcto_cotizacion;
    var autoguardando=false;
    var salir='';
    
    /*Cuadros de dialogo*/
    $('body').append('<div id="buscarCliente" title="Buscar cliente"></div>');
    $('#contenido').append('<div id="buscarProducto" title="Buscar producto"></div>');
    $('body').append('<div id="buscarcotizacion" title="Buscar cotizacion a copiar"></div>');
            
    $("#buscarCliente").dialog({
        autoOpen:false,
        resizable: false,
        width: 978,
        height:540,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                                        
        }                    
    });//Termina dialog.
    
    $("#buscarProducto").dialog({
        autoOpen:false,
        resizable: true,
        width: 978,
        height:600,
        modal: true,
        close: function(event,ui){
            $('body').css('overflow','auto');            
        },
        buttons: {
            'Cerrar': function() {
                $('body').css('overflow','auto');
                $('#buscarProducto').dialog('close');
            }
        }
    });//Termina dialog.
    
    $("#buscarcotizacion").dialog({
        autoOpen:false,
        resizable: false,
        width: 800,
        height:480,
        modal: true,
        close: function(event,ui){
            $('body').css('overflow','auto');						
        },
        buttons: {
            'Cerrar': function() {
                $('body').css('overflow','auto');
                $(this).dialog('close');
            }
        }
    });
    
    $('a[rel=guardar_borrador]').click(function(){		   
        $('#autoguardado').removeClass('correcto error');
        $('#autoguardado').addClass('cargando');
        $('#autoguardado').html('<span class="cargando"></span> Guardando borrador del cotizacion');
        var encabezado=[];
        var nit=$('input[name=nit]').val();
        var nombre_cliente=$('#datos_cliente input[name=nombre]').val();
        var contacto=$('#datos_cliente input[name=contacto]').val();
        var email=$('#datos_cliente input[name=email]').val();
        var direccion=$('#datos_cliente input[name=direccion]').val();
        var telefono=$('#datos_cliente input[name=telefono]').val();
        var ciudad=$('#datos_cliente input[name=ciudad]').val();
        var vendedor=$('#datos_cliente select[name=vendedor]').val();
        var plazo=$('#datos_cliente select[name=plazo]').val();
        var email_copias=$('input[name=email_copias]').val();
        var lista=$('#datos_cliente select[name=lista]').val();
        var retencion=$('#datos_cliente select[name=retencion]').val();
        var descuento_cotizacion=$('#datos_cliente select[name=descuento_cotizacion]').val();
        var direccion_envio=$('#datos_cliente input[name=direccion_envio]').val();
        var observaciones=$('textarea[name=observaciones]').val();
        var codigos=[];
        $("input[name=codigo]").each(function(){
            codigos.push($(this));
        });
        var productos_agregados=parseInt($('#productos_cotizacion >tbody >tr').length)-1;
        var nombres_productos=[];
        $("[id^=nombre_item_]").each(function(){
            nombres_productos.push($(this).text());	
        });
        var cantidades=[];
        $("input[name=cantidad]").each(function(){
            cantidades.push($(this).val());
        });
        var referencias=[];
        $("div[id^=referencia_item_]").each(function(){
            referencias.push($(this).text());
        });
        var precios=[];
        $("input[name=precio]").each(function(){
            precios.push($(this).val());
        });
        var descuentos=$("input[name=dcto]").parseNumber();
        var ivas=$("[id^=iva_item_]").parseNumber();
        var productos=[];
        for(n=0;n<cantidades.length;n++){
            producto={
                codigo: codigos[n].val(),
                nombre: nombres_productos[n],
                referencia: referencias[n],
                cantidad: cantidades[n],
                precio: precios[n],
                descuento: descuentos[n],
                iva: ivas[n]
            };
            productos.push(producto);
        }
        encabezado={
            nit:nit,
            nombre:nombre_cliente,
            contacto:contacto,
            email:email,
            direccion:direccion,
            ciudad:ciudad,
            telefono:telefono,
            vendedor:vendedor,
            plazo: plazo,
            descuento:descuento_cotizacion,
            lista:lista,
            retencion:retencion,
            direccion_envio:direccion_envio,            
            email_copias:email_copias,
            observaciones:observaciones
        };
        var url='<?php echo site_url('cotizaciones/autoguardar');?>';
        var form_data={
            numero_borrador:$('input[name=borrador]').val(),
            encabezado: encabezado,
            productos: productos,
            ajax:1
        };
        $.ajax({
           type: "POST",
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               $('#autoguardado').removeClass('cargando');
               $('#autoguardado').addClass('correcto');
               window.location.hash='borrador='+msg['id'];

               $('input[name=borrador]').val(msg['id']);
               if(autoguardando){
                   mostrar_mensaje('Se autoguardó el cotizacion con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                   $('#autoguardado').html('Borrador #'+msg['id']+' autoguardado. '+msg['fecha']);
               }else{
                   mostrar_mensaje('Se guardó el cotizacion con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                   $('#autoguardado').html('Borrador #'+msg['id']+' guardado. '+msg['fecha']);
               }
               $('input[name=cambios]').val(0);
               autoguardando=false;
               if(salir!=''){
                   location.href=salir;
               }
           },
           error: function(x,e){
               autoguardando=false;
               salir='';
               $('#autoguardado').removeClass('cargando');
               $('#autoguardado').addClass('error');
               mostrar_error(x,e);
           }
        });
        return false;
    });//Termina guardar borrador
    
    autoguardar();
    
    function autoguardar(){		   
        tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
        if($('input[name=cambios]').val()==1){
           autoguardando=true;
           $('a[rel=guardar_borrador]').click();
        }//Termina If.
    }//Termina autoguardar
    $('select[name=descuento_cotizacion]').live('change',function(){
        var descuento=$(this).val();
        dcto_cotizacion=descuento;
        $('input[name=dcto]').each(function(){
            $(this).val(descuento);
        });
        recalcularTodo();
    });
    $('input[name=nit]').live('change',function(){ 
        mostrar_mensaje('Buscando datos del cliente. Por favor espere...','alerta',0);
        var form_data={
            cliente: $(this).val(),
            ajax:'1'
        };
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('cotizaciones/datos_cliente');?>",
            data: form_data,
            dataType: 'json',
            success: function(msg){
                if(msg['estado']=='correcto'){
                    mostrar_mensaje(msg['mensaje'],'correcto',500);
                    var cliente=msg['cliente'];
                    cargar_datos_cliente(cliente.nombre,cliente.contacto,cliente.email,cliente.direccion,cliente.ciudad,cliente.telefono1,cliente.id_vendedor,cliente.plazo,null,cliente.lista,null,null);
                    $('input[name=nit]').removeClass('error');
                    $('input[name=cambios]').val(1);
                    var encontrado=false;
                    $('#productos_cotizacion >tbody input[name=codigo]').each(function(){
                        if($(this).val()=='' && encontrado==false){
                            $(this).focus();
                            encontrado=true;
                            return;
                        }
                    });
                }else{
                    mostrar_mensaje('El cliente no existe','alerta',200);                    
                }                                   
            },
            error: function(x,e){
                mostrar_error(x,e);
            }
        });//Termina ajax.
    });//Termina change
    function cargar_datos_cliente(nombre,contacto,email,direccion,ciudad,telefono1,vendedor,plazo,descuento,lista,retencion,direccion_envio){
        if(typeof nombre == "undefined"){
            nombre='';
        }
        if(typeof contacto == "undefined"){
            contacto='';
        }
        if(typeof email == "undefined"){
            email='';
        }
        if(typeof direccion == "undefined"){
            direccion='';
        }
        if(typeof ciudad == "undefined"){
            ciudad='';
        }
        if(typeof telefono1 == "undefined"){
            telefono1='';
        }
        if(typeof vendedor == "undefined"){
            vendedor='';
        }
        if(typeof plazo == "undefined"){
            plazo=0;
        }
        if(typeof descuento == "undefined"){
            descuento=0;
        }
        if(typeof lista == "undefined"){
            lista=1;
        }
        if(typeof retencion == "undefined"){
            retencion='';
        }
        if(typeof direccion_envio == "undefined"){
            direccion_envio='';
        }
        $('#datos_cliente input[name=nombre]').val(nombre);
        $('#datos_cliente input[name=contacto]').val(contacto);
        $('#datos_cliente input[name=email]').val(email);
        $('#datos_cliente input[name=direccion]').val(direccion);
        $('#datos_cliente input[name=ciudad]').val(ciudad);
        $('#datos_cliente input[name=telefono]').val(telefono1);
        $('#datos_cliente select[name=vendedor]').val(vendedor);
        
        <?php if($configuracion['listar_plazos']){?>
            $('#datos_cliente select[name=plazo]').val(plazo);
        <?php }?>
        <?php if($configuracion['listar_descuentos']){?>
            $('#datos_cliente select[name=descuento_cotizacion]').val(descuento);
        <?php }?>
        <?php if($configuracion['listar_listas']){?>
            $('#datos_cliente select[name=lista]').val(lista);
        <?php }?>
        <?php if($configuracion['listar_retenciones']){?>
            $('#datos_cliente select[name=retencion]').val(retencion);
        <?php }?>
        $('#datos_cliente input[name=direccion_envio]').val(direccion_envio);
    }
    $('a[rel=boton_buscar_cliente]').live('click',function(){
        $("#buscarCliente").dialog('open');
        $('#buscarCliente input[name=cliente]').focus();
        if($("#buscarCliente").html()==''){
            mostrar_mensaje('Cargando listado de clientes. Por favor espere...','alerta',0);
            var form_data={
                cliente: '',
                ajax:'1'
            };
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('cotizaciones/buscar_cliente');?>",
                data: form_data,
                dataType:'json',
                success: function(msg){
                    if(msg['estado']=='correcto'){
                        mostrar_mensaje(msg['mensaje'],'correcto',200);				   
                        $('#buscarCliente').html(msg['vista']);
                        acciones_buscar_cliente();
                        $('#buscarCliente input[name=cliente]').focus();
                        $('body').css('overflow','hidden');                            
                    }else{
                        $('body').css('overflow','auto');
                        mostrar_mensaje(msg['mensaje'],'error');				                                      
                    }
                },
                error: function(x,e){
                    $('body').css('overflow','auto');
                    mostrar_error(x,e);
                }
             });//Termina ajax.
         }        
        return false;
    });//Termina boton_buscar_cliente
	
    $('input[name=items_agregar]').keyup(function(event){
        if (event.keyCode == '13') {
            $('a[rel=boton_agregar_items]').click();
        }
        return false;
    });//Termina keyup.
    function agregar_item(n,codigo,referencia,nombre,ude,cantidad,precio,dcto,iva,total,cotizacion_texto){
        if(n % 2){
                clase="";
        }else{
                clase="altrow";
        }
        if(typeof codigo == "undefined"){
            codigo='';
        }
        if(typeof referencia == "undefined"){
            referencia='';
        }
        if(typeof nombre == "undefined"){
            nombre='';
        }
        if(typeof ude == "undefined"){
            ude='';
        }
        if(typeof cantidad == "undefined"){
            cantidad=0;
        }
        if(typeof precio == "undefined"){
            precio=0;
        }
        if(typeof dcto == "undefined"){
            dcto=0;
        }
        if(typeof iva == "undefined"){
            iva=0;
        }
        if(typeof total == "undefined"){
            total=0;
        }
        if(typeof cotizacion_texto == "undefined"){
            cotizacion_texto='';
        }
        $('<tr id="fila_'+n+'" class="'+clase+'" style="display:none;">'+
            '<td id="numero_item_'+n+'" class="item numero"><span class="numero">'+n+'</span><input type="checkbox" name="select_eliminar" tabindex="-1"/></td>'+
            '<td><input type="text" size="8" value="'+codigo+'" name="codigo"><?php echo anchor('','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="buscar_producto" tabindex="-1" onclick="return false;"');?>'+
            '<div id="referencia_item_'+n+'" class="referencia">'+referencia+'</div>'+cotizacion_texto+'</td>'+
            '<td id="nombre_item_'+n+'">'+nombre+'</td>'+
            '<td style="text-align:center;"><input type="text" size="5" value="'+cantidad+'" name="cantidad"><div class="ude" title="Unidad de empaque">'+ude+'</div></td>'+
            '<td style="text-align:center;">'+
            <?php if($configuracion['modificar_precio']){ ?>
            '<input type="text" style="text-align: right;" size="8" value="'+precio+'" name="precio">'+													
            <?php }else{ ?>
            '<input type="text" tabindex="-1"  readonly="readOnly" style="text-align: right;" size="8" value="'+precio+'" name="precio">'+
            <?php }?>
            '<div class="listas" title="Listas de precios"><ul></ul></div>' +
            '</td>'+
            '<td style="text-align:center;">'+
            <?php if($configuracion['modificar_dcto']){?>
            '<input type="text" size="2" value="'+dcto+'" name="dcto">'+
            <?php }else{ ?>
            '<input type="text" size="2" readonly="readOnly" tabindex="-1" value="'+dcto+'" name="dcto">'+
            <?php }?>
            '</td>'+
            '<td style="text-align:center;" id="iva_item_'+n+'">'+iva+'</td>'+
            '<td class="total_producto" id="total_item_'+n+'">0</td>'+
            '<td><a href="" tabindex="-1" rel="eliminar_producto" title="Eliminar" class="boton boton_gris"><span class="iconos_rojo eliminar"></span></a></td></tr>').insertBefore($('#productos_cotizacion tbody tr.agregar'));
        var fila=$('tr#fila_'+n);
        $('#total_productos_cotizacion').html('Total de productos en el cotizacion '+n);
        //Se formatean los campos para que acepten solo numeros y se les aplica la funcion de q al cambiar actualicen los totales.
        acciones_campos_fila_producto(fila);
    }
    $('a[rel=boton_agregar_items]').click(function(){
        var n=0;
        n=parseInt($('#productos_cotizacion tbody tr').length);
        var cantidad=$('input[name=items_agregar]').val();
        if(!cantidad || cantidad==0){
                cantidad=1;
        }
        var clase="";
        for(var f=0;f<cantidad;f++){
            if(n % 2){
                clase="";
            }else{
                clase="altrow";
            }					
            if(typeof dcto_cotizacion == "undefined"){
                dcto_cotizacion='0';
            }
            agregar_item(n,'','','','',0,0,dcto_cotizacion);
            n++;            
        }
        var encontrado=false;
        $('#productos_cotizacion >tbody input[name=codigo]').each(function(){
            if($(this).val()=='' && encontrado==false){
                $(this).focus();
                encontrado=true;
                return;
            }
        });
        $('input[name=items_agregar]').val(1);
        return false;
    });//Termina boton_agregar_items.
    
    function acciones_campos_fila_producto(fila){
        if(fila==null){
                fila=$('#datosProducto');
        }
        fila.find("input[name=precio]").numeric();
        fila.find("input[name=cantidad]").numeric('no');
        fila.find("input[name=dcto]").numeric();
        fila.fadeIn('slow');
    }//Termina acciones_campos_fila_producto
    
    $("input[name=precio]").live('focusout',function(){
        if($(this).val()==0){
            $(this).addClass('error');
        }else{
            $(this).removeClass('error');
        }
    });
    
    $("input[name=cantidad]").live('focusout',function(){
        if($(this).val()==0){
            $(this).addClass('error');
        }else{
            $(this).removeClass('error');
        }
    });
        
    $("input[name=precio]").live('change',function(){
        recalcularTodo();
        $('input[name=cambios]').val(1);
    });
        
    $("input[name=cantidad]").live('change',function(){
        recalcularTodo();					
        $('input[name=cambios]').val(1);
    });
    
    $("input[name=dcto]").live('change',function(){
            recalcularTodo();					
            $('input[name=cambios]').val(1);
    });
    
    $("input[name=codigo]").live('change',function(){ 
            mostrar_mensaje('Cargando datos del producto. Por favor espere...','alerta',0);
            var codigo_fila=$(this).val();
            var fila=$(this).parent().parent();
            var copias=false;
            var codigos="";

            $("input[name=codigo]").each(function(){
                    codigos=codigos+$(this).val()+' ';
            });
            //Quita colores de productos que ya no estan repetidos.
            $("input[name=codigo]").each(function(){
                    if($(this).val()!=""){
                            productos_repetidos=codigos.split($(this).val()).length-1;
                    }else{
                            productos_repetidos=0;
                    }
                    if(productos_repetidos>1){
                            $(this).parent().parent().addClass('repetido');
                            copias=true;					
                    }else{
                            $(this).parent().parent().removeClass('repetido');					
                    }					
            });
            //Termina quita colores de productos que ya no estan repetidos.
            var url='<?php echo site_url('cotizaciones/datos_producto');?>';
            var form_data={
                    codigo: $(this).val(),
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               dataType: 'json',
               data: form_data,
               success: function(msg){
                       if(msg['estado']=='correcto'){
                            if(typeof dcto_cotizacion == "undefined"){
                                    dcto_cotizacion=producto['descuento'];
                            }
                                                                                    
                                   mostrar_mensaje(msg['mensaje'],'correcto',500);
                                   fila.find('input[name=codigo]').removeClass('error');							   
                                   fila.find('[id^=nombre_item_]').text(msg['nombre']);
                                   fila.find('[id^=referencia_item_]').text(msg['referencia']);
                                   //fila.find('input[name=cantidad]').val('0');
                                   fila.find('[name=dcto]').val(dcto_cotizacion);
                                   fila.find('[name=precio]').val(msg['precio1']);
                                   fila.find('[id^=iva_item_]').text(msg['iva']);
                                   fila.find('input[name=cantidad]').select();						   
                                   var valor_cantidad='0';
                                   var valor_precio=msg['precio1'];
                                   var valor_dcto='0';
                                   fila.find('input[name=cantidad]').bind('blur',function(){
                                                if(valor_cantidad!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                   fila.find('input[name=precio]').bind('blur',function(){
                                                if(valor_precio!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                   fila.find('input[name=dcto]').bind('blur',function(){
                                                if(valor_dcto!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                        $('input[name=cambios]').val(1);                                                                                            		
                           
                            if(copias){
                                mostrar_mensaje('Existen productos repetidos.','error');                                            		
                            }
                       }else{
                           fila.find('input[name=codigo]').addClass('error');
                           fila.find('[id^=nombre_item_]').text(msg['nombre']);
                           fila.find('[id^=referencia_item_]').text('');
                           fila.find('input[name=cantidad]').val('0');
                           fila.find('input[name=dcto]').val(dcto_cotizacion);
                           fila.find('input[name=precio]').val('0');
                           fila.find('[id^=iva_item_]').text('0');
                           fila.find('input[name=codigo]').select();
                           mostrar_mensaje(msg['mensaje'],'error');                                   
                       }
               },
               error: function(x,e){
                   fila.find('[id^=nombre_item_]').text('');
                   fila.find('[id^=referencia_item_]').text('');
                   fila.find('[name=dcto]').val('0');
                   fila.find('[name=precio]').val('0');
                   fila.find('[id^=iva_item_]').text('0');
                   fila.find('input[name=cantidad]').val('0');
                   mostrar_error(x,e);
               }
            });//Termina ajax.
    });//Termina bind change.
    
    
        $('a[rel=eliminar_producto]').live('click',function(){
                var fila=$(this).parent().parent();
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                $('body').append('<div id="dialog-confirm" title="Eliminar producto"><span class="icono"></span>Está seguro de eliminar el producto seleccionado?</div>');
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    mostrar_mensaje('Eliminando item. Por favor espere...','alerta',0);
                                    
                                    var codigo_fila=fila.find('input[name=codigo]').val();
                                    var repetido=0;
                                    fila.css({background:"#FFD5D5",color:"#CC0000"});
                                    fila.animate({opacity: 0.5}, 200, function(){				  
                                            fila.remove();
                                            $('input[name=cambios]').val(1);
                                            var productos_agregados=parseInt($('#productos_cotizacion >tbody >tr').length)-1;
                                            var n=1;
                                            var codigos="";
                                            $('#productos_cotizacion >tbody >tr').each(function(){
                                                    if($(this).attr('class')!='agregar'){
                                                            codigos=codigos+$(this).find("input[name=codigo]").val()+' ';						
                                                            if(n % 2){
                                                                    $(this).removeClass('altrow');
                                                            }else{
                                                                    $(this).addClass('altrow');
                                                            }
                                                            $(this).attr('id','fila_item_'+n);
                                                            $(this).find('[id^=total_item_]').attr('id','total_item_'+n);
                                                            $(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
                                                            $(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
                                                            $(this).find('[id^=nombre_item_]').attr('id','nombre_item_'+n);
                                                            $(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
                                                            $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                                    }
                                                    n++;
                                            });
                                            $('#total_productos_cotizacion').html('Total de productos en el cotizacion '+productos_agregados);
                                            if(productos_agregados==0){
                                                    $('#total_productos_cotizacion').html('<span class="alerta"></span> No ha agregado productos al cotizacion.'); 
                                            }
                                            //Quita colores de productos que ya no estan repetidos.
                                            $("input[name=codigo]").each(function(){
                                                    if($(this).val()!=""){
                                                            productos_repetidos=codigos.split($(this).val()).length-1;
                                                    }else{
                                                            productos_repetidos=0;
                                                    }
                                                    if(productos_repetidos>1){
                                                            $(this).parent().parent().addClass('repetido');
                                                    }else{
                                                            $(this).parent().parent().removeClass('repetido');
                                                    }					
                                            });
                                            //Termina quita colores de productos que ya no estan repetidos.

                                            recalcularTodo();
                                            mostrar_mensaje('Se ha eliminado correctamente.','correcto',500);                                            
                                    });//Termina animate
                                    
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
                return false;
        });//Termina eliminar_producto.
	acciones_observaciones();
	recalcularTodo();
	function formatear(){
		$("input[name=cantidad]").format({format:"#,###", locale:"us"});
		$("input[name=dcto]").format({format:"#,###", locale:"us"});
		$("input[name=precio]").format({format:"#,###", locale:"us"});
		$("[id^=total_item_]").format({format:"#,###", locale:"us"});
	};
	function formatearTotales(){		
		$("#totales_subtotal").format({format:"$ #,###", locale:"us"});
		$("#totales_descuento").format({format:"$ #,###", locale:"us"});
		$("#totales_neto").format({format:"$ #,###", locale:"us"});
		$("#totales_iva").format({format:"$ #,###", locale:"us"});
		$("#totales_total").format({format:"$ #,###", locale:"us"});
	}
	function recalcularTodo(){		
		var totales_subtotal=0;
		var totales_dcto=0;
		var totales_iva=0;
		var subtotal=0;
		var dcto=0;
		var iva=0;
		var cantidades=$("input[name=cantidad]").parseNumber();
		var precios=$("input[name=precio]").parseNumber();
		var descuentos=$("input[name=dcto]").parseNumber();
		var ivas=$("[id^=iva_item_]").parseNumber();
		for(n=0;n<cantidades.length;n++){
			subtotal=cantidades[n]*precios[n];
			dcto=cantidades[n]*(precios[n] * descuentos[n]/100);
			iva=(subtotal-dcto)*(ivas[n]/100);
			totales_subtotal=totales_subtotal+subtotal;
			totales_dcto=totales_dcto+dcto;
			totales_iva=totales_iva+iva;
		}
		var totales_neto=totales_subtotal-totales_dcto;
		var totales_total=totales_neto+totales_iva;
		$("#totales_subtotal").text(totales_subtotal);
		$("#totales_descuento").text(totales_dcto);
		//$("#totales_neto").text(totales_neto);
		$("#totales_iva").text(totales_iva);
		$("#totales_total").text(Math.round(totales_total));
		
		$("[id^=total_item_]").calc(
			// the equation to use for the calculation
			"(cantidad * precio) - (cantidad * (precio * dcto / 100))",
			// define the variables used in the equation, these can be a jQuery object
			{
				cantidad: $("input[name=cantidad]"),
				precio: $("input[name=precio]"),
				dcto: $("input[name=dcto]")
			},
			// define the formatting callback, the results of the calculation are passed to this function
			function (s){
				// return the number as a dollar amount				
				return s.toFixed(2);
			},
			// define the finish callback, this runs after the calculation has been complete
			function ($this){
				// sum the total of the $("[id^=total_item]") selector
				var neto = $this.sum();
				$("#totales_neto").text(
					// round the results to 2 digits
					neto.toFixed(2)					
				);			
			}			
		);	   
		formatear();
		formatearTotales();                  
	}
        
	$('input[name=items_agregar]').numeric();
	$('a[rel=boton_copiar_desde_cotizacion]').click(function(){
                mostrar_mensaje('Cargando listado de cotizaciones. Por favor espere...','alerta',0);
		
		var form_data={
			cliente: '',
			ajax:1
		};
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('cotizaciones/buscar_cotizacion_a_copiar');?>",
			data: form_data,
                        dataType:'json',
			success: function(msg){
			   if(msg['estado']=='correcto'){
                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                $('#buscarcotizacion').html(msg['vista']);
                                $('#buscarcotizacion').dialog('open');
                                $('#buscarcotizacion input[name=cliente]').focus();
                                $('body').css('overflow','hidden');
                                actualizar_buscador_cotizacion();                                        
			   }else{
                               mostrar_mensaje(msg['mensaje'],'error');
			   }
			},
			error: function(x,e){
                            mostrar_error(x,e);
			}
		});//Termina ajax.		
		return false;
	});//Termina boton_copiar_desde_cotizacion.
	function actualizar_buscador_cotizacion(){
		$('#resultados_cotizaciones th a').click(function(){
                        mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
			var url=$(this).attr('href');
			var form_data={
				ajax:'1'
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
			   	   if(msg['estado']=='correcto'){				   
					$('#buscarcotizacion').html(msg['vista']);
                                        mostrar_mensaje(msg['mensaje'],'correcto',500);
					actualizar_buscador_cotizacion();
				   }else{
                                       mostrar_mensaje(msg['mensaje'],'error');
				   }
			   },
			   error: function(x,e){
                               mostrar_error(x,e);
			   }
			});//Termina ajax.
			return false;
		});//Termina th click.
		$('div.paginacion a').click(function(){
			mostrar_mensaje('Cargando la página. Por favor espere...','alerta',0);
			var url=$(this).attr('href');
			var form_data={
				ajax:'1'
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
				   if(msg['estado']=='correcto'){				   
					$('#buscarcotizacion').html(msg['vista']);
                                        mostrar_mensaje(msg['mensaje'],'correcto',500);
					actualizar_buscador_cotizacion();
				   }else{
					mostrar_mensaje(msg['mensaje'],'error');
				   }
			   },
			   error: function(x,e){
				  mostrar_error(x,e);
			   }
			});
			return false;
		});//Termina paginacion a.
		$('a[rel=buscar_cotizacion_a_copiar]').click(function(){
			var url=$(this).attr('href');
			var valor=$('#buscarcotizacion input[name=cliente]').val();
			if(valor=='Digite el nit o nombre del cliente'){
				valor='';
			}
			var form_data={
				buscar:valor,
				ajax:1
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
				   if(msg['estado']=='correcto'){
                                       mostrar_mensaje(msg['mensaje'],'correcto',500);
					$('#buscarcotizacion').html(msg['vista']);
					actualizar_buscador_cotizacion();
				   }else{
					mostrar_mensaje(msg['mensaje'],'error');   
				   }
			   },
			   error:function(x,e){
                               mostrar_error(x,e);
			   }
			});//Termina ajax.
			return false;
		});//Termina buscar_cotizacion_a_copiar.
                $('#buscarcotizacion input[name=cliente]').focus(function(){
                        var valor=$(this).val();
                        if(valor=='Digite el nit o nombre del cliente'){
                                $(this).val('');
                        }
                });
                $('#buscarcotizacion input[name=cliente]').blur(function(){
                        var valor=$(this).val();
                        if(valor==''){
                                $(this).val('Digite el nit o nombre del cliente');
                        }
                });
                $('#buscarcotizacion input[name=cliente]').keyup(function(event){
                        if (event.keyCode == '13') {
                          $('#buscarcotizacion a[rel=buscar_cotizacion_a_copiar]').click();
                        }
                        return false;
                });
                var buscar=$('#buscarcotizacion input[name=cliente]').val();
                if(buscar!=''){
                        $('#buscarcotizacion tbody td[title=nit]').highlight(buscar);
                        $('#buscarcotizacion tbody td[title=nombre]').highlight(buscar);
                }
            }
            $('a[rel=copiar_cotizacion]').live('click',function(){
                mostrar_mensaje('Cargando los datos del cotizacion seleccionado. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                var form_data={
                        ajax:1
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType:'json',
                   success: function(msg){
                       if(msg['estado']=='correcto'){
                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                           agregar_productos_cargados(msg['productos'],msg['cotizacion']);
                       }else{
                           mostrar_mensaje(msg['mensaje'],'error');
                       }
                   },
                   error:function(x,e){
                      mostrar_error(x,e);
                   }
                 });
                return false;
            });//Termina boton_agregar_items.
            function agregar_productos_cargados(productos,cotizacion){
                var n=0;
                $("input[name=codigo]").each(function(){
                    if($(this).val()==''){
                            $(this).parent().parent().remove();
                    }
                });
                n=parseInt($('#productos_cotizacion tbody tr').length);
                var cotizacion_texto='';
                if(!cotizacion){
                  cotizacion_texto='<div class="cotizacion_importado">Excell</div>';
                }else{
                  cotizacion_texto='<div class="cotizacion_copiado">'+cotizacion+'</div>';
                }
                var clase="";
                $.each(productos,function(k,v){
                      if(n % 2){
                                clase="";
                        }else{
                                clase="altrow";
                        }					
                        if(typeof dcto_cotizacion == "undefined"){
                                dcto_cotizacion='0';
                        }
                        agregar_item(n,v['codigo'],v['referencia'],v['nombre'],v['ude'],v['cantidad'],v['precio'],dcto_cotizacion,v['iva'],v['total'],cotizacion_texto);
                        n++;                        
                });
                recalcularTodo();
                verificar_productos_repetidos();
            }
		function acciones_buscar_cliente(){
			$('#buscarCliente input[name=cliente]').focus(function(){
				var valor=$(this).val();
				if(valor=='Digite el nit o nombre del cliente'){
					$(this).val('');
				}
			});
			$('#buscarCliente input[name=cliente]').focusout(function(){
				var valor=$(this).val();
				if(valor==''){
					$(this).val('Digite el nit o nombre del cliente');
				}
			});
			$('#buscarCliente input[name=cliente]').keyup(function(event){
				if (event.keyCode == '13') {
				  $('#buscarCliente a[rel=filtrar_cliente]').click();
				}
				return false;
			});
			$('#buscarCliente a[rel=filtrar_cliente]').click(function(){
                                mostrar_mensaje('Cargando clientes que coincidan con los datos de busqueda. Por favor espere...','alerta',0);
				var url = $(this).attr('href');
				var cliente=$('#buscarCliente input[name=cliente]').val();
				if(cliente=='Digite el nit o nombre del cliente'){
					cliente='';
					$('#buscarCliente input[name=cliente]').val('');
				}
				var form_data={
					cliente:cliente,
					ajax:1
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType:'json',
				   success: function(msg){
					   if(msg){				   
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                $('#buscarCliente').html(msg['vista']);
                                                acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente th a').click(function(){
                                mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
				var url=$(this).attr('href');
				var form_data={
					ajax:'1'
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType: 'json',
				   success: function(msg){
					   if(msg['estado']=='correcto'){	
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
						$('#buscarCliente').html(msg['vista']);
						acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente div.paginacion a').click(function(){
                                mostrar_mensaje('Cargando los resultados de la busqueda. Por favor espere...','alerta',0);
				var url=$(this).attr('href');
                                var form_data={
					ajax:1
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType: 'json',
				   success: function(msg){
                                          if(msg['estado']=='correcto'){	
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
						$('#buscarCliente').html(msg['vista']);
						acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente a[rel=boton_seleccionar_cliente]').click(function(){
                                $('input[name=nit]').val($(this).attr('id'));
				$('#buscarCliente').dialog('close');
				$('input[name=nit]').change();
				return false;
			});
			var buscar=$('#buscarCliente input[name=cliente]').val();
			if(buscar!=''){
				$('#buscarCliente table').highlight(buscar);
			}
		}
		var guardando=false;
		$('a[rel=guardar_cotizacion]').click(function(){
                    var boton=$(this);
			if(boton.text()!='Guardando cotizacion...' && !guardando){
				guardando==true;
				clearTimeout(tiempo_autoguardar);
				$(this).text('Guardando cotizacion...');
				var boton=$(this);
                                mostrar_mensaje('Verificando datos antes de guardar. Por favor espere...');
				var error=false;
				var encabezado=[];
				var nit=$('input[name=nit]').val();
                                var nombre_cliente=$('#datos_cliente input[name=nombre]').val();
                                var contacto=$('#datos_cliente input[name=contacto]').val();
                                var email=$('#datos_cliente input[name=email]').val();
                                var direccion=$('#datos_cliente input[name=direccion]').val();
                                var telefono=$('#datos_cliente input[name=telefono]').val();
                                var ciudad=$('#datos_cliente input[name=ciudad]').val();
                                var vendedor=$('#datos_cliente select[name=vendedor]').val();
                                var plazo=$('select[name=plazo]').val();
                                var email_copias=$('input[name=email_copias]').val();
                                var descuento_cotizacion=$('#datos_cliente select[name=descuento_cotizacion]').val();
                                var direccion_envio=$('input[name=direccion_envio]').val();
				var lista=$('select[name=lista]').val();
				var retencion=$('select[name=retencion]').val();
				var observaciones=$('textarea[name=observaciones]').val();
				if(!nit || nombre_cliente=='-'){
					error=true;
					$('input[name=nit]').addClass('error');
				}else{
					$('input[name=nit]').removeClass('error');
				}
                                if(!nombre_cliente){
                                    $('#datos_cliente input[name=nombre]').addClass('error');
                                    error=true;
                                }
                                if(!ciudad){
                                    $('#datos_cliente input[name=ciudad]').addClass('error');
                                    error=true;
                                }
                                if(!direccion){
                                    $('#datos_cliente input[name=direccion]').addClass('error');
                                    error=true;
                                }
                                if(!telefono){
                                    $('#datos_cliente input[name=telefono]').addClass('error');
                                    error=true;
                                }
				if(!observaciones){
					error=true;
					$('textarea[name=observaciones]').addClass('error');
				}else{
					$('textarea[name=observaciones]').removeClass('error');
				}
                                    if(email_copias!=''){
                                        $('input[name=email_copias]').removeClass('error');
                                                        var correos = $('input[name=email_copias]').val().split(',');
                                                        for (var n=0;n< correos.length;n++){
                                                                if(!es_mail(correos[n])){
                                                error=true;
                                            $('input[name=email_copias]').addClass('error');
                                        }
                                    }
				}
				if(retencion==undefined){
					retencion='null';
				}
				if(plazo==undefined){
					plazo='null';
				}
				if(lista==undefined){
					lista='null';
				}
				var codigos=[];
				var codigos_verificar_repetidos="";
				$("input[name=codigo]").each(function(){
					codigos_verificar_repetidos=codigos_verificar_repetidos+$(this).val()+' ';
					if($(this).val()==''){
						$(this).parent().parent().remove();
					}else{
						codigos.push($(this));
					}
				});

				var productos_agregados=parseInt($('#productos_cotizacion >tbody >tr').length)-1;
				var n=1;
				$('#productos_cotizacion >tbody >tr').each(function(){
					if($(this).attr('class')!='agregar'){
						if(n % 2){
							$(this).removeClass('altrow');
						}else{
							$(this).addClass('altrow');
						}
						$(this).attr('id','fila_item_'+n);
						$(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
						$(this).find('[id^=total_item_]').attr('id','total_item_'+n);
						$(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
						$(this).find('[id^=nombre_item_]').attr('id','nombre_item_'+n);
						$(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
						$(this).find('[id^=numero_item_]').find('span.numero').text(n);
					}
					n++;
				});
				$('#total_productos_cotizacion').html('Total de productos en el cotizacion '+productos_agregados);
				if(productos_agregados==0){
					$('#total_productos_cotizacion').html('<span class="alerta"></span> No ha agregado productos al cotizacion.'); 
				}
				var nombres_productos=[];
				$("[id^=nombre_item_]").each(function(){
					nombres_productos.push($(this).text());	
				});
				var cantidades=[];
				$("input[name=cantidad]").each(function(){
						cantidades.push($(this));
				});
				var referencias=[];
				$("div[id^=referencia_item_]").each(function(){
						referencias.push($(this).text());
				});
				var precios=[];
				$("input[name=precio]").each(function(){
					precios.push($(this));
				});

				var descuentos=$("input[name=dcto]").parseNumber();
				var ivas=$("[id^=iva_item_]").parseNumber();
				var subtotal=0;
				var dcto=0;
				var iva=0;
				var totales_subtotal=0;
				var totales_dcto=0;
				var totales_iva=0;
				var productos=[];
				var mensaje='';
				if(cantidades.length==0){
					error=true;
					mensaje='No hay productos en el cotizacion';
				}
				recalcularTodo();
				for(n=0;n<cantidades.length;n++){
					if(codigos[n].val()!='' && codigos[n].val().length==6 && nombres_productos[n]!='El producto no existe' && nombres_productos[n]!='Producto agotado' && nombres_productos[n]!='' && nombres_productos[n]!=' '){
						codigos[n].removeClass('error');
						cantidad_item=cantidades[n].parseNumber()[0];
						precio_item=precios[n].parseNumber()[0];
						if(cantidad_item==0){
							error=true;
							cantidades[n].addClass('error');
						}else{
							cantidades[n].removeClass('error');
						}
						if(precio_item==0){
							error=true;
							precios[n].addClass('error');
						}else{
							precios[n].removeClass('error');
						}
						subtotal=cantidad_item*precio_item;
						dcto=cantidad_item*(precio_item * descuentos[n]/100);
						iva=(subtotal-dcto)*(ivas[n]/100);
						totales_subtotal=totales_subtotal+subtotal;
						totales_dcto=totales_dcto+dcto;
						totales_iva=totales_iva+iva;
						
						producto={
							codigo: codigos[n].val(),
							referencia: referencias[n],
							cantidad: cantidad_item,
							precio: precio_item,
							descuento: descuentos[n],
							iva: ivas[n],
							total_sin_dcto:subtotal,
							total: (subtotal-dcto)
						};
						productos.push(producto);
					}else{
						error=true;
						codigos[n].addClass('error');
					}
				}
								
				//Quita colores de productos que ya no estan repetidos.
				$("input[name=codigo]").each(function(){
					if($(this).val()!=""){
						productos_repetidos=codigos_verificar_repetidos.split($(this).val()).length-1;
					}else{
						productos_repetidos=0;
					}
					if(productos_repetidos>1){
						$(this).parent().parent().addClass('repetido');
						error=true;
						mensaje=mensaje+" Existen productos repetidos";
					}else{
						$(this).parent().parent().removeClass('repetido');					
					}					
				});
				//Termina quita colores de productos que ya no estan repetidos.
				
				if(!error){
					var totales_subtotal=$("#totales_subtotal").parseNumber()[0];
					var totales_descuento=$("#totales_descuento").parseNumber()[0];
					var totales_neto=$("#totales_neto").parseNumber()[0];
					var totales_iva=$("#totales_iva").parseNumber()[0];
					var totales_total=Math.round($("#totales_total").parseNumber()[0]);
					encabezado={
						nit:nit,
						nombre:nombre_cliente,
                                                contacto:contacto,
                                                email:email,
                                                direccion:direccion,
                                                ciudad:ciudad,
                                                telefono:telefono,
                                                vendedor:vendedor,
                                                descuento:descuento_cotizacion,
						plazo: plazo,
						lista:lista,
                                                email_copias:email_copias,
						retencion:retencion,
                                                direccion_envio:direccion_envio,
						observaciones:observaciones,
						totales_subtotal:totales_subtotal,
						totales_descuento:totales_descuento,
						totales_neto:totales_neto,
						totales_iva:totales_iva,
						totales_total:totales_total
					}
                                        mostrar_mensaje('Guardando datos del cotizacion. Por favor espere...','alerta',0);
					var url=$(this).attr('href');
					var form_data={
						numero_borrador:$('input[name=borrador]').val(),
						encabezado: encabezado,
						productos: productos,
						ajax:1
					};
					$.ajax({
					   type: "POST",
                                           async:false,
					   url: url,
					   data: form_data,
                                           dataType: 'json',
					   success: function(msg){
						   if(msg){
                                                          if(msg['estado']=='error'){
                                                                mostrar_mensaje(msg['mensaje'],'error');
							   }else{
                                                                mostrar_mensaje('El cotizacion se guardó correctamente. Se va a enviar copia por correo. Por favor espere','correcto');
                                                                $('div#cotizacion_contenido').html(msg['mensaje']);
                                                                $('#envio_cotizaciones').html('<span class="cargando"></span> Enviando correo de los cotizaciones por favor espere...');
                                                                var url='<?php echo site_url('cotizaciones/enviar_cotizaciones');?>';
                                                                var form_data={
                                                                   cotizaciones:msg['numeros'],
                                                                   vendedor:encabezado['asesor'],
                                                                   cliente:encabezado['nit'],
                                                                   otros:encabezado['email_copias']
                                                                };
                                                                $.ajax({
                                                                   type: "POST",
                                                                   async:true,
                                                                   url: url,
                                                                   data: form_data,
                                                                   dataType: 'json',
                                                                   success: function(msg){
                                                                       if(msg['estado']=='correcto'){
                                                                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                                           $('input[name=envio_cotizaciones_correo]').val(0);
                                                                           $('#envio_cotizaciones').addClass('correcto');
                                                                       }else{
                                                                           mostrar_mensaje(msg['mensaje'],'error');
                                                                           $('input[name=envio_cotizaciones_correo]').val(3);
                                                                           $('#envio_cotizaciones').addClass('error');
                                                                       }
                                                                       $('#envio_cotizaciones').html(msg['mensaje']);
                                                                   },
                                                                   error: function(x,e){
                                                                       mostrar_error(x,e);                                                                                                                                              
                                                                   }
                                                                });
							   }							   
						   }else{
                                                       mostrar_mensaje('Se presentaron errores inesperados. Por favor intentelo nuevamente.','error');								
                                                       boton.text('Guardar definitivo y enviar');                                                       
						   }
					   },
					   error: function(x,e){
                                                mostrar_error(x,e)
                                                boton.text('Guardar definitivo y enviar');														
					   }
					 });
				}else{
					tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
					boton.text('Guardar definitivo y enviar');
					mostrar_mensaje('Se presentaron errores. Por favor verifique los datos en rojo y vuelva a intentar guardar el cotizacion. <br/>'+mensaje,'error');					
				}
			}else{
                            alert('Espere un momento que el cotizacion se está guardando.');
			}
			return false;
		});//Termina guardar cotizacion
		function acciones_observaciones(){
		   $('textarea[maxlength]').keyup(function(){
				var max = parseInt($(this).attr('maxlength'));
				if($(this).val().length > max){
					$(this).val($(this).val().substr(0, $(this).attr('maxlength')));
				}
				$(this).parent().find('.faltantes').html((max - $(this).val().length));
			});
                        $('a[rel=agregar_observacion]').click(function(){
				$('textarea[name=observaciones]').val($(this).text());
				 $('textarea[maxlength]').keyup();
				$('input[name=cambios]').val(1);
				return false;
			});
                        $('textarea[maxlength]').change(function(){
				$('input[name=cambios]').val(1);
			});
		}
		$('body').append('<div id="importar_cotizacion_excel" title="Importar cotizacion desde archivo de Excel"></div>');
                $("#importar_cotizacion_excel").dialog({
                                autoOpen:false,
                                resizable: false,
                                width: 800,
                                height:480,
                                modal: true,
                                close: function(event,ui){
                                        $('body').css('overflow','auto');                                        
                                },
                                buttons: {
                                        'Cerrar': function() {
                                                $('body').css('overflow','auto');
                                                $(this).dialog('close');
                                        }
                                }
                });
                $('#importar_cotizacion_excel form').live('submit',function(){
                        var url=$(this).attr('action');
                        $.ajaxFileUpload({
                                url:url,
                                secureuri:false,
                                fileElementId:'archivo_importar',
                                dataType: 'json',
                                success: function (data, status)
                                {
                                    agregar_productos_cargados(data);										
                                },
                                error: function (x,e)
                                {
                                  mostrar_error(x,e);
                                }
                        });
                        return false;
                });
               $('a[rel=importar_excel]').click(function(){
                       $('body').css('overflow','hidden');			   
                            mostrar_mensaje('Cargando el formulario. Por favor espere...','alerta',0);
                            var form_data={
                               ajax:1
                            };
                            $.ajax({
                               type: "POST",
                               url: "<?php echo site_url('cotizaciones/importar_subir_archivo');?>",
                               data: form_data,
                               dataType: 'json',
                               success: function(msg){
                                       if(msg['estado']=='correcto'){		
                                           mostrar_mensaje(msg['mensaje'],'correcto',200);
                                           $('#importar_cotizacion_excel').html(msg['vista']);
                                           $('#importar_cotizacion_excel').dialog('open');							
                                       }else{
                                            mostrar_mensaje(msg['mensaje'],'error');
                                       }
                               },
                               error: function(x,e){
                                  mostrar_error(x,e);
                               }
                             });					
                            return false;
                    });
	   function verificar_productos_repetidos(){
               var codigos="";
                $("input[name=codigo]").each(function(){
                    codigos=codigos+$(this).val()+' ';
                });
                var productos_repetidos;
                //Quita colores de productos que ya no estan repetidos.
                $("input[name=codigo]").each(function(){
                    if($(this).val()!=""){
                            productos_repetidos=codigos.split($(this).val()).length-1;
                    }else{
                            productos_repetidos=0;
                    }
                    if(productos_repetidos>1){
                            $(this).parent().parent().addClass('repetido');																	
                    }else{
                            $(this).parent().parent().removeClass('repetido');					
                    }					
                });
           }
           var jash=window.location.hash;
           if(jash.length > 1){ 
                $('input[name=borrador]').val(jash.replace('#borrador=',''));                               
           }else{
               <?php 
                    if(!isset($productos_json)){?>
                    $('a[rel=boton_agregar_items]').click();
                <?php }?>
           }
           if($('input[name=borrador]').val()!=''){
                cargar_borrador();
           }
           function cargar_borrador(){
               var borrador=$('input[name=borrador]').val();
               var form_data={
                   borrador:borrador,
                   ajax:1
               }
               mostrar_mensaje('Cargando la información del borrador. Por favor espere...','alerta',0);
               $.ajax({
                   type: "POST",
                   url: "<?php echo site_url('cotizaciones/cargar_borrador');?>/",
                   data: form_data,
                   dataType:'json',
                   success: function(msg){
                       if(msg['estado']=='correcto'){
                            var encabezado=msg['encabezado'];
                            if(encabezado['email_copias']!=null){
                                $('input[name=email_copias]').val(encabezado['email_copias']);
                            }
                            if(encabezado['nit']!=null){
                                var cliente=encabezado;
                                cargar_datos_cliente(cliente.nombre,cliente.contacto,cliente.email,cliente.direccion,cliente.ciudad,cliente.telefono,cliente.vendedor,cliente.plazo,cliente.descuento,cliente.lista,cliente.retencion,cliente.direccion_envio);
                                $('input[name=nit]').val(encabezado['nit']);                                                                        			
                            }
                            var productos=msg['productos'];	
                            if(productos.length>0){
                                            mostrar_mensaje('Cargando los productos. Por favor espere...','alerta',0);
                                            var n=0;
                                            n=parseInt($('#productos_cotizacion tbody tr').length);
                                            var cantidad=productos.length;
                                            var clase="";
                                            for(var f=0;f<cantidad;f++){
                                                    var producto=JSON.parse(productos[f]);
                                                    if(typeof dcto_cotizacion == "undefined"){
                                                            dcto_cotizacion=producto['descuento'];
                                                    }
                                                    agregar_item(n,producto['codigo'],producto['referencia'],producto['nombre'],producto['ude'],producto['cantidad'],producto['precio'],dcto_cotizacion,producto['iva'],0);
                                                    n++;                                                    
                                            }
                                            recalcularTodo();
                                            $('input[name=items_agregar]').val(1);                                            
                            }
                                                                    $('textarea[name=observaciones]').val(encabezado['observaciones']);
                                            $('textarea[name=observaciones]').keyup();
                            var codigos="";

                            $("input[name=codigo]").each(function(){
                                    codigos=codigos+$(this).val()+' ';
                            });
                            //Quita colores de productos que ya no estan repetidos.
                            $("input[name=codigo]").each(function(){
                                    if($(this).val()!=""){
                                            productos_repetidos=codigos.split($(this).val()).length-1;
                                    }else{
                                            productos_repetidos=0;
                                    }
                                    if(productos_repetidos>1){
                                            $(this).parent().parent().addClass('repetido');
                                    }else{
                                            $(this).parent().parent().removeClass('repetido');					
                                    }					
                            });
                            //Termina quita colores de productos que ya no estan repetidos.
                            $('input[name=cambios]').val(0);
                            mostrar_mensaje('Se ha cargado el borrador.','correcto',200);
                        }else{
                            mostrar_mensaje(msg['mensaje'],'error');
                            window.location.hash='';
                        }
                   },
                   error: function(x,e){
                      mostrar_error(x,e);
                   }
                });	
	   }
	   $('#menu a').click(function(){
            var url=$(this).attr('href');
            if($('input[name=cambios]').val()==1){
                $('#contenido').append('<div id="dialog-confirm" title="Está seguro de salir del cotizacion?"><span class="icono"></span>El cotizacion que esta haciendo tiene cambios que no se han guardado, desea guardarlos ahora?</div>');
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height:180,
                    modal: true,
                    dialogClass:'alerta',
                    buttons: {
                        'SI': function() {
                            salir=url;
                            $('a[rel=guardar_borrador]').click();							
                        },
                        'NO': function() {							
                            $(this).dialog('close');
                            location.href=url;
                        },
                        'Cancelar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            }
                        if($('input[name=envio_cotizaciones_correo]').val()==1){
                                $('#contenido').append('<div id="dialog-confirm" title="Un momento por favor"><span class="icono"></span>El cotizacion se ha guardado, en este momento se está enviando por correo a la empresa por favor espere</div>');
				$("#dialog-confirm").dialog({
                                    resizable: false,
                                    height:140,
                                    modal: true,
                                    dialogClass:'alerta',
                                    buttons: {
                                        'Listo': function() {
                                                    $(this).dialog('close');
                                                 }
                                    }
				});
				return false;
                        }
		});
    $('a[rel=eliminar_productos_seleccionados]').click(function(){
        var cantidad=$('#productos_cotizacion tbody input[type=checkbox]:checked').length;
         if(cantidad>0){
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                var mensaje;
                var eliminado;
                if(cantidad>1){
                    $('body').append('<div id="dialog-confirm" title="Eliminar productos seleccionados"><span class="icono"></span>Está seguro de eliminar los '+cantidad+' productos seleccionados?</div>');
                    mensaje='Se estan eliminando los productos seleccionados.';
                    eliminado='Se eliminaron los productos correctamente.';
                }else{
                    $('body').append('<div id="dialog-confirm" title="Eliminar productos seleccionados"><span class="icono"></span>Está seguro de eliminar el producto seleccionado?</div>');
                    mensaje='Se está eliminando el producto seleccionado.';
                    eliminado='Se eliminó el producto correctamente.';
                }
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    mostrar_mensaje(mensaje,'alerta',0);                                  
                                    $('#productos_cotizacion tbody input[type=checkbox]:checked').each(function(){
                                       $(this).css({background:"#FFD5D5",color:"#CC0000"});
                                       $(this).parent().parent().remove();
                                    });
                                    $('#seleccionar_todos').attr('checked',false);
                                    var repetido=0;
                                    $('input[name=cambios]').val(1);
                                    var productos_agregados=parseInt($('#productos_cotizacion >tbody >tr').length)-1;
                                    var n=1;
                                    var codigos="";
                                    $('#productos_cotizacion >tbody >tr').each(function(){
                                            if($(this).attr('class')!='agregar'){
                                                    codigos=codigos+$(this).find("input[name=codigo]").val()+' ';						
                                                    if(n % 2){
                                                            $(this).removeClass('altrow');
                                                    }else{
                                                            $(this).addClass('altrow');
                                                    }
                                                    $(this).attr('id','fila_item_'+n);
                                                    $(this).find('[id^=total_item_]').attr('id','total_item_'+n);
                                                    $(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
                                                    $(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
                                                    $(this).find('[id^=nombre_item_]').attr('id','nombre_item_'+n);
                                                    $(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
                                                    $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                            }
                                            n++;
                                    });
                                    $('#total_productos_cotizacion').html('Total de productos en el cotizacion '+productos_agregados);
                                    if(productos_agregados==0){
                                            $('#total_productos_cotizacion').html('<span class="alerta"></span> No ha agregado productos al cotizacion.'); 
                                    }
                                    //Quita colores de productos que ya no estan repetidos.
                                    $("input[name=codigo]").each(function(){
                                            if($(this).val()!=""){
                                                    productos_repetidos=codigos.split($(this).val()).length-1;
                                            }else{
                                                    productos_repetidos=0;
                                            }
                                            if(productos_repetidos>1){
                                                    $(this).parent().parent().addClass('repetido');
                                            }else{
                                                    $(this).parent().parent().removeClass('repetido');
                                            }					
                                    });
                                    //Termina quita colores de productos que ya no estan repetidos.
                                    
                                    recalcularTodo();
                                    $('#eliminar_rango_productos').hide();
                                    mostrar_mensaje(eliminado,'correcto',500);
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
        }else{
            alert('No ha seleccionado productos para eliminar');
        }
        return false; 
    });
    $('input[name=email_copias]').change(function(){
                    $(this).removeClass('error');
                    var emails=$(this).val();
                    var correos = emails.split(',');
                    for (var n=0;n< correos.length;n++){
                        if(!es_mail(correos[n])){
                           $(this).addClass('error');
                        }
                    }
    });
    $("input:checkbox[name='select_eliminar']").live('click',function(){
        $("#seleccionar_todos").attr ( "checked" , false );
        if($('#productos_cotizacion tbody input[type=checkbox]:checked').length>0){
           $('#eliminar_rango_productos').show();
        }else{
            $('#productos_cotizacion input[name=seleccionar_todos]').attr('checked',false);
            $('#eliminar_rango_productos').hide();
        }       
    });
    $('#seleccionar_todos').live('click',function(){
        if($(this).is(':checked')){
            $("input[name=select_eliminar]").attr ( "checked" ,'checked');
            $('#eliminar_rango_productos').show();
        }else{
            $("input[name=select_eliminar]").removeAttr ( "checked" );
            $('#eliminar_rango_productos').hide();
        }        
    });
    function es_mail(correo){
        var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if(correo.match(expresion)){
            return true;
        }else{
            return false;
        }
    }
    if($('input[name=nit]').val()==''){
            $('input[name=nit]').focus();
    }
    
    var version = navigator.appVersion;
	function showKeyCode(e)
	{
		var keycode =(window.event) ? event.keyCode : e.keyCode;
		if ((version.indexOf('MSIE') != -1))
		{
			if(keycode == 116 || keycode==505)
			{
				//IE
				event.keyCode = 0;
				event.returnValue = false;
				return false;
			}
		}
		else
		{
			if(keycode == 116 || keycode == 505)
			{
				if (e.preventDefault) {
					//IE
					e.preventDefault();
				}
				e.keyCode = 0;
				e.cancelBubble = true;
				e.returnValue = false;
				//e.stopPropagation works in Firefox.
				if (e.stopPropagation) {
					e.stopPropagation();
					e.preventDefault();
				}
				//Firefox, Chrome
				return false;
			}
		}
	}//Termina showKeyCode
	//Validar al presionar F5;
	$(document).keydown(function(event){
		if (event.keyCode == 116 || event.keyCode == 505) {
			// When F5 is pressed
			showKeyCode(event);
		}
	});//Termina keydown.
        
        $('a[rel=buscar_producto]').live('click',function(){
            var campo=$(this).parent().parent().attr('id');
            buscar_producto("<?php echo site_url('pedidos/buscar_producto');?>");
            $('#buscarProducto').attr('item',campo);
            $('#buscarProducto').dialog('open');
            /*var form_data={
                ajax:1
            };
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('pedidos/buscar_producto');?>",
                data: form_data,
                dataType: 'json',
                success: function(msg){
                    if(msg['estado']=='correcto'){				   
                        mostrar_mensaje(msg['mensaje'],'correcto',500);
                        $('#buscarProducto').attr('item',campo);
                        $('#buscarProducto').dialog('open');
                        $('#buscarProducto').html(msg['vista']);
                        $('#buscarProducto a[rel=mostrar_foto]').lightBox({
                            fixedNavigation:true,
                            overlayBgColor:'#FFFFFF',
                            overlayOpacity: 0.5,
                            imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
                            imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
                            imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
                            imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
                            imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
                        });   
                        $('#buscarProducto input[name=producto]').focus();                    
                    }else{
                        mostrar_mensaje(msg['mensaje'],'error');
                    }
                },
                error: function(x,e){
                    mostrar_error(x,e);
                }
            });*/
            $('body').css('overflow','hidden');
            return false;
        });//Termina buscar_producto
        //Busqueda de productos
        function buscar_producto(url){
            var buscar =$('#buscarProducto input[name=producto]').val();
            if(buscar=='Digite el código o nombre del producto'){
                    $('#buscarProducto input[name=producto]').val('');
                    buscar='';
            }
            var lista_cliente=$('select[name=lista]').val();
            if(!lista_cliente){
                lista_cliente=$('input[name=lista_cliente]').val();
            }
            var form_data={
                producto:buscar,
                lista_cliente:lista_cliente,
                ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               dataType: 'json',
               success: function(msg){
                       if(msg['estado']=='correcto'){				   
                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                            $('#buscarProducto').html(msg['vista']);
                            $('#buscarProducto a[rel=mostrar_foto]').lightBox({
                                fixedNavigation:true,
                                overlayBgColor:'#FFFFFF',
                                overlayOpacity: 0.5,
                                imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
                                imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
                                imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
                                imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
                                imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
                            });                 
                            if(buscar!=''){
                                    $('#buscarProducto #listado_productos').highlight(buscar);                        
                            }
                            columnConform();
                       }else{
                            mostrar_mensaje(msg['mensaje'],'error');
                       }
               }
             });
        }
        $('#buscarProducto form').live('submit',function(){
            mostrar_mensaje('Realizando la búsqueda. Por favor espere...','alerta',0);
            var url=$(this).attr('action');
            buscar_producto(url);
            return false;
	});	
	$('#resultados_buscar_productos th a').live('click',function(){
		mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
		var url=$(this).attr('href');
                buscar_producto(url);		
		return false;
	});
	$('#paginacion_productos a').live('click',function(){
                mostrar_mensaje('Cargando la página. Por favor espere...','alerta',0);
		var url=$(this).attr('href');
		buscar_producto(url);
		return false;
	});
	$('a[rel=boton_seleccionar_producto]').live('click',function(){
		$('body').css('overflow','auto');
                mostrar_mensaje('Agregando el producto al pedido. Por favor espere...','alerta',0);
		var fila=$('#buscarProducto').attr('item');
		$('tr#'+fila).find('input[name=codigo]').val($(this).parent().parent().find('[title=codigo]').text());
		$('tr#'+fila).find('input[name=codigo]').change();
		mostrar_mensaje('El producto fue agregado.','correcto',500);
		$('#buscarProducto').dialog('close');
		return false;
	});
	$('#buscarProducto input[name=producto]').live('focus',function(){
		var valor=$(this).val();
		if(valor=='Digite el código o nombre del producto'){
			$(this).val('');
		}
	});
	$('#buscarProducto input[name=producto]').live('focusout',function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el código o nombre del producto');
		}
	});
	
        var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array();

        function setConformingHeight(el, newHeight) {
                // set the height to something new, but remember the original height in case things change
                el.data("originalHeight", (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight")));
                el.height(newHeight);
        }

        function getOriginalHeight(el) {
                // if the height has changed, send the originalHeight
                return (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight"));
        }

        function columnConform() {
                // find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
                $('#listado_productos li.grid3').each(function() {

                        // "caching"
                        var $el = $(this);

                        var topPosition = $el.position().top;

                        if (currentRowStart != topPosition) {

                                // we just came to a new row.  Set all the heights on the completed row
                                for(currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) setConformingHeight(rowDivs[currentDiv], currentTallest);

                                // set the variables for the new row
                                rowDivs.length = 0; // empty the array
                                currentRowStart = topPosition;
                                currentTallest = getOriginalHeight($el);
                                rowDivs.push($el);

                        } else {

                                // another div on the current row.  Add it to the list and check if it's taller
                                rowDivs.push($el);
                                currentTallest = (currentTallest < getOriginalHeight($el)) ? (getOriginalHeight($el)) : (currentTallest);

                        }
                        // do the last row
                        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) setConformingHeight(rowDivs[currentDiv], currentTallest);

                });

        }


        $(window).resize(function() {
                columnConform();
        });

    // Dom Ready
    // You might also want to wait until window.onload if images are the things that
    // are unequalizing the blocks
    $(function() {
            columnConform();
    });
});
</script>
