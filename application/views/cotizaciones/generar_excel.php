<?php
error_reporting(E_ALL);
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($empresa->nombre)
							 ->setLastModifiedBy($empresa->nombre)
							 ->setTitle("Cotización")
							 ->setSubject("Copia de la cotización")
							 ->setDescription("Copia de la cotización.")
							 ->setKeywords("cotización")
							 ->setCategory("");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('H1', 'NIT. '.$empresa->nit.' - '.$empresa->nombre)
            ->setCellValue('A3', 'CLIENTE')
            ->setCellValue('H3', 'cotizacion '.$encabezado->nota_2)
            ->setCellValue('A4', 'Nit:')
            ->setCellValue('A5', 'Nombre:')
            ->setCellValue('A6', 'Contacto:')
            ->setCellValue('A7', 'Dirección:')
            ->setCellValue('A8', 'Ciudad:')
            ->setCellValue('A9', 'Teléfono:')
            ->setCellValue('A10', 'Vendedor:')
            ->setCellValue('A11', 'Plazo:')
            ->setCellValue('G4', 'Número:')
            ->setCellValue('G5', 'Fecha:')
            ->setCellValue('G6', 'Hora:')
            ->setCellValue('A13', '-')
            ->setCellValue('B13', 'CÓDIGO')
            ->setCellValue('C13', 'CANTIDAD')
            ->setCellValue('D13', 'DESCRIPCIÓN')
            ->setCellValue('E13', 'PRECIO')
            ->setCellValue('F13', '%DCTO')
            ->setCellValue('G13', '%IVA')
            ->setCellValue('H13', 'VALOR NETO');
            
            
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('C4', $encabezado->id_cliente)
            ->setCellValue('C5', $encabezado->nombre)
            ->setCellValue('C6', $encabezado->contacto)
            ->setCellValue('C7', $encabezado->direccion)
            ->setCellValue('C8', $encabezado->ciudad)
            ->setCellValue('C9', $encabezado->telefono1)
            ->setCellValue('C10', $encabezado->id_vendedor)
            ->setCellValue('C11', $encabezado->plazo)
            ->setCellValue('H4', $encabezado->id_maestro)
            ->setCellValue('H5', $encabezado->fecha)
            ->setCellValue('H6', $encabezado->hora);

$objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('C4:C11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('H4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setShowGridlines(false);
            
$objPHPExcel->getActiveSheet()->getStyle('A13:H13')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A13:H13')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setName('Calibri');
$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(13); 
$objPHPExcel->getActiveSheet()->getStyle('H3')->getFont()
												->setBold(true)
												->setName('Calibri')
												->setSize(13);
$styleArray = array(
	'borders' => array(
		'bottom' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);
$styleArray2 = array(
	'borders' => array(
		'bottom' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00E4E4E4'),
		),
	),
);

$objPHPExcel->getActiveSheet()->getStyle('A3:C3')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('G3:H3')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A13:H13')->applyFromArray($styleArray);

$n=14;
foreach($productos as $producto):
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$n, $producto->id_detalle);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$n, $producto->id_producto);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$n, $producto->cantidad);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$n)->getAlignment()->setIndent(1);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$n)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$n, $producto->nombre);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3,$n)->getAlignment()->setWrapText(true);	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$n, $producto->precio);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4,$n)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$n, $producto->descuento);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5,$n)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, $producto->iva);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $producto->total);	
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0');
	$rango="A".$n.":H".$n;
	$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray2);
	$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);	
	$n=$n+1;
endforeach;
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, 'Subtotal:');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $encabezado->subtotal_sin_dcto);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$n,"OBSERVACIONES");
$objPHPExcel->getActiveSheet()->mergeCells("A".$n.":F".$n);
$objPHPExcel->getActiveSheet()->getStyle("A".$n.":F".$n)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0,$n)->getFont()->setBold(true);
$n=$n+1;
$rango="A".$n.":F".($n*1+3);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$n, $encabezado->nota_1);
$objPHPExcel->getActiveSheet()->mergeCells($rango);
$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, 'Descuento:');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $encabezado->descuento_total);
//$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0');
$n=$n+1;
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, 'Neto:');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $encabezado->subtotal);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0');
$n=$n+1;
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, 'Iva:');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $encabezado->iva_total);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('#,##0');
$n=$n+1;
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$n, 'Total =>');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,$n)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$n, $encabezado->total);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getNumberFormat()->setFormatCode('$ #,##0');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,$n)->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.5);
$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.2);
$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.2);
$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.5);

$objPHPExcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPrintArea('A1:H'.$n);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('cotizacion '.$encabezado->id_maestro);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(52);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file
//$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
//Save Excel 2003
$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="cotizacion '.$encabezado->id_maestro.'.xls"');
header('Cache-Control: max-age=0');

$objWriter->save('php://output');
exit;

?>