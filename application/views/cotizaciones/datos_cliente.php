<ul class="formulario">
    <li class="left cuadro noderecha" style="height:50px;width:30%;"><p id="nombre_cliente"><?php echo $cliente->nombre;?></p><span class="sub_label">Nombre del cliente</span></li>
    <li class="left cuadro" style="height:50px;width:30%;"><?php echo $cliente->contacto;?><span class="sub_label">Contacto</span></li>
    <li class="left cuadro noizquierda" style="height:50px;width:40%;"><?php echo $cliente->email;?><span class="sub_label">E-mail</span></li>
    <div class="clear"></div>
    <li class="left cuadro noderecha noarriba" style="height:50px;width:40%;"><?php echo $cliente->direccion;?><span class="sub_label">Dirección</span></li>
    <li class="left cuadro noarriba" style="height:50px;width:30%;"><?php echo $cliente->ciudad;?><span class="sub_label">Ciudad</span></li>
    <li class="left cuadro noizquierda noarriba" style="height:50px;width:30%;"><?php echo $cliente->telefono1;?><span class="sub_label">Teléfono</span></li>
    <div class="clear"></div>
    <li class="left cuadro noarriba" style="height:50px;width:45%;">
	<?php 
	$adicional='';
	if(!$configuracion['cambiar_asesor']){
		$adicional='disabled="disabled"';
	}
	 echo form_dropdown('asesor',$vendedores,$cliente->id_vendedor,$adicional);
if(!in_array($cliente->id_vendedor,$this->session->userdata('numeros_vendedor'))){
	echo ' <div style="cursor:pointer;display:inline;color:#930;" title="Usted no aparece como el asesor comercial de este cliente. Por favor elija el número correcto y comuníquese con cartera para su corrección."><span class="alerta"></span>Alerta</div>';
}
?><span class="sub_label">Asesor</span></li>
	<?php if($configuracion['listar_plazos']){?>
    <li class="left cuadro noarriba" style="height:50px;width:10%;"><?php echo form_dropdown('plazo',array('0'=>'0','15'=>'15','30'=>'30','45'=>'45','60'=>'60','90'=>'90'),$cliente->plazo);?><span class="sub_label">Plazo</span></li>
    <?php }?>
	<?php if($configuracion['listar_descuentos']){?>
    <li class="left cuadro noarriba" style="height:50px;width:10%;"><?php echo form_dropdown('descuento_cotizacion',array('0'=>'0','10'=>'10','15'=>'15'));?><span class="sub_label">Descuento</span></li>
    <?php }?>
	<?php if($configuracion['listar_listas']){?>
	<li class="left cuadro noizquierda noarriba" style="height:50px;width:10%;"><?php $precios=array('1024'=>1,'1025'=>2,'1026'=>3); $precio=1; if(isset($precios[$cliente->precio_lista])){$precio=$precios[$cliente->precio_lista];} echo form_dropdown('lista',array('1'=>'1','2'=>'2','3'=>'3'),$precio);?><span class="sub_label">Lista</span></li>
    <?php }?>
	<?php if($configuracion['listar_retenciones']){?>
    <li class="left cuadro noizquierda noarriba" style="height:50px;width:35%;"><?php
echo form_dropdown('retencion',array(''=>'','1'=>'No retiene','2'=>'Retiene sobre la base','3'=>'Retiene sobre cualquier valor','3'=>'Autorretenedor y retiene sobre la base','4'=>'Autorretenedor y retiene sobre cualquier valor'));
?><span class="sub_label">Retención</span></li>
    <?php }?>
    <li class="left cuadro noizquierda noarriba" style="height:50px;width:22%;"><?php
echo form_input(array('name'=>'direccion_envio','value'=>'','maxlength'=>'25','size'=>25));?><span class="sub_label">Dirección de envío</span></li>
    <div class="clear"></div>
</ul>
<script type="text/javascript">
$(document).ready(function(){
	$('a[rel=boton_cambiar_cliente]').click(function(){
			var tipo=$(this).html();
			if(tipo=="Cambiar cliente"){
					$(this).html('Cancelar cambio');
					$(this).addClass('boton_cerrar');
					$('#buscarCliente').html('');
					$('#buscador_cliente').show();
			}
			else{
					$(this).html('Cambiar cliente');
					$(this).removeClass('boton_cerrar');
					$('#buscarCliente').html('');
					$('#buscador_cliente').hide();
			}
			return false;
	});
	$('a[rel=mostrar_ocultar]').click(function(){
		var valor=$(this).text();
		if(valor=='Ocultar cartera'){
			$(this).text('Mostrar cartera');
			$('#cartera_cliente').slideUp('fast');
		}else{
			$(this).text('Ocultar cartera');
			$('#cartera_cliente').slideDown('fast');
		}
		return false;
	});
});
</script>