<table class="tabla">
<thead><tr><th style="height:30px;">Vd</th><th>Subtotales</th></tr></thead>
<tbody>
<?php
if($cotizacioness_totales){
$suma=0;
foreach($cotizacioness_totales as $cotizacioness):
?>
<tr><td><?php echo $cotizacioness->id_vendedor;?></td><td style="text-align:right;"><?php echo '$ '.number_format($cotizacioness->subtotal,0,'.',',');?></td></tr>
<?php
$suma+=$cotizacioness->subtotal;
endforeach;
?>
	<tr><td class="item" style="color:#900;">Total</td><td class="item" style="text-align:right; color:#900; text-shadow: 0 1px 1px rgba(0,0,0,.3);"><?php echo '$ '.number_format($suma,0,'.',',');?></td></tr>
<?php }else{?>
<tr><td colspan="2">No hay datos en este mes</td></tr>
<?php }?>
</tbody>
</table>