<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url();?>/favicon.ico" type="image/ico"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/login.css" type="text/css" media="screen" charset="utf-8" />
<?php
if(isset($estilos)){
foreach($estilos as $estilo):
echo '<link rel="stylesheet" href="'.base_url().'css/'.$estilo.'.css" type="text/css" media="screen" charset="utf-8" />';
endforeach;
}
?>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-1.6.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-ui-1.8.10.custom.min.js"></script>
<title><?php if(isset($titulo)){echo $titulo;}else{echo "Pedidos";}?></title>
</head>
<body>
    <div style="display:table; width: 100%; height: 100%;">
    <div id="contenedor">
        <div id="contenido">
            <div class="logo_empresa">
                    <img src="<?php echo base_url();?>/img/logo_reypar.png" border="0" height="90px" />            
            </div>
            <div id="ingreso">
                        <?php
                        echo form_open('inicio/acceder');

                        echo form_input(array('name'=>'usuario','class'=>'usuario'));
                        echo form_password(array('name'=>'clave','class'=>'clave'));
                        echo form_submit('entrar','Entrar');
                        ?>            
                        <?php echo form_close();?>

            </div>
        </div>
        <div id="pie">&copy; 2010<?php $año=date('Y',time()); if($año>2010){echo ' - '.$año;}?> Todos los derechos reservados <a href="http://www.diegopego.com" target="_blank">Diego Peláez Gómez</a></div>        
    </div>
    </div>
    </body>
</html>
<script type="text/javascript">
$(document).ready(function(){
    $('#ingreso').append('<div id="mensajes"></div>');
	$('#ingreso form').submit(function(){
            var url=$(this).attr('action');
            var usuario=$('input[name=usuario]').val();
            var clave=$('input[name=clave]').val();
            var error=false;
	   $('input[name=usuario]').removeClass('error');
	   $('input[name=clave]').removeClass('error');
           $('#mensajes').removeClass('error correcto');
           $('#mensajes').html('<span class="cargando"></span> Verificando los datos, por favor espere');
	   if(usuario==''){
		   $('input[name=usuario]').addClass('error');
		   error=true;
	   }
	   if(clave==''){
		   $('input[name=clave]').addClass('error');
		   error=true;
	   }
	   if(!error){
			var form_data={
					usuario:usuario,
					clave:clave,
					ajax:1
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
                                  if(msg['estado']==true){
                                        $('#mensajes').html('Datos correctos. Espere un momento por favor.');
                                        $('#mensajes').addClass('correcto');
                                        location.href='<?php echo site_url('menu');?>';
				   }else{
                                       $('#mensajes').html('<span class="icono"></span> El usuario o clave no son correctos. Por favor verifique los datos.');
                                       $('#mensajes').addClass('error');
                                       $('input[name=usuario]').addClass('error');
                                       $('input[name=clave]').addClass('error');
                                       $('#ingreso').effect('shake',{},100);
				   }
			   },
			   error: function(x,e){
				   $('#cargando').addClass('error');
					if(x.status==0){
							$('#cargando').html('Se perdió la conexión!!. Por favor verifique su conexión a internet.');
					}else if(x.status==404){
							$('#cargando').html('La url buscada no se encontró.');
					}else if(x.status==500){
							$('#cargando').html('Error interno del servidor.');
					}else if(e=='parsererror'){
							$('#cargando').html('Error.\nParsing JSON Request failed.');
					}else if(e=='timeout'){
							$('#cargando').html('Se ha demorado mucho la operación, inténtelo nuevamente.');
					}else {
							$('#cargando').html('Error desconocido. '+x.responseText);
					}
			   }
			 });			 
	   }else{
               
               $('#ingreso').effect('shake',{},100);
               $('#mensajes').addClass('error');
               $('#mensajes').html('Los campos son obligatorios.');               
	   }	   
	   return false;
	});
});
</script>