<div id="pie">
    <div class="container16">
        <div class="grid12">&copy; 2010<?php $año=date('Y',time());if($año>2010){echo ' - '.$año;}?> Todos los derechos reservados <a href="http://www.diegopego.com" target="_blank">Diego Peláez Gómez</a></div>
        <div class="grid4" style="text-align: right;"><?php echo anchor('sipe/acerca','Acerca del '.img(array('src'=>'img/logo_sipe.png','height'=>'20px','border'=>0,'style'=>'vertical-align:middle;')),'onclick="return false;"');?></div>
        <div class="clear"></div>
    </div>
</div>
<div id="mensajes"></div>
    <div id="inactividad"></div>
</body>
</html>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.timers.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        var contador=0
        var tiempo_inactividad=900;
        var tiempo_expira=1200;
        $(document).everyTime(1000, 'controlled', function() {
            contador++;
            if(contador==tiempo_inactividad){
                $('#inactividad').html('Lleva 15 minutos de inactividad su session expira a los 20 minutos.');
                $('#inactividad').show('slow');
            }
            if(contador==tiempo_expira){
                location.href="<?php echo base_url();?>sipe/cerrar_sesion";
            }
	});
        $(document).mousemove(function(){
            $('#inactividad').hide('slow');
            contador=0;
        });	
        $(document).keypress(function(){
            $('#inactividad').hide('slow');
            contador=0;
        });
        $('#menu a').mouseover(function(){
            var titulo=$(this).find('img').attr('title');
            if(!titulo){
                titulo=$(this).attr('title');
            }
            if(titulo){
               $('body').append('<div id="cuadro_detalle_menu"><span class="flecha"></span><div class="titulo"></div></div>'); 
               $('#cuadro_detalle_menu .titulo').html(titulo);
               var anchoboton=$(this).width();
               var ancho_cuadro=$('#cuadro_detalle_menu').width();
               var izq=$(this).offset().left+(anchoboton/2)-ancho_cuadro/2;
               
               $('#cuadro_detalle_menu').css({'left':(izq)});
               $('#cuadro_detalle_menu .flecha').css({'left':($('#cuadro_detalle_menu').width()/2)});
               $('#cuadro_detalle_menu').show();
            }
        });
        $('#menu a').mouseout(function(){
            $('#cuadro_detalle_menu').remove();
        });        
    });
    $('#mensajes').click(function(){
       $(this).hide(); 
    });
</script>