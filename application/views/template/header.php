<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url();?>/favicon.ico" type="image/ico"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/grid_978.css" type="text/css" media="screen" charset="utf-8" />
<?php
if(isset($estilos)){
foreach($estilos as $estilo):
echo '<link rel="stylesheet" href="'.base_url().'css/'.$estilo.'.css" type="text/css" media="screen" charset="utf-8" />';
endforeach;
}
?>
<link rel="stylesheet" href="<?php echo base_url();?>css/sipe.css" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/sipe.js"></script>
<title><?php if(isset($titulo)){echo $titulo;}else{echo "Sistema de Pedidos";}?></title>
<style>
    .container16{
        <?php 
        $tipo_menu=0;
        if(isset($info_usuario)){
            if($info_usuario->tipo_vista == '0'){
                if($info_usuario->ancho_vista > 0){
                    echo 'width:'.$info_usuario->ancho_vista.';';
                }else{
                    echo 'width:978px;';
                }
            }else{
                
            }
            echo 'margin: 0 auto;';
            $tipo_menu=$info_usuario->tipo_menu;
        }?>        
    }
</style>
</head>
<body>
    <?php $segmento=$this->uri->segment(1);?>
    <div id="acceso">
        <table width="100%" cellspacing="0" cellpadding="0" class="container16" style="border-collapse:collapse;">
            <tr>
                <td style="text-align:left;">
                    <ul id="menu">
                        <li><?php echo anchor('menu',img(array('src'=>'img/logo_sipe_blanco.png','alt'=>'Tablero','border'=>'0')),'class="tablero"');?></li>
                        <li class="<?php if(strtolower($segmento)=="pedidos"){echo 'seleccionado';}?>"><?php echo anchor('pedidos',($tipo_menu==1)? img(array('src'=>'img/pedidos.png','alt'=>'Pedidos','title'=>'Pedidos','border'=>'0','height'=>'25px')): 'Pedidos');?></li>
                        <li class="<?php if(strtolower($segmento)=="cotizaciones"){echo 'seleccionado';}?>"><?php echo anchor('cotizaciones',($tipo_menu==1)? img(array('src'=>'img/cotizaciones.png','alt'=>'Cotizaciones','title'=>'Cotizaciones','border'=>'0','height'=>'25px')): 'Cotizaciones');?></li>
                        <li class="<?php if(strtolower($segmento)=="recibos"){echo 'seleccionado';}?>"><?php echo anchor('recibos',($tipo_menu==1)? img(array('src'=>'img/recibos.png','alt'=>'Recibos','title'=>'Recibos provisionales','border'=>'0','height'=>'25px')): 'Recibos');?></li>
                        <li class="<?php if(strtolower($segmento)=="productos"){echo 'seleccionado';}?>"><?php echo anchor('productos',($tipo_menu==1)? img(array('src'=>'img/productos.png','alt'=>'Productos','title'=>'Productos','border'=>'0','height'=>'25px')) : 'Productos');?></li>    
                    <?php if($this->session->userdata('rol')!=3){?>
                        <li class="<?php if(strtolower($segmento)=="clientes"){echo 'seleccionado';}?>"><?php echo anchor('clientes',($tipo_menu==1)? img(array('src'=>'img/clientes.png','alt'=>'Clientes','title'=>'Clientes','border'=>'0','height'=>'25px')) : 'Clientes');?></li>
                    <?php } ?>
                    <?php if($this->session->userdata('rol')!=3){?>
                        <li class="<?php if(strtolower($segmento)=="cartera"){echo 'seleccionado';}?>"><?php echo anchor('cartera',($tipo_menu==1)? img(array('src'=>'img/cartera.png','alt'=>'Cartera','title'=>'Cartera','border'=>'0','height'=>'25px')) : 'Cartera');?></li>
                        <li class="<?php if(strtolower($segmento)=="movimientos"){echo 'seleccionado';}?>"><?php echo anchor('movimientos',($tipo_menu==1)? img(array('src'=>'img/movimientos.png','alt'=>'Movimientos','title'=>'Movimientos','border'=>'0','height'=>'25px')) : 'Movimientos');?></li>
                    <?php } ?>
                    <?php if($this->session->userdata('rol')==1 || $this->session->userdata('rol')==4){?>
                        <li class="<?php if(strtolower($segmento)=="administrar"){echo 'seleccionado';}?>"><?php echo anchor('administrar',($tipo_menu==1)? img(array('src'=>'img/administrar.png','alt'=>'Administrar','title'=>'Administrar','border'=>'0','height'=>'25px')) : 'Administrar');?></li>
                    <?php }?>
                        <div class="clear"></div>
                    </ul>
                </td>
                <td style="text-align:right;">
                    <ul id="usuario-actual">
                        <li><?php echo anchor('cuenta',$this->session->userdata('nombreCompleto'),'class="usuario"');?></li>
                        <li class="cuenta <?php if(strtolower($segmento)=="cuenta"){echo 'seleccionado';}?>"><?php echo anchor('cuenta',img(array('src'=>'img/config.png','alt'=>'Mi cuenta','title'=>'Mi cuenta','border'=>'0','height'=>'25px')));?></li>
                        <li><?php echo anchor('sipe/cerrar_sesion','Salir','class="boton boton_cerrar"');?></li><div class="clear"></div>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div id="encabezado">
        <div class="container16">
            <div class="grid8" style="text-align:left;"><img src="<?php echo base_url();?>img/logo_empresa.png" border="0" height="50px" /></div>
            <div class="grid8" style="text-align:right;">
                <?php if(in_array($segmento,array('pedidos','cotizaciones','recibos','productos','clientes','cartera','movimientos','administrar'))){?>
                <img src="<?php echo base_url();?>img/<?php echo $segmento;?>.png" alt="A" border="0" height="50px" />
                <?php }?>
            </div>
        </div>           
    </div>