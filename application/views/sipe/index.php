<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url();?>/favicon.ico" type="image/ico"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/grid_978.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/sipe.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/login.css" type="text/css" media="screen" charset="utf-8" />
<?php
if(isset($estilos)){
foreach($estilos as $estilo):
echo '<link rel="stylesheet" href="'.base_url().'css/'.$estilo.'.css" type="text/css" media="screen" charset="utf-8" />';
endforeach;
}
?>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-ui-1.8.10.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/sipe.js"></script>
<title><?php if(isset($titulo)){echo $titulo;}else{echo "Pedidos";}?></title>
</head>
<body>
    <table id="contenedor" width="100%" height="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td style="vertical-align: middle;">
                <div id="contenido">
                    <div class="logo_empresa">
                            <img src="<?php echo base_url();?>/img/logo_empresa.png" border="0" height="160" />            
                    </div>
                    <div id="ingreso">
                                <?php
                                echo form_open('sipe/acceder');
                                echo '<div style="position:relative;">'.form_input('usuario','','class="usuario"');
                                echo '<span class="label_usuario">Usuario</span></div>';
                                echo '<div style="position:relative;">'.form_password('clave','','class="clave"');
                                echo '<span class="label_clave">Contraseña</span></div>';
                                echo form_submit('entrar','Entrar');
                                echo form_close();
                                ?>
                    </div>
                    <div>
                        <?php if($configuracion->mostrar_mensaje){?>
                <h1 style="text-align: center;"><?php echo $configuracion->mensaje;?></h1>
                    
                <?php }?>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td height="80px">
                <div id="pie">
                    <div class="container16">
                        <div class="grid12">&copy; 2010<?php $año=date('Y',time());if($año>2010){echo ' - '.$año;}?> Todos los derechos reservados <a href="http://www.diegopego.com" target="_blank">Diego Peláez Gómez</a></div>
                        <div class="grid4" style="text-align: right;"><?php echo anchor('sipe/acerca','Acerca del '.img(array('src'=>'img/logo_sipe.png','height'=>'20px','border'=>0,'style'=>'vertical-align:middle;')),'onclick="return false;"');?></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </td>
        </tr>                    
    </table>
    
    </body>
</html>
<script type="text/javascript">
$(document).ready(function(){
    $('#ingreso').append('<div id="mensajes"></div>');
    $('span.label_usuario').click(function(){
        $('input[name=usuario]').focus();
    });
    $('span.label_clave').click(function(){
        $('input[name=clave]').focus();
    });
    $('input[name=usuario]').focus(function(){
       $('span.label_usuario').hide();
    });
    $('input[name=usuario]').blur(function(){
        if($(this).val()==''){
            $('span.label_usuario').show();
        }else{
            $('span.label_usuario').hide();
        }       
    });
    $('input[name=clave]').focus(function(){
       $('span.label_clave').hide();
    });
    $('input[name=clave]').blur(function(){
        if($(this).val()==''){
            $('span.label_clave').show();
        }else{
            $('span.label_clave').hide();
        }       
    });
	$('#ingreso form').submit(function(){
            var url=$(this).attr('action');
            var usuario=$('input[name=usuario]').val();
            var clave=$('input[name=clave]').val();
            var error=false;
	   $('input[name=usuario]').removeClass('error');
	   $('input[name=clave]').removeClass('error');
           mostrar_mensaje('Verificando los datos, por favor espere...','alerta',0);
           if(usuario==''){
		   $('input[name=usuario]').addClass('error');
		   error=true;
	   }
	   if(clave==''){
		   $('input[name=clave]').addClass('error');
		   error=true;
	   }
	   if(!error){
			var form_data={
					usuario:usuario,
					clave:clave,
					ajax:1
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
                                  if(msg['estado']=='correcto'){
                                      mostrar_mensaje('Datos correctos. Espere un momento por favor...','correcto');
                                        location.href='<?php echo site_url('menu');?>';
				   }else{
                                       mostrar_mensaje(msg.mensaje,'error');
                                       $('input[name=usuario]').addClass('error');
                                       $('input[name=clave]').addClass('error');
                                       $('#ingreso').effect('shake',{},100);
				   }
			   },
			   error: function(x,e){
				   mostrar_error(x,e);
			   }
			 });			 
	   }else{               
               $('#ingreso').effect('shake',{},100);
               mostrar_mensaje('Los campos son obligatorios.','error');               
	   }	   
	   return false;
	});
});
</script>