<h1>Configurar mi cuenta</h1>
<div class="form grid8">
    <h2>Configuracion de la vista</h2>
    <div>
          <?php echo form_open('cuenta/configuracion_vista',array('name'=>'configuracion_vista'));
            echo form_label('Tamaño de vista:','tipo_vista');
            $tipo_vista=$info_usuario->tipo_vista;
            if($tipo_vista=='1'){
                $fluido=true;
                $fijo=false;
                $display='display:none;';
            }else{
                $fluido=false;
                $fijo=true;
                $display='display:block;';
            }
            $ancho_vista='978px';
            if($info_usuario->ancho_vista!='0'){
                $ancho_vista=$info_usuario->ancho_vista;
            }
            echo form_label(form_radio(array('name'=> 'tipo_vista','id'=> 'tipo_vista_fluido','value'=> 1,'checked'=>$fluido)).' Fluido (Ocupa toda la pantalla)','tipo_vista_fluido');
            echo form_label(form_radio(array('name'=> 'tipo_vista','id'=> 'tipo_vista_fijo','value'=> 0,'checked'=>$fijo)).' Fijo','tipo_vista_fijo');
            echo '<div class="ancho_fijo" style="'.$display.'">Ancho: '.form_input(array('name'=>'ancho_vista','value'=>$ancho_vista,'size'=>5)).'<span style="font-size:11px; color:#900;">Por defecto 978px</span></div>';            
          ?>            
    </div>
    <div style="margin-top:20px;">
        <?php
        echo form_label('Mostrar menu como:','tipo_menu');
        if($info_usuario->tipo_menu=='1'){
                $tm_texto=false;
                $tm_imagenes=true;                
            }else{
                $tm_texto=true;
                $tm_imagenes=false;
            }
        echo form_label(form_radio(array('name'=> 'tipo_menu','id'=> 'tipo_menu_texto','value'=> 0,'checked'=>$tm_texto)).' Texto','tipo_menu_texto');
        echo form_label(form_radio(array('name'=> 'tipo_menu','id'=> 'tipo_menu_imagenes','value'=> 1,'checked'=>$tm_imagenes)).' Imágenes','tipo_menu_imagenes');
        ?>
    </div>
    <div>
        <?php
        echo form_submit('guardar','Guardar cambios');
        echo form_close();
        ?>
    </div>
</div>
<div class="grid8" style="border-left: 1px dashed #CCC;">
    <div class="form" style="padding-left:20px;">
        <h2>Cambio de clave</h2>
        <?php echo form_open('cuenta/cambiar_clave',array('name'=>'clave'));?>
        <?php echo form_label('Clave actual','clave_actual');?>
        <?php echo form_password('clave_actual');?>
        <div style="margin-top:10px;">
        <?php echo form_label('Clave nueva','clave_nueva');?>
        <?php echo form_password('clave_nueva');?>
        </div>
        <div style="margin-top:10px;">
        <?php echo form_label('Repetir clave nueva','rep_clave_nueva');?>
        <?php echo form_password('rep_clave_nueva');?>
        </div>
        <div style="margin-top:10px;">
        <?php echo form_submit('enviar','Cambiar');?>
        </div>
        <?php echo form_close();?>
    </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
$(document).ready(function(){
        $('input[name=tipo_vista]').change(function(){
            var valor=$('input:radio[name=tipo_vista]:checked').val();
            if(valor==1){
                $('.ancho_fijo').hide();
            }else{
                $('.ancho_fijo').show();
            }
        });
	$('form[name=clave]').submit(function(){
                mostrar_mensaje('Verificando datos.','alerta',0);
		var url = $(this).attr('action');
		var clave_actual=$('input[name=clave_actual]').val();
		var clave_nueva=$('input[name=clave_nueva]').val();
		var rep_clave_nueva=$('input[name=rep_clave_nueva]').val();
		var error=false;
		$('span.error').remove();
		if(clave_actual==''){
			$('input[name=clave_actual]').after('<span class="error">* Debe escribir su clave actual</span>');
			$('input[name=clave_actual]').addClass('error');
			error=true;
		}else{
			$('input[name=clave_actual]').removeClass('error');
		}
		if(clave_nueva.length<4){
			$('input[name=clave_nueva]').after('<span class="error">* La clave debe contener mínimo 4 caracteres.</span>');
			$('input[name=clave_nueva]').addClass('error');
			error=true;
		}else{
			$('input[name=clave_nueva]').removeClass('error');
		}
		if(clave_nueva!=rep_clave_nueva){
			$('input[name=rep_clave_nueva]').after('<span class="error">* La clave no coincide con la clave nueva digitada.</span>');
			$('input[name=rep_clave_nueva]').addClass('error');
			error=true;
		}else{
			$('input[name=rep_clave_nueva]').removeClass('error');
		}
		if(!error){
                        mostrar_mensaje('Cambiando la clave. Espere un momento por favor.','alerta',0);
			var form_data={
				clave_actual:clave_actual,
				clave_nueva:clave_nueva
			};
			$.ajax({
			   type: "POST",
			   url: url,
			   data: form_data,
                           dataType: 'json',
			   success: function(msg){
					if(msg['estado']=='correcto'){
                                            mostrar_mensaje(msg['mensaje'],'correcto');
                                            $('input[name=clave_actual]').val('');
                                            $('input[name=clave_nueva]').val('');
                                            $('input[name=rep_clave_nueva]').val('');                                            
					}
				   else{
					mostrar_mensaje(msg['mensaje'],'error');
                                        $('input[name=clave_actual]').after('<span class="error">* La clave actual no es válida</span>');
				   }
			   },
			   error: function(x,e){
				   mostrar_error(x,e);			   
			   }
			});
		}else{
                    mostrar_mensaje('Verifique los datos marcados con error.','error');			
		}
		return false;
	});
        $('form[name=configuracion_vista]').submit(function(){
                var url = $(this).attr('action');
                mostrar_mensaje('Guardando su configuración. Espere un momento por favor.','alerta',0);
                var form_data={
                        tipo_vista:$('input:radio[name=tipo_vista]:checked').val(),
                        ancho_vista:$('input[name=ancho_vista]').val(),
                        tipo_menu:$('input:radio[name=tipo_menu]:checked').val(),
                        ajax:1
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType: 'json',
                   success: function(msg){
                           if(msg['estado']=='correcto'){
                                    mostrar_mensaje(msg['mensaje'],'correcto');                                    
                                    location.href='<?php echo site_url('cuenta');?>';
                           }
                           else{
                                mostrar_mensaje(msg['mensaje'],'error');                                
                           }
                   },
                   error: function(x,e){
                           mostrar_error(x,e);			   
                   }
                });
		return false;
	});
});
</script>
