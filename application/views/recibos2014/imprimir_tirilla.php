<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/imprimir.css" type="text/css" media="screen" charset="utf-8" /> 
<title>Imprimir recibo</title>
<style type="text/css" media="print">
    @page{
        margin:0cm;
        margin-top:0cm;
        margin-left:0.5cm;
        margin-right:0.5cm;
    }
    body,html{
            font-family:Arial;
            font-size:9px;
            margin:0;
            padding:0;
    }
    h1{
            font-size:9px;
            text-align:center;
    }
    #recibo
    {
            width:4.8cm;
    }
    table#documentos_recibo thead th{
        border: 1px solid #000;
        text-align: center;
    }
    table#documentos_recibo tbody td{
        padding:2px;
    }
    #imprimir{
        display:none;
    }
</style>
<style type="text/css" media="screen">
h1{
	font-size:9px;
	text-align:center;
}
#recibo
{
        margin: 0 auto;
        margin-bottom:12px;
    	background:#FFF;
	width:4.8cm;
}
</style>
<script type="javascript">
</script>
</head>
<body onload="javascript:window.print();">
    <div id="imprimir"><div><span class="logo"><?php echo anchor('',img(array('src'=>'img/logo_sipe_blanco.png','border'=>0)));?></span><?php $texto=img(array('src'=>'img/printer.png','border'=>0,'style'=>'vertical-align:middle;')).' Imprimir'; ?><a href="javascript:window.print();"><?php echo $texto;?></a></div><div class="clear"></div></div>
<div id="recibo" class="hoja">
<h1 style="text-align:center;"><?php echo $empresa->nombre;?><br/>NIT. <?php echo $empresa->nit;?></h1>
        <table style="width:100%; vertical-align: top;" cellspacing="0" cellpadding="1">
            <tr><td colspan="2" style="text-align:right;vertical-align: top;"><b>Recibo provisional # <?php echo str_pad($encabezado->id, 6, "0", STR_PAD_LEFT);?></b></td></tr>
            <tr><td colspan="2" style="text-align:right;font-size: 9px;vertical-align: top;"><b>Consecutivo vendedor: <?php echo $encabezado->numero*1;?></b></td></tr>                
            <tr><td style="text-align:left;vertical-align: top;">Diligenciado: </td><td style="text-align:right;vertical-align: top;"><?php echo $encabezado->fecha_realizado;?></td></tr>
        </table>
        <table cellspacing="0" cellpadding="1">
            <tr><td><b>Cliente: </b><?php echo $encabezado->cliente.' '.$encabezado->nombre;?></td></tr>
            <tr><td><?php if($encabezado->contacto){echo '<b>Contacto: </b>'.$encabezado->contacto;}?></td></tr>
            <tr><td><b>Asesor: </b><?php echo $encabezado->vendedor.' - '.$encabezado->nombre_vendedor;?></td></tr>
        </table>
        <table id="documentos_recibo" align="center" width="98%" style="border-collapse:collapse;" cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <th style="width:20%">Doc</th>
                <th>Detalle</th>                
            </tr>
            </thead>
        <tbody>
        <?php $n=1;foreach($documentos as $documento):
				/*$texto=$producto->id_producto.' -> '.$producto->nombre;
				$cantidad=ceil(strlen($texto)/20);
				for($n=0;$n<$cantidad;$n++){
					if($n==0){
						$palabra=substr($producto->nombre,0,20-strlen($producto->id_producto.' -> '));
					}else{
						$palabra=$palabra.'<br/>'.substr($texto,$n*20,20);
					}
				}*/
		?>
            <tr style="vertical-align:text-top;border:1px solid #777;">
               <td><?php echo $documento->documento;?></td>
               <td style="text-align:right;">
                   Valor: $ <?php echo number_format($documento->valor,0,'.',',');?>
                   <?php if($documento->descuento<>0){ echo '<br/>- DCTO: $ '.number_format($documento->descuento,0,'.',',');}?>
                   <?php if($documento->rete_fte<>0){ echo '<br/>- Rtefte: $ '.number_format($documento->rete_fte,0,'.',',');}?>
                   <?php if($documento->nota_credito<>0){ echo '<br/>- N.C. '.' ('.$documento->nota_credito_detalle.')'.': $ '.number_format($documento->nota_credito,0,'.',',');}?>
                   <?php if($documento->nota_debito<>0){ echo '<br/>+ Dcto canc y sob: $ '.number_format($documento->nota_debito,0,'.',',');}?>
                   <?php if($documento->cree<>0){ echo '<br/>- CREE: $ '.number_format($documento->cree,0,'.',',');}?>
                   <?php if($documento->otros<>0){ 
                       $decimal=strstr($documento->otros,'.');
                        if($decimal=='.00'){
                            $otros_valor = number_format($documento->otros,0,',','.');
                        }else{
                            $otros_valor = number_format($documento->otros,2,',','.');
                        }
                       echo '<br/>- Otros: $ '.$otros_valor;
                       
                   }?>
                   <div style="padding:4px 0px;"><span style="border-top:1px solid #000;padding-top:2px;"><b><?php  echo 'Total doc: $ '.number_format($documento->total,0,'.',',');?></b></span></div>
               </td>
            </tr>
        <?php $n++; endforeach;?>
        </tbody>
        </table>
        <?php
                if(is_array($cheques)){                   
                
            ?>
        <table align="center" style="margin-top:2px;font-size:7px;width: 98%; border:1px solid #000;">
            <tr>
                <th> RELACION DE CHEQUES</th>
            </tr>
            <tr>
                <th style="text-align:left;">BANCO -> CHEQUE -> POSFECHADO -> VALOR</th>
            </tr>
            <?php foreach($cheques as $cheque):?>
            <?php $valor=$cheque->valor;
                        $decimal=strstr($valor,'.');
                        if($decimal=='.00' || !$decimal){
                            $valor = number_format($valor,0,',','.');
                        }else{
                            $valor = number_format($valor,2,',','.');
                        }
            
            ?>
            <tr>
                <td><?php echo $cheque->banco.' -> '.$cheque->cheque.' -> '.(($cheque->posfechado==1)?'Si ('.$cheque->fecha_posfechado.')':'No').' -> '.$valor;?></td>
            </tr>
            <?php endforeach;?>
        </table>
        <?php 
            }?>
            <?php
                if(is_array($consignaciones)){                   
                
            ?>
        <table align="center" style="margin-top:2px;font-size:7px;width: 98%; border:1px solid #000;">
            <tr>
                <th> RELACION DE CONSIGNACIONES</th>
            </tr>
            <tr>
                <th style="text-align:left;">BANCO -> CUENTA -> FECHA -> NUMERO -> VALOR</th>
            </tr>
            <?php foreach($consignaciones as $consignacion):?>
            <?php $valor=$consignacion->valor;
                        $decimal=strstr($valor,'.');
                        if($decimal=='.00' || !$decimal){
                            $valor = number_format($valor,0,',','.');
                        }else{
                            $valor = number_format($valor,2,',','.');
                        }
            
            ?>
            <tr>
                <td><?php echo $consignacion->banco_nombre.' -> '.$consignacion->cuenta_numero.' -> '.$consignacion->fecha.' -> '.$consignacion->numero.' -> '.$valor;?></td>
            </tr>
            <?php endforeach;?>
        </table>
        <?php 
            }?>
        <table class="subtotales" style="width:100%;margin-top:2px; border-top:1px dashed #000;">
            <tr>
                  <td style="width:100px;" class="item">Total Efectivo:</td><td id="totales_subtotal" style="text-align:right;">$ <?php echo number_format($encabezado->total_efectivo,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Total Cheques:</td><td id="totales_subtotal" style="text-align:right;">$ <?php echo number_format($encabezado->total_cheques,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item"><b>Total Recibo:</b></td><td id="totales_subtotal" style="text-align:right;"><b>$ <?php echo number_format($encabezado->total,0,'.',',');?></b></td>
            </tr>            
        </table>        
</div>
</body>
</html>