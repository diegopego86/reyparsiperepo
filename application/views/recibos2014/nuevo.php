<style>
.ui-datepicker,.ui-dialog {
	font-size: 90%;
}

.alerta .ui-helper-clearfix button.ui-state-default {
	border: 1px solid #900;
	color: #FADDDE;
	background: #980C10;
	background: -webkit-gradient(linear, left top, left bottom, from(#ED1C24),to(#AA1317) );
	background: -moz-linear-gradient(center top, #ED1C24, #AA1317) repeat scroll 0 0 transparent;
	filter: progid :  DXImageTransform.Microsoft.gradient (startColorstr = '#ED1C24', endColorstr ='#AA1317' );
}

.alerta .ui-helper-clearfix button.ui-state-default:hover {
	background: #AA1317;
	background: -webkit-gradient(linear, left top, left bottom, from(#AA1317),to(#ED1C24) );
	background: -moz-linear-gradient(center top, #AA1317, #ED1C24) repeat scroll 0 0 transparent;
	filter: progid: DXImageTransform.Microsoft.gradient ( startColorstr = '#AA1317', endColorstr = '#ED1C24');
	color: #DE898C;
}
</style>
<div id="recibos" class="form">
<div class="grid16">
<h1>Nuevo recibo provisional de caja </h1>
<div>
    <?php echo anchor('recibos/guardar','Guardar definitivo y enviar','class="boton naranja" onclick="javascript:return false;" style="float:right;" rel="guardar_recibo"');?>
    <?php echo anchor('recibos/guardar_borrador','Guardar Borrador','class="boton" style="float:right;margin-right:2px;" rel="guardar_borrador" onclick="return false;"');?>
</div>
</div>
<div class="clear"></div>
<div class="grid16"><?php echo form_hidden('cambios',set_value('cambios',0));?>
<div id="autoguardado" style="text-align: right;"></div>
</div>
<div class="clear"></div>
<div class="grid16" style="margin-top: 20px;">
<div style="text-align: right;">
<ul style="float: right">
	<li style="text-align: left;"><?php echo form_label('(C.C.) Enviar copias del recibo a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','size'=>60));?></li>
</ul>
<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<div id="recibo">
<div class="grid16 alpha omega">
<div class="seccion">
<h2>Datos del cliente</h2>
</div>
<div><?php echo form_input(array('name'=>'nit','size'=>'15')); echo ' '.anchor('','Buscar  <span class="iconos_blancos buscar"></span>','class="boton" rel="boton_buscar_cliente"  onclick="return false;"');?><span
	style="margin-top: 5px;" class="sub_label">Nit</span></div>
<div id="datos_cliente">
<ul class="formulario">
	<li class="left" style="width: 30%;">
	<p id="nombre_cliente">-</p>
	<span class="sub_label">Nombre del cliente</span></li>
	<li class="left" style="width: 30%;">-<span class="sub_label">Contacto</span></li>
	<li class="left" style="width: 40%;">-<span
		class="sub_label">E-mail</span></li>
	<div class="clear"></div>
	<li class="left" style="width: 40%;">-<span
		class="sub_label">Dirección</span></li>
	<li class="left" style="width: 30%;">-<span
		class="sub_label">Ciudad</span></li>
	<li class="left" style="width: 30%;">-<span
		class="sub_label">Teléfono</span></li>
	<div class="clear"></div>
	<li class="left" style="width: 45%;">-<span
		class="sub_label">Asesor</span></li>
		
	<div class="clear"></div>
</ul>
</div>
</div>
<div class="clear"></div>
<div class="grid16 alpha omega" id="datos_productos">
<div class="seccion">
    <h2>Documentos</h2>
    <div id="eliminar_rango_documentos" style="display:none;">
        <?php echo anchor('','Eliminar seleccionados','rel="eliminar_documentos_seleccionados" class="boton boton_rojo" onclick="return false;"');?>
    </div>
</div>
<div id="datosRecibo">
<table id="documentos_recibo" class="tabla">
	<thead>
		<tr>
			<th height="30px;" style="width: 5%;"><input type="checkbox" id="seleccionar_todos" value="0"/></th>
			<th style="width: 10%;">Documento</th>
			<th style="width: 10%;">Valor</th>
			<th style="width: 10%;">Descuento</th>
			<th style="width: 10%;">R/Fuente</th>
			<th style="width: 10%;" title="Nota Crédito o devolución">N. Crédito o Devolución</th>
			<th style="width: 10%;" title="Descuento cancelado y sobrantes.">Dcto cancel. y Sobrantes</th>
			<th style="width: 10%;">CREE</th>
                        <th style="width: 10%;">Otros</th>
                        <th style="width: 10%;">Total</th>
			<th style="width: 5%;"></th>
		</tr>
	</thead>
	<tbody>
		<tr class="agregar">
			<td class="item numero">-</td>
			<td colspan="10"><?php echo form_input(array('name'=>'items_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_items" class="boton" onclick="return false;"');?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td id="total_documentos_recibo" class="numero" colspan="7"><span class="alerta"></span> No ha agregado documentos al recibo.</td>
                        <td colspan="2" style="text-align:right;font-weight: bold;">Total recibo =></td>
                        <td id="totales" style="font-weight: bold;text-align:right;font-size: 14px;"></td>
                        <td>&nbsp;</td>
		</tr>
	</tfoot>
</table>
</div>
</div>
<div class="clear"></div>
<div class="seccion" style="margin-top: 20px;">
    <h2>Relacion de cheques y efectivo</h2>
</div>
<div class="grid12 alpha" style="margin-top: 10px;">
    <table class="tabla" id="cheques">
        <thead>
            <tr>
                <th  height="30px;" colspan="6">Cheque(s)</th>
            </tr>
            <tr>
                <th height="30px;">#</th>
                <th>Banco</th>
                <th>No. cheque</th>
                <th>Valor</th>
                <th>Posfechado</th>
                <th>-</th>
            </tr>
        </thead>
        <tbody>            
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                        <?php echo form_input(array('name'=>'cheques_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); 
                        echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_cheques" class="boton" onclick="return false;"');?>
                </td>
            </tr>
            <tr>
                <td id="total_cheques_recibo" colspan="2"></td>
                <td><b>Total en cheques:</b></td>
                <td colspan="3" id="total_cheques_valor" style="text-align:right;"></td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="grid4 omega" style="margin-top: 10px;">
    <table class="tabla">
        <thead>
            <tr>
                <th height="30px;">Efectivo</th>
            </tr>            
        </thead>
        <tbody>
            <tr>
                <td><?php echo form_input(array('name'=>'efectivo','style'=>'width:95%'));?></td>
            </tr>            
        </tbody>        
    </table>
</div>
<div class="clear" style="margin-bottom: 10px;"></div>
<div class="grid16 alpha omega seccion" style="margin-top: 20px;margin-bottom: 20px;">
    <h2>Datos de la consignación</h2>
	<table id="consignaciones" class="tabla">
        <thead>
            <tr>
                <th height="30px;" style="width: 2%;">#</th>
                <th height="30px;" style="width: 30%;">Cuenta</th>
                <th style="width: 18%;">Fecha</th>
                <th style="width: 10%;">Número de la consignación</th>
                <th style="width: 20%;">Valor de la consignación</th>
                <th style="width: 17%;">Realizado por:</th>
                <th style="width: 3%;">-</th>
            </tr>
        </thead>
        <tbody>
			         
	</tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                        <div class="cuadro_agregar_consignaciones">
                        <?php echo form_input(array('name'=>'consignaciones_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); 
                        echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_consignacion" class="boton"  onclick="return false;"');?>
                        </div>
                </td>               
            </tr>    
            <tr>
                <td colspan="3"></td>
                <td colspan="2"><b>Total en consignaciones:</b></td>
                <td colspan="2" id="total_consignaciones" style="text-align:right;"></td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="clear" style="margin-bottom: 10px;"></div>
<div class="grid16 alpha omega">
<div style="padding: 10px; background: #F4F4F4;"><?php echo form_label('Observaciones: ','observaciones',array('style'=>'width:200px; float:left;')); echo '<div class="faltantes">300</div>'; 
echo '<div style="clear:both;"></div><div><ul id="observaciones_predeterminadas">';
            foreach($observaciones_predeterminadas as $observacion):
                echo '<li><a href="" rel="agregar_observacion">'.$observacion->detalle.'</a></li>';
            endforeach;
            echo '</ul></div>';
echo form_textarea(array('name'=>'observaciones','style'=>'width:99%;','rows'=>4,'maxlength'=>300));?></div>
</div>
</div>
<?php if(!isset($numero_borrador)){ $numero_borrador=null;} echo form_hidden('borrador',set_value('borrador',$numero_borrador));?>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numberformatter-1.1.2.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.calculator.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script> 
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/sipe.js"></script> 
<script type="text/javascript">
$(function(){
    var tiempo_autoguardar;
    var ajax;
    var autoguardando=false;
    var salir='';//Cuando se hace clic a un elemento del menu y no se han guardado cambios, aqui se almacena la url de destino para despues de guardar el borrador.
    $('a[rel=guardar_borrador]').click(function(){		   
       $('#autoguardado').removeClass('correcto error');
       $('#autoguardado').addClass('cargando');
       $('#autoguardado').html('<span class="cargando"></span> Guardando borrador del recibo');
        var encabezado=[];
        var nit=$('input[name=nit]').val();
        var nombre_cliente=$('p[id=nombre_cliente]').text();
        var asesor=$('select[name=asesor]').val();
        var consignacion=$('input[name=consignacion]').val();
        var email_copias=$('input[name=email_copias]').val();
        var observaciones=$('textarea[name=observaciones]').val();
        var total_efectivo=$('input[name=efectivo]').parseNumber()[0];
        var total_cheques=$('#total_cheques_valor').parseNumber()[0];
        var cuenta=$('select[name=cuenta]').val();
        var fecha=$('input[name=fecha]').val();
        var totales=Math.round($("#totales").parseNumber()[0]);
                                
                                
        var documentos_agregados=parseInt($('#documentos_recibo >tbody >tr').length)-1;
        var n=1;
        var documentos=[];
        var nota_credito=0;
        var detalle_nota_credito='';
        $('#documentos_recibo >tbody >tr').each(function(){
                if($(this).attr('class')!='agregar'){
                        if(n % 2){
                                $(this).removeClass('altrow');
                        }else{
                                $(this).addClass('altrow');
                        }
                        nota_credito=$(this).find('input[name=nota_credito]').parseNumber()[0];
                        detalle_nota_credito=$(this).find('input[name=detalle_nota_credito]').val();                            
                        var documento=$(this).find('input[name=documento]').val();
                        var otros=$(this).find('input[name=otros]').parseNumber()[0];
                        var valor=$(this).find('input[name=valor]').parseNumber()[0];
                        var documento={
                                documento:documento,
                                valor:valor,
                                descuento:$(this).find('input[name=descuento]').parseNumber()[0],
                                rete_fte:$(this).find('input[name=rete_fte]').parseNumber()[0],
                                nota_credito:nota_credito,
                                nota_credito_detalle:detalle_nota_credito,
                                nota_debito:$(this).find('input[name=nota_debito]').parseNumber()[0],
                                otros:otros,
                                cree:$(this).find('input[name=cree]').parseNumber()[0]
                        }
                        documentos.push(documento);
                }
                n++;
        });
                                
        var cheques=[];
        var consignaciones=[];
        var n=1;
        $('#cheques >tbody >tr').each(function(){
            if(n % 2){
                    $(this).removeClass('altrow');
            }else{
                    $(this).addClass('altrow');
            }
            var banco=$(this).find('select[name=banco]').val();
            var cheque=$(this).find('input[name=cheque]').val();
            var posfechado=$(this).find('select[name=posfechado]').val();
            var fecha_posfechado=$(this).find('input[name=fecha_posfechado]').val();
            var valor=$(this).find('input[name=valor_cheque]').parseNumber()[0];
            var cheque={
                banco:banco,
                cheque:cheque,
                posfechado:posfechado,
                fecha_posfechado:fecha_posfechado,
                valor:valor
            };
            cheques.push(cheque);
            n=n+1;
        });
        $('#consignaciones >tbody >tr').each(function(){
            if(n % 2){
                    $(this).removeClass('altrow');
            }else{
                    $(this).addClass('altrow');
            }
            var cuenta=$(this).find('select[name=cuenta]').val();
            var fecha=$(this).find('input[name=fecha]').val();
            var numero=$(this).find('input[name=numero_consignacion]').val();
            var valor=$(this).find('input[name=valor_consignacion]').val();
            var realizado_por=0;

            if($(this).find('input[type=radio]:checked').val()==1){
                realizado_por=1;
            }else{
                realizado_por=2;
            }
            var consignacion={
                cuenta:cuenta,
                fecha:fecha,
                numero:numero,
                valor:valor,
                realizado_por:realizado_por
            };
            consignaciones.push(consignacion);
            n=n+1;
        });
        
        encabezado={
                nit:nit,
                nombre:nombre_cliente,
                asesor:asesor,
                email_copias:email_copias,
                observaciones:observaciones,
                total_efectivo:total_efectivo,
                total_cheques:total_cheques,
                totales:totales						
        }
        mostrar_mensaje('Guardando datos del recibo. Por favor espere...','alerta',0);
        
        var url=$(this).attr('href');
        var form_data={
                numero_borrador:$('input[name=borrador]').val(),
                encabezado: encabezado,
                documentos: documentos,
                cheques:cheques,
                consignaciones:consignaciones,
                ajax:1
        };
        $.ajax({
           type: "POST",
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
                   $('#autoguardado').removeClass('cargando');
                   $('#autoguardado').addClass('correcto');
                   window.location.hash='borrador='+msg['id'];

                   $('input[name=borrador]').val(msg['id']);
                   if(autoguardando){
                       mostrar_mensaje('Se autoguardó el recibo con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                       $('#autoguardado').html('Borrador #'+msg['id']+' autoguardado. '+msg['fecha']);
                   }else{
                       mostrar_mensaje('Se guardó el recibo con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                       $('#autoguardado').html('Borrador #'+msg['id']+' guardado. '+msg['fecha']);
                   }
                   
                   $('input[name=cambios]').val(0);
                   autoguardando=false;
                   if(salir!=''){
                       location.href=salir;
                   }
           },
           error: function(x,e){
                   autoguardando=false;
                   salir='';
                   $('#autoguardado').removeClass('cargando');
                   $('#autoguardado').addClass('error');
                   mostrar_error(x,e);
           }
        });
        return false;
    });//Termina guardar borrador
    autoguardar();
    function autoguardar(){		   
            tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
            if($('input[name=cambios]').val()==1){
                autoguardando=true;
                $('a[rel=guardar_borrador]').click();
            }
    }
    
    function cargar_borrador(){
        var borrador=$('input[name=borrador]').val();
        var form_data={
           borrador:borrador,
           ajax:1
        }
        
           mostrar_mensaje('Cargando la información del borrador. Por favor espere...','alerta',0);
           $.ajax({
               type: "POST",
               url: "<?php echo site_url('recibos/leer_borrador');?>",
               data: form_data,
               dataType:'json',
               success: function(msg){
                   if(msg['estado']=='correcto'){
                        var encabezado=msg['encabezado'];
                        var documentos=msg['documentos'];
                        var cheques=msg['cheques'];
                        var consignaciones=msg['consignaciones'];
                        console.log(consignaciones);
                        
                        if(encabezado['email_copias']!=null){
                            $('input[name=email_copias]').val(encabezado['email_copias']);
                        }
                        if(encabezado['nit']!=null && encabezado['nit']!=''){
                                $('input[name=nit]').val(encabezado['nit']);
                                $('input[name=nit]').change();                                    			
                        }
                        $('select[name=cuenta]').val(encabezado['cuenta']);
                        $('input[name=fecha]').val(encabezado['fecha']);
                        $('input[name=consignacion]').val(encabezado['consignacion']);
                        $('input[name=efectivo]').val(encabezado['total_efectivo']);
                        $('textarea[name=observaciones]').val(encabezado['observaciones']);
                        
                        var n=0;
                        $(documentos).each(function(id,documento){
                           n=n+1;
                           agregar_fila(n,documento['documento'],documento['valor'],documento['descuento'],documento['rete_fte'],documento['nota_credito'],documento['nota_credito_detalle'],documento['nota_debito'],documento['otros'],documento['total'],documento['cree']);                           
                        });
                        var n=0;
                        $(cheques).each(function(id,cheque){
                           n=n+1;
                           agregar_cheque(n,cheque['banco'],cheque['cheque'],cheque['posfechado'],cheque['fecha_posfechado'],cheque['valor']);                           
                        });
                        n=0;
                        $(consignaciones).each(function(id,consignacion){
                           n=n+1;
                           agregar_consignacion(n,consignacion['cuenta'],consignacion['fecha'],consignacion['numero'],consignacion['valor'],consignacion['realizado_por']);                           
                        });
                        recalcular_cheques();
                        recalcular_consignaciones();
                        recalcularTodo();
                        
                        $('input[name=cambios]').val(0);
                        mostrar_mensaje('Se ha cargado el borrador.','correcto',200);
                    }else{
                        mostrar_mensaje(msg['mensaje'],'error');
                    }
               },
               error: function(x,e){
                  mostrar_error(x,e);
               }
            });
    }//Cargar borrador
    
    //Para evitar acciones al presionar F5
    var version = navigator.appVersion;
    function showKeyCode(e)
    {
            var keycode =(window.event) ? event.keyCode : e.keyCode;
            if ((version.indexOf('MSIE') != -1))
            {
                    if(keycode == 116 || keycode==505)
                    {
                            //IE
                            event.keyCode = 0;
                            event.returnValue = false;
                            return false;
                    }
            }
            else
            {
                    if(keycode == 116 || keycode == 505)
                    {
                            if (e.preventDefault) {
                                    //IE
                                    e.preventDefault();
                            }
                            e.keyCode = 0;
                            e.cancelBubble = true;
                            e.returnValue = false;
                            //e.stopPropagation works in Firefox.
                            if (e.stopPropagation) {
                                    e.stopPropagation();
                                    e.preventDefault();
                            }
                            //Firefox, Chrome
                            return false;
                    }
            }
    }//Termina showKeyCode
    
    //Validar al presionar F5;
    $(document).keydown(function(event){
            if (event.keyCode == 116 || event.keyCode == 505) {
                    // When F5 is pressed
                    showKeyCode(event);
            }
    });//Termina keydown.
        
        
        
    //Datos del cliente
    $('input[name=nit]').change(function(){
        mostrar_mensaje('Buscando datos del cliente. Por favor espere...','alerta',0);
        var form_data={
                cliente: $(this).val(),
                ajax:'1'
        };
        if(ajax){
            ajax.abort();
        }
        ajax= $.ajax({
           type: "POST",
           url: "<?php echo site_url('recibos/datos_cliente');?>",
           data: form_data,
           dataType: 'json',
           success: function(msg){
                if(msg['estado']=='correcto'){
                    $('input[name=nit]').removeClass('error');
                    mostrar_mensaje(msg['mensaje'],'correcto',500);
                    $('#datos_cliente').html(msg['vista']);
                    $('input[name=cambios]').val(1);
                    var encontrado=false;
                    $('#documentos_recibo >tbody input[name=documento]').each(function(){
                            if($(this).val()=='' && encontrado==false){
                                    $(this).focus();
                                    encontrado=true;
                                    return;
                            }
                    });
                }else{
                    $('input[name=nit]').addClass('error');
                    mostrar_mensaje(msg['mensaje'],'error');
                    $('#datos_cliente').html('<ul class="formulario"><li class="left cuadro noderecha" style="width:30%;"><p id="nombre_cliente">-</p><span class="sub_label">Nombre del cliente</span></li><li class="left cuadro" style="width:30%;">-<span class="sub_label">Contacto</span></li><li class="left cuadro noizquierda" style="width:40%;">-<span class="sub_label">E-mail</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:40%;">-<span class="sub_label">Dirección</span></li><li class="left cuadro noarriba" style="width:30%;">-<span class="sub_label">Ciudad</span></li><li class="left cuadro noizquierda noarriba" style="width:30%;">-<span class="sub_label">Teléfono</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:45%;">-<span class="sub_label">Asesor</span></li><li class="left cuadro noarriba" style="width:10%;">-<span class="sub_label">Plazo</span></li><li class="left cuadro noizquierda noarriba" style="width:10%;">-<span class="sub_label">Lista</span></li><li class="left cuadro noizquierda noarriba" style="width:35%;">-<span class="sub_label">Retención</span></li><div class="clear"></div></ul>');                   					
               }
           },
           error: function(x,e){
               mostrar_error(x,e);                                    
           }
        });//Termina ajax.
    });//Termina change
    $('a[rel=boton_buscar_cliente]').click(function(){
        if($('#buscarCliente').html()==''){
            mostrar_mensaje('Cargando listado de clientes. Por favor espere','alerta',0);
            var form_data={
                    cliente: '',
                    ajax:'1'
            };
            if(ajax){
                ajax.abort();
            }
            ajax=$.ajax({
               type: "POST",
               url: "<?php echo site_url('recibos/buscar_cliente');?>",
               data: form_data,
               success: function(msg){
                   if(msg){
                       mostrar_mensaje('Se ha cargado el listado de clientes correctamente.','correcto',500);
                        $('#buscarCliente').html(msg);						
                        $('#buscarCliente').dialog('open');
                        $('#buscarCliente input[name=cliente]').focus();
                   }else{
                        mostrar_mensaje('Se presentaron errores al intentar cargar los clientes. Inténtelo nuevamente','error');
                   }
               },
               error: function(x,e){
                            mostrar_error(x,e);
               }
            });//Termina ajax.
        }else{
            $('#buscarCliente').dialog('open');
            $('#buscarCliente input[name=cliente]').focus();
        }
        $('body').css('overflow','hidden');
        return false;
    });//Termina boton_buscar_cliente
    $('body').append('<div id="buscarCliente" title="Buscar cliente"></div>');
    $("#buscarCliente").dialog({
                        autoOpen: false,
                        width: 800,
                        height:600,
                        modal: true,
                        resizable:true,
                        hide:'fold',
                        close: function(event,ui){
                                $('body').css('overflow','auto');
                        },
                        buttons: {
                                'Cerrar': function() {
                                        $('body').css('overflow','auto');
                                        $(this).dialog('close');
                                }
                        }
        });//Termina dialog.        
        
        $('#buscarCliente input[name=cliente]').live('focus',function(){
                var valor=$(this).val();
                if(valor=='Digite el nit o nombre del cliente'){
                        $(this).val('');
                }
        });
        $('#buscarCliente input[name=cliente]').live('blur',function(){
                var valor=$(this).val();
                if(valor==''){
                        $(this).val('Digite el nit o nombre del cliente');
                }
        });
        $('#buscarCliente input[name=cliente]').live('keyup',function(event){
                if (event.keyCode == '13') {
                  $('#buscarCliente a[rel=filtrar_cliente]').click();
                }
                return false;
        });
        function buscar_cliente(url){
            var cliente=$('#buscarCliente input[name=cliente]').val();
            if(cliente=='Digite el nit o nombre del cliente'){
                    cliente='';
                    $('#buscarCliente input[name=cliente]').val('');
            }
            var form_data={
                    cliente:cliente,
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       if(msg){
                           mostrar_mensaje('Se han listado las coincidencias','correcto',500);
                            $('#buscarCliente').html(msg); 
                            var buscar=$('#buscarCliente input[name=cliente]').val();
                            if(buscar!=''){
                                    $('#buscarCliente table').highlight(buscar);
                            } 
                       }else{
                            mostrar_mensaje('Se presentaron errores al realizar la busqueda, inténtelo nuevamente.','error');
                       }
               },
               error: function(x,e){
                  mostrar_error(x,e);
               }
             });
       }
        $('#buscarCliente a[rel=filtrar_cliente]').live('click',function(){
                mostrar_mensaje('Buscando clientes con los datos ingresados. Por favor espere...','alerta',0);
                var url = $(this).attr('href');
                buscar_cliente(url);
                return false;
        });
        $('#buscarCliente th a').live('click',function(){
                mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                buscar_cliente(url);
                return false;
        });
        $('#buscarCliente div.paginacion a').live('click',function(){
                mostrar_mensaje('Cargando la página. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                buscar_cliente(url)
                return false;
        });
        $('#buscarCliente a[rel=boton_seleccionar_cliente]').live('click',function(){
                $('input[name=nit]').val($(this).attr('id'));
                $('#buscarCliente').dialog('close');
                $('input[name=nit]').change();
                return false;
        });
        $('a[rel=agregar_documento]').live('click',function(){
           var obj=$(this).parent().parent();
           var documento=obj.find('td.documento').text();
           var saldo_actual=obj.find('td.saldo_actual').text();
           var n=0;
           var agregado=false;
           $('#documentos_recibo >tbody >tr').each(function(){
               n=n+1;
               if($(this).find('input[name=documento]').val()==''){
                   $(this).find('input[name=documento]').val(documento);
                   $(this).find('input[name=valor]').val(saldo_actual);
                   recalcularTodo();
                   agregado=true;
                   return false;
               }               
           });
           if(!agregado){
                agregar_fila(n,documento,saldo_actual);
                $('#total_documentos_recibo').html('Total de documentos en el recibo provisional de caja '+n);
                n++;
                recalcularTodo();
           }
           $(this).hide();
           return false;
        });
        
        //Acciones items recibo
        //Boton para agregar filas en la tabla de documentos.
        $('input[name=items_agregar]').keyup(function(event){
                if (event.keyCode == '13') {
                  $('a[rel=boton_agregar_items]').click();
                }
                return false;
        });//Termina keyup.
        function agregar_fila(n,documento,valor,descuento,rete_fte,nota_credito,nota_credito_detalle,nota_debito,otros,total,cree){
            if(n % 2){
                    clase="";
            }else{
                    clase="altrow";
            }
            if(typeof documento=='undefined'){
                documento='';
            }
            if(typeof valor=='undefined'){
                valor='';
            }
            if(typeof rete_fte=='undefined'){
                rete_fte='';
            }
            if(typeof descuento=='undefined'){
                descuento='';
            }
            if(typeof nota_credito=='undefined'){
                nota_credito='';
            }
            if(typeof nota_credito_detalle=='undefined'){
                nota_credito_detalle='';
            }
            if(typeof nota_debito=='undefined'){
                nota_debito='';
            }
            if(typeof otros=='undefined'){
                otros='';
            }
            if(typeof total=='undefined'){
                total='';
            }
            if(typeof cree=='undefined'){
                cree='';
            }
            var fila=$('<tr id="fila_'+n+'" class="'+clase+'" style="display:none;vertical-align:top;">'+
                '<td id="numero_item_'+n+'" class="item numero"><span class="numero">'+n+'</span><br/><input type="checkbox" name="select_eliminar" tabindex="-1"/></td>'+
                '<td><input type="text" name="documento" value="'+documento+'" size="7"></td>'+
                '<td><input type="text" name="valor" value="'+valor+'" size="9"></td>'+
                '<td style="text-align:center;"><span style="font-weight:bold;margin-top:2px; color:#900;">(-)</span> <?php echo form_input(array('name'=>'descuento','style'=>'width:70%'));?></td>'+
                '<td><span style="font-weight:bold;margin-top:2px; color:#900;">(-)</span> <?php echo form_input(array('name'=>'rete_fte','style'=>'width:70%')); ?></td>'+
                '<td><span style="font-weight:bold;margin-top:2px; color:#900;">(-)</span> <?php echo form_input(array('name'=>'nota_credito','style'=>'width:70%')); ?><div class="detalle_nota_credito" style="display:none;margin-top:2px;"><?php echo form_label('Detalle:','detalle_nota_credito');echo form_input(array('name'=>'detalle_nota_credito','size'=>'9','maxlength'=>'20','style'=>'background:#c6daa6;')); ?></div></td>'+
                '<td><span style="font-weight:bold;margin-top:2px; color:#900;">(+)</span> <?php echo form_input(array('name'=>'nota_debito','style'=>'width:70%')); ?><div class="detalle_nota_debito" style="display:none;margin-top:2px;"><?php echo form_label('Detalle:','detalle_nota_debito');echo form_input(array('name'=>'detalle_nota_debito','size'=>'9','style'=>'background:#c6daa6;')); ?></div></td>'+
                '<td><span style="font-weight:bold;margin-top:2px; color:#900;">(-)</span> <?php echo form_input(array('name'=>'cree','style'=>'width:70%')); ?></td>'+
                '<td><span style="font-weight:bold;margin-top:2px; color:#900;">(-)</span> <?php echo form_input(array('name'=>'otros','style'=>'width:70%')); ?></td>'+
                '<td class="total_item" style="text-align:right;"></td>'+
                '<td><a href="" tabindex="-1"  onclick="return false;" rel="eliminar_documento" title="Eliminar" class="boton boton_gris">'+
                '<span class="iconos_rojo eliminar"></span></a></td></tr>').insertBefore($('#documentos_recibo tbody tr.agregar'));
            fila.find('input[name=descuento]').val(descuento);
            fila.find('input[name=rete_fte]').val(rete_fte);
            fila.find('input[name=nota_credito]').val(nota_credito);
            if(nota_credito!=0){
                fila.find('.detalle_nota_credito').show();
            }
            fila.find('input[name=detalle_nota_credito]').val(nota_credito_detalle);
            fila.find('input[name=nota_debito]').val(nota_debito);
            fila.find('input[name=otros]').val(otros);
            fila.find('input[name=cree]').val(cree);
            fila.find('td.total_item').text(total);
            acciones_campos_fila_documento(fila);
        }
        $('input[name=nota_credito]').live('blur',function(){
           if($(this).val()!=''){
               $(this).parent().parent().find('input[name=detalle_nota_credito]').focus();
           } 
        });
        $('input[name=valor],input[name=descuento],input[name=rete_fte],input[name=nota_credito],input[name=nota_debito],input[name=otros]').live('focus',function(){
           if($(this).val()==0){
               $(this).val('');
           } 
        });
        $('a[rel=boton_agregar_items]').click(function(){
                var n=0;
                n=parseInt($('#documentos_recibo tbody tr').length);
                var cantidad=$('input[name=items_agregar]').val();
                if(!cantidad || cantidad==0){
                        cantidad=1;
                }
                var clase="";
                for(var f=0;f<cantidad;f++){
                        agregar_fila(n,'','');
                        $('#total_documentos_recibo').html('Total de documentos en el recibo provisional de caja '+n);
                        n++;                        
                }
                var encontrado=false;
                $('#documentos_recibo >tbody input[name=documento]').each(function(){
                        if($(this).val()=='' && encontrado==false){
                                $(this).focus();
                                encontrado=true;
                                return;
                        }
                });
                $('input[name=items_agregar]').val(1);
                $('#cargando').removeClass('correcto error');
                $('#cargando').hide();			
                return false;
        });//Termina boton_agregar_items.
        //Terminan acciones items recibo
        
        //Acciones agregar cheques
        $('input[name=cheques_agregar]').keyup(function(event){
                if (event.keyCode == '13') {
                  $('a[rel=boton_agregar_cheques]').click();
                }
                return false;
        });//Termina keyup.
        $('a[rel=boton_agregar_cheques]').click(function(){
                var n=0;
                n=parseInt($('#cheques tbody tr').length)+1;
                var cantidad=$('input[name=cheques_agregar]').val();
                if(!cantidad || cantidad==0){
                        cantidad=1;
                }
                var fila='';
                var clase="";
                for(var f=0;f<cantidad;f++){
                        agregar_cheque(n);
                        n++;                        
                }
                $('input[name=cheques_agregar]').val(1);
                return false;
        });//Termina boton_agregar_items.
        $('input[name=consignaciones_agregar]').keyup(function(event){
                if (event.keyCode == '13') {
                  $('a[rel=boton_agregar_consignacion]').click();
                }
                return false;
        });//Termina keyup.
        $('a[rel=boton_agregar_consignacion]').click(function(){
                var n=0;
                n=parseInt($('#consignaciones tbody tr').length)+1;
                var cantidad=$('input[name=consignaciones_agregar]').val();
                if(!cantidad || cantidad==0){
                        cantidad=1;
                }
                var fila='';
                var clase="";
                for(var f=0;f<cantidad;f++){
                        agregar_consignacion(n);
                        n++;                        
                }
                $('input[name=consignaciones_agregar]').val(1);
                return false;
        });//Termina boton_agregar_items
        function agregar_cheque(n,banco,cheque,posfechado,fecha_posfechado,valor){
            var clase='';
            if(n % 2){
                    clase="";
            }else{
                    clase="altrow";
            }
            if(typeof banco=='undefined'){
                banco='';
            }
            if(typeof cheque=='cheque'){
                cheque='';
            }
            if(typeof posfechado=='undefined'){
                posfechado='0';
            }
            if(typeof fecha_posfechado=='undefined'){
                fecha_posfechado='';
            }
            if(typeof valor=='undefined'){
                valor='';
            }
            var fila = $('<tr class="'+clase+'">'+'<td class="numero">'+n+'</td>'+
                                                '<td><?php echo $bancos;?></td>'+
                                                '<td><?php echo form_input(array('name'=>'cheque','size'=>'15','maxlenght'=>'20')); ?></td>'+
                                                '<td><?php echo form_input(array('name'=>'valor_cheque','size'=>'8'));?></td>'+
                                                '<td style="width:110px;"><select name="posfechado" style="width:100%;"><option value="0">No</option><option value="1">Si</option></select><div class="posfechado">Fecha: <br/><?php echo form_input(array('name'=>'fecha_posfechado','size'=>'10'));?></div></td>'+
                                                '<td><a href="" tabindex="-1" rel="eliminar_cheque"  onclick="return false;" title="Eliminar" class="boton boton_gris"><span class="iconos_rojo eliminar"></span></a></td></tr>');
           fila.find('input[name=valor_cheque]').numeric('no');
           fila.find('select[name=banco]').val(banco);
           fila.find('input[name=cheque]').val(cheque);
           fila.find('select[name=posfechado]').val(posfechado);
           fila.find('input[name=fecha_posfechado]').val(fecha_posfechado);
           fila.find('input[name=fecha_posfechado]').datepicker({
                showOn:'both',
                buttonImage: "<?php echo site_url('img/calendar.png');?>",
                buttonImageOnly: true
            });
            
           fila.find('input[name=valor_cheque]').val(valor);
           
           $('#cheques tbody').append(fila);           
           if(posfechado=="1"){
               fila.find('div.posfechado').show();
           }
        }
        //Terminan acciones items recibo
        var maximo_consignaciones_recibo=<?php echo $configuracion['maximo_consignaciones_recibo'];?>;
        function agregar_consignacion(n,cuenta,fecha,numero,valor,realizado_por){
            if(maximo_consignaciones_recibo==0 || n<=maximo_consignaciones_recibo ){
                var clase='';
                if(n % 2){
                        clase="";
                }else{
                        clase="altrow";
                }
                if(typeof cuenta=='undefined'){
                    cuenta='';
                }
                if(typeof fecha=='undefined'){
                    fecha='';
                }
                if(typeof numero=='undefined'){
                    numero='';
                }
                if(typeof valor=='undefined'){
                    valor='';
                }
                if(typeof realizado_por=='undefined'){
                    realizado_por='';
                }
                var fila = $('<tr class="'+clase+'">'
                    +'<?php echo form_open();?>'
                    +'<td class="numero">'+n+'</td>'
                    +'<td><?php echo eregi_replace("[\n|\r|\n\r]", ' ', form_dropdown('cuenta',$cuentas));?></td>'
                    +'<td><?php echo form_input(array('name'=>'fecha','size'=>'10'));?></td>'
                    +'<td><?php echo form_input(array('name'=>'numero_consignacion','size'=>12));?></td>'   
                    +'<td><?php echo form_input(array('name'=>'valor_consignacion','size'=>12));?></td>'
                    +'<td><ul>'
                            +'<li class="realizado_por_cliente"><?php echo form_label(form_radio(array('name'=>'realizado_por','id'=>'cliente','value'=>'1')).' Cliente','cliente');?></li>'
                            +'<li class="realizado_por_vendedor"><?php echo form_label(form_radio(array('name'=>'realizado_por','id'=>'vendedor','value'=>'2')).' Vendedor','vendedor');?></li>'
                        +'</ul>'
                    +'</td>'
                    +'<td><a href="" tabindex="-1" rel="eliminar_consignacion" onclick="return false;" title="Eliminar" class="boton boton_gris">'+
                    '<span class="iconos_rojo eliminar"></span></a></td>'
                    +'<?php echo form_close();?>'
                    +'</tr>');            
               fila.find('input[name=valor]').numeric('no');
               fila.find('select[name=cuenta]').val(cuenta);
               fila.find('input[id=cliente]').attr('id','cliente_'+n);
               fila.find('input[id=vendedor]').attr('id','vendedor_'+n);
               fila.find('li.realizado_por_cliente label').attr('for','cliente_'+n);
               fila.find('li.realizado_por_vendedor label').attr('for','vendedor_'+n);
               fila.find('li.realizado_por_cliente input').attr('name','realizado_por_'+n);
               fila.find('li.realizado_por_vendedor input').attr('name','realizado_por_'+n);
               if(realizado_por=='1'){
                    fila.find('li.realizado_por_cliente input').attr('checked','checked');
                }else{
                    fila.find('li.realizado_por_vendedor input').attr('checked','checked');
                }
               fila.find('input[name=fecha]').val(fecha);
               fila.find('input[name=numero_consignacion]').val(numero);
               fila.find('input[name=valor_consignacion]').val(valor);
               fila.find('input[name=fecha]').datepicker({
                    showOn:'both',
                    maxDate:'+0d',
                    buttonImage: "<?php echo site_url('img/calendar.png');?>",
                    buttonImageOnly: true
                });
                fila.find('input[name=fecha]').val(fecha);

                $('#consignaciones tbody').append(fila);                      
                if((n) == maximo_consignaciones_recibo){
                    $('#consignaciones div.cuadro_agregar_consignaciones').hide();
                }
           }
        }
        //Terminan acciones items recibo
        $('a[rel=eliminar_consignacion]').live('click',function(){
            var fila=$(this).parent().parent();
            $('#dialog-confirm').dialog('destroy');
            $('#dialog-confirm').remove();
            $('body').append('<div id="dialog-confirm" title="Eliminar consignacion"><span class="icono"></span>Está seguro de eliminar la consignación seleccionado?</div>');
            $("#dialog-confirm").dialog({
                    resizable: false,
                    height:140,
                    dialogClass: 'dialog_confirm',
                    modal: true,
                    buttons: {
                            'SI': function() {
                                $('#cargando').html('Eliminando consignacion.');
                                $('#cargando').show();
                                fila.css({background:"#FFD5D5",color:"#CC0000"});
                                fila.animate({opacity: 0.5}, 200, function(){				  
                                        fila.remove();
                                        $('input[name=cambios]').val(1);
                                        var consignaciones=parseInt($('#consignaciones >tbody >tr').length);
                                        var n=1;
                                        $('#consignaciones >tbody >tr').each(function(){
                                                if(n % 2){
                                                        $(this).removeClass('altrow');
                                                }else{
                                                        $(this).addClass('altrow');
                                                }
                                                $(this).find('td.numero').text(n);
                                                $(this).find('li.realizado_por_cliente input').attr('id','cliente_'+n);
                                                $(this).find('li.realizado_por_vendedor input').attr('id','vendedor_'+n);
                                                $(this).find('li.realizado_por_cliente input').attr('name','realizado_por_'+n);
                                                $(this).find('li.realizado_por_vendedor input').attr('name','realizado_por_'+n);
                                                $(this).find('li.realizado_por_cliente label').attr('for','cliente_'+n);
                                                $(this).find('li.realizado_por_vendedor label').attr('for','vendedor_'+n);						
                                                n++;
                                        });
                                        if(n <= maximo_consignaciones_recibo){
                                            $('#consignaciones div.cuadro_agregar_consignaciones').show();
                                        }
                                        recalcular_consignaciones();                        
                                });//Termina animate
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');                                
                             },
                            'NO': function() {
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
                            }
                    }
            });               
            return false;
        });//Termina eliminar cheque.
        $('select[name=posfechado]').live('change',function(){
            if($(this).val()==1){                
                $(this).parent().parent().find('div.posfechado').show(); 
            }else{
                $(this).parent().parent().find('input[name=fecha_posfechado]').val('');
                $(this).parent().parent().find('input[name=fecha_posfechado]').change();
                $(this).parent().parent().find('div.posfechado').hide();
            }
        });
        $('input[name=fecha_posfechado]').live('change',function(){
            if($(this).val()!=''){
                $('input[name=fecha]').datepicker("option","maxDate",'');
            }else{
                $('input[name=fecha]').datepicker("option","maxDate",'+0d');
            }
        });
        //Eliminar cheque
        $('a[rel=eliminar_cheque]').live('click',function(){
            var fila=$(this).parent().parent();                                
            $('#dialog-confirm').dialog('destroy');
            $('#dialog-confirm').remove();
            $('body').append('<div id="dialog-confirm" title="Eliminar cheque"><span class="icono"></span>Está seguro de eliminar el cheque seleccionado?</div>');
            $("#dialog-confirm").dialog({
                    resizable: false,
                    height:140,
                    dialogClass: 'dialog_confirm',
                    modal: true,
                    buttons: {
                            'SI': function() {
                                 $('#cargando').html('Eliminando cheque.');
                                $('#cargando').show();
                                fila.css({background:"#FFD5D5",color:"#CC0000"});
                                fila.animate({opacity: 0.5}, 200, function(){				  
                                        fila.remove();
                                        $('input[name=cambios]').val(1);
                                        var cheques=parseInt($('#cheques >tbody >tr').length);
                                        var n=1;
                                        $('#cheques >tbody >tr').each(function(){
                                                if(n % 2){
                                                        $(this).removeClass('altrow');
                                                }else{
                                                        $(this).addClass('altrow');
                                                }
                                                $(this).find('td.numero').text(n);						
                                                n++;
                                        });
                                        recalcular_cheques();
                                });//Termina animate
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                            },
                            'NO': function() {
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
                            }
                    }
            });               
            return false;
        });//Termina eliminar cheque.
        
        //Acciones campos fila items recibo
        $("input[name=valor]").live('blur',function(){
                if($(this).val()==0){
                        $(this).addClass('error');
                }else{
                        $(this).removeClass('error');
                }
        });//Termina bind.
        
        
        //Cambios en el recibo
        $("input[name=efectivo]").live('change',function(){
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=valor_cheque]").live('change',function(){
                recalcular_cheques();
                $('input[name=cambios]').val(1);
        });
        $("input[name=valor_consignacion]").live('change',function(){
                recalcular_consignaciones();
                $('input[name=cambios]').val(1);
        });
        
        $('input[name=cheque],select[name=banco],input[name=consignacion],select[name=cuenta],input[name=fecha]').live('change',function(){
            $('input[name=cambios]').val(1);
        });
        $("input[name=valor]").live('change',function(){
                recalcularTodo();
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=descuento]").live('change',function(){
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=rete_fte]").live('change',function(){
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=nota_credito]").live('change',function(){
            if($(this).val() != 0){
                $(this).parent().find('.detalle_nota_credito').show();
            }else{
                $(this).parent().find('.detalle_nota_credito').hide();
            }
            recalcularTodo();					
            $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=nota_debito]").live('change',function(){
            /*if($(this).val() != 0){
                $(this).parent().find('.detalle_nota_debito').show();
            }else{
                $(this).parent().find('.detalle_nota_debito').hide();
            }*/
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=otros]").live('change',function(){
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $("input[name=cree]").live('change',function(){
                recalcularTodo();					
                $('input[name=cambios]').val(1);
        });//Termina bind.
        $('input[name=documento]').live('change',function(){
            var codigos='';
            $('#documentos_recibo >tbody >tr').each(function(){
                    if($(this).attr('class')!='agregar'){
                            codigos=codigos+$(this).find("input[name=documento]").val()+' ';                            
                    }                    
            });
            //Quita colores de productos que ya no estan repetidos.
            var documentos_repetidos;
            $("input[name=documento]").each(function(){
                    if($(this).val()!=""){
                            documentos_repetidos=codigos.split($(this).val()).length-1;
                    }else{
                            documentos_repetidos=0;
                    }
                    if(documentos_repetidos>1){
                            $(this).parent().parent().addClass('repetido');
                    }else{
                            $(this).parent().parent().removeClass('repetido');
                    }					
            });
        });
        $('a[rel=eliminar_documento]').live('click',function(){
                var fila=$(this).parent().parent();
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                $('body').append('<div id="dialog-confirm" title="Eliminar documento"><span class="icono"></span>Está seguro de eliminar el documento seleccionado?</div>');
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    mostrar_mensaje('Eliminando el item. Por favor espere...','alerta',0);
                                    var codigo_fila=fila.find('input[name=documento]').val();
                                    var repetido=0;
                                    fila.css({background:"#FFD5D5",color:"#CC0000"});
                                    fila.animate({opacity: 0.5}, 200, function(){				  
                                            fila.remove();
                                            $('input[name=cambios]').val(1);
                                            var documentos_agregados=parseInt($('#documentos_recibo >tbody >tr').length)-1;
                                            var n=1;
                                            var codigos="";
                                            $('#documentos_recibo >tbody >tr').each(function(){
                                                    if($(this).attr('class')!='agregar'){
                                                            codigos=codigos+$(this).find("input[name=documento]").val()+' ';						
                                                            if(n % 2){
                                                                    $(this).removeClass('altrow');
                                                            }else{
                                                                    $(this).addClass('altrow');
                                                            }
                                                            $(this).attr('id','fila_item_'+n);						
                                                            $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                                    }
                                                    n++;
                                            });
                                            $('#total_documentos_recibo').html('Total de documentos en el recibo '+documentos_agregados);
                                            if(documentos_agregados==0){
                                                    $('#total_documentos_recibo').html('<span class="alerta"></span> No ha agregado documentos al recibo.'); 
                                            }
                                            //Quita colores de productos que ya no estan repetidos.
                                            var documentos_repetidos;
                                            $("input[name=documento]").each(function(){
                                                    if($(this).val()!=""){
                                                            documentos_repetidos=codigos.split($(this).val()).length-1;
                                                    }else{
                                                            documentos_repetidos=0;
                                                    }
                                                    if(documentos_repetidos>1){
                                                            $(this).parent().parent().addClass('repetido');
                                                    }else{
                                                            $(this).parent().parent().removeClass('repetido');
                                                    }					
                                            });
                                            //Termina quita colores de productos que ya no estan repetidos.                                             
                                            recalcularTodo();                        
                                    });//Termina animate
                                    $('tr#documento_'+codigo_fila+' a[rel=agregar_documento]').show();
                                    mostrar_mensaje('Se ha eliminado el item correctamente','correcto',500);
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
                return false;
                
        });//Termina bind.
        $('a[rel=eliminar_documentos_seleccionados]').click(function(){
        var cantidad=$('#documentos_recibo tbody input[type=checkbox]:checked').length;
         if(cantidad>0){
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                if(cantidad>1){
                    $('body').append('<div id="dialog-confirm" title="Eliminar documentos seleccionados"><span class="icono"></span>Está seguro de eliminar los '+cantidad+' documentos seleccionados?</div>');
                }else{
                    $('body').append('<div id="dialog-confirm" title="Eliminar documentos seleccionados"><span class="icono"></span>Está seguro de eliminar el documento seleccionado?</div>');
                }
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    $('#documentos_recibo tbody input[type=checkbox]:checked').each(function(){
                                       $(this).css({background:"#FFD5D5",color:"#CC0000"});
                                       var docum=$(this).parent().parent().find('input[name=documento]').val();
                                       $('tr#documento_'+docum+' a[rel=agregar_documento]').show();
                                       $(this).parent().parent().remove();
                                    });
                                    $('#seleccionar_todos').attr('checked',false);
                                    var repetido=0;
                                    $('input[name=cambios]').val(1);
                                    var documentos_agregados=parseInt($('#documentos_recibo >tbody >tr').length)-1;
                                    var n=1;
                                    var codigos="";
                                    $('#documentos_recibo >tbody >tr').each(function(){
                                            if($(this).attr('class')!='agregar'){
                                                    codigos=codigos+$(this).find("input[name=documento]").val()+' ';						
                                                    if(n % 2){
                                                            $(this).removeClass('altrow');
                                                    }else{
                                                            $(this).addClass('altrow');
                                                    }
                                                    $(this).attr('id','fila_item_'+n);						
                                                    $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                            }
                                            n++;
                                    });
                                    $('#total_documentos_recibo').html('Total de documentos en el recibo '+documentos_agregados);
                                    if(documentos_agregados==0){
                                            $('#total_documentos_recibo').html('<span class="alerta"></span> No ha agregado documentos al recibo.'); 
                                    }
                                    //Quita colores de productos que ya no estan repetidos.
                                    var documentos_repetidos;
                                    $("input[name=documento]").each(function(){
                                            if($(this).val()!=""){
                                                    documentos_repetidos=codigos.split($(this).val()).length-1;
                                            }else{
                                                    documentos_repetidos=0;
                                            }
                                            if(documentos_repetidos>1){
                                                    $(this).parent().parent().addClass('repetido');
                                            }else{
                                                    $(this).parent().parent().removeClass('repetido');
                                            }					
                                    });
                                    //Termina quita colores de productos que ya no estan repetidos.

                                    recalcularTodo();
                                    $('#eliminar_rango_documentos').hide();
                                    
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
        }else{
            alert('No ha seleccionado productos para eliminar');
        }
        return false; 
    });
        //Termina Acciones campos fila items recibo
	function acciones_campos_fila_documento(fila){
		if(fila==null){
			fila=$('#datosRecibo');
		}
		fila.find("input[name=valor]").numeric('no');
		fila.find("input[name=descuento]").numeric('no');
		fila.find("input[name=rete_fte]").numeric('no');
		fila.find("input[name=nota_credito]").numeric('no');
		fila.find("input[name=nota_debito]").numeric('no');
		fila.find("input[name=otros]").numeric('no');
		fila.fadeIn('slow');
	}//Termina acciones_campos_fila_producto
	acciones_observaciones();
	recalcularTodo();
        $("input[name=efectivo]").numeric('no');
	function formatear(){
		$("input[name=valor]").format({format:"#,###", locale:"us"});
		$("input[name=descuento]").format({format:"#,###", locale:"us"});
		$("input[name=rete_fte]").format({format:"#,###", locale:"us"});
		$("input[name=nota_credito]").format({format:"#,###", locale:"us"});
		$("input[name=nota_debito]").format({format:"#,###", locale:"us"});
                $("input[name=cree]").format({format:"#,###", locale:"us"});
		$("input[name=otros]").format({format:"#,###", locale:"us"});
                $("input[name=efectivo]").format({format:"#,###", locale:"us"});
                $("input[name=valor_cheque]").format({format:"#,###", locale:"us"});
                $("input[name=valor_consignacion]").format({format:"#,###", locale:"us"});
                $('#total_cheques_valor').format({format:"#,###", locale:"us"});
                $('#total_consignaciones').format({format:"#,###", locale:"us"});
		$(".total_item").format({format:"#,###", locale:"us"});
	};
	function formatearTotales(){		
		$("#totales").format({format:"$ #,###", locale:"us"});
	}
        function recalcular_cheques(){
            var total_cheques=0;
            $('#cheques tbody tr').each(function(){
               total_cheques=total_cheques+$(this).find('input[name=valor_cheque]').parseNumber()[0]; 
            });
            $('#total_cheques_valor').text(total_cheques);
            formatear();
        }
        function recalcular_consignaciones(){
            var total_consignaciones=0;
            $('#consignaciones tbody tr').each(function(){
               total_consignaciones=total_consignaciones+$(this).find('input[name=valor_consignacion]').parseNumber()[0]; 
            });
            $('#total_consignaciones').text(total_consignaciones);
            formatear();
        }
	function recalcularTodo(){
                var suma=0;
                $('#documentos_recibo > tbody > tr').each(function(){
                   var valor=$(this).find('input[name=valor]').parseNumber();
                   var descuento=$(this).find('input[name=descuento]').parseNumber();
                   var retencion=$(this).find('input[name=rete_fte]').parseNumber();
                   var nota_credito=$(this).find('input[name=nota_credito]').parseNumber();
                   var nota_debito=$(this).find('input[name=nota_debito]').parseNumber();
                   var otros=$(this).find('input[name=otros]').parseNumber();
                   var cree=$(this).find('input[name=cree]').parseNumber();
                   
                   var total=(+valor)-(+descuento)-(+retencion)-(+nota_credito)+(+nota_debito)-(+otros)-(+cree);
                   suma=suma+(+total);
                   $(this).find('.total_item').text(total);
                });
                $("#totales").text(Math.round(suma));
                   
		formatear();
		formatearTotales();                
	}
	$('input[name=items_agregar]').numeric();
	var guardando=false;
	$('a[rel=guardar_recibo]').click(function(){
            var mensajes=[];
            		if($(this).text()!='Guardando recibo...' && !guardando){
				guardando==true;
				clearTimeout(tiempo_autoguardar);
                                $(this).text('Guardando recibo...');
				var boton=$(this);
                                mostrar_mensaje('Verificando los datos antes de guardar. Por favor espere...','alerta',0);
				recalcular_cheques();
                                recalcularTodo();
				var error=false;
				var encabezado=[];
                                var documentos=[];
                                var cheques=[];
                                var consignaciones=[];
				var nit=$('input[name=nit]').val();
				var nombre_cliente=$('p[id=nombre_cliente]').text();
				var asesor=$('select[name=asesor]').val();
                                var email_copias=$('input[name=email_copias]').val();
				var observaciones=$('textarea[name=observaciones]').val();
                                var total_efectivo=$('input[name=efectivo]').parseNumber()[0];
                                var total_cheques=$('#total_cheques_valor').parseNumber()[0];
                                var total_consignaciones=$('#total_consignaciones').parseNumber()[0];
                                var totales=Math.round($("#totales").parseNumber()[0]);
                                
                                $('select[name=cuenta]').removeClass('error');
                                $('input[name=fecha]').removeClass('error');
                                $('input[name=consignacion]').removeClass('error');
                                
                                if(!nit || nombre_cliente=='-'){
					error=true;
                                        mensajes.push('Debe seleccionar un cliente');
					$('input[name=nit]').addClass('error');
				}else{
					$('input[name=nit]').removeClass('error');
				}
				$('input[name=email_copias]').removeClass('error');
                                if(email_copias!=''){
                                    var correos = $('input[name=email_copias]').val().split(',');
                                    for (var n=0;n< correos.length;n++){
                                        if(!es_mail(correos[n])){
                                            error=true;
                                            mensajes.push('El correo '+correos[n]+' no es válido');
                                            $('input[name=email_copias]').addClass('error');
                                        }
                                    }
				}
				var documentos_agregados=parseInt($('#documentos_recibo >tbody >tr').length)-1;
                                var codigos='';
				var n=1;                                
                                var nota_credito=0;
                                var detalle_nota_credito='';
                                var otros_valores=false; //Para verificar si se coloco algun valor en la casilla otros con el fin de obligar a colocar observaciones.
				$('#documentos_recibo >tbody >tr').each(function(){
                                        if($(this).attr('class')!='agregar'){
						if(n % 2){
							$(this).removeClass('altrow');
						}else{
							$(this).addClass('altrow');
						}
                                                codigos=codigos+$(this).find("input[name=documento]").val()+' ';                            
                                                nota_credito=$(this).find('input[name=nota_credito]').parseNumber()[0];
                                                detalle_nota_credito='';
                                                if(nota_credito>0){
                                                    detalle_nota_credito=$(this).find('input[name=detalle_nota_credito]').val();
                                                    if(detalle_nota_credito==""){
                                                        error=true;
                                                        mensajes.push('Debe colocar el número de la Nota crédito en el documento '+$(this).find('input[name=documento]').val());
                                                        $(this).find('input[name=detalle_nota_credito]').addClass('error');
                                                    }
                                                }
                                                var documento=$(this).find('input[name=documento]').val();
                                                var otros=$(this).find('input[name=otros]').parseNumber()[0];
                                                var valor=$(this).find('input[name=valor]').parseNumber()[0];
                                                if(otros>0){
                                                    otros_valores=true;
                                                }
                                                if(valor==0 || valor==""){
                                                    error=true;
                                                    mensajes.push('Debe colocar el valor en el documento '+$(this).find('input[name=documento]').val());
                                                    $(this).find('input[name=valor]').addClass('error');
                                                }
                                                if(documento){
                                                    var documento={
                                                        documento:documento,
                                                        valor:valor,
                                                        descuento:$(this).find('input[name=descuento]').parseNumber()[0],
                                                        rete_fte:$(this).find('input[name=rete_fte]').parseNumber()[0],
                                                        nota_credito:nota_credito,
                                                        nota_credito_detalle:detalle_nota_credito,
                                                        nota_debito:$(this).find('input[name=nota_debito]').parseNumber()[0],
                                                        cree:$(this).find('input[name=cree]').parseNumber()[0],
                                                        otros:otros
                                                    }
                                                    documentos.push(documento);
                                                }
                                                
					}
					n++;
				});
                                var repetidos=false;
                                var documentos_repetidos;
                                $("input[name=documento]").each(function(){
                                        if($(this).val()!=""){
                                                documentos_repetidos=codigos.split($(this).val()).length-1;
                                        }else{
                                                documentos_repetidos=0;
                                        }
                                        if(documentos_repetidos>1){
                                                error=true;
                                                repetidos=true;
                                                $(this).parent().parent().addClass('repetido');
                                        }else{
                                                $(this).parent().parent().removeClass('repetido');
                                        }					
                                });
                                if(repetidos){
                                    mensajes.push('Existen documentos repetidos.');
                                }
                                if(documentos.length==0){
					error=true;
                                        mensajes.push('No hay documentos en el recibo');					
				}else{
                                    $('select[name=banco]').removeClass('error');
                                    $('input[name=cheque]').removeClass('error');
                                    $('input[name=fecha_posfechado]').removeClass('error');
                                    $('input[name=valor_cheque]').removeClass('error');
                                    var n=1;
                                    $('#cheques >tbody >tr').each(function(){
                                        if(n % 2){
                                                $(this).removeClass('altrow');
                                        }else{
                                                $(this).addClass('altrow');
                                        }
                                        var banco=$(this).find('select[name=banco]').val();
                                        var cheque=$(this).find('input[name=cheque]').val();
                                        var posfechado=$(this).find('select[name=posfechado]').val();
                                        var fecha_posfechado=$(this).find('input[name=fecha_posfechado]').val();
                                        var valor=$(this).find('input[name=valor_cheque]').parseNumber()[0];
                                        if( banco != "" && cheque != "" && valor != ""){
                                            if((posfechado==1 && fecha_posfechado!='') || posfechado==0){
                                                var cheque={
                                                    banco:banco,
                                                    cheque:cheque,
                                                    posfechado:posfechado,
                                                    fecha_posfechado:fecha_posfechado,
                                                    valor:valor
                                                };
                                                cheques.push(cheque);
                                            }else{
                                                error=true;
                                                $(this).find('input[name=fecha_posfechado]').addClass('error');
                                                mensajes.push('Debe colocar la fecha del posfechado en el item # '+$(this).find('td.numero').text()+' en cheques');                                                                                            
                                            }
                                        }else{
                                            if(banco=="" && cheque=="" && valor==""){
                                                $(this).remove();
                                                var m=1;
                                                $('#cheques >tbody >tr').each(function(){
                                                        if(m % 2){
                                                                $(this).removeClass('altrow');
                                                        }else{
                                                                $(this).addClass('altrow');
                                                        }
                                                        $(this).find('td.numero').text(m);						
                                                        m++;
                                                });                                            
                                            }else{
                                                error=true;
                                                if(banco==""){
                                                    $(this).find('select[name=banco]').addClass('error');
                                                    mensajes.push('Debe elegir un banco en el item # '+$(this).find('td.numero').text()+' en cheques');                                            
                                                }
                                                if(cheque==""){
                                                    $(this).find('input[name=cheque]').addClass('error');
                                                    mensajes.push('Debe colocar el No. del cheque en el item # '+$(this).find('td.numero').text()+' en cheques');                                            
                                                }
                                                if(valor==""){
                                                    $(this).find('input[name=valor_cheque]').addClass('error');
                                                    mensajes.push('Debe colocar el valor del cheque en el item # '+$(this).find('td.numero').text()+' en cheques');                                            
                                                }
                                                if(posfechado=="1" && fecha_posfechado==''){
                                                    $(this).find('input[name=fecha_posfechado]').addClass('error');
                                                    mensajes.push('Debe colocar la fecha del posfechado en el item # '+$(this).find('td.numero').text()+' en cheques');                                                                                            
                                                }
                                            }
                                        }
                                        n=n+1;
                                    });
                                    
                                    $('#consignaciones >tbody >tr').each(function(){
                                        if(n % 2){
                                                $(this).removeClass('altrow');
                                        }else{
                                                $(this).addClass('altrow');
                                        }
                                        var cuenta=$(this).find('select[name=cuenta]').val();
                                        var fecha=$(this).find('input[name=fecha]').val();
                                        var numero=$(this).find('input[name=numero_consignacion]').val();
                                        var valor=$(this).find('input[name=valor_consignacion]').parseNumber()[0];
                                        var realizado_por=0;

                                        if($(this).find('input[type=radio]:checked').val()==1){
                                            realizado_por=1;
                                        }else{
                                            realizado_por=2;
                                        }
                                        if(realizado_por==0){
                                            error=true;
                                            mensajes.push('Falta seleccionar quien realizó la consignacion '+n);                                    
                                        }
                                        if(!cuenta){
                                            error=true;
                                            $('select[name=cuenta]').addClass('error');
                                            mensajes.push('Debe seleccionar la cuenta en donde realizó la consignación '+n);                                    
                                        }
                                        if(!fecha){
                                            error=true;
                                            $('input[name=fecha]').addClass('error');
                                            mensajes.push('Debe colocar la fecha en la que realizó la consignación '+n);                                    
                                        }
                                        if(!numero){
                                            error=true;
                                            $('input[name=numero]').addClass('error');
                                            mensajes.push('Debe colocar el número físico de la consignación '+n);                                    
                                        }
                                        if(!valor){
                                            error=true;
                                            $('input[name=valor]').addClass('error');
                                            mensajes.push('Debe colocar el valor de la consignación '+n);                                    
                                        }
                                        var consignacion={
                                            cuenta:cuenta,
                                            fecha:fecha,
                                            numero:numero,
                                            valor:valor,
                                            realizado_por:realizado_por
                                        };
                                        consignaciones.push(consignacion);
                                        n=n+1;
                                    });

                                    if(totales!= (total_efectivo+total_cheques)){
                                        error=true;
                                        mensajes.push('El total del recibo debe ser igual a la suma del total en efectivo mas el total en cheques en este recibo esta:<br/> Total recibo: $ '+totales+'<br/> Total en efectivo: $ '+total_efectivo+'<br/> + Total en cheques: $ '+total_cheques+' <br/> ='+(total_efectivo+total_cheques)+'<br/> Diferencia: $'+(totales-(total_efectivo+total_cheques)));
                                    }
                                    if(totales!=(total_consignaciones)){
                                        error=true;
                                        mensajes.push('El total del recibo debe ser igual a la suma de las consignaciones:<br/> Total recibo: $ '+totales+'<br/> Total en consignaciones: $ '+total_consignaciones+'<br/> Diferencia: $'+(totales-(total_consignaciones)));
                                    }
                                    if(otros_valores==true){
                                        if(!observaciones){
                                            error=true;
                                            mensajes.push('Debe colocar las observaciones correspondiente a los documento que contienen valores en la casilla otros');                                    
                                        }
                                    }
                                }
				if(!error){
                                        encabezado={
                                                vendedor:asesor,
						cliente:nit,
						total_efectivo:total_efectivo,
                                                total_cheques:total_cheques,
                                                observaciones:observaciones,
						total:totales						
					}
                                        mostrar_mensaje('Se verificaron los datos correctamente, guardando el recibo. Por favor espere...','alerta',0);
					var url=$(this).attr('href');
                                        var form_data={
						numero_borrador:$('input[name=borrador]').val(),
						encabezado: encabezado,
						documentos: documentos,
                                                cheques:cheques,
                                                consignaciones:consignaciones,
                                                email_copias:email_copias,
                                                vendedor:asesor,
                                                ajax:1
					};
					$.ajax({
					   type: "POST",
                                           async:false,
					   url: url,
					   data: form_data,
                                           dataType: 'json',
					   success: function(msg){
						   if(msg['estado']=='correcto'){
                                                       mostrar_mensaje('Se guardó el recibo. Se está enviando copia por email. Por favor espere...','alerta',0);
                                                        $('div#recibos').html(msg['mensaje']);
                                                        $('#envio_recibos').html('<span class="cargando"></span> Enviando correo del recibo por favor espere...');
                                                        var url='<?php echo site_url('recibos/enviar_recibos');?>';
                                                        var form_data={
                                                           recibos:msg['numero'],
                                                           vendedor:encabezado['asesor'],
                                                           cliente:encabezado['nit'],
                                                           otros:encabezado['email_copias']
                                                        };
                                                        $.ajax({
                                                           type: "POST",
                                                           async:true,
                                                           url: url,
                                                           data: form_data,
                                                           dataType: 'json',
                                                           success: function(msg){
                                                               if(msg['estado']=='correcto'){
                                                                   $('input[name=envio_recibos_correo]').val(0);
                                                                   $('#envio_recibos').addClass('correcto');
                                                               }else{
                                                                   $('input[name=envio_recibos_correo]').val(3);
                                                                   $('#envio_recibos').addClass('error');
                                                               }
                                                               $('#envio_recibos').html(msg['mensaje']);
                                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                           },
                                                           error: function(x,e){
                                                               mostrar_error(x,e);
                                                               boton.text('Guardar definitivo y enviar');                                                               
                                                           }
                                                        });
                                                    }else{
                                                        mostrar_mensaje(msg['mensaje'],'error');
                                                    }     
					   },
					   error: function(x,e){
                                                mostrar_error(x,e)
                                                boton.text('Guardar definitivo y enviar');														
					   }
					 });
				}else{
					tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
					$(this).text('Guardar definitivo y enviar');
					/*if(mensajes.length>0){
						$('#cargando').html('<span class="icono"></span> '+mensajes.join('<br/>'));
					}else{
						$('#cargando').html('<span class="icono"></span> Por favor verifique los datos en rojo y vuelva a intentar guardar el recibo.');
					}*/
                                        mostrar_mensaje('Existen errores en el recibo. Por favor verifique los datos en rojo y vuelva a intentar guardar el recibo.<br/>'+mensajes.join('<br/>'),'error',20000);					
				}
			}else{
				alert('Espere un momento que el recibo se está guardando.');
			}
			return false;
		});//Termina guardar recibo
    function acciones_observaciones(){
       $('textarea[maxlength]').keyup(function(){
                    var max = parseInt($(this).attr('maxlength'));
                    if($(this).val().length > max){
                            $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
                    }
                    $(this).parent().find('.faltantes').html((max - $(this).val().length));
       });
       $('a[rel=agregar_observacion]').click(function(){
               if($('textarea[name=observaciones]').val()!=''){
                   $('textarea[name=observaciones]').val($('textarea[name=observaciones]').val()+'\n'+$(this).text());                
               }else{
                   $('textarea[name=observaciones]').val($(this).text());  
               }
                $('textarea[maxlength]').keyup();
                $('input[name=cambios]').val(1);
                return false;
       });
       $('textarea[maxlength]').change(function(){
            $('input[name=cambios]').val(1);
       });
    }    
    $('#menu a').click(function(){
            var url=$(this).attr('href');
            if($('input[name=cambios]').val()==1){
                    $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>El recibo que esta haciendo tiene cambios que no se han guardado, desea guardarlos ahora?</div>');
                    $("#dialog-confirm").dialog({
                            resizable: false,
                            height:180,
                            modal: true,
                            dialogClass:'alerta',
                            buttons: {
                                    'SI': function() {
                                            salir=url;
                                            $('a[rel=guardar_borrador]').click();                                            
                                    },
                                    'NO': function() {							
                                            $(this).dialog('close');
                                            location.href=url;
                                    },
                                    'Cancelar': function(){
                                            $(this).dialog('close');
                                    }
                            }
                    });
                    return false;
            }
    });
    $('input[name=email_copias]').change(function(){
                $(this).removeClass('error');
                var emails=$(this).val();
                var correos = emails.split(',');
                for (var n=0;n< correos.length;n++){
                    if(!es_mail(correos[n])){
                       $(this).addClass('error');
                    }
                }
    });
    function es_mail(correo){
        var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if(correo.match(expresion)){
            return true;
        }else{
            return false;
        }
    }
    if($('input[name=nit]').val()==''){
            $('input[name=nit]').focus();
    }
    $("input:checkbox[name='select_eliminar']").live('click',function(){
        $("#seleccionar_todos").attr ( "checked" , false );
        if($('#documentos_recibo tbody input[type=checkbox]:checked').length>0){
           $('#eliminar_rango_documentos').show();
        }else{
            $('#documentos_recibo input[name=seleccionar_todos]').attr('checked',false);
            $('#eliminar_rango_documentos').hide();
        }       
    });
    $('#seleccionar_todos').live('click',function(){
        if($(this).is(':checked')){
            $("input[name=select_eliminar]").attr ( "checked" ,'checked');
            $('#eliminar_rango_documentos').show();
        }else{
            $("input[name=select_eliminar]").removeAttr ( "checked" );
            $('#eliminar_rango_documentos').hide();
        }        
    });
    $('a[rel=mostrar_ocultar]').live('click',function(){
		var valor=$(this).text();
		if(valor=='Ocultar cartera'){
			$(this).text('Mostrar cartera');
			$('#cartera_cliente').slideUp('fast');
		}else{
			$(this).text('Ocultar cartera');
			$('#cartera_cliente').slideDown('fast');
		}
		return false;
    });
    var jash=window.location.hash;
    if(jash.length > 1){ 
        $('input[name=borrador]').val(jash.replace('#borrador=',''));                               
    }else{
        $('a[rel=boton_agregar_items]').click();        
        $('a[rel=boton_agregar_cheques]').click();       
        $('a[rel=boton_agregar_consignacion]').click();       
    }
    if($('input[name=borrador]').val()!=''){
        cargar_borrador();
    }
});
</script>
