<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div style="font-family: Arial, helvetica; border:1px solid #CCC; padding:10px;">
        <?php echo img('img/logo_sipe.png');?>
        <h1>Notificaciones</h1>
        <div style="border-bottom:1px solid #CCC;">Resumen de cheques posfechados.</div>
    <?php
    //print_r($cheques);
    foreach($cheques as $cheque):
        $cheques_dia[$cheque->fecha_posfechado]['fecha']=$cheque->fecha_posfechado;
        $cheques_dia[$cheque->fecha_posfechado]['vendedores'][$cheque->vendedor]['vendedor']=array('numero'=>$cheque->vendedor,'nombre'=>$cheque->vendedor_nombre);
        $cheques_dia[$cheque->fecha_posfechado]['vendedores'][$cheque->vendedor]['cheques'][]=$cheque;
        $cheques_dia[$cheque->fecha_posfechado]['vendedores'][$cheque->vendedor]['total']=((isset($cheques_dia[$cheque->fecha_posfechado]['vendedores'][$cheque->vendedor]['total'])? $cheques_dia[$cheque->fecha_posfechado]['vendedores'][$cheque->vendedor]['total'] : 0))+$cheque->valor;
    endforeach;
    $color_head='#EFEFEF';
    foreach($cheques_dia as $dia):    
    ?>
        <h2 style="color:#444; font-size: 14px;">Posfechados del <?php echo $dia['fecha'];?> </h2>
        <?php foreach($dia['vendedores'] as $vendedor):?>
        <h2 style="color:#00620C; font-size: 14px;"><?php echo $vendedor['vendedor']['numero'].' - '.$vendedor['vendedor']['nombre'];?></h2>
        <table style="border:1px solid #CCC; font-weight: normal;border-collapse: collapse; font-size: 12px;" width="600px;">
            <thead>
                <tr>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Nit</th>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Cliente</th>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Recibo</th>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Banco</th>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Cheque</th>
                    <th style="background: <?php echo $color_head;?>;padding: 4px;">Valor</th>
                </tr>
            </thead>
            <tbody>
    <?php
        $n=0;
        foreach($vendedor['cheques'] as $cheque):
            ?>
                <tr>
                    <td style="padding: 4px;border:1px solid #CCC;"><?php echo $cheque->cliente;?></td>
                    <td style="padding: 4px;border:1px solid #CCC;"><?php echo $cheque->nombre;?><?php if($cheque->contacto){?><div style="color:#AAA; border-top:1px solid #CCC;"><?php echo $cheque->contacto;?></div><?php }?></td>
                    <td style="padding: 4px;border:1px solid #CCC;"><?php echo str_pad($cheque->recibo, 6, "0", STR_PAD_LEFT);?></td>
                    <td style="padding: 4px;border:1px solid #CCC;"><?php echo $cheque->banco_codigo.' -> '.$cheque->banco_nombre;?></td>
                    <td style="padding: 4px;border:1px solid #CCC;"><?php echo $cheque->cheque;?></td>
                    <td style="padding: 4px;border:1px solid #CCC; text-align: right;">$ 
                    <?php $valor=$cheque->valor;
                        $decimal=strstr($valor,'.');
                        if($decimal=='.00' || !$decimal){
                            $valor = number_format($valor,0,',','.');
                        }else{
                            $valor = number_format($valor,2,',','.');
                        }
                        echo $valor;
                        ?>
                    </td>
                </tr>
                <?php
            $n=$n+1;
        endforeach;
        ?>
            </tbody>
            <tfoot>
                <tr>
                    <td style="padding: 4px;"><b>Total -></b></td>
                    <td colspan="4" style="text-align: right;padding: 4px;"><b><?php echo (($n==1)?'1 cheque': $n.' cheques');?></b></td>
                    <td style="text-align: right;padding: 4px;"><b>$ 
                        <?php $valor=$vendedor['total'];
                        $decimal=strstr($valor,'.');
                        if($decimal=='.00' || !$decimal){
                            $valor = number_format($valor,0,',','.');
                        }else{
                            $valor = number_format($valor,2,',','.');
                        }
                        echo $valor;
                        ?></b>
                    </td>
                </tr>
            </tfoot>
        </table>
    <?php
        endforeach;
    endforeach;
    ?>
        <div style="font-size:11px; background: #EFEFEF; color:#444; padding: 10px; margin-top:20px;">SIPE &copy; 2010<?php $año=date('Y',time());if($año>2010){echo ' - '.$año;}?> Implementado a <?php echo $empresa->nombre;?>. Todos los derechos reservados <a href="http://www.diegopego.com" style="color:#005702" target="_blank">Diego Peláez Gómez.</a></div>
    </div>
</body>
</html>