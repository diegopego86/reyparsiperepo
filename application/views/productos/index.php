<div class="form grid16 alpha omega">
    <h1>Productos</h1>
    <?php
    echo form_open('productos');
    echo form_label('Producto: ','producto');
    if(!$buscar){
        $buscar='Digite el código o nombre del producto';
    }
    echo form_input(array('name'=>'producto','value'=>set_value('producto',$buscar),'style'=>'width:400px;'));
    echo form_submit('buscar','Buscar');
    echo form_close();
    //echo anchor('pedidos/buscar_producto','Agregar/Buscar','class="boton" id="boton_buscar_producto"');
    ?>
    <div id="buscarProducto">
    <div class="resultados"><?php echo $this->pagination->resultados();?></div>
    <div id="paginacion_productos" class="paginacion"><?php echo $this->pagination->create_links();?></div>
        <div class="clearfix">
		<?php if($productos){ $n=0;?>
                <?php foreach($productos as $producto):
                    $n=$n+1;
                    ?>
                <div class="<?php if($n==1){echo 'alpha';}?> grid4 <?php if($n==4){echo 'omega';}?>">
                    <div class="producto">
                    <div class="imagen">
                    <?php 
                    $main_image = base_url("/img/sin_foto.jpg");
                    if($producto->imagenes != ""){
                        $images = json_decode($producto->imagenes);
                        if(count($images) > 0){
                            $main_image=$images[0];
                        }
                    }
                        echo anchor($main_image,'<img src="'.$main_image.'" alt="'.$producto->nombre.'"/>','class="boton_gris" rel="mostrar_foto"  tabindex="-1" alt="-" title="Foto del producto '.$producto->id_producto.' - '.$producto->nombre.'"').' ';
                    ?></div>
                    <div title="Codigo" class="codigo"><?php echo $producto->id_producto;?></div>
                    <div class="nombre"><?php echo $producto->nombre;?></div>
				    <?php if($configuracion['buscar_productos_mostrar_tipo']){?>
                    <div title="Tipo" class="tipo"><?php echo $producto->tipo;?></div>
                    <?php }?>
                    <?php if($configuracion['buscar_productos_mostrar_unidad']){?>
                    <div class="unidad">U.d.e: <?php echo $producto->unidad;?></div>
                    <?php }?>
                    <?php if($configuracion['buscar_productos_mostrar_existencia']){?>
                    <div class="stock">Stock: <?php echo $producto->stock;?></div>
                    <?php }?>
                    <div title="Referencia" class="grid8 alpha referencia"><?php echo ($producto->referencia)?$producto->referencia:'&nbsp;';?></div>				    
                    <div class="precio grid8 omega"><?php echo number_format($producto->precio1,0);?></div>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <?php 
                if($n==4){
                    echo '</div><div class="clearfix">';
                    $n=0;
                }
                endforeach;?>
        <?php }else{?>
        <tr><td colspan="4"><span class="alerta"></span> No se encontraron productos con la búsqueda</td></tr>
        <?php }?>
        </tbody>
        </table>
	    <div id="paginacion_productos" class="paginacion"><?php echo $this->pagination->create_links();?></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	/*$('#resultados_buscar_productos th a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
			$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	/*$('#paginacion_productos a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	$('input[name=producto]').focus(function(){
		var valor=$(this).val();
		if(valor=='Digite el código o nombre del producto'){
			$(this).val('');
		}
	});
	$('input[name=producto]').focusout(function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el código o nombre del producto');
		}
	});	
	var buscar=$('input[name=producto]').val();
	if(buscar!=''){
		$('#buscarProducto').highlight(buscar);
	}
    $('a[rel=mostrar_foto]').lightBox({
        fixedNavigation:true,
        overlayBgColor:'#FFFFFF',
        overlayOpacity: 0.5,
        imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
        imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
        imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
        imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
        imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
    });    
});
</script>