<div class="form grid16">
    <h1>Productos</h1>
    <?php
    echo form_open('productos');
    echo form_label('Producto: ','producto');
    if(!$buscar){
        $buscar='Digite el código o nombre del producto';
    }
    echo form_input(array('name'=>'producto','value'=>set_value('producto',$buscar),'style'=>'width:400px;'));
    echo form_submit('buscar','Buscar');
    echo form_close();
    //echo anchor('pedidos/buscar_producto','Agregar/Buscar','class="boton" id="boton_buscar_producto"');
    ?>
    <div id="buscarProducto">
    <div class="resultados"><?php echo $this->pagination->resultados();?></div>
    <div id="paginacion_productos" class="paginacion"><?php echo $this->pagination->create_links();?></div>
    <table id="resultados_buscar_productos" class="tabla">
        <thead>
        <tr>
            <th style="width:10%;"><?php echo $this->pagination->ordenar('id_producto','Código','title="Código del producto"');?></th>
            <th style="width:34%;"><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
            <?php if($configuracion['buscar_productos_mostrar_tipo']){?>
            <th style="width:6%;"><?php echo $this->pagination->ordenar('tipo','Tipo');?></th>
            <?php }?>
            <th style="width:10%;"><?php echo $this->pagination->ordenar('referencia','Ref','title="Referencia"');?></th>
            <?php if($configuracion['buscar_productos_mostrar_unidad']){?>
            <th style="width:10%;"><?php echo $this->pagination->ordenar('unidad','Und','title="Unidad de empaque"');?></th>
            <?php }?>
            <?php if($configuracion['buscar_productos_mostrar_existencia']){?>
            <th style="width:10%;"><?php echo $this->pagination->ordenar('stock','Exis','title="Existencia en inventario"');?></th>
            <?php }?>
            <th style="width:10%;"><?php echo $this->pagination->ordenar('precio1','Precio');?></th>
        </tr>
        </thead>
        <tbody>
		<?php if($productos){?>
                <?php foreach($productos as $producto):?>
                <tr>
                    <td>
                    <?php 
                    $ruta=$this->config->item('ruta_imagenes');
					$url=@fopen($ruta.$producto->id_producto.".JPG",'r');
					if($url)
					{
						echo anchor($ruta.$producto->id_producto.'.JPG','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="mostrar_foto"  tabindex="-1" alt="-" title="Foto del producto '.$producto->id_producto.' - '.$producto->nombre.'"').' ';
					}else{
							echo anchor('/img/sin_foto.jpg','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="mostrar_foto"  tabindex="-1" alt="-" title="Foto del producto '.$producto->id_producto.' - '.$producto->nombre.'"').' ';
					}
                    echo $producto->id_producto;?></td><td><?php echo $producto->nombre;?></td>
				    <?php if($configuracion['buscar_productos_mostrar_tipo']){?>
                    <td style="text-align:center;"><?php echo $producto->tipo;?></td>
                    <?php }?>
                    <td><?php echo $producto->referencia;?></td>
				    <?php if($configuracion['buscar_productos_mostrar_unidad']){?>
                    <td style="text-align:right;"><?php echo $producto->unidad;?></td>
                    <?php }?>
                    <?php if($configuracion['buscar_productos_mostrar_existencia']){?>
                    <td style="text-align:right;"><?php echo $producto->stock;?></td>
                    <?php }?>
                    <td style="text-align:right;"><?php echo number_format($producto->precio1,0);?></td>
                </tr>
                <?php endforeach;?>
        <?php }else{?>
        <tr><td colspan="4"><span class="alerta"></span> No se encontraron productos con la búsqueda</td></tr>
        <?php }?>
        </tbody>
        </table>
	    <div id="paginacion_productos" class="paginacion"><?php echo $this->pagination->create_links();?></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	/*$('#resultados_buscar_productos th a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
			$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	/*$('#paginacion_productos a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	$('input[name=producto]').focus(function(){
		var valor=$(this).val();
		if(valor=='Digite el código o nombre del producto'){
			$(this).val('');
		}
	});
	$('input[name=producto]').focusout(function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el código o nombre del producto');
		}
	});	
	var buscar=$('input[name=producto]').val();
	if(buscar!=''){
		$('#buscarProducto').highlight(buscar);
	}
    $('a[rel=mostrar_foto]').lightBox({
        fixedNavigation:true,
        overlayBgColor:'#FFFFFF',
        overlayOpacity: 0.5,
        imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
        imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
        imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
        imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
        imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
    });    
});
</script>