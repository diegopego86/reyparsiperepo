<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/imprimir.css" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-1.6.2.min.js"></script>
<title>Imprimir rango de recibos</title>
<style type="text/css" media="screen">
    .contenido-recibo{
        padding-top:0.5cm;
    }
    .contenido-recibo table.tabla{
	border-collapse:collapse;	
    }
    .contenido-recibo table.tabla thead tr{
            border-bottom:1px solid #000;
            padding:1px 1px;
    }
    .contenido-recibo table.tabla tbody tr{
            border-bottom:1px solid #CCC;
            padding:1px 2px;
    }
    .contenido-recibo table.tabla tbody td{
            vertical-align:text-top;
    }
    .contenido-recibo table.tabla thead th{
            padding-bottom:0px;
    }            
</style>
<style media="print">
    @page { 
        margin:1cm;        
    }
    #imprimir{
	display:none;
    }
    body,html{
        margin: 0cm;
        padding: 0cm;
        margin-top:0cm;
    }
    .contenido-recibo{
        margin: 0px;
        padding: 0px;
        margin-top:0.5cm;
        width: auto;
    }
    .salto{
        margin-bottom: 0px;
        page-break-before:always;
    }    
</style>
</head>
<body>
    <div id="imprimir"><div><span class="logo"><?php echo anchor('',img(array('src'=>'img/logo_sipe_blanco.png','border'=>0)));?></span><?php $texto=img(array('src'=>'img/printer.png','border'=>0,'style'=>'vertical-align:middle;')).' Imprimir'; ?><a href="javascript:window.print();"><?php echo $texto;?></a></div><div class="clear"></div></div>
<?php 
$n=0;
if(!$recibos){
    echo '<div class="pagina" style="padding:10px;text-align:center; font-size:14px; color:#900;">No hay recibos para imprimir</div>';
}else{
foreach($recibos as $recibo):
    $total_desc=0;$total_rtefte=0;$total_notac=0;$total_sobrantes=0; $total_reteiva=0; $total_reteica=0; $total_otros=0;$total_valor=0;
    $encabezado=$recibo['encabezado'];
    if(isset($recibo['documentos'])){
        $documentos=$recibo['documentos'];
    }else{
        $documentos=null;
    }
    $consignaciones=$recibo['consignaciones'];
    //$cheques=json_decode($encabezado->cheques);
    if(isset($recibo['cheques'])){
        $cheques=$recibo['cheques'];
    }else{
        $cheques=null;
    }
    ?>    
<div class="contenido-recibo" style="font-size:10px; font-family:Arial, Helvetica, sans-serif;">
    <div style="font-size:0.8em;text-align:right;">NIT. <?php echo $empresa->nit.' - '.$empresa->nombre;?></div>
    <table width="100%" style="margin-top:0px;border-collapse:collapse;" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" style="vertical-align:top">
                <table class="encabezado" style="border-collapse:collapse;">
                    <tr><td colspan="2" style="border-bottom:1px solid #777;"><div style="font-weight:bold; font-size:12px; text-align:left; ">CLIENTE</div></td></tr>
                    <tr><td colspan="2" style="font-size:12px; font-weight: bold;"><?php echo $encabezado->cliente;?> <span style="margin-left:10px;"><?php echo $encabezado->nombre;?></span></td></tr>
                    <tr><td>Contacto:</td><td><?php echo $encabezado->contacto;?></td></tr>
                    <tr><td>Ciudad:</td><td><?php echo $encabezado->ciudad;?>, Teléfono: <?php echo $encabezado->telefono1;?></td></tr>                    
                </table>
            </td>
            <td width="50%" style="vertical-align:top; text-align: right;">    	
                <table class="encabezado" style="float:right;">
                    <tr><td colspan="2" style="border-bottom:1px solid #777;text-align: right;"><div style="font-weight:bold; font-size:1.2em; text-align:right;">RECIBO PROVISIONAL # <?php echo str_pad($encabezado->id, 6, "0", STR_PAD_LEFT);?></div><div style="font-weight:bold; font-size:9px; text-align:right;">Consecutivo vendedor: <?php echo $encabezado->numero*1;?></div></td></tr>
                    <tr><td style="text-align:left;">Diligenciado:</td><td style="text-align:right;"><?php echo $encabezado->fecha_realizado;?></td></tr>                    
                    <tr><td style="text-align:left;">Vendedor:</td><td style="text-align:left;"><?php echo $encabezado->vendedor.' - '.$encabezado->nombre_vendedor;?></td></tr>                    
                    <tr><td style="text-align:left; font-weight: bold;">Total recibo:</td><td style="text-align:right; font-weight: bold;">$ 
                        <?php $valor=$encabezado->total;
                                $decimal=strstr($valor,'.');
                                if($decimal=='.00' || !$decimal){
                                    $valor = number_format($valor,0,',','.');
                                }else{
                                    $valor = number_format($valor,2,',','.');
                                }
                                echo $valor;
                         ?>
                        </td>
                    </tr>    
                </table>
            </td>
        </tr>
    </table>
    <div id="datos_productos" style="width:100%; margin-top:0px;">
        <table width="100%" autosize="1" id="documentos_recibo" class="tabla" style="border-collapse:collapse;">
            <thead>
            <tr>
               <th style="width:2%;border:1px solid #999;padding:1px;">-</th>
                <th style="width:10%;border:1px solid #999;padding:1px;">Documento</th>
                <th style="width:10%;border:1px solid #999;padding:1px;">Valor</th>
                <th style="width:10%;border:1px solid #999;padding:1px;">Descuento</th>
                <th style="width:8%;border:1px solid #999;padding:1px;">Rte fte</th>
                <th style="width:10%;border:1px solid #999;padding:1px;">Nota Crédito</th>
                <th style="width:8%;border:1px solid #999;padding:1px;">Dcto cancel. y Sobrantes</th>
                <th style="width:8%;border:1px solid #999;padding:1px;">RTE IVA</th>
                <th style="width:8%;border:1px solid #999;padding:1px;">RTE ICA</th>
                <th style="width:8%;border:1px solid #999;padding:1px;">Otros</th>
                <th style="width:18%;border:1px solid #999;padding:1px;">Total</th>
            </tr>
            </thead>
        <tbody>
        <?php $n=1;foreach($documentos as $documento):?>
            <tr>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $n;?></td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $documento->documento;?></td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->valor,0,'.',',');?></td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->descuento,0,'.',',');?>
                <?php $total_desc=$total_desc+($documento->descuento*1); ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->rete_fte,0,'.',','); $total_rtefte=$total_rtefte+($documento->rete_fte*1);?></td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;">
               <?php echo number_format($documento->nota_credito,0,'.',',');
               $total_notac=$total_notac+($documento->nota_credito*1); 
                    if($documento->nota_credito_detalle){
                      echo '</br>('.$documento->nota_credito_detalle.')';
                    }
               ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->nota_debito,0,'.',',');?>
                   <?php $total_sobrantes=$total_sobrantes+($documento->nota_debito*1); ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->cree,2,'.',',');?>
                   <?php $total_reteiva=$total_reteiva+($documento->cree*1); ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->rete_ica,2,'.',',');?>
                   <?php $total_reteica=$total_reteica+($documento->rete_ica*1); ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo number_format($documento->otros,2,'.',',');?>
                   <?php $total_otros=$total_otros+($documento->otros*1); ?>
               </td>
               <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;">$ <?php echo number_format($documento->total,0,'.',',');?></td>
            </tr>
        <?php $n++; endforeach;?>
        <?php if($n<=3){
                do{
        ?><tr>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $n;?></td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
               <td style="border:1px solid #999;padding:3px;vertical-align:middle;">&nbsp;</td>
            </tr>
        <?php
                $n=$n+1;
                }while($n<=3);
            }
        ?>
            <tfoot>
                <tr>
                    <td colspan="2" style="border:1px solid #999;padding:1px;"><b>TOTAL -></b></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_valor,0,'.',',');?></td>
                   <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_desc,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_rtefte,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_notac,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_sobrantes,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_reteiva,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_reteica,0,'.',',');?></td>
                    <td style="border:1px solid #999;padding:1px 3px 1px 1px;text-align:right;"><?php echo number_format($total_otros,0,'.',',');?></td>
                    <td style="text-align:right;border:1px solid #999;padding:1px;"><b>$ <?php echo number_format($encabezado->total,0,'.',',');?></b></td>
                </tr>
            </tfoot>
        </tbody>
        </table>
    </div>
    <div style="position:relative; padding-top: 2px;">
        <table width="100%" autosize="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
            <tr>
                <td width="60%" style="vertical-align:top; text-align:left;">
                    <?php if($cheques){?>
                    <table width="100%" style="border-collapse:collapse;">
                        <thead>
                            <tr>
                                <th width="500px" colspan="4" style="border:1px solid #999;padding:0px; text-align: center;">RELACION DE CHEQUES</th>
                            </tr>
                            <tr>
                                <th width="30%" style="border:1px solid #999;padding:1px; text-align: center;">BANCO</th>
                                <th width="30%" style="border:1px solid #999;padding:1px; text-align: center;">CUENTA</th>
                                <th width="20%" style="border:1px solid #999;padding:1px; text-align: center;">POSFECHADO?</th>
                                <th width="30%" style="border:1px solid #999;padding:1px; text-align: center;">VALOR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($cheques as $cheque):?>
                            <tr>
                                <td style="border:1px solid #999;padding:1px;"><?php echo $cheque->banco;?></td>
                                <td style="border:1px solid #999;padding:1px;"><?php echo $cheque->cheque;?></td>
                                <td style="border:1px solid #999;padding:1px;"><?php echo (($cheque->posfechado==1)?'Si  ('.$cheque->fecha_posfechado.')':'No');?></td>                                
                                <td style="text-align:right;border:1px solid #999;padding:1px;">$ 
                                    <?php $valor=$cheque->valor;
                                            $decimal=strstr($valor,'.');
                                            if($decimal=='.00' || !$decimal){
                                                $valor = number_format($valor,0,',','.');
                                            }else{
                                                $valor = number_format($valor,2,',','.');
                                            }
                                            echo $valor;
                                     ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>            
                    </table>
                    <?php }else{ echo "&nbsp;";}?>
                </td>
                <td width="40%" style="vertical-align:top; padding-left:5px; text-align:right;">
                    <table width="100%" class="tabla" style="border-collapse:collapse;">
                        <tbody>
                            <tr>
                                <td style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><b>REALIZADO POR</b></td>
                                <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo ($encabezado->es_cliente)? 'CLIENTE':''; echo ($encabezado->es_vendedor)? 'VENDEDOR':'';?></td>
                            </tr>
                            <tr>
                                <td width="50%" style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><b>TOTAL EFECTIVO</b></td>
                                <td width="50%" style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;">$ <?php echo number_format($encabezado->total_efectivo,0,'.',',');?></td>                                
                            </tr>
                            <tr>
                                <td style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><b>TOTAL CHEQUES</b></td>
                                <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;">$ <?php echo number_format($encabezado->total_cheques,0,'.',',');?></td>                                
                            </tr>
                        </tbody>            
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top:2px;">
        <table width="100%" class="tabla" style="border-collapse:collapse;">
            <tr>
                <th style="border:1px solid #999;padding:1px; text-align: center;">Consignado en</th>
                <th style="border:1px solid #999;padding:1px; text-align: center;">Número</th>
                <th style="border:1px solid #999;padding:1px; text-align: center;">Fecha</th>
                <th style="border:1px solid #999;padding:1px; text-align: center;">Realizado por</th>
                <th style="border:1px solid #999;padding:1px; text-align: center;">Valor</th>
            </tr>
            <?php foreach($consignaciones as $consignacion):?>
            <tr>
                <td style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $consignacion->banco_nombre;?>  -> <?php echo $consignacion->cuenta_numero;?></td>
                <td style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $consignacion->numero;?></td>
                <td style="text-align:center;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo $consignacion->fecha;?></td>
                <td style="text-align:left;border:1px solid #999;padding:3px;vertical-align:middle;"><?php echo ($consignacion->realizado_por==1)? 'CLIENTE':'VENDEDOR';?></td>
                <td style="text-align:right;border:1px solid #999;padding:3px;vertical-align:middle;">$ 
                    <?php $valor=$consignacion->valor;
                            $decimal=strstr($valor,'.');
                            if($decimal=='.00' || !$decimal){
                                $valor = number_format($valor,0,',','.');
                            }else{
                                $valor = number_format($valor,2,',','.');
                            }
                            echo $valor;
                    ?>
                </td>
            </tr>
            <?php endforeach;?>
        </table>
    </div>
    <div style="position:relative; padding-top: 2px;">
        <table>
            <tr>
                <td><b>Observaciones:</b> <?php echo $encabezado->observaciones;?></td>
            </tr>
        </table>
    </div>
    <div style="border-bottom: 1px dashed #777777; margin: 10px 0px 2px;"></div>
</div>
<?php $n++; endforeach;}?>
</div>
</body>
</html>
<script>
$(function(){
   var alto_total=0;
   var alto_maximo=969;
   var n=1;
   $('.contenido-recibo').each(function() {
       alto_total=alto_total+$(this).height()*1+10;
       if(alto_total > alto_maximo){
           $(this).addClass('salto');
           alto_total=+$(this).height()*1+10;
           n=n+1;
       } $(this).addClass('pagina'+n);
     }
   );
   function actualizar(){
    for(p=1;p<=n;p++){
        $('.pagina'+p).wrapAll('<div class="pagina"/>');           
    }
    <?php if($recibos){ echo 'window.print();';}?>    
    }
    actualizar();
});
</script>