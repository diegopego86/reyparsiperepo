<div class="grid16">
<div style="margin-top:20px; padding:4px; text-align:center;background:#D9EFAF;color:#060;font-style:italic;">
<?php
    echo 'Se guardó el recibo # '.$numero;
    echo ' '.anchor('recibos/imprimir_copia/'.$numero.'/'.sha1($numero.$seguridad),'<span class="print"></span>','class="boton_gris peque" target="_blank" title="Imprimir en formato de recibo"');
    echo ' '.anchor('recibos/generar_pdf/'.$numero.'/'.sha1($numero.$seguridad),'<span class="pdf"></span>','class="boton_gris peque" title="Generar copia en pdf" rel="enviar_mail"');
    echo ' '.anchor('recibos/generar_excel/'.$numero.'/'.sha1($numero.$seguridad),'<span class="xls"></span>','class="boton_gris peque" title="Generar copia en Excel"');
?>
</div>    
<?php 

echo '<div id="envio_recibos" style="text-align:center; font-size:18px;"></div>';
echo form_hidden('envio_recibos_correo',1);
echo '<div style="width:700px; margin:0 auto;margin-top:20px">'.$mostrar.'</div>';
?>
</div>
<div class="clear"></div>