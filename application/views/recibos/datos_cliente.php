<ul class="formulario">
    <li class="left cuadro noderecha" style="height:50px;width:30%;"><p id="nombre_cliente"><?php echo $cliente->nombre;?></p><span class="sub_label">Nombre del cliente</span></li>
    <li class="left cuadro" style="height:50px;width:30%;"><?php echo $cliente->contacto;?><span class="sub_label">Contacto</span></li>
    <li class="left cuadro noizquierda" style="height:50px;width:40%;"><?php echo $cliente->email;?><span class="sub_label">E-mail</span></li>
    <div class="clear"></div>
    <li class="left cuadro noderecha noarriba" style="height:50px;width:40%;"><?php echo $cliente->direccion;?><span class="sub_label">Dirección</span></li>
    <li class="left cuadro noarriba" style="height:50px;width:30%;"><?php echo $cliente->ciudad;?><span class="sub_label">Ciudad</span></li>
    <li class="left cuadro noizquierda noarriba" style="height:50px;width:30%;"><?php echo $cliente->telefono1;?><span class="sub_label">Teléfono</span></li>
    <div class="clear"></div>
    <li class="left cuadro noderecha noarriba" style="height:50px;width:45%;">
	<?php 
	$adicional='';
		$adicional='disabled="disabled"';
	 echo form_dropdown('asesor',$vendedores,$cliente->id_vendedor,$adicional);
if(!in_array($cliente->id_vendedor,$this->session->userdata('numeros_vendedor'))){
	echo ' <div style="cursor:pointer;display:inline;color:#930;" title="Usted no aparece como el asesor comercial de este cliente. Por favor elija el número correcto y comuníquese con cartera para su corrección."><span class="alerta"></span>Alerta</div>';
}
?><span class="sub_label">Asesor</span></li>
	
    <div class="clear"></div>
</ul>
<?php if($cartera){?>
<div class="seccion"><h2>Cartera del cliente</h2><?php echo anchor('','Ocultar cartera','class="boton" rel="mostrar_ocultar"');?></div>
<div id="cartera_cliente" style="display:block;">
<table class="tabla">
<thead>
<tr><th style="height:30px;" title="Tipo de documento F: documento NC:Nota crédito ND:Nota débito R:Recibo de caja">Tipo</th><th>Documento</th><th>Fecha</th><th>Vence</th><th title="Dias de vencido">Dias</th><th title="Número del vendedor">Vd</th><th>Valor Inicial</th><th>Saldo Anterior</th><th>Débitos Mes</th><th>Créditos Mes</th><th>Saldo Actual</th><th>-</th></tr>
</thead>
<tbody>
<?php 
$valores_iniciales=0;
$saldos_anteriores=0;
$debitos=0;
$creditos=0;
$saldos_actuales=0;
foreach($cartera as $dato):
$clase='';
$valores_iniciales=$valores_iniciales+$dato->valor_inicial;
$saldos_anteriores=$saldos_anteriores+$dato->saldo_ant;
$debitos=$debitos+$dato->cargo;
$creditos=$creditos+$dato->abono;
$saldos_actuales=$saldos_actuales+$dato->saldo;
if($dato->dias>0 && $dato->saldo>0){
	$clase='vencido';
}
?>
<tr id="documento_<?php echo $dato->id_documento;?>" class="<?php echo $clase;?>">
<td><?php echo $dato->tipo;?></td>
<td class="documento"><?php echo $dato->id_documento;?></td>
<td style="text-align:center;"><?php echo $dato->fecha;?></td>
<td style="text-align:center;"><?php echo $dato->fecha_vence;?></td>
<td title="Dias de vencido" style="text-align:center;"><?php echo $dato->dias;?></td>
<td style="text-align:center;"><?php echo $dato->id_vendedor;?></td>
<td style="text-align:right;"><?php echo number_format($dato->valor_inicial,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->saldo_ant,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->cargo,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->abono,0,'.',',');?></td>
<td class="saldo_actual" style="text-align:right;"><?php echo number_format($dato->saldo,0,'.',',');?></td><td><?php if($dato->saldo<>0){ echo anchor('','<span class="iconos_azules mas"></span>','class="boton_gris" rel="agregar_documento" tabindex="-1" onclick="return false;"');}?></td></tr>
<?php endforeach;?>
</tbody>
<tfoot style="font-weight:bold;">
<tr style="background:#FFC"><td colspan="6">Totales</td><td style="text-align:right;"><?php echo number_format($valores_iniciales,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_anteriores,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($debitos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($creditos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_actuales,0,'.',',');?></td><td></td></tr>
</tfoot>
</table>
</div>
<?php }?>