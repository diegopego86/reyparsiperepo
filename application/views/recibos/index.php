<div class="grid16 alpha omega">
<h1>Recibos provisionales de caja</h1>
<div id="recibos">
    <?php echo anchor('recibos/nuevo','Nuevo recibo provisional de caja','class="boton"');?>
    <?php echo anchor('recibos/imprimir_rango','Imprimir rango','class="boton" rel="imprimir_rango" onclick="return false;"');?>
<div style="margin-top:20px"><?php echo form_open('recibos/index');
echo form_input(array('name'=>'buscar','size'=>50,'value'=>set_value('buscar',$buscar)));
echo form_submit('enviar','Buscar');
echo form_close();?></div>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<table class="tabla">
	<thead>
		<tr>
			<th style="width: 2%;"></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('id','Num','title="Número"');?></th>
            <th style="width: 13%;"><?php echo $this->pagination->ordenar('cliente','Nit');?></th>
			<th style="width: 30%;"><?php echo $this->pagination->ordenar('nombre_cliente','Nombre del cliente');?></th>
			<th style="width: 5%;"><?php echo $this->pagination->ordenar('vendedor','vd','title="Id Vendedor"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('numero','Consec vd','title="Consecutivo vendedor."');?></th>
            <th style="width: 10%;"><?php echo $this->pagination->ordenar('fecha_realizado','Fecha','title="Subtotal sin descuento"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('total','Total','title="Total"');?></th>
			<th style="width: 10%;">Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php if($recibos){
		$n=$desde;
		foreach($recibos as $recibo):
		?>
		<tr>
			<td class="item"><?php echo $n?></td>
			<td title="id" style="text-align:center;"><?php echo str_pad($recibo->id, 6, "0", STR_PAD_LEFT);?></td>
                        <td title="nit"><?php echo $recibo->cliente;?></td>
			<td title="nombre"><?php if($recibo->nombre_cliente){echo $recibo->nombre_cliente; if($recibo->contacto){echo '<br/><span class="sub_label">'.$recibo->contacto.'</span>';}}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
			<td style="text-align: center;"><?php echo $recibo->vendedor;?></td>
			<td title="numero" style="text-align:right;"><?php echo $recibo->numero*1;?></td>
                        <td style="text-align: right;"><?php echo $recibo->fecha_realizado;?></td>
			<td style="text-align: right;"><?php echo number_format($recibo->total,0,'.',',');?></td>
			<td style="text-align: center;"><?php 
			//echo anchor_popup('recibos/ver/'.$recibo->id.'/'.sha1($recibo->id.$seguridad), '<span class="iconos_blanco ver"></span>', array('width'=> '900px','height' => '600','scrollbars' => 'yes','status'=> 'yes','resizable'=> 'yes','directories'=>'no','location'=>'no','class'=>'boton peque','title'=>'Ver el recibo'));
                        echo anchor('recibos/ver/'.$recibo->id.'/'.sha1($recibo->id.$seguridad), '<span class="iconos_blanco ver"></span>', 'class="boton peque" title="Ver el recibo" target="_blank"');
			echo ' '.anchor('recibos/enviar_copia/'.$recibo->id.'/'.sha1($recibo->id.$seguridad),'<span class="iconos_blanco enviar"></span>','class="boton peque" title="Enviar copia por correo" rel="enviar_mail"');			 
			echo ' '.anchor('recibos/imprimir_copia/'.$recibo->id.'/'.sha1($recibo->id.$seguridad),'<span class="print"></span>','class="boton peque" target="_blank" title="Imprimir en formato de recibo"'); 
                        echo '<br/>';
			echo ' '.anchor('recibos/generar_pdf/'.$recibo->id.'/'.sha1($recibo->id.$seguridad),'<span class="pdf"></span>','class="boton_gris peque" title="Generar copia en pdf"');
			//echo ' '.anchor('recibos/generar_excel/'.$recibo->id.'/'.sha1($recibo->id.$seguridad),'<span class="xls"></span>','class="boton_gris peque" title="Generar copia en Excel"');
			?></td>
		</tr>
		<?php
		$n++;
		endforeach;
		?>
		<?php }else{?>
		<tr>
			<td colspan="10"><span class="alerta"></span> No se encontraron recibos.</td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
</div>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="imprimir_rango_recibos" title="Imprimir rango de recibos"></div>');
	$('a[rel=enviar_mail]').click(function(){
		$('#contenido').append('<div id="enviarMail" title="Enviar Mail"></div>');
		$('#enviarMail').html('<div class="form"><ul><li style="text-align:left;"><?php echo form_label('(C.C.) Enviar copias del recibo a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','style'=>'width:100%;'));?></li><li><?php echo form_label('Mensaje adicional :','mensaje'); ?></li><li><?php echo form_textarea(array('name'=>'mensaje','style'=>'width:100%;'));?></li></ul></div>');
		$('#enviarMail input[name=email_copias]').change(function(){
			$('#enviarMail input[name=email_copias]').removeClass('error');
			var error=false;
			var correos = $('#enviarMail input[name=email_copias]').val().split(',');
			$('#enviarMail input[name=email_copias]').next('span').remove();
			if($('#enviarMail input[name=email_copias]').val()!=""){
				for (var n=0;n< correos.length;n++){
					if(!es_mail(correos[n])){
						error=true;
					   $('#enviarMail input[name=email_copias]').addClass('error');
					}
				}
				if(error){
				   $('#enviarMail input[name=email_copias]').after('<span class="error">* Existen email no válidos</span>');
				}
			}else{
			   $('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
			}
	    });
		var url=$(this).attr('href');
	    $('#cargando').removeClass('correcto error');
		$("#enviarMail").dialog({
				resizable: false,
				width: 800,
				height:480,
				modal: true,
				close: function(event,ui){
					$('#enviarMail').dialog('destroy');
					$('#enviarMail').remove();
					$('#enviarMail').dialog('close');
				},
				buttons: {
					'Cerrar': function() {
						$('#enviarMail').dialog('destroy');
						$('#enviarMail').remove();
						$('#enviarMail').dialog('close');
					},
					'Enviar': function(){
						var email=$('#enviarMail input[name=email_copias]').val();
						var mensaje=$('#enviarMail textarea[name=mensaje]').val();
						var error=false;
						mensaje=mensaje.replace(/\n/g,'<br/>');
						if($('#cargando').text()!='Enviando copia del recibo'){
							if(email){
								$('#cargando').html('Enviando copia del recibo');
								$('#cargando').show();
								$('#enviarMail input[name=email_copias]').removeClass('error');
								var correos = $('#enviarMail input[name=email_copias]').val().split(',');
								for (var n=0;n< correos.length;n++){
									if(!es_mail(correos[n])){
										error=true;
									   $('#enviarMail input[name=email_copias]').addClass('error');
									}
								}
							}else{
								$('#enviarMail input[name=email_copias]').addClass('error');
								error=true;
							}
							$('#enviarMail input[name=email_copias]').next('span').remove();
							if(error){
								$('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
							}else{
								var form_data={
									email:email,
									mensaje:mensaje,
									ajax:1
								};
								$.ajax({
								   type: "POST",
								   url: url,
								   data: form_data,
                                                                   dataType:'json',
								   success: function(msg){
									   $('#cargando').removeClass('correcto error');
									   if(msg['estado']=='Correcto'){
										   $('#cargando').html('Enviado correctamente');
                                                                                    $('#cargando').html('Enviado correctamente');
                                                                                   $('#enviarMail').dialog('destroy');
                                                                                    $('#enviarMail').remove();
                                                                                    $('#enviarMail').dialog('close');
									   }else{
										   $('#cargando').html('Se presentaron errores. Inténtelo nuevamente.');
										   $('#cargando').addClass('error');										   
									   }
								   },
								   error: function(x,e){
									  $('#cargando').addClass('error');
										if(x.status==0){
												$('#cargando').html('Se perdió la conexión!!. Por favor verifique su conexión a internet.');
										}else if(x.status==404){
												$('#cargando').html('La url buscada no se encontró.');
										}else if(x.status==500){
												$('#cargando').html('Error interno del servidor.'+x.responseText);
										}else if(e=='parsererror'){
												$('#cargando').html('Error.\nParsing JSON Request failed.');
										}else if(e=='timeout'){
												$('#cargando').html('Se ha demorado mucho la operación, inténtelo nuevamente.');
										}else {
												$('#cargando').html('Error desconocido. '+x.responseText);
										}
										$('#cargando').delay(3000).slideUp('fast',function(){ $('#cargando').removeClass('correcto error')});
								   }
								 });					
							}
						}
					}
				}
		});
		return false;
	});
	function es_mail(correo){
		var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		if(correo.match(expresion)){
			return true;
		}else{
			return false;
		}
	}
	var buscar=$('input[name=buscar]').val();
	if(buscar!=''){
		$('table tbody td[title=numero]').highlight(buscar);
		$('table tbody td[title=nit]').highlight(buscar);
		$('table tbody td[title=nombre]').highlight(buscar);
	}
        $('#imprimir_rango_recibos').dialog({
            autoOpen: false,
            show:'fold',
            hide:'fold',
            height: 250,
            width: 600,
            resizable:false,
            modal: true            
        });
        $('a[rel=imprimir_rango]').click(function(){
            var url=$(this).attr('href');
            var form_data={
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       $('#cargando').removeClass('correcto error');
                       $('#imprimir_rango_recibos').html(msg);
                       $('#imprimir_rango_recibos').dialog('open');
                       if(msg){
                               $('#cargando').html('Enviado correctamente');
                       }else{
                               $('#cargando').html('Se presentaron errores. Inténtelo nuevamente.');
                               $('#cargando').addClass('error');
                               alert("error");
                       }
               },
               error: function(x,e){
                      mostrar_error(x,e);
               }
           });
           return false; 
        });
        $('#imprimir_rango_recibos form').live('submit',function(){
            if($('#imprimir_rango_recibos input[name=rango]').val()==''){
                return false;
            }
            $('#imprimir_rango_recibos').dialog('close');
        });
});
</script>
