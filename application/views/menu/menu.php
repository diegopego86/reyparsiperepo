<style>
    #ui-datepicker-div{
        display:none;
        z-index:200 !important;
    }
</style>
<?php if ($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){?>
<div class="grid16 alpha omega">
    <h1>Total pedidos</h1>
    <div style="text-align: center;">Elija la fecha: <input id="fecha_calendario_pedidos" type="hidden" /></div>
    <div id='calendario_pedidos'></div>
    <div style="margin-top:10px;" id="total_pedidos_mes"><?php echo $total_pedidos_mes;?></div>
</div>
<div class="grid16 alpha omega">
    <h1>Total recibos</h1>
    <div style="text-align: center;">Elija la fecha: <input id="fecha_calendario_recibos" type="hidden" /></div>
    <div id='calendario_recibos'></div>
    <div style="margin-top:10px;" id="total_recibos_mes"><?php echo $total_recibos_mes;?></div>
</div>
<div class="clear"></div>
<?php }?>
<div id="" class="grid8 alpha">
    <div style="margin-bottom: 10px;">
        <h2>Borradores de pedido</h2>
        <table class="tabla">
            <thead>
            <tr>
                <th>Id</th>
                <th>Descripcion</th>    
                <th>Fecha</th>
                <th>-</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($borradores){
            ?>
            <?php
            $n=1;
                    foreach($borradores as $borrador):
                    $encabezado=json_decode($borrador->encabezado);
                    $total_productos=count(json_decode($borrador->productos));
                    $descripcion='';
                    if(isset($encabezado->nit)){
                        $descripcion=$descripcion.$encabezado->nit.'<br/>';
                    }
                    if(isset($encabezado->nombre)){
                        $descripcion=$descripcion.$encabezado->nombre.'<br/>';
                    }
                    if(isset($encabezado->observaciones)){
                        $descripcion=$descripcion.$encabezado->observaciones.'<br/>';
                    }
                    $descripcion=$descripcion.'<span style="color:#F60;font-style:italic">'.$total_productos.' ';
                    if($total_productos==1){
                        $descripcion.='producto ';
                    }else{
                        $descripcion.='productos ';
                    }
                    $descripcion.='en el borrador.</span>';
            ?>

            <tr style="vertical-align:top;">
            <td title="Numero"><?php echo $borrador->id;?></td>
            <td><?php echo $descripcion;?></td>
            <td style="text-align:center;"><?php echo $borrador->fecha;?></td>
            <td style="width:60px;"><?php echo anchor('pedidos/nuevo#borrador='.$borrador->id,'<span class="iconos_blanco abrir"></span>','class="boton peque" title="Continuar con el borrador."');?> <?php echo anchor('pedidos/eliminar_borrador/'.$borrador->id,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="boton_eliminar_borrador"');?></td>
            </tr>
            <?php
                    $n++;
                    endforeach;
            ?>
            <?php }else{?>
            <tr><td colspan="4"><span class="alerta"></span>No tiene borradores de pedidos guardados.</td></tr>
            <?php }?>
            </tbody>
        </table>
    </div>
<h2 style="margin-top:20px;">Últimos pedidos realizados</h2>
<table class="tabla">
<thead>
<tr>
    <th title="Número">Num</th>
    <th>Nit</th>
    <th>Nombre del cliente</th>    
    <th title="Id Vendedor">vd</th>
    <th title="Valor Neto">V. Neto</th>
    <th>Fecha</th>
    <th>Acciones</th>
    </tr>
</thead>
<tbody>
<?php
if($ultimos_pedidos){
$n=1;
	foreach($ultimos_pedidos as $pedido):
?>
<tr><td><?php echo $pedido->id_maestro;?></td><td><?php echo $pedido->id_cliente;?></td>
<td><?php if($pedido->nombre_cliente){echo $pedido->nombre_cliente; if($pedido->contacto){echo '<br/><span class="sub_label">'.$pedido->contacto.'</span>';}}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
<td style="text-align:center;"><?php echo $pedido->id_vendedor;?></td>
<td style="text-align:right;"><?php echo number_format($pedido->subtotal,'0',',',".");?></td>
<td style="color:#930; text-align:center; font-size:0.7em;"><?php echo $pedido->fecha;?></td>
<td style="text-align:center;"><?php echo anchor('pedidos/ver/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad), '<span class="iconos_blanco ver"></span>', ' target="_blank" class="boton peque"');?></td>
</tr>
<?php
	$n++;
	endforeach;
}else{?>
<tr><td colspan="7"><span class="alerta"></span>No ha realizado pedidos.</td></tr>
<?php }?>
</tbody>
</table>
	<div style="margin-top:10px">
	<?php 
    echo anchor('pedidos/index','Ver todos los pedidos','class="boton"');
    ?>
    </div>
</div>
<div class="form grid8 omega">
    <?php if($borradores_cotizaciones){?>
    <div style="margin-bottom: 10px;">
        <h2>Borradores de cotizaciones</h2>
        <table class="tabla">
            <thead>
            <tr>
                <th>Id</th>
                <th>Descripcion</th>    
                <th>Fecha</th>
                <th>-</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($borradores_cotizaciones){
            ?>
            <?php
            $n=1;
                    foreach($borradores_cotizaciones as $borrador):
                    $encabezado=json_decode($borrador->encabezado);
                    $total_productos=count(json_decode($borrador->productos));
                    $descripcion='';
                    if(isset($encabezado->nit)){
                        $descripcion=$descripcion.$encabezado->nit.'<br/>';
                    }
                    if(isset($encabezado->nombre)){
                        $descripcion=$descripcion.$encabezado->nombre.'<br/>';
                    }
                    if(isset($encabezado->observaciones)){
                        $descripcion=$descripcion.$encabezado->observaciones.'<br/>';
                    }
                    $descripcion=$descripcion.'<span style="color:#F60;font-style:italic">'.$total_productos.' ';
                    if($total_productos==1){
                        $descripcion.='producto ';
                    }else{
                        $descripcion.='productos ';
                    }
                    $descripcion.='en el borrador.</span>';
            ?>

            <tr style="vertical-align:top;">
            <td title="Numero"><?php echo $borrador->id;?></td>
            <td><?php echo $descripcion;?></td>
            <td style="text-align:center;"><?php echo $borrador->fecha;?></td>
            <td style="width:60px;"><?php echo anchor('cotizaciones/nuevo#borrador='.$borrador->id,'<span class="iconos_blanco abrir"></span>','class="boton peque" title="Continuar con el borrador."');?> <?php echo anchor('cotizaciones/eliminar_borrador/'.$borrador->id,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="boton_eliminar_borrador"');?></td>
            </tr>
            <?php
                    $n++;
                    endforeach;
            ?>
            <?php }else{?>
            <tr><td colspan="4"><span class="alerta"></span>No tiene borradores de cotizaciones guardados.</td></tr>
            <?php }?>
            </tbody>
        </table>
    </div>
    <?php }?>
    <?php if($borradores_recibos){?>
    <div style="margin-bottom: 10px;">
        <h2>Borradores de recibos</h2>
        <table class="tabla">
            <thead>
            <tr>
                <th>Id</th>
                <th>Descripcion</th>    
                <th>Fecha</th>
                <th>-</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($borradores_recibos){
            ?>
            <?php
            $n=1;
                    foreach($borradores_recibos as $borrador):
                    $encabezado=json_decode($borrador->encabezado);
                    $total_productos=count(json_decode($borrador->documentos));
                    $descripcion='';
                    if(isset($encabezado->nit)){
                        $descripcion=$descripcion.$encabezado->nit.'<br/>';
                    }
                    if(isset($encabezado->nombre)){
                        $descripcion=$descripcion.$encabezado->nombre.'<br/>';
                    }
                    if(isset($encabezado->observaciones)){
                        $descripcion=$descripcion.$encabezado->observaciones.'<br/>';
                    }
                    $descripcion=$descripcion.'<span style="color:#F60;font-style:italic">'.$total_productos.' ';
                    if($total_productos==1){
                        $descripcion.='documento ';
                    }else{
                        $descripcion.='documentos ';
                    }
                    $descripcion.='en el borrador.</span>';
            ?>

            <tr style="vertical-align:top;">
            <td title="Numero"><?php echo $borrador->id;?></td>
            <td><?php echo $descripcion;?></td>
            <td style="text-align:center;"><?php echo $borrador->fecha;?></td>
            <td style="width:60px;"><?php echo anchor('recibos/nuevo#borrador='.$borrador->id,'<span class="iconos_blanco abrir"></span>','class="boton peque" title="Continuar con el borrador."');?> <?php echo anchor('recibos/eliminar_borrador/'.$borrador->id,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="boton_eliminar_borrador"');?></td>
            </tr>
            <?php
                    $n++;
                    endforeach;
            ?>
            <?php }else{?>
            <tr><td colspan="4"><span class="alerta"></span>No tiene borradores de recibos guardados.</td></tr>
            <?php }?>
            </tbody>
        </table>
    </div>
    <?php }?>
    <div>
</div>
</div>
<div class="clear"></div>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-ui-1.8.10.custom.min.js"></script>
<script type='text/javascript' src='<?php echo base_url();?>js/fullcalendar.js'></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/date.format.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('a[rel=boton_eliminar_borrador]').click(function(){
		var url=$(this).attr('href');
		var obj=$(this).parent().parent();
		var numero=obj.children('[title=Numero]').text();
		$('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el borrador #'+numero+' ?</div>');
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				'SI': function() {
                                        mostrar_mensaje('Se está eliminando el borrador. Por favor espere...','alerta',0);
					var form_data={
						ajax:1
					};
					$.ajax({
						type: "POST",
						url: url,
						data: form_data,
                                                dataType: 'json',
						success: function(msg){
							if(msg['estado']=='correcto'){
								obj.fadeOut('slow',function(){ $(this).remove();});
                                                                mostrar_mensaje(msg['mensaje'],'correcto',1000);								
							}else{
                                                            mostrar_mensaje(msg['mensaje'],'error');								
							}
							$('#dialog-confirm').dialog('destroy');
							$('#dialog-confirm').remove();
							$('#dialog-confirm').dialog('close');
						},
						error: function(x,e){
							mostrar_error(x,e)
							$('#dialog-confirm').dialog('destroy');
							$('#dialog-confirm').remove();
							$('#dialog-confirm').dialog('close');							
						}
					});
					//location.href=url;
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
					$('#dialog-confirm').remove();
					$('#dialog-confirm').dialog('close');
				}
			}
		});
		return false;
	});
        <?php if ($this->session->userdata('rol')!=1 && $this->session->userdata('rol')!=4){?>
        $('#fecha_calendario_pedidos').datepicker({
               changeMonth: true,
               changeYear: true,
               dateFormat: 'mm-dd-yy',
               showOn: "button",
               buttonImage: "<?php echo base_url();?>img/calendar.png",
               buttonImageOnly: true,
               onSelect: function(dateText, inst) {
                   var fecha = new Date(dateText);
                   $('#calendario_pedidos').fullCalendar( 'gotoDate', fecha.getFullYear(),fecha.getMonth(),fecha.getDate() );
               }
            });
        $('#calendario_pedidos').fullCalendar({			
                header:{                          
                    left:'prev,right,next',                          
                    center:'title',                          
                    right:'basicDay,basicWeek,month'                        
                },         
                weekMode: 'variable',
                defaultView:'basicWeek',                        
                editable: false,                        
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],                        
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Apb', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],                        
                dayNames:['Domingo', 'Lunes', 'Martes', 'Miércoles','Jueves', 'Viernes', 'Sábado'],                        
                dayNamesShort:['Dom', 'Lun', 'Mar', 'Mié','Jue', 'Vie', 'Sáb'],                        
                buttonText: {                                        
                    today: 'Hoy',                                        
                    month:    'Mes',                                        
                    week:     'Semana',                                        
                    day:      'Día'                                    
                },                       
                height:'100',                       
                //events: "http://190.248.67.66/sipred/tablero/eventos",                       
                events: function(start, end, callback) {                                
                   var start_timestamp = Math.round(start.getTime() / 1000);
                   var end_timestamp = Math.round(end.getTime() / 1000);
                   var actual=start_timestamp+((end_timestamp-start_timestamp)/2);
                   //var actual=Math.round($('#calendario_pedidos').fullCalendar('getDate').getTime()/ 1000);
                   $.ajax({
                        url: "<?php echo site_url('menu/calendario_pedidos');?>/" + start_timestamp + "/" + end_timestamp+'/'+actual,
                        dataType: 'json',
                        success: function(events) {
                            if(events.total_mes){
                                $('#total_pedidos_mes').html(events.total_mes);
                            }else{
                                $('#total_pedidos_mes').html('');
                            }
                            if(events){
                                callback(events.eventos);
                            }else{
                                mostrar_mensaje('No hay eventos.','correcto',500);
                            }
                        }
                    });                        
                },                        
                eventRender: function(event, element) {
                    element.html('<div>'+event.nombre+'</div>');                        
                },                       
                eventDrop: function(event, delta) {				
                    //alert(event.title + ' was moved ' + delta + ' days\n' +	'(should probably update your database)');			
                },			
                loading: function(bool) {				
                    if (bool) mostrar_mensaje('Cargando información','alerta');
                    else mostrar_mensaje('Datos cargados','correcto',500);
                }		
            }); 
            $('#fecha_calendario_recibos').datepicker({
               changeMonth: true,
               changeYear: true,
               dateFormat: 'mm-dd-yy',
               showOn: "button",
               buttonImage: "<?php echo base_url();?>img/calendar.png",
               buttonImageOnly: true,
               onSelect: function(dateText, inst) {
                   var fecha = new Date(dateText);
                   $('#calendario_recibos').fullCalendar( 'gotoDate', fecha.getFullYear(),fecha.getMonth(),fecha.getDate() );
               }
            });
            
            $('#calendario_recibos').fullCalendar({			
                header:{                          
                    left:'prev,right,next',                          
                    center:'title',                          
                    right:'basicDay,basicWeek,month'                        
                },         
                weekMode: 'variable',
                defaultView:'basicWeek',                        
                editable: false,                        
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],                        
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Apb', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],                        
                dayNames:['Domingo', 'Lunes', 'Martes', 'Miércoles','Jueves', 'Viernes', 'Sábado'],                        
                dayNamesShort:['Dom', 'Lun', 'Mar', 'Mié','Jue', 'Vie', 'Sáb'],                        
                buttonText: {                                        
                    today: 'Hoy',                                        
                    month:    'Mes',                                        
                    week:     'Semana',                                        
                    day:      'Día'                                    
                },                       
                height:'100',                       
                //events: "http://190.248.67.66/sipred/tablero/eventos",                       
                events: function(start, end, callback) {                                
                   var start_timestamp = Math.round(start.getTime() / 1000);
                   var end_timestamp = Math.round(end.getTime() / 1000);
                   //var actual=Math.round($('#calendario_recibos').fullCalendar('getDate').getTime()/ 1000);
                   var actual=start_timestamp+((end_timestamp-start_timestamp)/2);
                   var url="<?php echo site_url('menu/calendario_recibos');?>/" + start_timestamp + "/" + end_timestamp+'/'+actual;
                   //alert(url);
                   $.ajax({
                        url: url,
                        dataType: 'json',
                        success: function(events) {
                            if(events.total_mes){
                                $('#total_recibos_mes').html(events.total_mes);
                            }else{
                                $('#total_recibos_mes').html('');
                            }
                            if(events){
                                callback(events.eventos);   
                            }else{
                                mostrar_mensaje('No hay eventos.','correcto',500);
                            }
                        }
                    });                        
                },                        
                eventRender: function(event, element) {
                        element.html('<div>'+event.nombre+'</div>');                                                
                },                       
                eventDrop: function(event, delta) {				
                    //alert(event.title + ' was moved ' + delta + ' days\n' +	'(should probably update your database)');			
                },			
                loading: function(bool) {				
                    if (bool) mostrar_mensaje('Cargando información','alerta');
                    else mostrar_mensaje('Datos cargados','correcto',500);
                }		
            }); 
            <?php }?>
});
</script>