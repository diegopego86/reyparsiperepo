<div style="margin-top:20px">
    <?php 
    echo form_open($url);
    echo form_input(array('name'=>'buscar','size'=>50,'value'=>set_value('buscar',$buscar)));
    echo form_submit('enviar','Buscar');
    echo form_close();
    ?>
</div>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<table class="tabla">
	<thead>
		<tr>
			<th style="width: 3%;"></th>
			<th style="width: 5%;"><?php echo $this->pagination->ordenar('id_maestro','Num','title="Número"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('id_cliente','Nit');?></th>
			<th style="width: 20%;"><?php echo $this->pagination->ordenar('nombre_cliente','Nombre del cliente');?></th>
			<th style="width: 4%;"><?php echo $this->pagination->ordenar('id_vendedor','vd','title="Id Vendedor"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('subtotal_sin_dcto','Subtotal','title="Subtotal sin descuento"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('iva_total','Iva','title="Iva"');?></th>
			<th style="width: 10%;"><?php echo $this->pagination->ordenar('total','Total','title="Total"');?></th>
			<th style="width: 8%;"><?php echo $this->pagination->ordenar('fecha','Fecha');?></th>
			<th style="width: 10%;">Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php if($pedidos){
		$n=$desde;
		foreach($pedidos as $pedido):
		?>
		<tr>
			<td class="item"><?php echo $n?></td>
			<td title="numero"><?php echo $pedido->id_maestro;?></td>
			<td title="nit"><?php echo $pedido->id_cliente;?></td>
			<td title="nombre"><?php if($pedido->nombre_cliente){echo $pedido->nombre_cliente; if($pedido->contacto){echo '<br/><span class="sub_label">'.$pedido->contacto.'</span>';}}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
			<td style="text-align: center;"><?php echo $pedido->id_vendedor;?></td>
			<td style="text-align: right;"><?php echo number_format($pedido->subtotal_sin_dcto,'0',',',".");?><?php if($pedido->descuento_total>0){ echo '<div class="descuento">-dcto '.number_format($pedido->descuento_total,'0',',',".").'</div>';}?></td>
			<td style="text-align: right;"><?php echo number_format($pedido->iva_total,'0',',',".");?></td>
			<td style="text-align: right;"><?php echo number_format($pedido->total,'0',',',".");?></td>
			<td style="color: #930; text-align: center; font-size: 0.7em;"><?php echo $pedido->fecha;?></td>
			<td style="text-align: center;"><?php 
			//echo anchor_popup('pedidos/ver/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad), '<span class="iconos_blanco ver"></span>', array('width'=> '900','height' => '600','scrollbars' => 'yes','status'=> 'yes','resizable'=> 'yes','directories'=>'no','location'=>'no','class'=>'boton peque','title'=>'Ver el pedido'));
			echo ' '.anchor('pedidos/ver/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_blanco ver"></span>','target="_blank" class="boton peque" title="Ver el pedido" rel="ver_pedido"');			 
                        echo ' '.anchor('pedidos/enviar_copia/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_blanco enviar"></span>','class="boton peque" title="Enviar copia por correo" rel="enviar_mail" numero="'.$pedido->id_maestro.'"');			 
			echo ' '.anchor('pedidos/imprimir_copia/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="print"></span>','class="boton peque" target="_blank" title="Imprimir en formato de recibo"'); 
                        echo '<br/>';
			echo ' '.anchor('pedidos/generar_pdf/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="pdf"></span>','target="_blank" class="boton_gris peque" title="Generar copia en pdf"');
			echo ' '.anchor('pedidos/generar_excel/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="xls"></span>','target="_blank" class="boton_gris peque" title="Generar copia en Excel"');
			?></td>
		</tr>
		<?php
		$n++;
		endforeach;
		?>
		<?php }else{?>
		<tr>
			<td colspan="10"><span class="alerta"></span> No se encontraron pedidos.</td>
		</tr>
		<?php }?>
	</tbody>
</table>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>