<div style="margin-top:20px;">
<?php
if(!$cliente || $cliente==''){
	$cliente='Digite el nit o nombre del cliente';
}
echo form_input(array('name'=>'cliente','value'=>$cliente,'style'=>'width:400px;'));
echo anchor('pedidos/buscar_cliente','Buscar','class="boton" rel="filtrar_cliente"');
?>
</div>
<?php if($clientes){?>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<table id="resultados_buscar_clientes" class="tabla">
<thead>
<tr>
    <th style="width:90px;"><?php echo $this->pagination->ordenar('id_cliente','Nit','title="Nit del cliente"');?></th>
    <th style=""><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
    <?php if($configuracion['clientes_mostrar_contacto']==1){?>
    <th style=""><?php echo $this->pagination->ordenar('contacto','Contacto');?></th>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_vendedor']==1){?>
    <th style="width:35px;"><?php echo $this->pagination->ordenar('id_vendedor','Vd');?></th>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_ciudad']==1){?>
    <th style=""><?php echo $this->pagination->ordenar('ciudad','Ciudad');?></th>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_direccion']==1){?>
    <th style=""><?php echo $this->pagination->ordenar('direccion','Dirección');?></th>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_telefono1']==1){?>
    <th style="width:80px;"><?php echo $this->pagination->ordenar('telefono1','Teléfono 1');?> <?php if($configuracion['clientes_mostrar_telefono2']==1){ echo ' / Teléfono 2';}?> </th>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_email']==1){?>
    <th style=""><?php echo $this->pagination->ordenar('email','E-mail');?></th>
    <?php }?>
    <th style="">Acción</th>
</tr>
</thead>
<tbody>
<?php foreach($clientes as $cliente):?>
<tr>
<td><?php echo $cliente->id_cliente;?></td>
<td><?php echo $cliente->nombre;?></td>
<?php if($configuracion['clientes_mostrar_contacto']==1){?>
<td><?php echo $cliente->contacto;?></td>
<?php }?>
<?php if($configuracion['clientes_mostrar_vendedor']==1){?>
<td><?php echo $cliente->id_vendedor;?></td>
<?php }?>
<?php if($configuracion['clientes_mostrar_ciudad']==1){?>
<td><?php echo $cliente->ciudad;?></td>
<?php }?>
<?php if($configuracion['clientes_mostrar_direccion']==1){?>
<td><?php echo $cliente->direccion;?></td>
<?php }?>
<?php if($configuracion['clientes_mostrar_telefono1']==1){?>
<td><?php echo $cliente->telefono1;?>
    <?php 
        if($configuracion['clientes_mostrar_telefono2']==1 && $cliente->telefono2){
            echo " / ".$cliente->telefono2;
        }
     ?>    
</td>
<?php }?>
<?php if($configuracion['clientes_mostrar_email']==1){?>
<td><?php echo $cliente->email;?></td>
<?php }?>
<td style="text-align:center;"><?php echo anchor('pedidos/datos_cliente/'.$cliente->id_cliente,'+','class="boton" id="'.$cliente->id_cliente.'" rel="boton_seleccionar_cliente"');?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
<?php }else{?>
<table class="tabla">
<thead>
<tr><th>Nit</th><th>Nombre</th><th>Contacto</th></tr>
</thead>
<tbody>
<tr><td colspan="3"><span class="alerta"></span> No existen clientes con los datos de busqueda.</td></tr>
</tbody>
</table>
<?php }?>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>