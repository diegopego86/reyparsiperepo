<div class="form" style="margin-top:30px;">
    <?php echo form_open('pedidos/imprimir_rango_pedidos',array('target'=>'_blank'));
        echo form_input(array('name'=>'rango','style'=>'width:90%'));
        ?>
        <div style="margin:10px 0px;">Escriba números de pedidos e intervalos separados por comas. <br/>Por ejemplo: 1,3, 6-12, 14</div><?php
        echo form_label(form_radio(array('name'=> 'tipo','value'=> 1,'id'=>'tipo_impresora','checked'=> true)).' En impresora, hoja tamaño carta','tipo_impresora');
        echo form_label(form_radio(array('name'=> 'tipo','value'=> 2,'id'=>'tipo_tirilla')).' En tirilla ','tipo_tirilla');
        echo form_submit('imprimir','Imprimir');
        echo form_close();
    ?>
</div>