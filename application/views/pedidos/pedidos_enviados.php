<div class="grid16">
<div style="margin-top:20px; padding:4px; text-align:center;background:#D9EFAF;color:#060;font-style:italic;">
<?php
$cantidad=count($numeros);
if($cantidad==1){
	echo 'Se guardó el pedido # '.$numeros[0];
	echo ' '.anchor('pedidos/imprimir_copia/'.$numeros[0].'/'.sha1($numeros[0].$seguridad),'<span class="print"></span>','class="boton_gris peque" target="_blank" title="Imprimir en formato de recibo"');
	echo ' '.anchor('pedidos/generar_pdf/'.$numeros[0].'/'.sha1($numeros[0].$seguridad),'<span class="pdf"></span>','class="boton_gris peque" title="Generar copia en pdf" rel="enviar_mail"');
	echo ' '.anchor('pedidos/generar_excel/'.$numeros[0].'/'.sha1($numeros[0].$seguridad),'<span class="xls"></span>','class="boton_gris peque" title="Generar copia en Excel"');
}else{
	echo 'Se guardaron los pedidos: <br/>';
	foreach($numeros as $numero):
		echo $numero;
		echo ' '.anchor('pedidos/imprimir_copia/'.$numero.'/'.sha1($numero.$seguridad),'<span class="print"></span>','class="boton_gris peque" target="_blank" title="Imprimir en formato de recibo"');
		echo ' '.anchor('pedidos/generar_pdf/'.$numero.'/'.sha1($numero.$seguridad),'<span class="pdf"></span>','class="boton_gris peque" title="Generar copia en pdf" rel="enviar_mail"');
		echo ' '.anchor('pedidos/generar_excel/'.$numero.'/'.sha1($numero.$seguridad),'<span class="xls"></span>','class="boton_gris peque" title="Generar copia en Excel"');
		echo '<br/>';
	endforeach;
}
?>
</div>    
<?php 

echo '<div id="envio_pedidos" style="text-align:center; font-size:18px;"></div>';
echo form_hidden('envio_pedidos_correo',1);
echo '<div style="width:700px; margin:0 auto;margin-top:20px">'.$mostrar.'</div>';
?>
</div>
<div class="clear"></div>