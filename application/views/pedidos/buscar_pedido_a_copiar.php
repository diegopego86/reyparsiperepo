<div style="margin-top:20px;">
<?php
echo form_open('pedidos/buscar_pedido_a_copiar',array('name'=>'buscar_pedido_a_copiar','onsubmit'=>'return false;'));
echo form_label('Cliente: ','cliente');
if($cliente==''){$cliente='Digite el nit o nombre del cliente';}
echo form_input(array('name'=>'cliente','value'=>set_value('cliente',$cliente),'style'=>'width:400px;'));
echo form_submit('buscar','Buscar');
//echo anchor('pedidos/buscar_pedido_a_copiar','Buscar','class="boton" rel="buscar_pedido_a_copiar"');
?>
</div>
    <div id="resultados_pedidos">
        <div class="resultados"><?php echo $this->pagination->resultados();?></div>
        <div class="paginacion"><?php echo $this->pagination->create_links();?></div>
        <table class="tabla">
            <thead>
            <tr>
            <th style="width:3%;"></th>
                <th style="width:5%;"><?php echo $this->pagination->ordenar('id_maestro','Num','title="Número"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('id_cliente','Nit');?></th>
                <th style="width:21%;"><?php echo $this->pagination->ordenar('nombre_cliente','Nombre del cliente');?></th>   
                <th style="width:5%;"><?php echo $this->pagination->ordenar('id_vendedor','vd','title="Id Vendedor"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('subtotal_sin_dcto','Subtotal','title="Subtotal sin descuento"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('iva_total','Iva','title="Iva"');?></th>
                <th style="width:10%;"><?php echo $this->pagination->ordenar('total','Total','title="Total"');?></th>
                <th style="width:8%;"><?php echo $this->pagination->ordenar('fecha','Fecha');?></th>
                <th style="width:8%;">-</th>
                </tr>
            </thead>
            <tbody>
        <?php if($pedidos){
                $n=$desde;
                foreach($pedidos as $pedido):
            ?>
            <tr>
                <td class="item"><?php echo $n?></td><td><?php echo $pedido->id_maestro;?></td>
                <td title="nit"><?php echo $pedido->id_cliente;?></td>
                <td title="nombre"><?php if($pedido->nombre_cliente){echo $pedido->nombre_cliente;}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
                <td style="text-align:center;"><?php echo $pedido->id_vendedor;?></td>
                <td style="text-align:right;"><?php echo number_format($pedido->subtotal_sin_dcto,'0',',',".");?><?php if($pedido->descuento_total>0){ echo '<div class="descuento">-dcto '.number_format($pedido->descuento_total,'0',',',".").'</div>';}?></td>
                <td style="text-align:right;"><?php echo number_format($pedido->iva_total,'0',',',".");?></td>
                <td style="text-align:right;"><?php echo number_format($pedido->total,'0',',',".");?></td>
                <td style="color:#930; text-align:center; font-size:0.7em;"><?php echo $pedido->fecha;?></td>
                <td style="text-align:center;"><?php echo anchor_popup('pedidos/ver/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad), '<span class="iconos_blanco ver"></span>', array('width'=> '800','height'     => '600','scrollbars' => 'yes','status'=> 'yes','resizable'=> 'yes','directories'=>'no','location'=>'no','class'=>'boton peque')).' '.anchor('pedidos/copiar_productos_pedido/'.$pedido->id_maestro,'<span class="iconos_blanco copiar"></span>','class="boton peque" rel="copiar_pedido" title="Copiar información del pedido"');?></td>
            </tr>
            <?php
                $n++;
                endforeach;
            ?>
        <?php }else{?>
        	<tr><td colspan="10"><span class="alerta"></span> No tiene pedidos realizados para copiar.</td></tr>
        <?php }?>
            </tbody>
        </table>
        <div class="paginacion"><?php echo $this->pagination->create_links();?></div>
    </div>