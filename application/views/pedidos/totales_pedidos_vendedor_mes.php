<table class="tabla">
    <thead><tr><th style="height:30px;">#</th><th>Nombre</th><th>Total pedidos <?php echo $mes;?></th></tr></thead>
<tbody>
<?php
if($pedidos_totales){
$suma=0;
foreach($pedidos_totales as $pedidos):
?>
<tr><td><?php echo $pedidos->id_vendedor;?></td><td><?php echo $pedidos->nombre;?></td><td style="text-align:right;"><?php echo '$ '.number_format($pedidos->subtotal,0,'.',',');?></td></tr>
<?php
$suma+=$pedidos->subtotal;
endforeach;
?>
	<tr><td class="item" style="color:#900;">Total</td><td colspan="2" class="item" style="text-align:right; color:#900; text-shadow: 0 1px 1px rgba(0,0,0,.3);"><?php echo '$ '.number_format($suma,0,'.',',');?></td></tr>
<?php }else{?>
<tr><td colspan="3">No hay datos en este mes</td></tr>
<?php }?>
</tbody>
</table>