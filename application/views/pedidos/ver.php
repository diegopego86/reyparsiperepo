<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if($mostrar_encabezado){ ?><link rel="stylesheet" href="<?php echo base_url();?>css/imprimir.css" type="text/css" media="screen" charset="utf-8" /><?php }?>
<title>Mostrar pedido</title>
<style>
    #contenido-pedido,#contenido-pedido table{
            font-family:Arial, Helvetica, sans-serif;
            font-size:small;
    }
    #contenido-pedido span.sub_label{
            font-size:0.9em;
            color:#999;
            display:block;
    }
    #contenido-pedido .seccion{
            margin:10px 0px;
            padding-bottom:10px;
            border-bottom:1px dotted #CCC;
    }
    #contenido-pedido .seccion h2{
            font-size:160%;
            font-weight:normal;
            margin:0 0 0.2em;	
    }
    #contenido-pedido ul,#contenido-pedido p{
            margin:0;
            padding:0;
    }
    #contenido-pedido ul.formulario{
            list-style:none;
    }
    #contenido-pedido ul.formulario li{
            margin:0;
            padding:6px 1% 9px;	
       -moz-box-sizing:    border-box;
       -webkit-box-sizing: border-box;
        box-sizing:        border-box;
    }
    #contenido-pedido .right{
            clear:none;
            float:right;
    }
    #contenido-pedido .left{
            float:left;
    }
    #contenido-pedido table.tabla{
            border-collapse:collapse;
    }
    #contenido-pedido table.tabla tbody tr{
            border-bottom:1px solid #CCC;
            padding:1px 2px;
    }
    #contenido-pedido table.tabla tbody td{
            vertical-align:text-top;
    }
    #contenido-pedido table.tabla th{
            padding-bottom:0px;	
    }

    #contenido-pedido table.tabla{
            font-size:0.85em;	
    }
</style>
<style media="screen">
    .pagina{
        margin-bottom: 1cm;
        background: #FFF;
        padding:10px;
        min-height: 969px;        
        -webkit-box-shadow: 2px 5px 5px rgba(0,0,0,.7);
        -moz-box-shadow: 2px 5px 5px rgba(0,0,0,.7);
        box-shadow: 2px 5px 5px rgba(0,0,0,.7);
    }
</style>
<style media="print">
    @page{
        margin:1cm;
    }
    body{
        width:8.5in;
        height:11in;
    }
    #imprimir{
            display:none;
    }
    .salto{
        page-break-after: always;
    }
</style>
</head>
<body <?php if(isset($autoimprimir)){ if($autoimprimir){ echo 'onload="javascript:window.print();"';} } ?>>
   <?php if($mostrar_encabezado){ ?><div id="imprimir"><div><?php if($mostrar_logo){ ?><span class="logo"><?php echo anchor('',img(array('src'=>'img/logo_sipe_blanco.png','border'=>0)));?></span><?php } $texto=img(array('src'=>'img/printer.png','border'=>0,'style'=>'vertical-align:middle;')).' Imprimir'; if($enlace){ ?><a href="javascript:window.print();"><?php echo $texto;?></a> <?php } else{ echo anchor('pedidos/ver/'.$encabezado->id_maestro.'/'.sha1($encabezado->id_maestro.$seguridad).'/1',$texto); } ?></div><div class="clear"></div></div>   <?php } ?>
<div id="contenido-pedido" class="pagina">    
        <div style="font-size:0.8em; margin-bottom:10px; text-align:right;">NIT. <?php echo $empresa->nit.' - '.$empresa->nombre;?></div>
        <table width="100%">
            <tr>
                <td width="60%">
                    <table width="100%"  style="vertical-align:top;">
                        <tr>
                            <td colspan="2" style="font-weight:bold; font-size:1.2em; text-align:left; border-bottom:1px solid #777;">
                                CLIENTE
                            </td>
                        </tr>
                        <tr>
                            <td style="width:80px;">Nit:</td>
                            <td><?php echo $encabezado->id_cliente;?></td>
                        </tr>
                        <tr>
                            <td>Nombre:</td>
                            <td><?php echo $encabezado->nombre;?></td>
                        </tr>
                        <tr>
                            <td>Contacto:</td>
                            <td><?php echo $encabezado->contacto;?></td>
                        </tr>
                        <tr>
                            <td>Dirección:</td>
                            <td><?php echo $encabezado->direccion;?></td><td> <?php if($encabezado->direccion_envio){ echo "Dirección de envío:";}?></td><td><?php echo $encabezado->direccion_envio;?></td>
                        </tr>
                        <tr>
                            <td>Ciudad:</td>
                            <td><?php echo $encabezado->ciudad;?></td>
                        </tr>
                        <tr>
                            <td>Teléfono:</td>
                            <td><?php echo $encabezado->telefono1;?></td>
                        </tr>
                        <tr>
                            <td>Vendedor:</td>
                            <td><?php echo $encabezado->id_vendedor.' - '.$encabezado->nombre_vendedor;?></td>
                        </tr>
                        <tr>
                            <td>Plazo:</td>
                            <td><?php echo $encabezado->plazo;?></td>
                        </tr>
                        <?php if($configuracion['listar_listas']){?>
                        <tr>
                            <td>Lista:</td>
                            <td><?php echo $encabezado->lista;?></td>
                        </tr>
                        <? }?>
                        <?php if($configuracion['listar_retenciones']){
                            $retenciones=array('1'=>'No retiene','2'=>'Retiene sobre la base','3'=>'Autorretenedor y retiene sobre la base','4'=>'Autorretenedor y retiene sobre cualquier valor');
                            ?>
                        
                        <tr>
                            <td>Retención:</td>
                            <td><?php if($encabezado->retencion){echo $retenciones[$encabezado->retencion];}?></td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
                <td width="40%" style="text-align: right; vertical-align: top;">
                    <div align="right">
                        <table style="float:right;">
                            <tr>
                                <td colspan="2" style="font-weight:bold; font-size:1.2em; text-align:right; border-bottom:1px solid #777;">
                                    PEDIDO <?php echo $encabezado->nota_2;?>
                                </td>
                            </tr>
                            <tr>
                                <td>Número:</td>
                                <td style="text-align:right;"><b><?php echo $encabezado->id_maestro;?></b></td>
                            </tr>
                            <tr>
                                <td>Fecha:</td>
                                <td style="text-align:right;"><?php echo $encabezado->fecha;?></td></tr>
                            <tr>
                                <td>Hora:</td>
                                <td style="text-align:right;"><?php echo $encabezado->hora;?></td>
                            </tr>
                            <tr><td>
                            <?php if($encabezado->anulado){?>
        <div class="anulado" style="color:#900; font-size: 40px;">ANULADO</div>
        <?php }?></td></tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
            <table id="productos_pedido" class="tabla" style="width:100%; border-collapse:collapse;margin-top:10px;">
                <thead>
                <tr>
                    <th style="width:3%;border-bottom: 1px solid #000;padding: 4px 0px;">-</th>
                    <th style="width:12%;border-bottom: 1px solid #000;padding: 4px 0px;">CÓDIGO</th>
                    <th style="width:8%;border-bottom: 1px solid #000;padding: 4px 0px;">CANTIDAD</th>
                    <th style="width:42%;border-bottom: 1px solid #000;padding: 4px 0px;">DESCRIPCIÓN</th>
                    <th style="width:10%;border-bottom: 1px solid #000;padding: 4px 0px;">PRECIO</th>
                    <th style="width:5%;border-bottom: 1px solid #000;padding: 4px 0px;" title="Descuento">%DCTO</th>
                    <th style="width:5%;border-bottom: 1px solid #000;padding: 4px 0px;" title="Iva">%IVA</th>
                    <th style="width:15%;border-bottom: 1px solid #000;padding: 4px 0px;">VALOR NETO</th>
                </tr>
                </thead>
            <tbody>
            <?php $n=1;foreach($productos as $producto):?>
                <tr>
                   <td style="padding: 2px 0px;border-bottom: 1px solid #CCC;"><?php echo $n;?></td>
                   <td style="padding: 2px 0px;border-bottom: 1px solid #CCC;"><?php echo $producto->id_producto;?></td>
                   <td style="text-align:right; padding: 2px 30px 2px 0px;border-bottom: 1px solid #CCC;">
                   <?php 
                    if($configuracion['redondear_cantidad']){
                        echo number_format($producto->cantidad,0,'.',',');                        
                    }else{
                        echo number_format($producto->cantidad,2,'.',',');
                    }
                   ?>
                   </td>
                   <td style="padding: 2px 0px;border-bottom: 1px solid #CCC;"><?php echo $producto->nombre;?></td>
                   <td style="text-align:right;padding: 2px 0px;border-bottom: 1px solid #CCC;">
                       <?php 
                    if($configuracion['redondear_precio']){
                        echo number_format($producto->precio,0,'.',',');
                    }else{
                        echo number_format($producto->precio,2,'.',',');
                    }
                   ?>
                   </td>
                   <td style="text-align:right;padding: 2px 0px;border-bottom: 1px solid #CCC;">
                       <?php 
                        if($configuracion['redondear_descuento']){
                            echo number_format($producto->descuento,0,'.',',');
                        }else{
                            echo number_format($producto->descuento,2,'.',',');
                        }
                       ?>                        
                   </td>
                   <td style="text-align:right;padding: 2px 0px;border-bottom: 1px solid #CCC;"><?php echo $producto->iva.'%';?></td>
                   <td style="text-align:right;padding: 2px 0px;border-bottom: 1px solid #CCC;">
                       <?php 
                    if($configuracion['redondear_total']){
                        echo number_format($producto->total,0,'.',',');
                    }else{
                        echo number_format($producto->total,2,'.',',');
                    }
                   ?>                    
                   </td>
                </tr>
            <?php $n++; endforeach;?>
            </tbody>
            </table>
        <div style="clear:both;margin-bottom:10px;"></div>
        <table style="width:100%">
            <tr>
                <td style="width:70%; vertical-align: top;">
                    <?php if($configuracion['observaciones']){?>
                    <div style="padding-right:10px;"><div style="font-weight:bold;text-align:left;border-bottom:1px solid #777;"><span style="font-size:30px;">*</span>OBSERVACIONES</div><p style="font-size:18px;"><?php echo $encabezado->nota_1;?></p></div>
                    <?php }?>        
                </td>
                <td style="width:30%; vertical-align: top; text-align: right;">
                    <table class="subtotales" style="width:100%;">
                        <tr>
                              <td style="width:100px;" class="item">Subtotal:</td><td id="totales_subtotal" style="text-align:right;"><?php echo number_format($encabezado->subtotal_sin_dcto,0,'.',',');?></td>
                        </tr>
                        <tr class="altrow">
                            <td style="width:100px;" class="item">Descuento:</td><td id="totales_descuento" style="text-align:right;"><?php echo number_format($encabezado->descuento_total,0,'.',',');?></td>
                        </tr>
                        <tr>
                            <td style="width:100px;" class="item">Neto:</td><td id="totales_neto" style="text-align:right;"><?php echo number_format($encabezado->subtotal,0,'.',',');?></td>
                        </tr>
                        <tr class="altrow">
                            <td style="width:100px;" class="item">Iva:</td><td id="totales_iva" style="text-align:right;"><?php echo number_format($encabezado->iva_total,0,'.',',');?></td>
                        </tr>
                        <tr>
                            <td style="width:100px;" class="item"><b>Total =&gt;</b></td><td id="totales_total" style="text-align:right;"><b><?php echo number_format($encabezado->total,0,'.',',');?></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>        
    </div>
</body>
</html>