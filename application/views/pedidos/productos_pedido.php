<table id="productos_pedido" class="tabla">
    <thead>
    <tr>
       <th height="30px;" style="width:3%;"></th>
        <th style="width:14%;">Código</th>
        <th style="width:37%;">Nombre del producto</th>
        <th style="width:8%;">Cantidad</th>
        <th style="width:10%;">Precio</th>
        <th style="width:5%;" title="Descuento">Dcto</th>
        <th style="width:5%;" title="Iva">Iva</th>
        <th style="width:15%;">Total</th>
        <th style="width:3%;"></th>
    </tr>
    </thead>
<tbody>
<?php
$clase='';
$n=1;
foreach($productos as $producto):
	if($n % 2){
		$clase="";
	}else{
		$clase="altrow";
	}
?>
<tr id="fila_<?php echo $n;?>" class="<?php echo $clase;?>">
	<td id="numero_item_<?php echo $n;?>" class="item numero"><?php echo $n;?></td>
    <td><?php echo form_input(array('name'=>'codigo','size'=>'10','value'=>$producto->id_producto)); echo anchor('','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="buscar_producto" tabindex="-1"');?><div id="referencia_item_<?php echo $n;?>"class="referencia"><?php echo $producto->referencia;?></div></td>
    <td id="nombre_item_<?php echo $n;?>"><?php echo $producto->nombre;?></td>
    <td style="text-align:center;"><?php echo form_input(array('name'=>'cantidad','size'=>8,'value'=>number_format($producto->cantidad,'0')));?>
    <?php if($configuracion['productos_mostrar_unidad']){?><div class="ude" title="Unidad de empaque"><?php echo $producto->unidad;?></div><?php }?>    	
     </td>
    <?php 
		if($configuracion['modificar_precio']){
			$read_only='';
			$tabindex='';
		}else{
			$read_only='readOnly';
			$tabindex = "-1";
		}
	?>
    <td style="text-align:center;"><?php echo form_input(array('name'=>'precio','size'=>'12','style'=>"text-align:right;tabindex:$tabindex;",'value'=>number_format($producto->precio1,0),'readOnly'=>$read_only, 'tabindex'=>$tabindex));?>		
    <div class="listas" title="Listas de precios"><ul>
    <?php if($configuracion['productos_mostrar_lista1']){?><li>L1: <?php echo $producto->precio1;?></li><?php }?>
    <?php if($configuracion['productos_mostrar_lista2']){?><li>L2: <?php echo $producto->precio2;?></li><?php }?>
    <?php if($configuracion['productos_mostrar_lista3']){?><li>L3: <?php echo $producto->precio3;?></li><?php }?>
    <?php if($configuracion['productos_mostrar_lista4']){?><li>L4: <?php echo $producto->precio4;?></li><?php }?>
    </ul></div></td>
    <?php 
		if($configuracion['modificar_dcto']){
			$read_only='';
			$tabindex = '';
		}else{
			$read_only='readOnly';
			$tabindex = "-1";
		}
	?>
    <td style="text-align:center;"><?php echo form_input(array('name'=>'dcto','size'=>2,'value'=>number_format($producto->descuento,0),'readOnly'=>$read_only,'tabindex'=>$tabindex));?></td>
	<td style="text-align:center;" id="iva_item_<?php echo $n;?>"><?php echo number_format($producto->iva,0);?></td>
	<td class="total_producto" id="total_item_<?php echo $n;?>"><?php echo number_format($producto->total,0);?></td>
    <td><a href="" tabindex="-1" rel="eliminar_producto" title="Eliminar"><span class="iconos_rojo eliminar"></span></a></td>
</tr>
<?php
$n++;
endforeach;
?>
    <tr class="agregar"><td class="item numero">-</td><td colspan="8"><?php echo form_input(array('name'=>'items_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_items" class="boton"');?></td></tr>
</tbody>
<tfoot>            
   <tr><td id="total_productos_pedido" class="numero" colspan="9"><span class="alerta"></span> No ha agregado productos al pedido.</td></tr>
</tfoot>
</table>