<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/imprimir.css" type="text/css" media="screen" charset="utf-8" />
<title>Imprimir rango de pedidos</title>
<style media="screen">
    #contenido-pedido{
        background: #FFF;
        padding: 20px;
        border:1px solid #CCC;
        min-height: 20cm;
        margin:0 auto;
    }
    #contenido-pedido.salto{
        /*margin-top: 40px;*/
    }
    #contenido-pedido.pagina{
        margin-bottom: 1cm;
    }
</style>
<style type="text/css">
    @page { 
        margin:1cm;  
        widows:1;
    }
    #contenido-pedido,#contenido-pedido table{
            font-family:Arial, Helvetica, sans-serif;
            font-size:small;
    }
    #contenido-pedido span.sub_label{
            font-size:0.9em;
            color:#999;
            display:block;
    }
    #contenido-pedido .seccion{
            margin:10px 0px;
            padding-bottom:10px;
            border-bottom:1px dotted #CCC;
    }
    #contenido-pedido .seccion h2{
            font-size:160%;
            font-weight:normal;
            margin:0 0 0.2em;	
    }
    #contenido-pedido ul,#contenido-pedido p{
            margin:0;
            padding:0;
    }
    #contenido-pedido ul.formulario{
            list-style:none;
    }
    #contenido-pedido ul.formulario li{
            margin:0;
            padding:6px 1% 9px;	
       -moz-box-sizing:    border-box;
       -webkit-box-sizing: border-box;
        box-sizing:        border-box;
    }
    #contenido-pedido .right{
            clear:none;
            float:right;
    }
    #contenido-pedido .left{
            float:left;
    }
    #contenido-pedido table.tabla{
            border-collapse:collapse;
    }
    #contenido-pedido table.tabla thead th{
            border-bottom:1px solid #000;
            padding:1px 2px;
    }
    #contenido-pedido table.tabla tbody td{
            border-bottom:1px solid #CCC;
            padding:1px 2px;
    }
    #contenido-pedido table.tabla tbody td{
            vertical-align:text-top;
    }
    #contenido-pedido table.tabla thead th{
            padding-bottom:0px;
            height:20px;
    }
    #contenido-pedido table.tabla{
            font-size:0.85em;	
    }
    #contenido-pedido.salto{
        page-break-before: always;
        page-break-inside: avoid;
    }
    @media print{
        #contenido-pedido.salto{
            margin: 0px;
            padding: 0px;
            page-break-after: always;
        }
    }
</style>
<style media="print">
    @page { 
        margin:1cm;  
        widows:1;
    }
    #imprimir{
            display:none;
    }
    #contenido-pedido{
        position:relative;
        page-break-after: always;
    }
    #contenido-pedido.salto{
        position:static;
        margin: 0px;
        padding: 0px;
        page-break-after: always;
        page-break-inside: avoid;             
    }
    body,html{
        margin:0;
        padding: 0;
    }    
</style>
</head>
<body <?php if($pedidos){ echo 'onload="javascript:window.print();"';}?>>
    <div id="imprimir"><div><span class="logo"><?php echo anchor('',img(array('src'=>'img/logo_sipe_blanco.png','border'=>0)));?></span><?php $texto=img(array('src'=>'img/printer.png','border'=>0,'style'=>'vertical-align:middle;')).' Imprimir'; ?><a href="javascript:window.print();"><?php echo $texto;?></a></div><div class="clear"></div></div>
<?php 
$n=0;
if(!$pedidos){
    echo "No hay pedidos para imprimir";
}else{
foreach($pedidos as $pedido):?>    
<div id="contenido-pedido" class="pagina <?php if($n>0){ echo 'salto';}?>" style="width:700px;">
	<div style="font-size:0.8em; margin-bottom:10px; text-align:right;">NIT. <?php echo $empresa->nit.' - '.$empresa->nombre;?></div>
        <table width="100%">
            <tr>
                <td width="60%">
                    <table style="">
                            <tr><td colspan="2"><div style="font-weight:bold; font-size:1.2em; text-align:left; border-bottom:1px solid #777;">CLIENTE</div></td></tr>
                        <tr><td style="width:80px;">Nit:</td><td><?php echo $pedido['encabezado']->id_cliente;?></td></tr>
                        <tr><td>Nombre:</td><td><?php echo $pedido['encabezado']->nombre;?></td></tr>
                        <tr><td>Contacto:</td><td><?php echo $pedido['encabezado']->contacto;?></td></tr>
                        <tr><td>Dirección:</td><td><?php echo $pedido['encabezado']->direccion;?></td></tr>
                        <tr><td>Ciudad:</td><td><?php echo $pedido['encabezado']->ciudad;?></td></tr>
                        <tr><td>Teléfono:</td><td><?php echo $pedido['encabezado']->telefono1;?></td></tr>
                        <tr><td>Vendedor:</td><td><?php echo $pedido['encabezado']->id_vendedor.' - '.$pedido['encabezado']->nombre_vendedor;?></td></tr>
                        <tr><td>Plazo:</td><td><?php echo $pedido['encabezado']->plazo;?></td></tr>
                        <?php if($configuracion['listar_listas']){?>
                        <tr><td>Lista:</td><td><?php echo $pedido['encabezado']->lista;?></td></tr>
                        <? }?>
                        <?php if($configuracion['listar_retenciones']){
                            $retenciones=array('1'=>'No retiene','2'=>'Retiene sobre la base','3'=>'Autorretenedor y retiene sobre la base','4'=>'Autorretenedor y retiene sobre cualquier valor');
                            ?>
                        
                        <tr>
                            <td>Retención:</td>
                            <td><?php if($pedido['encabezado']->retencion){echo $retenciones[$pedido['encabezado']->retencion];}?></td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
                <td width="40%" style="text-align: right; vertical-align: top;">
                    <div align="right">                    
                    <table>
                            <tr><td colspan="2"><div style="font-weight:bold; font-size:1.2em; text-align:right; border-bottom:1px solid #777;">PEDIDO <?php echo $pedido['encabezado']->nota_2;?></div></td></tr>
                        <tr><td>Número:</td><td style="text-align:right;"><b><?php echo $pedido['encabezado']->id_maestro;?></b></td></tr>
                        <tr><td>Fecha:</td><td style="text-align:right;"><?php echo $pedido['encabezado']->fecha;?></td></tr>
                        <tr><td>Hora:</td><td style="text-align:right;"><?php echo $pedido['encabezado']->hora;?></td></tr>
                    </table>
                    </div>
                </td>
            </tr>
        </table>
    <div id="datos_productos" style="width:100%; margin-top:10px; float:left;">
        <table id="productos_pedido" class="tabla" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
               <th height="30px;" style="width:3%;">-</th>
                <th style="width:12%;">CÓDIGO</th>
                <th style="width:8%;">CANTIDAD</th>
                <th style="width:42%;">DESCRIPCIÓN</th>
                <th style="width:10%;">PRECIO</th>
                <th style="width:5%;" title="Descuento">%DCTO</th>
                <th style="width:5%;" title="Iva">%IVA</th>
                <th style="width:15%;">VALOR NETO</th>
            </tr>
            </thead>
        <tbody>
        <?php $n=1;foreach($pedido['productos'] as $producto):?>
            <tr>
               <td><?php echo $n;?></td>
               <td><?php echo $producto->id_producto;?></td>
               <td style="text-align:right; padding-right:30px;"><?php echo number_format($producto->cantidad,0,'.',',');?></td>
               <td><?php echo $producto->nombre;?></td>
               <td style="text-align:right;"><?php echo number_format($producto->precio,0,'.',',');?></td>
               <td style="text-align:right;"><?php echo number_format($producto->descuento,0,'.',',').'%';?></td>
               <td style="text-align:right;"><?php echo number_format($producto->iva,0).'%';?></td>
               <td style="text-align:right"><?php echo number_format($producto->total,0,'.',',');?></td>
            </tr>
        <?php $n++; endforeach;?>
        </tbody>
        </table>
    </div>
    <div style="clear:both;margin-bottom:10px;"></div>
    <div style="margin-top:10px;float:left; width:70%;">
		<?php if($configuracion['observaciones']){?>
        <div style="padding-right:10px;"><div style="font-weight:bold;text-align:left;border-bottom:1px solid #777;"><span style="font-size:30px;">*</span>OBSERVACIONES</div><p style="font-size:18px;"><?php echo $pedido['encabezado']->nota_1;?></p></div>
        <?php }?>        
    </div>
    <div style="float:left; width:30%">
        <table class="subtotales" style="float:right; width:100%">
            <tr>
                  <td style="width:100px;" class="item">Subtotal:</td><td id="totales_subtotal" style="text-align:right;"><?php echo number_format($pedido['encabezado']->subtotal_sin_dcto,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Descuento:</td><td id="totales_descuento" style="text-align:right;"><?php echo number_format($pedido['encabezado']->descuento_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item">Neto:</td><td id="totales_neto" style="text-align:right;"><?php echo number_format($pedido['encabezado']->subtotal,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Iva:</td><td id="totales_iva" style="text-align:right;"><?php echo number_format($pedido['encabezado']->iva_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item"><b>Total =&gt;</b></td><td id="totales_total" style="text-align:right;"><b><?php echo number_format($pedido['encabezado']->total,0,'.',',');?></b></td>
            </tr>
        </table>
    </div>
    <div style="clear:both;"></div>
</div>
<?php $n++; endforeach;}?>
</body>
</html>