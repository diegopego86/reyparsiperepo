<div class="grid16" style="margin-top:20px;">
    <div class="seccion"><h2>Datos del cliente</h2></div>
    <div>
        <?php
        if (isset($cliente->id_cliente)) {
            echo form_input(array('name' => 'nit', 'size' => '15', 'value' => $cliente->id_cliente));
        } else {
            echo form_input(array('name' => 'nit', 'size' => '15'));
        }
        echo ' ' . anchor('', 'Buscar  <span class="iconos_blancos buscar"></span>', 'class="boton" rel="boton_buscar_cliente"');
        ?><span style="margin-top:5px;" class="sub_label">Nit</span></div>
    <div id="datos_cliente" style="margin-top:10px;">
        <ul class="formulario">
            <li class="left cuadro noderecha" style="height:50px;width:30%;"><p id="nombre_cliente">
                    <?php
                    if (isset($cliente->nombre)) {
                        echo $cliente->nombre;
                    } else {
                        echo '-';
                    }
                    ?></p><span class="sub_label">Nombre del cliente</span></li>
                <li class="left cuadro" style="height:50px;width:30%;">
                <?php
                    if (isset($cliente->contacto)) {
                        echo $cliente->contacto;
                    } else {
                        echo '-';
                    } ?><span class="sub_label">Contacto</span></li>
                <li class="left cuadro noizquierda" style="height:50px;width:40%;">
                <?php
                    if (isset($cliente->email)) {
                        echo $cliente->email;
                    } else {
                        echo '-';
                    } ?><span class="sub_label">E-mail</span></li>
                <div class="clear"></div>
                <li class="left cuadro noderecha noarriba" style="height:50px;width:40%;">
                <?php
                    if (isset($cliente->direccion)) {
                        echo $cliente->direccion;
                    } else {
                        echo '-';
                    } ?><span class="sub_label">Dirección</span></li>
                <li class="left cuadro noarriba" style="height:50px;width:30%;">
<?php
                    if (isset($cliente->ciudad)) {
                        echo $cliente->ciudad;
                    } else {
                        echo '-';
                    } ?><span class="sub_label">Ciudad</span></li>
                <li class="left cuadro noizquierda noarriba" style="height:50px;width:30%;">
<?php
                    if (isset($cliente->telefono1)) {
                        echo $cliente->telefono1;
                    } else {
                        echo '-';
                    } ?><span class="sub_label">Teléfono</span></li>
                <div class="clear"></div>
                <li class="left cuadro noderecha noarriba" style="height:50px;width:45%;">
                <?php
                    $adicional = '';
                    if (!$configuracion['cambiar_asesor']) {
                        $adicional = 'disabled="disabled"';
                    }
                    if (isset($cliente->id_vendedor)) {
                        echo form_dropdown('asesor', $vendedores, $cliente->id_vendedor, $adicional);
                        if (!in_array($cliente->id_vendedor, $this->session->userdata('numeros_vendedor'))) {
                            echo ' <div style="cursor:pointer;display:inline;color:#930;" title="Usted no aparece como el asesor comercial de este cliente. Por favor elija el número correcto y comuníquese con cartera para su corrección."><span class="alerta"></span>Alerta</div>';
                        }
                    } else {
                        echo '-';
                    }
                ?><span class="sub_label">Asesor</span></li>
            <?php if ($configuracion['listar_plazos']) {
            ?>
                        <li class="left cuadro noarriba" style="height:50px;width:10%;"><?php echo form_dropdown('plazo', array('0' => '0', '15' => '15', '30' => '30', '45' => '45', '60' => '60', '90' => '90'), $cliente->plazo); ?><span class="sub_label">Plazo</span></li>
                <?php } ?>
                <?php if ($configuracion['listar_descuentos']) {
                ?>
                    <li class="left cuadro noarriba" style="height:50px;width:10%;"><?php echo form_dropdown('descuento_pedido', array('0' => '0', '10' => '10', '15' => '15')); ?><span class="sub_label">Descuento</span></li>
                <?php } ?>
                <?php if ($configuracion['listar_listas']) {
 ?>
                    <li class="left cuadro noizquierda noarriba" style="height:50px;width:10%;"><?php
                        $precios = array('1024' => 1, '1025' => 2, '1026' => 3);
                        $precio = 1;
                        if (isset($precios[$cliente->precio_lista])) {
                            $precio = $precios[$cliente->precio_lista];
                        } echo form_dropdown('lista', array('1' => '1', '2' => '2', '3' => '3'), $precio); ?><span class="sub_label">Lista</span></li>
<?php } ?>
<?php if ($configuracion['listar_retenciones']) { ?>
                            <li class="left cuadro noizquierda noarriba" style="height:50px;width:35%;"><?php
                        echo form_dropdown('retencion', array('' => '', '1' => 'No retiene', '2' => 'Retiene sobre la base', '3' => 'Retiene sobre cualquier valor', '3' => 'Autorretenedor y retiene sobre la base', '4' => 'Autorretenedor y retiene sobre cualquier valor'));
?><span class="sub_label">Retención</span></li>
<?php } ?>
                        <div class="clear"></div>
                    </ul>
                    <?php if ($cartera) {
 ?>
            <div class="seccion"><h2>Cartera del cliente</h2><?php echo anchor('', 'Mostrar cartera', 'class="boton" rel="mostrar_ocultar"'); ?></div>
            <div id="cartera_cliente" style="display:none;">
                <table class="tabla">
                    <thead>
                        <tr><th style="height:30px;" title="Tipo de documento F: Factura NC:Nota crédito ND:Nota débito R:Recibo de caja">Tipo</th><th>Documento</th><th>Fecha</th><th>Vence</th><th title="Dias de vencido">Dias</th><th title="Número del vendedor">Vd</th><th>Valor Inicial</th><th>Saldo Anterior</th><th>Débitos Mes</th><th>Créditos Mes</th><th>Saldo Actual</th></tr>
                    </thead>
                    <tbody>
<?php
                        foreach ($cartera as $dato):
                            $clase = '';
                            if ($dato->dias > 0) {
                                $clase = 'vencido';
                            }
?>
                            <tr class="<?php echo $clase; ?>">
                                <td><?php echo $dato->tipo; ?></td>
                                <td><?php echo $dato->id_documento; ?></td>
                                <td style="text-align:center;"><?php echo $dato->fecha; ?></td>
                                <td style="text-align:center;"><?php echo $dato->fecha_vence; ?></td>
                                <td title="Dias de vencido" style="text-align:center;"><?php echo $dato->dias; ?></td>
                                <td style="text-align:center;"><?php echo $dato->id_vendedor; ?></td>
                                            <td style="text-align:right;"><?php echo number_format($dato->valor_inicial, 0, '.', ','); ?></td>
                                            <td style="text-align:right;"><?php echo number_format($dato->saldo_ant, 0, '.', ','); ?></td>
                                            <td style="text-align:right;"><?php echo number_format($dato->cargo, 0, '.', ','); ?></td>
                                            <td style="text-align:right;"><?php echo number_format($dato->abono, 0, '.', ','); ?></td>
                                            <td style="text-align:right;"><?php echo number_format($dato->saldo, 0, '.', ','); ?></td></tr>
<?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
<?php } ?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="grid16" id="datos_productos">
                    <div class="seccion"><h2>Productos</h2></div>
                    <div id="datosProducto">
                        <table id="productos_pedido" class="tabla">
                            <thead>
                                <tr>
                                    <th height="30px;" style="width:3%;"></th>
                                    <th style="width:14%;">Código</th>
                                    <th style="width:37%;">Nombre del producto</th>
                                    <th style="width:8%;">Cantidad</th>
                                    <th style="width:10%;">Precio</th>
                                    <th style="width:5%;" title="Descuento">Dcto</th>
                                    <th style="width:5%;" title="Iva">Iva</th>
                                    <th style="width:15%;">Total</th>
                                    <th style="width:3%;"></th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                        $clase = '';
                        $n = 1;
                        foreach ($productos as $producto):
                            if ($n % 2) {
                                $clase = "";
                            } else {
                                $clase = "altrow";
                            }
?>
                        <tr id="fila_<?php echo $n; ?>" class="<?php echo $clase; ?>">
                            <td id="numero_item_<?php echo $n; ?>" class="item numero"><?php echo $n; ?></td>
                            <td><?php echo form_input(array('name' => 'codigo', 'size' => '10', 'value' => $producto['codigo']));
                            echo anchor('', '<span class="iconos_azules buscar"></span>', 'class="boton_gris" rel="buscar_producto" tabindex="-1"'); ?><div id="referencia_item_<?php echo $n; ?>" class="referencia"><?php echo $producto['referencia']; ?></div></td>
                            <td id="nombre_item_<?php echo $n; ?>"><?php echo $producto['nombre']; ?></td>
                            <td style="text-align:center;"><?php echo form_input(array('name' => 'cantidad', 'size' => 8, 'value' => number_format($producto['cantidad'], 0, '.', ','))); ?>
                    <?php if ($configuracion['productos_mostrar_unidad']) {
                    ?><div class="ude" title="Unidad de empaque"><?php echo $producto->unidad; ?></div><?php } ?>
                            </td>
<?php
                            if ($configuracion['modificar_precio']) {
                            	$read_only = '';
                                $tabindex = '';
                            } else {
                                $read_only = 'readOnly';
                                $tabindex = "-1";
                            }
?>
                    <td style="text-align:center;"><?php echo form_input(array('name' => 'precio', 'size' => '12', 'style' => 'text-align:right;', 'value' => $producto['precio'], 'readOnly' => $read_only, 'tabindex' => $tabindex)); ?>
                                <div class="listas" title="Listas de precios">
                                    <ul>
                    <?php if ($configuracion['productos_mostrar_lista1']) {
 ?><li>L1: <?php echo $producto->precio1; ?></li><?php } ?>
                    <?php if ($configuracion['productos_mostrar_lista2']) {
                    ?><li>L2: <?php echo $producto->precio2; ?></li><?php } ?>
                    <?php if ($configuracion['productos_mostrar_lista3']) {
                    ?><li>L3: <?php echo $producto->precio3; ?></li><?php } ?>
<?php if ($configuracion['productos_mostrar_lista4']) { ?><li>L4: <?php echo $producto->precio4; ?></li><?php } ?>
                                    </ul></div></td>
<?php
                            if ($configuracion['modificar_dcto']) {
                            	$read_only = '';
                                $tabindex = '';
                            } else {
                                $read_only = 'readOnly';
                                $tabindex = "-1";
                            }
?>
                        <td style="text-align:center;"><?php echo form_input(array('name' => 'dcto', 'size' => 2, 'value' => '0', 'readOnly' => $read_only, 'tabindex' => $tabindex)); ?></td>
                        <td style="text-align:center;" id="iva_item_<?php echo $n; ?>"><?php echo number_format($producto['iva'],0,'.',','); ?></td>
                        <td class="total_producto" id="total_item_<?php echo $n; ?>"><?php echo '0'; ?></td>
                        <td><a href="" tabindex="-1" rel="eliminar_producto" title="Eliminar"><span class="iconos_rojo eliminar"></span></a></td>
                    </tr>
<?php
                            $n++;
                        endforeach;
?>
                <tr class="agregar"><td class="item numero">-</td><td colspan="8"><?php echo form_input(array('name' => 'items_agregar', 'size' => '2', 'maxlength' => '2', 'value' => '1'));
                        echo anchor('', 'Agregar <span class="iconos_blancos mas"></span>', 'rel="boton_agregar_items" class="boton"'); ?></td></tr>
                                </tbody>
                                <tfoot>
                                    <tr><td id="total_productos_pedido" class="numero" colspan="9"><span class="alerta"></span> No ha agregado productos al pedido.</td></tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="clear" style="margin-bottom:10px;"></div>
                    <div class="grid10">
        <?php if ($configuracion['observaciones']) {
 ?>
                            <div style="padding:10px; background:#F4F4F4;"><?php
                            echo form_label('Observaciones: ', 'observaciones', array('style' => 'width:200px; float:left;'));
                            echo '<div class="faltantes">150</div>';
                            echo '<div style="clear:both;"></div><div><ul id="observaciones_predeterminadas"><li><a href="" rel="agregar_observacion">PRECIOS NETOS POR PAGO DE CONTADO (15% INCLUIDO)</a></li><li><a href="" rel="agregar_observacion">PRECIOS NETOS POR PAGO A 30 DIAS (10% INCLUIDO)</a></li><li><a href="" rel="agregar_observacion">POR PAGO DE CONTADO 15% DSTO SOBRE EL SUBTOTAL (15% NO INCLUIDO)</a></li><li><a href="" rel="agregar_observacion">POR PAGO A 30 DIAS 10% DSTO SOBRE EL SUBTOTAL (10% NO INCLUIDO)</a></li></ul></div>';
                            echo form_textarea(array('name' => 'observaciones', 'style' => 'width:99%;', 'rows' => 4, 'maxlength' => 300, 'value' => $observaciones));
        ?></div>
<?php } ?>        
</div>
<div class="grid6">
    <table class="tabla">
        <tr>
            <td style="width:100px;" class="item">Subtotal</td><td id="totales_subtotal" style="text-align:right;"></td>
        </tr>
        <tr class="altrow">
            <td style="width:100px;" class="item">Descuento</td><td id="totales_descuento" style="text-align:right;"></td>
        </tr>
        <tr>
            <td style="width:100px;" class="item">Neto</td><td id="totales_neto" style="text-align:right;"></td>
        </tr>
        <tr class="altrow">
            <td style="width:100px;" class="item">Iva</td><td id="totales_iva" style="text-align:right;"></td>
        </tr>
        <tr>
            <td style="width:100px;" class="item">Total</td><td id="totales_total" style="text-align:right;"></td>
        </tr>
    </table>
</div>
<div class="clear"></div>
<script type="text/javascript">
    $(document).ready(function(){
        $('a[rel=boton_cambiar_cliente]').click(function(){
            var tipo=$(this).html();
            if(tipo=="Cambiar cliente"){
                $(this).html('Cancelar cambio');
                $(this).addClass('boton_cerrar');
                $('#buscarCliente').html('');
                $('#buscador_cliente').show();
            }
            else{
                $(this).html('Cambiar cliente');
                $(this).removeClass('boton_cerrar');
                $('#buscarCliente').html('');
                $('#buscador_cliente').hide();
            }
            return false;
        });
        $('a[rel=mostrar_ocultar]').click(function(){
            var valor=$(this).text();
            if(valor=='Ocultar cartera'){
                $(this).text('Mostrar cartera');
                $('#cartera_cliente').slideUp('fast');
            }else{
                $(this).text('Ocultar cartera');
                $('#cartera_cliente').slideDown('fast');
            }
            return false;
        });
    });
</script>