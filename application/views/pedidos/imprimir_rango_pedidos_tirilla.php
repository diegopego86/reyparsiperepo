<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo base_url();?>css/imprimir.css" type="text/css" media="screen" charset="utf-8" />
<title>Imprimir rango de pedidos</title>
<style type="text/css" media="screen">
body{
	font-family:Arial;
	font-size:9px;
	margin:0;
	padding:0;
	background:#666;
}
h1{
	font-size:9px;
	text-align:center;
}
#recibo
{
	border:1px solid #333;
	margin:12px;
	background:#FFF;
	width:4.8cm;
}
</style>
</head>
<body <?php if($pedidos){ echo 'onload="javascript:window.print();"';}?>>
    <div id="imprimir" style="font-size:0.8em; margin:0 auto; width:700px;margin-bottom: 20px; text-align:right;"><a href="javascript:window.print();" class="boton">imprimir</a></div>
<?php 
$n=0;
if(!$pedidos){
    echo "No hay pedidos para imprimir";
}else{
foreach($pedidos as $pedido):?> 

    <div id="recibo">
<h1 style="text-align:center;"><?php echo $empresa->nombre;?><br/>NIT. <?php echo $empresa->nit;?></h1>
        <table style="float:right;">
            <tr><td style="text-align:right;"><b>Pedido # <?php echo $pedido['encabezado']->id_maestro;?></b></td></tr>
            <tr><td style="text-align:right;"><?php echo $pedido['encabezado']->fecha.' '.$pedido['encabezado']->hora;?></td></tr>
        	<tr><td style="text-align:right;"><?php if($pedido['encabezado']->nota_2!='1 de 1'){echo $pedido['encabezado']->nota_2;}?></td></tr>
        </table>
        <div style="clear:both;"></div>
        <table style="">
            <tr><td><b>Cliente: </b><?php echo $pedido['encabezado']->id_cliente.' '.$pedido['encabezado']->nombre;?></td></tr>
            <tr><td><?php if($pedido['encabezado']->contacto){echo '<b>Contacto: </b>'.$pedido['encabezado']->contacto;}?></td></tr>
            <tr><td><b>Asesor: </b><?php echo $pedido['encabezado']->id_vendedor.' - '.$pedido['encabezado']->nombre_vendedor;?></td></tr>
        </table>
        <table id="productos_pedido" width="100%">
            <thead>
            <tr>
                <th style="width:70%"><span style="text-decoration:underline;">Código</span> -&gt; Descripción</th>
                <th style="width:15%;">Cant.</th>
                <th style="width:15%;">Precio</th>
            </tr>
            </thead>
        <tbody>
        <?php $n=1;foreach($pedido['productos'] as $producto):
				/*$texto=$producto->id_producto.' -> '.$producto->nombre;
				$cantidad=ceil(strlen($texto)/20);
				for($n=0;$n<$cantidad;$n++){
					if($n==0){
						$palabra=substr($producto->nombre,0,20-strlen($producto->id_producto.' -> '));
					}else{
						$palabra=$palabra.'<br/>'.substr($texto,$n*20,20);
					}
				}*/
		?>
            <tr style="vertical-align:text-top;">
               <td><?php echo '<span style="text-decoration:underline;">'.$producto->id_producto.'</span> -> '.$producto->nombre;?></td><td style="text-align:right;"><?php echo number_format($producto->cantidad,0,'.',',');?></td><td style="text-align:right;"><?php echo '$'.number_format($producto->precio,0,'.',',');?></td>
            </tr>
        <?php $n++; endforeach;?>
        </tbody>
        </table>
        <?php if($pedido['encabezado']->nota_1){?>
        <table style="width:100%;">
            <tr>
                <td><b>Observaciones:</b></td>                
            </tr>
            <tr>
                <td><?php echo $pedido['encabezado']->nota_1;?></td>                
            </tr>
        </table>
        <?php }?>
        <table class="subtotales" style="width:100%; border-top:1px dashed #000;">
            <tr>
                  <td style="width:100px;" class="item">Subtotal:</td><td id="totales_subtotal" style="text-align:right;">$ <?php echo number_format($pedido['encabezado']->subtotal_sin_dcto,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Descuento:</td><td id="totales_descuento" style="text-align:right;">$ <?php echo number_format($pedido['encabezado']->descuento_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item">Neto:</td><td id="totales_neto" style="text-align:right;">$ <?php echo number_format($pedido['encabezado']->subtotal,0,'.',',');?></td>
            </tr>
            <tr class="altrow">
                <td style="width:100px;" class="item">Iva:</td><td id="totales_iva" style="text-align:right;">$ <?php echo number_format($pedido['encabezado']->iva_total,0,'.',',');?></td>
            </tr>
            <tr>
                <td style="width:100px;" class="item"><b>Total =&gt;</b></td><td id="totales_total" style="text-align:right;"><b>$ <?php echo number_format($pedido['encabezado']->total,0,'.',',');?></b></td>
            </tr>
        </table>        
</div>
<?php endforeach;}?>    
</body>
</html>