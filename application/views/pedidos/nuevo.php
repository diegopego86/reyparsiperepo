<style>
    .ui-datepicker,.ui-dialog {
            font-size: 90%;
    }

    .alerta .ui-helper-clearfix button.ui-state-default {
            border: 1px solid #900;
            color: #FADDDE;
            background: #980C10;
            background: -webkit-gradient(linear, left top, left bottom, from(#ED1C24),to(#AA1317) );
            background: -moz-linear-gradient(center top, #ED1C24, #AA1317) repeat scroll 0 0 transparent;
            filter: progid:DXImageTransform.Microsoft.gradient (startColorstr = '#ED1C24', endColorstr ='#AA1317' );
    }

    .alerta .ui-helper-clearfix button.ui-state-default:hover {
            background: #AA1317;
            background: -webkit-gradient(linear, left top, left bottom, from(#AA1317),to(#ED1C24) );
            background: -moz-linear-gradient(center top, #AA1317, #ED1C24) repeat scroll 0 0 transparent;
            filter: progid:DXImageTransform.Microsoft.gradient ( startColorstr = '#AA1317', endColorstr = '#ED1C24');
            color: #DE898C;
    }
    .dialog_confirm button.ui-state-default{
            border:1px solid #900;
            color:#FADDDE;	
            background: #980C10;
            background: -webkit-gradient(linear, left top, left bottom, from(#ED1C24), to(#AA1317));
            background:-moz-linear-gradient(center top , #ED1C24, #AA1317) repeat scroll 0 0 transparent;
            filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ED1C24', endColorstr='#AA1317');	
    }
    .dialog_confirm button.ui-state-default:hover{
            background: #AA1317;
            background: -webkit-gradient(linear, left top, left bottom, from(#AA1317), to(#ED1C24));
            background:-moz-linear-gradient(center top , #AA1317, #ED1C24) repeat scroll 0 0 transparent;
            filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#AA1317', endColorstr='#ED1C24');	
            color:#DE898C;
    }
</style>
<div id="pedido_contenido" class="form">
    <div class="grid16">
        <h1>Nuevo Pedido</h1>
    </div>
    <div class="grid8">
        <div><?php echo anchor('','Importar datos desde archivo de excel','class="boton" rel="importar_excel" onclick="return false;"');?></div>
        <div style="margin-top:10px;"><?php echo anchor('','Copiar productos desde otro pedido','class="boton" rel="boton_copiar_desde_pedido" onclick="return false;"');?></div>
        <div style="margin-top:10px;"><?php echo anchor('','Cargar cotización','class="boton" rel="boton_cargar_cotizacion" onclick="return false;"');?></div>
    </div>
    <div class="grid8" style="text-align:right;">
        <div>
            <?php echo anchor('pedidos/guardar_borrador','Guardar Borrador','class="boton" style="margin-right:2px;" rel="guardar_borrador" onclick="return false;"');?>
            <?php echo anchor('pedidos/guardar','Guardar definitivo y enviar','class="boton naranja" style="" rel="guardar_pedido" onclick="return false;"');?>        
            <?php echo anchor('pedidos/guardar_test','Guardar test','class="boton naranja" rel="guardar_pedido_test" style="display:none;"');?>        
        </div>
        <div style="text-align: right;margin-top:10px;">
            <ul style="float: right">
                    <li style="text-align: left;"><?php echo form_label('(C.C.) Enviar copias del pedido a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','size'=>60));?></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="grid16"><?php echo form_hidden('cambios',set_value('cambios',0));?>
        <div id="autoguardado" style="text-align: right;"></div>
    </div>
    <div class="clear"></div>
    <div class="clear"></div>
    <div id="pedido">
        <div class="grid16" style="margin-top: 20px;">
            <div class="seccion">
                <h2>Datos del cliente</h2>
            </div>
            <div>
                <?php echo form_input(array('name'=>'nit','size'=>'15')); echo ' '.anchor('','Buscar  <span class="iconos_blancos buscar"></span>','class="boton" rel="boton_buscar_cliente"');?>
                <span style="margin-top: 5px;" class="sub_label">Nit</span>
            </div>
            <div id="datos_cliente" style="margin-top: 10px;">
                <ul class="formulario">
                    <li class="left cuadro noderecha" style="width: 30%;">
                    <p id="nombre_cliente">-</p>
                    <span class="sub_label">Nombre del cliente</span></li>
                    <li class="left cuadro" style="width: 30%;">-<span class="sub_label">Contacto</span></li>
                    <li class="left cuadro noizquierda" style="width: 40%;">-<span
                            class="sub_label">E-mail</span></li>
                    <div class="clear"></div>
                    <li class="left cuadro noderecha noarriba" style="width: 40%;">-<span
                            class="sub_label">Dirección</span></li>
                    <li class="left cuadro noarriba" style="width: 30%;">-<span
                            class="sub_label">Ciudad</span></li>
                    <li class="left cuadro noizquierda noarriba" style="width: 30%;">-<span
                            class="sub_label">Teléfono</span></li>
                    <div class="clear"></div>
                    <li class="left cuadro noderecha noarriba" style="width: 45%;">-<span
                            class="sub_label">Asesor</span></li>
                            <?php if($configuracion['listar_plazos']){?>
                    <li class="left cuadro noarriba" style="width: 10%;">-<span
                            class="sub_label">Plazo</span></li>
                            <?php }?>
                            <?php if($configuracion['listar_descuentos']){?>
                    <li class="left cuadro noarriba" style="width: 10%;">-<span
                            class="sub_label">Descuento</span></li>
                            <?php }?>
                            <?php if($configuracion['listar_listas']){?>
                    <li class="left cuadro noizquierda noarriba" style="width: 10%;">-<span
                            class="sub_label">Lista</span></li>
                            <?php }?>
                            <?php if($configuracion['listar_retenciones']){?>
                    <li class="left cuadro noizquierda noarriba" style="width: 35%;">-<span
                            class="sub_label">Retención</span></li>
                        <?php }?>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <div class="grid16" id="datos_productos">
            <div class="seccion">
                <h2>Productos</h2>
                <div id="eliminar_rango_productos" style="display:none;">
                            <?php echo anchor('','Eliminar seleccionados','rel="eliminar_productos_seleccionados" class="boton boton_rojo" onclick="return false;"');?>
                </div>
            </div>
            <div id="datosProducto">
                <table id="productos_pedido" class="tabla">
                        <thead>
                                <tr>
                                        <th height="30px;" style="width: 3%;"><input type="checkbox" id="seleccionar_todos" value="0"/></th>
                                        <th style="width: 138px;">Código</th>
                                        <th style="">Nombre del producto</th>
                                        <th style="width:95px;">Cantidad</th>
                                        <th style="width:115px">Precio</th>
                                        <th style="width:50px;" title="Descuento">Dcto</th>
                                        <th style="width:40px;" title="Iva">Iva</th>
                                        <th style="width:70px;">Total</th>
                                        <th style="width:30px;"></th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr class="agregar">
                                        <td class="item numero">-</td>
                                        <td colspan="8"><?php echo form_input(array('name'=>'items_agregar','size'=>'2','maxlength'=>'2','value'=>'1')); echo anchor('','Agregar <span class="iconos_blancos mas"></span>','rel="boton_agregar_items" class="boton" onclick="return false;"');?></td>
                                </tr>
                        </tbody>
                        <tfoot>
                                <tr>
                                        <td id="total_productos_pedido" class="numero" colspan="9"><span
                                                class="alerta"></span> No ha agregado productos al pedido.</td>
                                </tr>
                        </tfoot>
                </table>
            </div>
        </div>
        <div class="clear"></div>
        <div class="totales_pedido" style="margin-top:10px;">
            <div class="grid10"><?php if($configuracion['observaciones']){?>
            <div style="padding: 10px; background: #F4F4F4;"><?php echo form_label('Observaciones: ','observaciones',array('style'=>'width:200px; float:left;')); echo '<div class="faltantes">300</div>'; 
            echo '<div style="clear:both;"></div><div><ul id="observaciones_predeterminadas">';
            foreach($observaciones_predeterminadas as $observacion):
                echo '<li><a href="" rel="agregar_observacion">'.$observacion->detalle.'</a></li>';
            endforeach;
            echo '</ul></div>';
            echo form_textarea(array('name'=>'observaciones','style'=>'width:99%;','rows'=>4,'maxlength'=>300));?></div>
            <?php }?> </div>
            <div class="grid6">
            <table class="tabla">
                    <tr>
                            <td style="width: 100px;" class="item">Subtotal</td>
                            <td id="totales_subtotal" style="text-align: right;"></td>
                    </tr>
                    <tr class="altrow">
                            <td style="width: 100px;" class="item">Descuento</td>
                            <td id="totales_descuento" style="text-align: right;"></td>
                    </tr>
                    <tr>
                            <td style="width: 100px;" class="item">Neto</td>
                            <td id="totales_neto" style="text-align: right;"></td>
                    </tr>
                    <tr class="altrow">
                            <td style="width: 100px;" class="item">Iva</td>
                            <td id="totales_iva" style="text-align: right;"></td>
                    </tr>
                    <tr>
                            <td style="width: 100px;" class="item">Total</td>
                            <td id="totales_total" style="text-align: right;"></td>
                    </tr>
            </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?php if(!isset($numero_borrador)){ $numero_borrador=null;} echo form_hidden('borrador',set_value('borrador',$numero_borrador));?>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.highlight-3.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.numberformatter-1.1.2.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery.calculator.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    /*Declaracion de variables*/
    var tiempo_autoguardar;	
    var dcto_pedido;
    var plazo_pedido;
    var direccion_envio;
    var autoguardando=false;
    var salir='';
    var error_cargando_borrador=false;
    
    /*Cuadros de dialogo*/
    $('body').append('<div id="buscarCliente" title="Buscar cliente"></div>');
    $('#contenido').append('<div id="buscarProducto" title="Buscar producto"></div>');
    $('body').append('<div id="buscarPedido" title="Buscar pedido a copiar"></div>');
            
    $("#buscarCliente").dialog({
        autoOpen:false,
        resizable: false,
        width: 978,
        height:540,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                                        
        }                    
    });//Termina dialog.
    
    $("#buscarProducto").dialog({
        autoOpen:false,
        resizable: true,
        width: 978,
        height:600,
        modal: true,
        close: function(event,ui){
            $('body').css('overflow','auto');            
        },
        buttons: {
            'Cerrar': function() {
                $('body').css('overflow','auto');
                $('#buscarProducto').dialog('close');
            }
        }
    });//Termina dialog.
    
    $("#buscarPedido").dialog({
        autoOpen:false,
        resizable: false,
        width: 800,
        height:480,
        modal: true,
        close: function(event,ui){
            $('body').css('overflow','auto');						
        },
        buttons: {
            'Cerrar': function() {
                $('body').css('overflow','auto');
                $(this).dialog('close');
            }
        }
    });
    
    $('a[rel=guardar_borrador]').click(function(){		   
        $('#autoguardado').removeClass('correcto error');
        $('#autoguardado').addClass('cargando');
        $('#autoguardado').html('<span class="cargando"></span> Guardando borrador del pedido');
        var encabezado=[];
        var nit=$('input[name=nit]').val();
        var nombre_cliente=$('p[id=nombre_cliente]').text();
        var asesor=$('select[name=asesor]').val();
        var plazo=$('select[name=plazo]').val();
        var email_copias=$('input[name=email_copias]').val();
        var lista=$('select[name=lista]').val();
        if(!lista){
            lista=$('input[name=lista_cliente]').val();
        }
        var retencion=$('select[name=retencion]').val();
        var descuento_pedido=$('select[name=descuento_pedido]').val();
        var direccion_envio=$('input[name=direccion_envio]').val();
        var observaciones=$('textarea[name=observaciones]').val();
        var codigos=[];
        var unidades=[];
        $("input[name=codigo]").each(function(){
            codigos.push($(this));
        });
        var productos_agregados=parseInt($('#productos_pedido >tbody >tr').length)-1;
        var nombres_productos=[];
        $("div.nombre_producto").each(function(){
            if($(this).find('div.agotado')){
                $(this).find('div.agotado').text()
            }else{
                nombres_productos.push($(this).text());	
            }
        });
        $("div.ude").each(function(){
            unidades.push($(this).text());	 
        });
        var cantidades=[];
        $("input[name=cantidad]").each(function(){
            cantidades.push($(this).val());
        });
        var referencias=[];
        $("div[id^=referencia_item_]").each(function(){
            referencias.push($(this).text());
        });
        var precios=[];
        $("input[name=precio]").each(function(){
            precios.push($(this).val());
        });
        var descuentos=$("input[name=dcto]").parseNumber();
        var ivas=$("[id^=iva_item_]").parseNumber();
        var productos=[];
        for(var n=0;n<cantidades.length;n++){
            var producto={
                codigo: codigos[n].val(),
                nombre: nombres_productos[n],
                referencia: referencias[n],
                unidad: unidades[n],
                cantidad: cantidades[n],
                precio: precios[n],
                descuento: descuentos[n],
                iva: ivas[n]
            };
            productos.push(producto);
        }
        encabezado={
            nit:nit,
            nombre:nombre_cliente,
            asesor:asesor,
            plazo: plazo,
            lista:lista,
            retencion:retencion,
            direccion_envio:direccion_envio,
            descuento:descuento_pedido,
            email_copias:email_copias,
            observaciones:observaciones
        };
        var url='<?php echo site_url('pedidos/autoguardar');?>';
        var form_data={
            numero_borrador:$('input[name=borrador]').val(),
            encabezado: encabezado,
            productos: productos,
            ajax:1
        };
        $.ajax({
           type: "POST",
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               $('#autoguardado').removeClass('cargando');
               $('#autoguardado').addClass('correcto');
               window.location.hash='borrador='+msg['id'];

               $('input[name=borrador]').val(msg['id']);
               if(autoguardando){
                   mostrar_mensaje('Se autoguardó el pedido con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                   $('#autoguardado').html('Borrador #'+msg['id']+' autoguardado. '+msg['fecha']);
               }else{
                   mostrar_mensaje('Se guardó el pedido con el borrador #'+msg['id']+' de '+msg['fecha'],'correcto',2000);
                   $('#autoguardado').html('Borrador #'+msg['id']+' guardado. '+msg['fecha']);
               }
               $('input[name=cambios]').val(0);
               autoguardando=false;
               if(salir!=''){
                   location.href=salir;
               }
           },
           error: function(x,e){
               autoguardando=false;
               salir='';
               $('#autoguardado').removeClass('cargando');
               $('#autoguardado').addClass('error');
               mostrar_error(x,e);
           }
        });
        return false;
    });//Termina guardar borrador
    autoguardar();
    function autoguardar(){		   
        if(!error_cargando_borrador){
            tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
            if($('input[name=cambios]').val()==1){
               autoguardando=true;
               $('a[rel=guardar_borrador]').click();
            }//Termina If.
        }
    }//Termina autoguardar
    
    $('input[name=nit]').live('change',function(){ 
        mostrar_mensaje('Buscando datos del cliente. Por favor espere...','alerta',0);
        var form_data={
            cliente: $(this).val(),
            ajax:'1'
        };
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('pedidos/datos_cliente');?>",
            data: form_data,
            dataType: 'json',
            success: function(msg){
                if(msg['estado']=='correcto'){
                    mostrar_mensaje(msg['mensaje'],'correcto',500);
                    $('input[name=nit]').removeClass('error');
                    $('#datos_cliente').html(msg['vista']);
                    if(plazo_pedido){
                        $('select[name=plazo]').val(plazo_pedido);
                    }
                    if(dcto_pedido){
                        $('select[name=descuento_pedido]').val(dcto_pedido);
                    }
                    if(direccion_envio){
                        $('input[name=direccion_envio]').val(direccion_envio);
                    }
                    $('input[name=cambios]').val(1);
                    var encontrado=false;
                    $('#productos_pedido >tbody input[name=codigo]').each(function(){
                        if($(this).val()=='' && encontrado==false){
                            $(this).focus();
                            encontrado=true;
                            return;
                        }
                    });
                }else{
                    mostrar_mensaje(msg['mensaje'],'error');
                    $('input[name=nit]').addClass('error');
                    $('#datos_cliente').html('<ul class="formulario"><li class="left cuadro noderecha" style="width:30%;"><p id="nombre_cliente">-</p><span class="sub_label">Nombre del cliente</span></li><li class="left cuadro" style="width:30%;">-<span class="sub_label">Contacto</span></li><li class="left cuadro noizquierda" style="width:40%;">-<span class="sub_label">E-mail</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:40%;">-<span class="sub_label">Dirección</span></li><li class="left cuadro noarriba" style="width:30%;">-<span class="sub_label">Ciudad</span></li><li class="left cuadro noizquierda noarriba" style="width:30%;">-<span class="sub_label">Teléfono</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:45%;">-<span class="sub_label">Asesor</span></li><li class="left cuadro noarriba" style="width:10%;">-<span class="sub_label">Plazo</span></li><li class="left cuadro noizquierda noarriba" style="width:10%;">-<span class="sub_label">Lista</span></li><li class="left cuadro noizquierda noarriba" style="width:35%;">-<span class="sub_label">Retención</span></li><div class="clear"></div></ul>');
                }                                   
            },
            error: function(x,e){
                $('#datos_cliente').html('<ul class="formulario"><li class="left cuadro noderecha" style="width:30%;"><p id="nombre_cliente">-</p><span class="sub_label">Nombre del cliente</span></li><li class="left cuadro" style="width:30%;">-<span class="sub_label">Contacto</span></li><li class="left cuadro noizquierda" style="width:40%;">-<span class="sub_label">E-mail</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:40%;">-<span class="sub_label">Dirección</span></li><li class="left cuadro noarriba" style="width:30%;">-<span class="sub_label">Ciudad</span></li><li class="left cuadro noizquierda noarriba" style="width:30%;">-<span class="sub_label">Teléfono</span></li><div class="clear"></div><li class="left cuadro noderecha noarriba" style="width:45%;">-<span class="sub_label">Asesor</span></li><li class="left cuadro noarriba" style="width:10%;">-<span class="sub_label">Plazo</span></li><li class="left cuadro noizquierda noarriba" style="width:10%;">-<span class="sub_label">Lista</span></li><li class="left cuadro noizquierda noarriba" style="width:35%;">-<span class="sub_label">Retención</span></li><div class="clear"></div></ul>');
                mostrar_error(x,e);
            }
        });//Termina ajax.
    });//Termina change
    $('select[name=descuento_pedido]').live('change',function(){
        var descuento=$(this).val();
        dcto_pedido=descuento;
        $('input[name=dcto]').each(function(){
            $(this).val(descuento);
        });
        recalcularTodo();
    });
    $('a[rel=boton_buscar_cliente]').live('click',function(){
        $("#buscarCliente").dialog('open');
        $('#buscarCliente input[name=cliente]').focus();
        if($("#buscarCliente").html()==''){
            mostrar_mensaje('Cargando listado de clientes. Por favor espere...','alerta',0);
            var form_data={
                cliente: '',
                ajax:'1'
            };
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('pedidos/buscar_cliente');?>",
                data: form_data,
                dataType:'json',
                success: function(msg){
                    if(msg['estado']=='correcto'){
                        mostrar_mensaje(msg['mensaje'],'correcto',200);				   
                        $('#buscarCliente').html(msg['vista']);
                        acciones_buscar_cliente();
                        $('#buscarCliente input[name=cliente]').focus();
                        $('body').css('overflow','hidden');                            
                    }else{
                        $('body').css('overflow','auto');
                        mostrar_mensaje(msg['mensaje'],'error');				                                      
                    }
                },
                error: function(x,e){
                    $('body').css('overflow','auto');
                    mostrar_error(x,e);
                }
             });//Termina ajax.
         }        
        return false;
    });//Termina boton_buscar_cliente
	
    $('input[name=items_agregar]').keyup(function(event){
        if (event.keyCode == '13') {
            $('a[rel=boton_agregar_items]').click();
        }
        return false;
    });//Termina keyup.
    
    function agregar_item(n,codigo,referencia,nombre,ude,existencia,cantidad,precio,dcto,iva,total,pedido_texto){
        if(n % 2){
                clase="";
        }else{
                clase="altrow";
        }
        if(typeof codigo == "undefined"){
            codigo='';
        }
        if(typeof referencia == "undefined"){
            referencia='';
        }
        if(typeof nombre == "undefined"){
            nombre='';
        }
        if(typeof ude == "undefined"){
            ude='';
        }
        if(typeof existencia == "undefined"){
            existencia='';
        }
        if(typeof cantidad == "undefined"){
            cantidad=0;
        }
        if(typeof precio == "undefined"){
            precio=0;
        }
        if(typeof dcto == "undefined"){
            dcto=0;
        }
        if(typeof iva == "undefined"){
            iva=0;
        }
        if(typeof total == "undefined"){
            total=0;
        }
        if(typeof pedido_texto == "undefined"){
            pedido_texto='';
        }
        if(nombre!='' && precio==0){
            nombre='<div class="agotado">'+nombre+'</div><span>Producto agotado.</span>';
        }
        $('<tr id="fila_'+n+'" class="'+clase+'" style="display:none;">'
            +'<td id="numero_item_'+n+'" class="item numero"><span class="numero">'+n+'</span><input type="checkbox" name="select_eliminar" tabindex="-1"/></td>'
            +'<td><input type="text" size="8" value="'+codigo+'" name="codigo"><?php echo anchor('','<span class="iconos_azules buscar"></span>','class="boton_gris" rel="buscar_producto" tabindex="-1" onclick="return false;"');?>'
            +'<div id="referencia_item_'+n+'" class="referencia">'+referencia+'</div>'+pedido_texto+
            <?php if($configuracion['mostrar_existencia']){ ?>
            '<div class="existencia" title="Existencia">Exis.: <span class="cantidad">'+existencia+'</span></div>'+
            <?php }?>            
            '</td>'
            +'<td><div class="nombre_producto">'+nombre+'</div><div class="descripcion"></div></td>'
            +'<td style="text-align:center;">'
            +'<input type="text" size="5" value="'+cantidad+'" name="cantidad">'+
            <?php if($configuracion['ver_ude']){ ?>
            '<div class="ude" title="Unidad de empaque">Ude: '+ude+'</div></td>'+
            <?php } ?>
            '<td style="text-align:center;">'+
            <?php if($configuracion['modificar_precio']){ ?>
            '<input type="text" style="text-align: right;" size="8" value="'+precio+'" name="precio">'+													
            <?php }else{ ?>
            '<input type="text" tabindex="-1"  readonly="readOnly" style="text-align: right;" size="8" value="'+precio+'" name="precio">'+
            <?php }?>
            '<div class="listas" title="Listas de precios"><ul></ul></div>' +
            '</td>'+
            '<td style="text-align:center;">'+
            <?php if($configuracion['modificar_dcto']){?>
            '<input type="text" size="2" value="'+dcto+'" name="dcto">'+
            <?php }else{ ?>
            '<input type="text" size="2" readonly="readOnly" tabindex="-1" value="'+dcto+'" name="dcto">'+
            <?php }?>
            '</td>'+
            '<td style="text-align:center;" id="iva_item_'+n+'">'+iva+'</td>'+
            '<td class="total_producto" id="total_item_'+n+'">0</td>'+
            '<td><a href="" tabindex="-1" rel="eliminar_producto" title="Eliminar" class="boton boton_gris"><span class="iconos_rojo eliminar"></span></a></td></tr>').insertBefore($('#productos_pedido tbody tr.agregar'));
        var fila=$('tr#fila_'+n);
        if(existencia<=0 && existencia!=''){
            fila.find('div.existencia').addClass('agotada');
        }
        $('#total_productos_pedido').html('Total de productos en el pedido '+n);
        //Se formatean los campos para que acepten solo numeros y se les aplica la funcion de q al cambiar actualicen los totales.
        acciones_campos_fila_producto(fila);
    }
    $('a[rel=boton_agregar_items]').click(function(){
        var n=0;
        n=parseInt($('#productos_pedido tbody tr').length);
        var cantidad=$('input[name=items_agregar]').val();
        if(!cantidad || cantidad==0){
                cantidad=1;
        }
        var clase="";
        for(var f=0;f<cantidad;f++){
            if(n % 2){
                clase="";
            }else{
                clase="altrow";
            }					
            if(typeof dcto_pedido == "undefined"){
                dcto_pedido='0';
            }
            agregar_item(n,'','','','','',0,0,dcto_pedido);
            n++;            
        }
        var encontrado=false;
        $('#productos_pedido >tbody input[name=codigo]').each(function(){
            if($(this).val()=='' && encontrado==false){
                $(this).focus();
                encontrado=true;
                return;
            }
        });
        $('input[name=items_agregar]').val(1);
        return false;
    });//Termina boton_agregar_items.
    
    function acciones_campos_fila_producto(fila){
        if(fila==null){
                fila=$('#datosProducto');
        }
        fila.find("input[name=precio]").numeric();
        fila.find("input[name=cantidad]").numeric('no');
        fila.find("input[name=dcto]").numeric();
        fila.fadeIn('slow');
    }//Termina acciones_campos_fila_producto
    
    $("input[name=precio]").live('focusout',function(){
        if($(this).val()==0){
            $(this).addClass('error');
        }else{
            $(this).removeClass('error');
        }
    });
    
    $("input[name=cantidad]").live('focusout',function(){
        if($(this).val()==0){
            $(this).addClass('error');
        }else{
            $(this).removeClass('error');
        }
    });
        
    $("input[name=precio]").live('change',function(){
        recalcularTodo();
        $('input[name=cambios]').val(1);
    });
        
    $("input[name=cantidad]").live('change',function(){
        recalcularTodo();					
        $('input[name=cambios]').val(1);
    });
    
    $("input[name=dcto]").live('change',function(){
            recalcularTodo();					
            $('input[name=cambios]').val(1);
    });
    
    
    $("input[name=codigo]").live('change',function(){ 
            mostrar_mensaje('Cargando datos del producto. Por favor espere...','alerta',0);
            var codigo_fila=$(this).val();
            var fila=$(this).parent().parent();
            var copias=false;
            var codigos="";

            $("input[name=codigo]").each(function(){
                    codigos=codigos+$(this).val()+' ';
            });
            //Quita colores de productos que ya no estan repetidos.
            <?php if(!$configuracion['repetir_productos']){?>
            $("input[name=codigo]").each(function(){
                    if($(this).val()!=""){
                            productos_repetidos=codigos.split($(this).val()).length-1;
                    }else{
                            productos_repetidos=0;
                    }
                    if(productos_repetidos>1){
                            $(this).parent().parent().addClass('repetido');
                            copias=true;					
                    }else{
                            $(this).parent().parent().removeClass('repetido');					
                    }					
            });
            <?php }?>
            //Termina quita colores de productos que ya no estan repetidos.
            var url='<?php echo site_url('pedidos/datos_producto');?>';
            var form_data={
                    codigo: $(this).val(),
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               dataType: 'json',
               data: form_data,
               success: function(msg){
                       if(msg['estado']=='correcto'){
                            if(typeof dcto_pedido == "undefined"){
                                    dcto_pedido=producto['descuento'];
                            }
                                                                                    
                                   mostrar_mensaje(msg['mensaje'],'correcto',500);
                                   fila.find('input[name=codigo]').removeClass('error');
                                   fila.find('div.nombre_producto').html(msg.nombre);
                                   fila.find('[id^=referencia_item_]').text(msg['referencia']);
                                   //fila.find('input[name=cantidad]').val('0');
                                   <?php if($configuracion['ver_ude']){?>
                                    fila.find('div.ude').text('Ude: '+msg['unidad']);
                                   <?php }?>
                                   <?php if($configuracion['mostrar_existencia']){?>
                                       fila.find('div.existencia span.cantidad').text(msg['stock']);
                                        if(msg['stock']<=0 && msg['stock']!=''){
                                            fila.find('div.existencia').addClass('agotada');
                                        }                                       
                                   <?php }?>
                                   <?php if($configuracion['lista_tipo_por_defecto']==0){?>
                                       if(msg['precio<?php echo $configuracion['lista_numero_por_defecto'];?>']==0){
                                            fila.find('div.nombre_producto').html('<div class="agotado">'+msg.nombre+'</div><span>Producto agotado</span>');
                                       }
                                        fila.find('[name=precio]').val(msg['precio<?php echo $configuracion['lista_numero_por_defecto'];?>']);
                                   <?php }else{?>
                                       var lista_cliente=$('select[name=lista]').val();
                                        if(!lista_cliente){
                                            lista_cliente=$('input[name=lista_cliente]').val();
                                        }
                                        if(msg['precio'+lista_cliente]==0){
                                            fila.find('div.nombre_producto').html('<div class="agotado">'+msg.nombre+'</div><span>Producto agotado</span>');
                                       }
                                       fila.find('[name=precio]').val(msg['precio'+lista_cliente]);
                                   <?php }?>
                                   fila.find('[id^=iva_item_]').text(msg['iva']);
                                   fila.find('input[name=cantidad]').select();						   
                                   var valor_cantidad='0';
                                   var valor_precio=msg['precio1'];
                                   var valor_dcto='0';
                                   fila.find('input[name=cantidad]').bind('blur',function(){
                                                if(valor_cantidad!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                   fila.find('input[name=precio]').bind('blur',function(){
                                                if(valor_precio!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                   fila.find('input[name=dcto]').bind('blur',function(){
                                                if(valor_dcto!=$(this).val()){
                                                        recalcularTodo();
                                                        $(this).unbind('blur');
                                                }
                                        });
                                        $('input[name=cambios]').val(1);                                                                                            		
                           
                            if(copias){
                                mostrar_mensaje('Existen productos repetidos.','error');                                            		
                            }
                       }else{
                           fila.find('input[name=codigo]').addClass('error');
                           fila.find('div.nombre_producto').html(msg['nombre']);
                           fila.find('[id^=referencia_item_]').text('');
                           fila.find('input[name=cantidad]').val('0');
                           fila.find('input[name=dcto]').val(dcto_pedido);
                           fila.find('input[name=precio]').val('0');
                           fila.find('[id^=iva_item_]').text('0');
                           fila.find('input[name=codigo]').select();
                           mostrar_mensaje(msg['mensaje'],'error');                                   
                       }
               },
               error: function(x,e){
                   fila.find('div.nombre_producto').text('');
                   fila.find('[id^=referencia_item_]').text('');
                   fila.find('[name=dcto]').val('0');
                   fila.find('[name=precio]').val('0');
                   fila.find('[id^=iva_item_]').text('0');
                   fila.find('input[name=cantidad]').val('0');
                   mostrar_error(x,e);
               }
            });//Termina ajax.
    });//Termina bind change.
    
    
        $('a[rel=eliminar_producto]').live('click',function(){
                var fila=$(this).parent().parent();
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                $('body').append('<div id="dialog-confirm" title="Eliminar producto"><span class="icono"></span>Está seguro de eliminar el producto seleccionado?</div>');
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    mostrar_mensaje('Eliminando item. Por favor espere...','alerta',0);
                                    
                                    var codigo_fila=fila.find('input[name=codigo]').val();
                                    var repetido=0;
                                    fila.css({background:"#FFD5D5",color:"#CC0000"});
                                    fila.animate({opacity: 0.5}, 200, function(){				  
                                            fila.remove();
                                            $('input[name=cambios]').val(1);
                                            var productos_agregados=parseInt($('#productos_pedido >tbody >tr').length)-1;
                                            var n=1;
                                            var codigos="";
                                            $('#productos_pedido >tbody >tr').each(function(){
                                                    if($(this).attr('class')!='agregar'){
                                                            codigos=codigos+$(this).find("input[name=codigo]").val()+' ';						
                                                            if(n % 2){
                                                                    $(this).removeClass('altrow');
                                                            }else{
                                                                    $(this).addClass('altrow');
                                                            }
                                                            $(this).attr('id','fila_item_'+n);
                                                            $(this).find('[id^=total_item_]').attr('id','total_item_'+n);
                                                            $(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
                                                            $(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
                                                            $(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
                                                            $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                                    }
                                                    n++;
                                            });
                                            $('#total_productos_pedido').html('Total de productos en el pedido '+productos_agregados);
                                            if(productos_agregados==0){
                                                    $('#total_productos_pedido').html('<span class="alerta"></span> No ha agregado productos al pedido.'); 
                                            }
                                            <?php if(!$configuracion['repetir_productos']){?>
                                            //Quita colores de productos que ya no estan repetidos.
                                            $("input[name=codigo]").each(function(){
                                                    if($(this).val()!=""){
                                                            productos_repetidos=codigos.split($(this).val()).length-1;
                                                    }else{
                                                            productos_repetidos=0;
                                                    }
                                                    if(productos_repetidos>1){
                                                            $(this).parent().parent().addClass('repetido');
                                                    }else{
                                                            $(this).parent().parent().removeClass('repetido');
                                                    }					
                                            });
                                            //Termina quita colores de productos que ya no estan repetidos.
                                            <?php }?>
                                            recalcularTodo();
                                            mostrar_mensaje('Se ha eliminado correctamente.','correcto',500);                                            
                                    });//Termina animate
                                    
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
                return false;
        });//Termina eliminar_producto.
	acciones_observaciones();
	recalcularTodo();
	function formatear(){
                <?php if($configuracion['redondear_cantidad']){?>
		$("input[name=cantidad]").format({format:"#,###", locale:"us"});
                <?php }?>
                <?php if($configuracion['redondear_precio']){?>
		$("input[name=precio]").format({format:"#,###", locale:"us"});
                <?php }?>
                <?php if($configuracion['redondear_descuento']){?>
		$("input[name=dcto]").format({format:"#,###", locale:"us"});
                <?php }?>                
                <?php if($configuracion['redondear_total']){?>
		$("[id^=total_item_]").format({format:"#,###", locale:"us"});
                <?php }?>
	};
	function formatearTotales(){		
		$("#totales_subtotal").format({format:"$ #,###", locale:"us"});
		$("#totales_descuento").format({format:"$ #,###", locale:"us"});
		$("#totales_neto").format({format:"$ #,###", locale:"us"});
		$("#totales_iva").format({format:"$ #,###", locale:"us"});
		$("#totales_total").format({format:"$ #,###", locale:"us"});
	}
	function recalcularTodo(){		
		var totales_subtotal=0;
		var totales_dcto=0;
		var totales_iva=0;
		var subtotal=0;
		var dcto=0;
		var iva=0;
		var cantidades=$("input[name=cantidad]").parseNumber();
		var precios=$("input[name=precio]").parseNumber();
		var descuentos=$("input[name=dcto]").parseNumber();
		var ivas=$("[id^=iva_item_]").parseNumber();
		for(n=0;n<cantidades.length;n++){
			subtotal=cantidades[n]*precios[n];
			dcto=cantidades[n]*(precios[n] * descuentos[n]/100);
			iva=(subtotal-dcto)*(ivas[n]/100);
			totales_subtotal=totales_subtotal+subtotal;
			totales_dcto=totales_dcto+dcto;
			totales_iva=totales_iva+iva;
		}
		var totales_neto=totales_subtotal-totales_dcto;
		var totales_total=totales_neto+totales_iva;
		$("#totales_subtotal").text(totales_subtotal);
		$("#totales_descuento").text(totales_dcto);
		//$("#totales_neto").text(totales_neto);
		$("#totales_iva").text(totales_iva);
		$("#totales_total").text(Math.round(totales_total));
		
		$("[id^=total_item_]").calc(
			// the equation to use for the calculation
			"(cantidad * precio) - (cantidad * (precio * dcto / 100))",
			// define the variables used in the equation, these can be a jQuery object
			{
				cantidad: $("input[name=cantidad]"),
				precio: $("input[name=precio]"),
				dcto: $("input[name=dcto]")
			},
			// define the formatting callback, the results of the calculation are passed to this function
			function (s){
				// return the number as a dollar amount				
				return s.toFixed(2);
			},
			// define the finish callback, this runs after the calculation has been complete
			function ($this){
				// sum the total of the $("[id^=total_item]") selector
				var neto = $this.sum();
				$("#totales_neto").text(
					// round the results to 2 digits
					neto.toFixed(2)					
				);			
			}			
		);	   
		formatear();
		formatearTotales();                  
	}
        
	$('input[name=items_agregar]').numeric();
        $('a[rel=boton_cargar_cotizacion]').click(function(){
                mostrar_mensaje('Cargando listado de cotizaciones. Por favor espere...','alerta',0);
		var form_data={
			cliente: '',
			ajax:1
		};
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('pedidos/buscar_cotizacion_a_cargar');?>",
			data: form_data,
                        dataType:'json',
			success: function(msg){
			   if(msg['estado']=='correcto'){
                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                               $('#buscarPedido').dialog("option","title","Buscar cotización a cargar");
                                $('#buscarPedido').html(msg['vista']);
                                $('#buscarPedido').dialog('open');
                                $('#buscarPedido input[name=cliente]').focus();
                                $('body').css('overflow','hidden');
                                actualizar_buscador_pedido_cotizacion();                                        
			   }else{
                               mostrar_mensaje(msg['mensaje'],'error');
			   }
			},
			error: function(x,e){
                            mostrar_error(x,e);
			}
		});//Termina ajax.		
		return false;
	});//Termina boton_cargar_cotizacion.
        $('#resultados_pedidos th a').live('click',function(){
                mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                var form_data={
                        ajax:'1'
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType: 'json',
                   success: function(msg){
                           if(msg['estado']=='correcto'){				   
                                $('#buscarPedido').html(msg['vista']);
                                mostrar_mensaje(msg['mensaje'],'correcto',500);
                                actualizar_buscador_pedido_cotizacion();
                           }else{
                               mostrar_mensaje(msg['mensaje'],'error');
                           }
                   },
                   error: function(x,e){
                       mostrar_error(x,e);
                   }
                });//Termina ajax.
                return false;
        });//Termina th click.
        $('div.paginacion a').live('click',function(){
                mostrar_mensaje('Cargando la página. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                var form_data={
                        ajax:'1'
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType: 'json',
                   success: function(msg){
                           if(msg['estado']=='correcto'){				   
                                $('#buscarPedido').html(msg['vista']);
                                mostrar_mensaje(msg['mensaje'],'correcto',500);
                                actualizar_buscador_pedido_cotizacion();
                           }else{
                                mostrar_mensaje(msg['mensaje'],'error');
                           }
                   },
                   error: function(x,e){
                          mostrar_error(x,e);
                   }
                });
                return false;
        });//Termina paginacion a.
        $('#buscarPedido form[name=buscar_pedido_a_copiar],#buscarPedido form[name=buscar_cotizacion_a_cargar]').live('submit',function(){
                var url=$(this).attr('action');
                var valor=$('#buscarPedido input[name=cliente]').val();
                if(valor=='Digite el nit o nombre del cliente'){
                        valor='';
                }
                var form_data={
                        buscar:valor,
                        ajax:1
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType: 'json',
                   success: function(msg){
                           if(msg['estado']=='correcto'){
                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                $('#buscarPedido').html(msg['vista']);
                                actualizar_buscador_pedido_cotizacion();
                           }else{
                                mostrar_mensaje(msg['mensaje'],'error');   
                           }
                   },
                   error:function(x,e){
                       mostrar_error(x,e);
                   }
                });//Termina ajax.
                return false;
        });//Termina buscar_pedido_a_copiar.
        $('a[rel=boton_copiar_desde_pedido]').click(function(){
                mostrar_mensaje('Cargando listado de pedidos. Por favor espere...','alerta',0);
		
		var form_data={
			cliente: '',
			ajax:1
		};
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('pedidos/buscar_pedido_a_copiar');?>",
			data: form_data,
                        dataType:'json',
			success: function(msg){
			   if(msg['estado']=='correcto'){
                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                $('#buscarPedido').dialog("option","title","Buscar pedido a copiar");
                                $('#buscarPedido').html(msg['vista']);
                                $('#buscarPedido').dialog('open');
                                $('#buscarPedido input[name=cliente]').focus();
                                $('body').css('overflow','hidden');
                                actualizar_buscador_pedido_cotizacion();                                        
			   }else{
                               mostrar_mensaje(msg['mensaje'],'error');
			   }
			},
			error: function(x,e){
                            mostrar_error(x,e);
			}
		});//Termina ajax.		
		return false;
	});//Termina boton_copiar_desde_pedido.
        function actualizar_buscador_pedido_cotizacion(){
		$('#buscarPedido input[name=cliente]').focus(function(){
                        var valor=$(this).val();
                        if(valor=='Digite el nit o nombre del cliente'){
                                $(this).val('');
                        }
                });
                $('#buscarPedido input[name=cliente]').blur(function(){
                        var valor=$(this).val();
                        if(valor==''){
                                $(this).val('Digite el nit o nombre del cliente');
                        }
                });
                var buscar=$('#buscarPedido input[name=cliente]').val();
                if(buscar!=''){
                        $('#buscarPedido tbody td[title=nit]').highlight(buscar);
                        $('#buscarPedido tbody td[title=nombre]').highlight(buscar);
                }
            }
            $('a[rel=copiar_pedido]').live('click',function(){
                mostrar_mensaje('Cargando los datos del pedido seleccionado. Por favor espere...','alerta',0);
                var url=$(this).attr('href');
                var form_data={
                        ajax:1
                };
                $.ajax({
                   type: "POST",
                   url: url,
                   data: form_data,
                   dataType:'json',
                   success: function(msg){
                       if(msg['estado']=='correcto'){
                           mostrar_mensaje(msg['mensaje'],'correcto',2000);
                           agregar_productos_cargados(msg['productos'],msg['pedido']);
                           $('#buscarPedido').dialog('close');
                       }else{
                           mostrar_mensaje(msg['mensaje'],'error');
                       }
                   },
                   error:function(x,e){
                      mostrar_error(x,e);
                   }
                 });
                return false;
            });//Termina boton_agregar_items.
            function agregar_productos_cargados(productos,pedido){
                var n=0;
                $("input[name=codigo]").each(function(){
                    if($(this).val()==''){
                            $(this).parent().parent().remove();
                    }
                });
                n=parseInt($('#productos_pedido tbody tr').length);
                var pedido_texto='';
                if(!pedido || pedido=='undefined'){
                  pedido_texto='<div class="pedido_importado">Excell</div>';
                }else{
                  pedido_texto='<div class="pedido_copiado">'+pedido+'</div>';
                }
                var clase="";
                $.each(productos,function(k,v){
                      if(n % 2){
                                clase="";
                        }else{
                                clase="altrow";
                        }					
                        if(typeof dcto_pedido == "undefined"){
                                dcto_pedido='0';
                        }
                        agregar_item(n,v['codigo'],v['referencia'],v['nombre'],v['ude'],v['stock'],v['cantidad'],v['precio'],dcto_pedido,v['iva'],v['total'],pedido_texto);
                        n++;                        
                });
                recalcularTodo();
                <?php if(!$configuracion['repetir_productos']){?>
                verificar_productos_repetidos();
                <?php }?>
            }
		function acciones_buscar_cliente(){
			$('#buscarCliente input[name=cliente]').focus(function(){
				var valor=$(this).val();
				if(valor=='Digite el nit o nombre del cliente'){
					$(this).val('');
				}
			});
			$('#buscarCliente input[name=cliente]').focusout(function(){
				var valor=$(this).val();
				if(valor==''){
					$(this).val('Digite el nit o nombre del cliente');
				}
			});
			$('#buscarCliente input[name=cliente]').keyup(function(event){
				if (event.keyCode == '13') {
				  $('#buscarCliente a[rel=filtrar_cliente]').click();
				}
				return false;
			});
			$('#buscarCliente a[rel=filtrar_cliente]').click(function(){
                                mostrar_mensaje('Cargando clientes que coincidan con los datos de busqueda. Por favor espere...','alerta',0);
				var url = $(this).attr('href');
				var cliente=$('#buscarCliente input[name=cliente]').val();
				if(cliente=='Digite el nit o nombre del cliente'){
					cliente='';
					$('#buscarCliente input[name=cliente]').val('');
				}
				var form_data={
					cliente:cliente,
					ajax:1
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType:'json',
				   success: function(msg){
					   if(msg){				   
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                $('#buscarCliente').html(msg['vista']);
                                                acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente th a').click(function(){
                                mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
				var url=$(this).attr('href');
				var form_data={
					ajax:'1'
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType: 'json',
				   success: function(msg){
					   if(msg['estado']=='correcto'){	
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
						$('#buscarCliente').html(msg['vista']);
						acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente div.paginacion a').click(function(){
                                mostrar_mensaje('Cargando los resultados de la busqueda. Por favor espere...','alerta',0);
				var url=$(this).attr('href');
                                var form_data={
					ajax:1
				};
				$.ajax({
				   type: "POST",
				   url: url,
				   data: form_data,
                                   dataType: 'json',
				   success: function(msg){
                                          if(msg['estado']=='correcto'){	
                                               mostrar_mensaje(msg['mensaje'],'correcto',500);
						$('#buscarCliente').html(msg['vista']);
						acciones_buscar_cliente();
					   }else{
						mostrar_mensaje(msg['mensaje'],'error');
					   }
				   },
                                   error: function(x,e){
                                      mostrar_error(x,e);
                                   }
				 });
				return false;
			});
			$('#buscarCliente a[rel=boton_seleccionar_cliente]').click(function(){
                                $('input[name=nit]').val($(this).attr('id'));
				$('#buscarCliente').dialog('close');
				$('input[name=nit]').change();
				return false;
			});
			var buscar=$('#buscarCliente input[name=cliente]').val();
			if(buscar!=''){
				$('#buscarCliente table').highlight(buscar);
			}
		}
		var guardando=false;
		$('a[rel=guardar_pedido]').click(function(){
                    var boton=$(this);
			if(boton.text()!='Guardando pedido...' && !guardando){
				guardando==true;
				clearTimeout(tiempo_autoguardar);
				$(this).text('Guardando pedido...');
				var boton=$(this);
                                mostrar_mensaje('Verificando datos antes de guardar. Por favor espere...');
				var error=false;
				var encabezado=[];
				var nit=$('input[name=nit]').val();
				var nombre_cliente=$('p[id=nombre_cliente]').text();
				var asesor=$('select[name=asesor]').val();
				var plazo=$('select[name=plazo]').val();
                                var email_copias=$('input[name=email_copias]').val();
                                var direccion_envio=$('input[name=direccion_envio]').val();
				var lista=$('select[name=lista]').val();
				var retencion=$('select[name=retencion]').val();
				var observaciones=$('textarea[name=observaciones]').val();
				if(!nit || nombre_cliente=='-'){
					error=true;
					$('input[name=nit]').addClass('error');
				}else{
					$('input[name=nit]').removeClass('error');
				}
				if(!observaciones){
					error=true;
					$('textarea[name=observaciones]').addClass('error');
				}else{
					$('textarea[name=observaciones]').removeClass('error');
				}
                                    if(email_copias!=''){
                                        $('input[name=email_copias]').removeClass('error');
                                                        var correos = $('input[name=email_copias]').val().split(',');
                                                        for (var n=0;n< correos.length;n++){
                                                                if(!es_mail(correos[n])){
                                                error=true;
                                            $('input[name=email_copias]').addClass('error');
                                        }
                                    }
				}
				if(retencion==undefined){
					retencion='null';
				}
				if(plazo==undefined){
					plazo='null';
				}
				if(lista==undefined){
					lista='null';
				}
				var codigos=[];
				var codigos_verificar_repetidos="";
				$("input[name=codigo]").each(function(){
					codigos_verificar_repetidos=codigos_verificar_repetidos+$(this).val()+' ';
					if($(this).val()==''){
						$(this).parent().parent().remove();
					}else{
						codigos.push($(this));
					}
				});

				var productos_agregados=parseInt($('#productos_pedido >tbody >tr').length)-1;
				var n=1;
				$('#productos_pedido >tbody >tr').each(function(){
					if($(this).attr('class')!='agregar'){
						if(n % 2){
							$(this).removeClass('altrow');
						}else{
							$(this).addClass('altrow');
						}
						$(this).attr('id','fila_item_'+n);
						$(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
						$(this).find('[id^=total_item_]').attr('id','total_item_'+n);
						$(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
						$(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
						$(this).find('[id^=numero_item_]').find('span.numero').text(n);
					}
					n++;
				});
				$('#total_productos_pedido').html('Total de productos en el pedido '+productos_agregados);
				if(productos_agregados==0){
					$('#total_productos_pedido').html('<span class="alerta"></span> No ha agregado productos al pedido.'); 
				}
				var nombres_productos=[];
				$("div.nombre_producto").each(function(){
					nombres_productos.push($(this).text());	
				});
				var cantidades=[];
				$("input[name=cantidad]").each(function(){
						cantidades.push($(this));
				});
				var referencias=[];
				$("div[id^=referencia_item_]").each(function(){
						referencias.push($(this).text());
				});
				var precios=[];
				$("input[name=precio]").each(function(){
					precios.push($(this));
				});

				var descuentos=$("input[name=dcto]").parseNumber();
				var ivas=$("[id^=iva_item_]").parseNumber();
				var subtotal=0;
				var dcto=0;
				var iva=0;
				var totales_subtotal=0;
				var totales_dcto=0;
				var totales_iva=0;
				var productos=[];
				var mensaje='';
				if(cantidades.length==0){
					error=true;
					mensaje='No hay productos en el pedido';
				}
				recalcularTodo();
				for(n=0;n<cantidades.length;n++){
					if(codigos[n].val()!='' && nombres_productos[n]!='El producto no existe' && nombres_productos[n]!='Producto agotado' && nombres_productos[n]!='' && nombres_productos[n]!=' '){
						codigos[n].removeClass('error');
						cantidad_item=cantidades[n].parseNumber()[0];
						precio_item=precios[n].parseNumber()[0];
						if(cantidad_item==0){
							error=true;
							cantidades[n].addClass('error');
						}else{
							cantidades[n].removeClass('error');
						}
						if(precio_item==0){
							error=true;
							precios[n].addClass('error');
						}else{
							precios[n].removeClass('error');
						}
						subtotal=cantidad_item*precio_item;
						dcto=cantidad_item*(precio_item * descuentos[n]/100);
						iva=(subtotal-dcto)*(ivas[n]/100);
						totales_subtotal=totales_subtotal+subtotal;
						totales_dcto=totales_dcto+dcto;
						totales_iva=totales_iva+iva;
						
						producto={
							codigo: codigos[n].val(),
							referencia: referencias[n],
							cantidad: cantidad_item,
							precio: precio_item,
							descuento: descuentos[n],
							iva: ivas[n],
							total_sin_dcto:subtotal,
							total: (subtotal-dcto)
						};
						productos.push(producto);
					}else{
						error=true;
						codigos[n].addClass('error');
					}
				}
				<?php if(!$configuracion['repetir_productos']){?>				
				//Quita colores de productos que ya no estan repetidos.
				$("input[name=codigo]").each(function(){
					if($(this).val()!=""){
						productos_repetidos=codigos_verificar_repetidos.split($(this).val()).length-1;
					}else{
						productos_repetidos=0;
					}
					if(productos_repetidos>1){
						$(this).parent().parent().addClass('repetido');
						error=true;
						mensaje=mensaje+" Existen productos repetidos";
					}else{
						$(this).parent().parent().removeClass('repetido');					
					}					
				});
				//Termina quita colores de productos que ya no estan repetidos.
				<?php }?>
				if(!error){
					var totales_subtotal=$("#totales_subtotal").parseNumber()[0];
					var totales_descuento=$("#totales_descuento").parseNumber()[0];
					var totales_neto=$("#totales_neto").parseNumber()[0];
					var totales_iva=$("#totales_iva").parseNumber()[0];
					var totales_total=Math.round($("#totales_total").parseNumber()[0]);
					encabezado={
						nit:nit,
						asesor:asesor,
						plazo: plazo,
						lista:lista,
                                                email_copias:email_copias,
						retencion:retencion,
                                                direccion_envio:direccion_envio,
						observaciones:observaciones,
						totales_subtotal:totales_subtotal,
						totales_descuento:totales_descuento,
						totales_neto:totales_neto,
						totales_iva:totales_iva,
						totales_total:totales_total
					}
                                        mostrar_mensaje('Guardando datos del pedido. Por favor espere...','alerta',0);
					var url=$(this).attr('href');
					var form_data={
                                                ajax:1,
						numero_borrador:$('input[name=borrador]').val(),
						encabezado: encabezado,
						productos: productos						
					};
                                        console.log(form_data);
					$.ajax({
					   type: "POST",
                                           async: false,
					   url: url,
					   data: form_data,
                                           dataType: 'json',
					   success: function(msg){
						   if(msg){
                                                       
                                                          if(msg['estado']=='error'){
                                                                mostrar_mensaje(msg['mensaje'],'error');
							   }else{
                                                                mostrar_mensaje('El pedido se guardó correctamente. Se va a enviar copia por correo. Por favor espere','correcto');
                                                                $('div#pedido_contenido').html(msg['vista']);
                                                                $('#envio_pedidos').html('<span class="cargando"></span> Enviando correo de los pedidos por favor espere...');
                                                                var url='<?php echo site_url('pedidos/enviar_pedidos');?>';
                                                                var form_data={
                                                                   pedidos:msg['numeros'],
                                                                   vendedor:encabezado['asesor'],
                                                                   cliente:encabezado['nit'],
                                                                   otros:encabezado['email_copias']
                                                                };
                                                                console.log(form_data);
                                                                $.ajax({
                                                                   type: "POST",
                                                                   async:true,
                                                                   url: url,
                                                                   data: form_data,
                                                                   dataType: 'json',
                                                                   success: function(msg){
                                                                       if(msg['estado']=='correcto'){
                                                                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                                           $('input[name=envio_pedidos_correo]').val(0);
                                                                           $('#envio_pedidos').addClass('correcto');
                                                                       }else{
                                                                           mostrar_mensaje(msg['mensaje'],'error');
                                                                           $('input[name=envio_pedidos_correo]').val(3);
                                                                           $('#envio_pedidos').addClass('error');
                                                                       }
                                                                       $('#envio_pedidos').html(msg['mensaje']);
                                                                   },
                                                                   error: function(x,e){
                                                                       mostrar_error(x,e);                                                                                                                                              
                                                                   }
                                                                });
							   }							   
						   }else{
                                                       mostrar_mensaje('Se presentaron errores inesperados. Por favor intentelo nuevamente.','error');								
                                                       boton.text('Guardar definitivo y enviar');                                                       
						   }
					   },
					   error: function(x,e){
                                                mostrar_error(x,e)
                                                boton.text('Guardar definitivo y enviar');														
					   }
					 });
				}else{
					tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
					boton.text('Guardar definitivo y enviar');
					mostrar_mensaje('Se presentaron errores. Por favor verifique los datos en rojo y vuelva a intentar guardar el pedido. <br/>'+mensaje,'error');					
				}
			}else{
                            alert('Espere un momento que el pedido se está guardando.');
			}
			return false;
		});//Termina guardar pedido
                $('a[rel=guardar_pedido_test]').click(function(){
                    var boton=$(this);
			if(boton.text()!='Guardando pedido...' && !guardando){
				guardando==true;
				clearTimeout(tiempo_autoguardar);
				$(this).text('Guardando pedido...');
				var boton=$(this);
                                mostrar_mensaje('Verificando datos antes de guardar. Por favor espere...');
				var error=false;
				var encabezado=[];
				var nit=$('input[name=nit]').val();
				var nombre_cliente=$('p[id=nombre_cliente]').text();
				var asesor=$('select[name=asesor]').val();
				var plazo=$('select[name=plazo]').val();
                                var email_copias=$('input[name=email_copias]').val();
                                var direccion_envio=$('input[name=direccion_envio]').val();
				var lista=$('select[name=lista]').val();
				var retencion=$('select[name=retencion]').val();
				var observaciones=$('textarea[name=observaciones]').val();
				if(!nit || nombre_cliente=='-'){
					error=true;
					$('input[name=nit]').addClass('error');
				}else{
					$('input[name=nit]').removeClass('error');
				}
				if(!observaciones){
					error=true;
					$('textarea[name=observaciones]').addClass('error');
				}else{
					$('textarea[name=observaciones]').removeClass('error');
				}
                                    if(email_copias!=''){
                                        $('input[name=email_copias]').removeClass('error');
                                                        var correos = $('input[name=email_copias]').val().split(',');
                                                        for (var n=0;n< correos.length;n++){
                                                                if(!es_mail(correos[n])){
                                                error=true;
                                            $('input[name=email_copias]').addClass('error');
                                        }
                                    }
				}
				if(retencion==undefined){
					retencion='null';
				}
				if(plazo==undefined){
					plazo='null';
				}
				if(lista==undefined){
					lista='null';
				}
				var codigos=[];
				var codigos_verificar_repetidos="";
				$("input[name=codigo]").each(function(){
					codigos_verificar_repetidos=codigos_verificar_repetidos+$(this).val()+' ';
					if($(this).val()==''){
						$(this).parent().parent().remove();
					}else{
						codigos.push($(this));
					}
				});

				var productos_agregados=parseInt($('#productos_pedido >tbody >tr').length)-1;
				var n=1;
				$('#productos_pedido >tbody >tr').each(function(){
					if($(this).attr('class')!='agregar'){
						if(n % 2){
							$(this).removeClass('altrow');
						}else{
							$(this).addClass('altrow');
						}
						$(this).attr('id','fila_item_'+n);
						$(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
						$(this).find('[id^=total_item_]').attr('id','total_item_'+n);
						$(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
						$(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
						$(this).find('[id^=numero_item_]').find('span.numero').text(n);
					}
					n++;
				});
				$('#total_productos_pedido').html('Total de productos en el pedido '+productos_agregados);
				if(productos_agregados==0){
					$('#total_productos_pedido').html('<span class="alerta"></span> No ha agregado productos al pedido.'); 
				}
				var nombres_productos=[];
				$("div.nombre_producto").each(function(){
					nombres_productos.push($(this).text());	
				});
				var cantidades=[];
				$("input[name=cantidad]").each(function(){
						cantidades.push($(this));
				});
				var referencias=[];
				$("div[id^=referencia_item_]").each(function(){
						referencias.push($(this).text());
				});
				var precios=[];
				$("input[name=precio]").each(function(){
					precios.push($(this));
				});

				var descuentos=$("input[name=dcto]").parseNumber();
				var ivas=$("[id^=iva_item_]").parseNumber();
				var subtotal=0;
				var dcto=0;
				var iva=0;
				var totales_subtotal=0;
				var totales_dcto=0;
				var totales_iva=0;
				var productos=[];
				var mensaje='';
                                var cantidad_item=0;
                                var precio_item=0;
                                var producto;
				if(cantidades.length==0){
					error=true;
					mensaje='No hay productos en el pedido';
				}
				recalcularTodo();
				for(n=0;n<cantidades.length;n++){
					if(codigos[n].val()!='' && nombres_productos[n]!='El producto no existe' && nombres_productos[n]!='Producto agotado' && nombres_productos[n]!='' && nombres_productos[n]!=' '){
						codigos[n].removeClass('error');
						cantidad_item=cantidades[n].parseNumber()[0];
						precio_item=precios[n].parseNumber()[0];
						if(cantidad_item==0){
							error=true;
							cantidades[n].addClass('error');
						}else{
							cantidades[n].removeClass('error');
						}
						if(precio_item==0){
							error=true;
							precios[n].addClass('error');
						}else{
							precios[n].removeClass('error');
						}
						subtotal=cantidad_item*precio_item;
						dcto=cantidad_item*(precio_item * descuentos[n]/100);
						iva=(subtotal-dcto)*(ivas[n]/100);
						totales_subtotal=totales_subtotal+subtotal;
						totales_dcto=totales_dcto+dcto;
						totales_iva=totales_iva+iva;
						
						producto={
							codigo: codigos[n].val(),
							referencia: referencias[n],
							cantidad: cantidad_item,
							precio: precio_item,
							descuento: descuentos[n],
							iva: ivas[n],
							total_sin_dcto:subtotal,
							total: (subtotal-dcto)
						};
						productos.push(producto);
					}else{
						error=true;
						codigos[n].addClass('error');
					}
				}
				<?php if(!$configuracion['repetir_productos']){?>	
                                    var productos_repetidos=0;
				//Quita colores de productos que ya no estan repetidos.
				$("input[name=codigo]").each(function(){
					if($(this).val()!=""){
						productos_repetidos=codigos_verificar_repetidos.split($(this).val()).length-1;
					}else{
						productos_repetidos=0;
					}
					if(productos_repetidos>1){
						$(this).parent().parent().addClass('repetido');
						error=true;
						mensaje=mensaje+" Existen productos repetidos";
					}else{
						$(this).parent().parent().removeClass('repetido');					
					}					
				});
				//Termina quita colores de productos que ya no estan repetidos.
				<?php }?>
				if(!error){
					var totales_subtotal=$("#totales_subtotal").parseNumber()[0];
					var totales_descuento=$("#totales_descuento").parseNumber()[0];
					var totales_neto=$("#totales_neto").parseNumber()[0];
					var totales_iva=$("#totales_iva").parseNumber()[0];
					var totales_total=Math.round($("#totales_total").parseNumber()[0]);
					encabezado={
						nit:nit,
						asesor:asesor,
						plazo: plazo,
						lista:lista,
                                                email_copias:email_copias,
						retencion:retencion,
                                                direccion_envio:direccion_envio,
						observaciones:observaciones,
						totales_subtotal:totales_subtotal,
						totales_descuento:totales_descuento,
						totales_neto:totales_neto,
						totales_iva:totales_iva,
						totales_total:totales_total
					}
                                        mostrar_mensaje('Guardando datos del pedido. Por favor espere...','alerta',0);
					var url=$(this).attr('href');
					var form_data={
                                                ajax:1,
						numero_borrador:$('input[name=borrador]').val(),
						encabezado: encabezado,
						productos: productos						
					};
                                        console.log(form_data);
					$.ajax({
					   type: "POST",
                                           async: false,
					   url: url,
					   data: form_data,
                                           dataType: 'json',
					   success: function(msg){
						   if(msg){
                                                       console.log(msg);
                                                          if(msg['estado']=='error'){
                                                                mostrar_mensaje(msg['mensaje'],'error');
							   }else{
                                                                mostrar_mensaje('El pedido se guardó correctamente. Se va a enviar copia por correo. Por favor espere','correcto');
                                                                $('div#pedido_contenido').html(msg['vista']);
                                                                $('#envio_pedidos').html('<span class="cargando"></span> Enviando correo de los pedidos por favor espere...');
                                                                var url='<?php echo site_url('pedidos/enviar_pedidos');?>';
                                                                var form_data={
                                                                   pedidos:msg['numeros'],
                                                                   vendedor:encabezado['asesor'],
                                                                   cliente:encabezado['nit'],
                                                                   otros:encabezado['email_copias']
                                                                };
                                                                $.ajax({
                                                                   type: "POST",
                                                                   async:true,
                                                                   url: url,
                                                                   data: form_data,
                                                                   dataType: 'json',
                                                                   success: function(msg){
                                                                       if(msg['estado']=='correcto'){
                                                                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                                                                           $('input[name=envio_pedidos_correo]').val(0);
                                                                           $('#envio_pedidos').addClass('correcto');
                                                                       }else{
                                                                           mostrar_mensaje(msg['mensaje'],'error');
                                                                           $('input[name=envio_pedidos_correo]').val(3);
                                                                           $('#envio_pedidos').addClass('error');
                                                                       }
                                                                       $('#envio_pedidos').html(msg['mensaje']);
                                                                   },
                                                                   error: function(x,e){
                                                                       mostrar_error(x,e);                                                                                                                                              
                                                                   }
                                                                });
							   }							   
						   }else{
                                                       mostrar_mensaje('Se presentaron errores inesperados. Por favor intentelo nuevamente.','error');								
                                                       boton.text('Guardar definitivo y enviar');                                                       
						   }
					   },
					   error: function(x,e){
                                                mostrar_error(x,e)
                                                boton.text('Guardar definitivo y enviar');														
					   }
					 });
				}else{
					tiempo_autoguardar = setTimeout(function(){ autoguardar();}, 20000);
					boton.text('Guardar definitivo y enviar');
					mostrar_mensaje('Se presentaron errores. Por favor verifique los datos en rojo y vuelva a intentar guardar el pedido. <br/>'+mensaje,'error');					
				}
			}else{
                            alert('Espere un momento que el pedido se está guardando.');
			}
			return false;
		});//Termina guardar pedido
		function acciones_observaciones(){
		   $('textarea[maxlength]').keyup(function(){
				var max = parseInt($(this).attr('maxlength'));
				if($(this).val().length > max){
					$(this).val($(this).val().substr(0, $(this).attr('maxlength')));
				}
				$(this).parent().find('.faltantes').html((max - $(this).val().length));
			});
                        $('a[rel=agregar_observacion]').click(function(){
				$('textarea[name=observaciones]').val($(this).text());
				 $('textarea[maxlength]').keyup();
				$('input[name=cambios]').val(1);
				return false;
			});
                        $('textarea[maxlength]').change(function(){
				$('input[name=cambios]').val(1);
			});
		}
		$('body').append('<div id="importar_pedido_excel" title="Importar pedido desde archivo de Excel"></div>');
                $("#importar_pedido_excel").dialog({
                                autoOpen:false,
                                resizable: false,
                                width: 800,
                                height:480,
                                modal: true,
                                close: function(event,ui){
                                        $('body').css('overflow','auto');                                        
                                },
                                buttons: {
                                        'Cerrar': function() {
                                                $('body').css('overflow','auto');
                                                $(this).dialog('close');
                                        }
                                }
                });
                $('#importar_pedido_excel form').live('submit',function(){
                        var url=$(this).attr('action');
                        mostrar_mensaje('Cargando el archivo. Por favor espere...','alerta',0);
                        $.ajaxFileUpload({
                                url:url,
                                secureuri:false,
                                fileElementId:'archivo_importar',
                                dataType: 'json',
                                success: function (data, status)
                                {
                                    agregar_productos_cargados(data);                                    
                                    mostrar_mensaje('Se agregaron los productos al pedido. Puede seguir importando mas pedidos','correcto',2000);
                                    $("#importar_pedido_excel").dialog('close');
                                },
                                error: function (x,e)
                                {
                                  mostrar_error(x,e);
                                }
                        });
                        return false;
                });
               $('a[rel=importar_excel]').click(function(){
                       $('body').css('overflow','hidden');			   
                            mostrar_mensaje('Cargando el formulario. Por favor espere...','alerta',0);
                            var form_data={
                               ajax:1
                            };
                            $.ajax({
                               type: "POST",
                               url: "<?php echo site_url('pedidos/importar_subir_archivo');?>",
                               data: form_data,
                               dataType: 'json',
                               success: function(msg){
                                       if(msg['estado']=='correcto'){		
                                           mostrar_mensaje(msg['mensaje'],'correcto',200);
                                           $('#importar_pedido_excel').html(msg['vista']);
                                           $('#importar_pedido_excel').dialog('open');							
                                       }else{
                                            mostrar_mensaje(msg['mensaje'],'error');
                                       }
                               },
                               error: function(x,e){
                                  mostrar_error(x,e);
                               }
                             });					
                            return false;
                    });
	   function verificar_productos_repetidos(){
               var codigos="";
                $("input[name=codigo]").each(function(){
                    codigos=codigos+$(this).val()+' ';
                });
                var productos_repetidos;
                //Quita colores de productos que ya no estan repetidos.
                $("input[name=codigo]").each(function(){
                    if($(this).val()!=""){
                            productos_repetidos=codigos.split($(this).val()).length-1;
                    }else{
                            productos_repetidos=0;
                    }
                    if(productos_repetidos>1){
                            $(this).parent().parent().addClass('repetido');																	
                    }else{
                            $(this).parent().parent().removeClass('repetido');					
                    }					
                });
           }
           var jash=window.location.hash;
           if(jash.length > 1){ 
                $('input[name=borrador]').val(jash.replace('#borrador=',''));                               
           }else{
               <?php 
                    if(!isset($productos_json)){?>
                    $('a[rel=boton_agregar_items]').click();
                <?php }?>
           }
           if($('input[name=borrador]').val()!=''){
                cargar_borrador();
           }
           function cargar_borrador(){
               var borrador=$('input[name=borrador]').val();
               var form_data={
                   borrador:borrador,
                   ajax:1
               }
               mostrar_mensaje('Cargando la información del borrador. Por favor espere...','alerta',0);
               $.ajax({
                   type: "POST",
                   url: "<?php echo site_url('pedidos/cargar_borrador');?>/",
                   data: form_data,
                   dataType:'json',
                   success: function(msg){
                       if(msg['estado']=='correcto'){
                            var encabezado=msg['encabezado'];
                            if(encabezado['email_copias']!=null){
                                $('input[name=email_copias]').val(encabezado['email_copias']);
                            }
                            plazo_pedido=encabezado['plazo'];
                            dcto_pedido=encabezado['descuento'];
                            direccion_envio=encabezado['direccion_envio'];
                            if(encabezado['nit']!=null && encabezado['nit']!=''){
                                    $('input[name=nit]').val(encabezado['nit']);
                                    $('input[name=nit]').change();
                            }
                            var productos=msg['productos'];	
                            if(productos.length>0){
                                            mostrar_mensaje('Cargando los productos. Por favor espere...','alerta',0);
                                            var n=0;
                                            n=parseInt($('#productos_pedido tbody tr').length);
                                            var cantidad=productos.length;
                                            var clase="";
                                            for(var f=0;f<cantidad;f++){
                                                    var producto=JSON.parse(productos[f]);
                                                    if(typeof dcto_pedido == "undefined"){
                                                            dcto_pedido=producto['descuento'];
                                                    }
                                                    var ude='';
                                                    var existencia='';
                                                    <?php if($configuracion['ver_ude']){?>
                                                            ude=producto['unidad'];
                                                    <?php }else{?>
                                                        ude='';
                                                    <?php }?>
                                                    <?php if($configuracion['mostrar_existencia']){?>
                                                            existencia=producto['stock'];
                                                    <?php }else{?>
                                                        existencia='';
                                                    <?php }?>
                                                    var precio=0;
                                                    <?php if($configuracion['lista_tipo_por_defecto']==0){?>
                                                        precio=producto['precio<?php echo $configuracion['lista_numero_por_defecto'];?>'];
                                                    <?php } else{?>
                                                        var lista_cliente=encabezado['lista'];
                                                        if(!lista_cliente){
                                                            lista_cliente=1;                                                            
                                                        }
                                                        precio=producto['precio'+lista_cliente];                                                        
                                                    <?php }?>
                                                    
                                                    agregar_item(n,producto['codigo'],producto['referencia'],producto['nombre'],ude,existencia,producto['cantidad'],precio,dcto_pedido,producto['iva'],0);
                                                    n++;                                                    
                                            }
                                            recalcularTodo();
                                            $('input[name=items_agregar]').val(1);                                            
                            }
                                                                    $('textarea[name=observaciones]').val(encabezado['observaciones']);
                                            $('textarea[name=observaciones]').keyup();
                            var codigos="";

                            $("input[name=codigo]").each(function(){
                                    codigos=codigos+$(this).val()+' ';
                            });
                            <?php if(!$configuracion['repetir_productos']){?>
                            //Quita colores de productos que ya no estan repetidos.
                            $("input[name=codigo]").each(function(){
                                    if($(this).val()!=""){
                                            productos_repetidos=codigos.split($(this).val()).length-1;
                                    }else{
                                            productos_repetidos=0;
                                    }
                                    if(productos_repetidos>1){
                                            $(this).parent().parent().addClass('repetido');
                                    }else{
                                            $(this).parent().parent().removeClass('repetido');					
                                    }					
                            });
                            //Termina quita colores de productos que ya no estan repetidos.
                            <?php }?>
                            $('input[name=cambios]').val(0);
                            mostrar_mensaje('Se ha cargado el borrador.','correcto',200);
                        }else{
                            mostrar_mensaje(msg['mensaje'],'error');
                            window.location.hash='';
                        }
                   },
                   error: function(x,e){
                      mostrar_error(x,e);
                      error_cargando_borrador=true;
                   }
                });	
	   }
	   $('#menu a').click(function(){
            var url=$(this).attr('href');
            if($('input[name=cambios]').val()==1){
                $('#contenido').append('<div id="dialog-confirm" title="Está seguro de salir del pedido?"><span class="icono"></span>El pedido que esta haciendo tiene cambios que no se han guardado, desea guardarlos ahora?</div>');
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height:180,
                    modal: true,
                    dialogClass:'alerta',
                    buttons: {
                        'SI': function() {
                            salir=url;
                            $('a[rel=guardar_borrador]').click();							
                        },
                        'NO': function() {							
                            $(this).dialog('close');
                            location.href=url;
                        },
                        'Cancelar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
                return false;
            }
                        if($('input[name=envio_pedidos_correo]').val()==1){
                                $('#contenido').append('<div id="dialog-confirm" title="Un momento por favor"><span class="icono"></span>El pedido se ha guardado, en este momento se está enviando por correo a la empresa por favor espere</div>');
				$("#dialog-confirm").dialog({
                                    resizable: false,
                                    height:140,
                                    modal: true,
                                    dialogClass:'alerta',
                                    buttons: {
                                        'Listo': function() {
                                                    $(this).dialog('close');
                                                 }
                                    }
				});
				return false;
                        }
		});
    $('a[rel=eliminar_productos_seleccionados]').click(function(){
        var cantidad=$('#productos_pedido tbody input[type=checkbox]:checked').length;
         if(cantidad>0){
                $('#dialog-confirm').dialog('destroy');
                $('#dialog-confirm').remove();
                var mensaje;
                var eliminado;
                if(cantidad>1){
                    $('body').append('<div id="dialog-confirm" title="Eliminar productos seleccionados"><span class="icono"></span>Está seguro de eliminar los '+cantidad+' productos seleccionados?</div>');
                    mensaje='Se estan eliminando los productos seleccionados.';
                    eliminado='Se eliminaron los productos correctamente.';
                }else{
                    $('body').append('<div id="dialog-confirm" title="Eliminar productos seleccionados"><span class="icono"></span>Está seguro de eliminar el producto seleccionado?</div>');
                    mensaje='Se está eliminando el producto seleccionado.';
                    eliminado='Se eliminó el producto correctamente.';
                }
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
                        dialogClass: 'dialog_confirm',
			modal: true,
			buttons: {
				'SI': function() {
                                    mostrar_mensaje(mensaje,'alerta',0);                                  
                                    $('#productos_pedido tbody input[type=checkbox]:checked').each(function(){
                                       $(this).css({background:"#FFD5D5",color:"#CC0000"});
                                       $(this).parent().parent().remove();
                                    });
                                    $('#seleccionar_todos').attr('checked',false);
                                    var repetido=0;
                                    $('input[name=cambios]').val(1);
                                    var productos_agregados=parseInt($('#productos_pedido >tbody >tr').length)-1;
                                    var n=1;
                                    var codigos="";
                                    $('#productos_pedido >tbody >tr').each(function(){
                                            if($(this).attr('class')!='agregar'){
                                                    codigos=codigos+$(this).find("input[name=codigo]").val()+' ';						
                                                    if(n % 2){
                                                            $(this).removeClass('altrow');
                                                    }else{
                                                            $(this).addClass('altrow');
                                                    }
                                                    $(this).attr('id','fila_item_'+n);
                                                    $(this).find('[id^=total_item_]').attr('id','total_item_'+n);
                                                    $(this).find('[id^=referencia_item_]').attr('id','referencia_item_'+n);
                                                    $(this).find('[id^=iva_item_]').attr('id','iva_item_'+n);
                                                    $(this).find('[id^=numero_item_]').attr('id','numero_item_'+n);
                                                    $(this).find('[id^=numero_item_]').find('span.numero').text(n);
                                            }
                                            n++;
                                    });
                                    $('#total_productos_pedido').html('Total de productos en el pedido '+productos_agregados);
                                    if(productos_agregados==0){
                                            $('#total_productos_pedido').html('<span class="alerta"></span> No ha agregado productos al pedido.'); 
                                    }
                                    <?php if(!$configuracion['repetir_productos']){?>
                                    //Quita colores de productos que ya no estan repetidos.
                                    $("input[name=codigo]").each(function(){
                                            if($(this).val()!=""){
                                                    productos_repetidos=codigos.split($(this).val()).length-1;
                                            }else{
                                                    productos_repetidos=0;
                                            }
                                            if(productos_repetidos>1){
                                                    $(this).parent().parent().addClass('repetido');
                                            }else{
                                                    $(this).parent().parent().removeClass('repetido');
                                            }					
                                    });
                                    //Termina quita colores de productos que ya no estan repetidos.
                                    <?php }?>
                                    recalcularTodo();
                                    $('#eliminar_rango_productos').hide();
                                    mostrar_mensaje(eliminado,'correcto',500);
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
				},
				'NO': function() {
					$('#dialog-confirm').dialog('destroy');
                                        $('#dialog-confirm').remove();
                                        $('#dialog-confirm').dialog('close');
				}
			}
		});
        }else{
            alert('No ha seleccionado productos para eliminar');
        }
        return false; 
    });
    $('input[name=email_copias]').change(function(){
                    $(this).removeClass('error');
                    var emails=$(this).val();
                    var correos = emails.split(',');
                    for (var n=0;n< correos.length;n++){
                        if(!es_mail(correos[n])){
                           $(this).addClass('error');
                        }
                    }
    });
    $("input:checkbox[name='select_eliminar']").live('click',function(){
        $("#seleccionar_todos").attr ( "checked" , false );
        if($('#productos_pedido tbody input[type=checkbox]:checked').length>0){
           $('#eliminar_rango_productos').show();
        }else{
            $('#productos_pedido input[name=seleccionar_todos]').attr('checked',false);
            $('#eliminar_rango_productos').hide();
        }       
    });
    $('#seleccionar_todos').live('click',function(){
        if($(this).is(':checked')){
            $("input[name=select_eliminar]").attr ( "checked" ,'checked');
            $('#eliminar_rango_productos').show();
        }else{
            $("input[name=select_eliminar]").removeAttr ( "checked" );
            $('#eliminar_rango_productos').hide();
        }        
    });
    function es_mail(correo){
        var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if(correo.match(expresion)){
            return true;
        }else{
            return false;
        }
    }
    if($('input[name=nit]').val()==''){
            $('input[name=nit]').focus();
    }
    
    var version = navigator.appVersion;
	function showKeyCode(e)
	{
		var keycode =(window.event) ? event.keyCode : e.keyCode;
		if ((version.indexOf('MSIE') != -1))
		{
			if(keycode == 116 || keycode==505)
			{
				//IE
				event.keyCode = 0;
				event.returnValue = false;
				return false;
			}
		}
		else
		{
			if(keycode == 116 || keycode == 505)
			{
				if (e.preventDefault) {
					//IE
					e.preventDefault();
				}
				e.keyCode = 0;
				e.cancelBubble = true;
				e.returnValue = false;
				//e.stopPropagation works in Firefox.
				if (e.stopPropagation) {
					e.stopPropagation();
					e.preventDefault();
				}
				//Firefox, Chrome
				return false;
			}
		}
	}//Termina showKeyCode
	//Validar al presionar F5;
	$(document).keydown(function(event){
		if (event.keyCode == 116 || event.keyCode == 505) {
			// When F5 is pressed
			showKeyCode(event);
		}
	});//Termina keydown.
        
        $('a[rel=buscar_producto]').live('click',function(){
            var campo=$(this).parent().parent().attr('id');
            buscar_producto("<?php echo site_url('pedidos/buscar_producto');?>");
            $('#buscarProducto').attr('item',campo);
            $('#buscarProducto').dialog('open');
            /*var form_data={
                ajax:1
            };
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('pedidos/buscar_producto');?>",
                data: form_data,
                dataType: 'json',
                success: function(msg){
                    if(msg['estado']=='correcto'){				   
                        mostrar_mensaje(msg['mensaje'],'correcto',500);
                        $('#buscarProducto').attr('item',campo);
                        $('#buscarProducto').dialog('open');
                        $('#buscarProducto').html(msg['vista']);
                        $('#buscarProducto a[rel=mostrar_foto]').lightBox({
                            fixedNavigation:true,
                            overlayBgColor:'#FFFFFF',
                            overlayOpacity: 0.5,
                            imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
                            imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
                            imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
                            imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
                            imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
                        });   
                        $('#buscarProducto input[name=producto]').focus();                    
                    }else{
                        mostrar_mensaje(msg['mensaje'],'error');
                    }
                },
                error: function(x,e){
                    mostrar_error(x,e);
                }
            });*/
            $('body').css('overflow','hidden');
            return false;
        });//Termina buscar_producto
        //Busqueda de productos
        function buscar_producto(url){
            var buscar =$('#buscarProducto input[name=producto]').val();
            if(buscar=='Digite el código o nombre del producto'){
                    $('#buscarProducto input[name=producto]').val('');
                    buscar='';
            }
            var lista_cliente=$('select[name=lista]').val();
            if(!lista_cliente){
                lista_cliente=$('input[name=lista_cliente]').val();
            }
            var form_data={
                producto:buscar,
                lista_cliente:lista_cliente,
                ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               dataType: 'json',
               success: function(msg){
                       if(msg['estado']=='correcto'){				   
                           mostrar_mensaje(msg['mensaje'],'correcto',500);
                            $('#buscarProducto').html(msg['vista']);
                            $('#buscarProducto a[rel=mostrar_foto]').lightBox({
                                fixedNavigation:true,
                                overlayBgColor:'#FFFFFF',
                                overlayOpacity: 0.5,
                                imageLoading:'<?php echo base_url();?>img/lightbox/lightbox-ico-loading.gif',
                                imageBtnPrev:'<?php echo base_url();?>img/lightbox/lightbox-btn-prev.gif',
                                imageBtnNext:'<?php echo base_url();?>img/lightbox/lightbox-btn-next.gif',
                                imageBtnClose:'<?php echo base_url();?>img/lightbox/lightbox-btn-close.gif',
                                imageBlank:'<?php echo base_url();?>img/lightbox/lightbox-blank.gif'
                            });                 
                            if(buscar!=''){
                                    $('#buscarProducto #listado_productos').highlight(buscar);                        
                            }
                            columnConform();
                       }else{
                            mostrar_mensaje(msg['mensaje'],'error');
                       }
               }
             });
        }
        $('#buscarProducto form').live('submit',function(){
            mostrar_mensaje('Realizando la búsqueda. Por favor espere...','alerta',0);
            var url=$(this).attr('action');
            buscar_producto(url);
            return false;
	});	
	$('#resultados_buscar_productos th a').live('click',function(){
		mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
		var url=$(this).attr('href');
                buscar_producto(url);		
		return false;
	});
	$('#paginacion_productos a').live('click',function(){
                mostrar_mensaje('Cargando la página. Por favor espere...','alerta',0);
		var url=$(this).attr('href');
		buscar_producto(url);
		return false;
	});
	$('a[rel=boton_seleccionar_producto]').live('click',function(){
		$('body').css('overflow','auto');
                mostrar_mensaje('Agregando el producto al pedido. Por favor espere...','alerta',0);
		var fila=$('#buscarProducto').attr('item');
		$('tr#'+fila).find('input[name=codigo]').val($(this).parent().parent().find('[title=codigo]').text());
		$('tr#'+fila).find('input[name=codigo]').change();
		mostrar_mensaje('El producto fue agregado.','correcto',500);
		$('#buscarProducto').dialog('close');
		return false;
	});
	$('#buscarProducto input[name=producto]').live('focus',function(){
		var valor=$(this).val();
		if(valor=='Digite el código o nombre del producto'){
			$(this).val('');
		}
	});
	$('#buscarProducto input[name=producto]').live('focusout',function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el código o nombre del producto');
		}
	});
	
        var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array();

        function setConformingHeight(el, newHeight) {
                // set the height to something new, but remember the original height in case things change
                el.data("originalHeight", (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight")));
                el.height(newHeight);
        }

        function getOriginalHeight(el) {
                // if the height has changed, send the originalHeight
                return (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight"));
        }

        function columnConform() {
                // find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
                $('#listado_productos li.grid3').each(function() {

                        // "caching"
                        var $el = $(this);

                        var topPosition = $el.position().top;

                        if (currentRowStart != topPosition) {

                                // we just came to a new row.  Set all the heights on the completed row
                                for(currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) setConformingHeight(rowDivs[currentDiv], currentTallest);

                                // set the variables for the new row
                                rowDivs.length = 0; // empty the array
                                currentRowStart = topPosition;
                                currentTallest = getOriginalHeight($el);
                                rowDivs.push($el);

                        } else {

                                // another div on the current row.  Add it to the list and check if it's taller
                                rowDivs.push($el);
                                currentTallest = (currentTallest < getOriginalHeight($el)) ? (getOriginalHeight($el)) : (currentTallest);

                        }
                        // do the last row
                        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) setConformingHeight(rowDivs[currentDiv], currentTallest);

                });

        }


        $(window).resize(function() {
                columnConform();
        });

    // Dom Ready
    // You might also want to wait until window.onload if images are the things that
    // are unequalizing the blocks
    $(function() {
            columnConform();
    });
    $('a[rel=mostrar_ocultar]').live('click',function(){
		var valor=$(this).text();
		if(valor=='Ocultar cartera'){
			$(this).text('Mostrar cartera');
			$('#cartera_cliente').slideUp('fast');
		}else{
			$(this).text('Ocultar cartera');
			$('#cartera_cliente').slideDown('fast');
		}
		return false;
	});
});
</script>
