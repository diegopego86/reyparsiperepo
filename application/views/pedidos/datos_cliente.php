<ul class="formulario">
    
    <li class="left" style="height:50px;width:30%;">
        <p id="nombre_cliente">
            <?php echo $cliente->nombre;?>
        </p>
        <span class="sub_label">Nombre del cliente</span>
    </li>
    <?php if($configuracion['clientes_mostrar_contacto']){?>
    <li class="left" style="height:50px;width:30%;">
        <?php echo $cliente->contacto;?>
        <span class="sub_label">Contacto</span>
    </li>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_email']){?>
    <li class="left" style="height:50px;width:40%;">
        <?php echo $cliente->email;?>
        <span class="sub_label">E-mail</span>
    </li>
    <?php }?>
    <div class="clear"></div>
    <?php if($configuracion['clientes_mostrar_direccion']){?>
    <li class="left" style="height:50px;width:40%;">
        <?php echo $cliente->direccion;?>
        <span class="sub_label">Dirección</span>
    </li>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_ciudad']){?>
    <li class="left" style="height:50px;width:30%;">
        <?php echo $cliente->ciudad;?>
        <span class="sub_label">Ciudad</span>
    </li>
    <?php }?>
    <?php if($configuracion['clientes_mostrar_telefono1']){?>
    <li class="left" style="height:50px;width:30%;">
        <?php echo $cliente->telefono1;?>
        <span class="sub_label">Teléfono</span>
    </li>
    <?php }?>
    <div class="clear"></div>
    <li class="left" style="height:50px;width:45%;">
	<?php 
	$adicional='';
	if(!$configuracion['cambiar_asesor']){
		$adicional='disabled="disabled"';
	}
	 echo form_dropdown('asesor',$vendedores,$cliente->id_vendedor,$adicional);
if(!in_array($cliente->id_vendedor,$this->session->userdata('numeros_vendedor'))){
	echo ' <div style="cursor:pointer;display:inline;color:#930;" title="Usted no aparece como el asesor comercial de este cliente. Por favor elija el número correcto y comuníquese con cartera para su corrección."><span class="alerta"></span>Alerta</div>';
}
?><span class="sub_label">Asesor</span></li>
	<?php if($configuracion['listar_plazos']){?>
    <li class="left" style="height:50px;width:10%;">
            <?php $plazos=json_decode($configuracion['plazos']);
                foreach($plazos as $plazo):
                    $plazos_pedido[$plazo]=$plazo;
                endforeach;
            ?>
            <?php echo form_dropdown('plazo',$plazos_pedido,$cliente->plazo);?>
        <span class="sub_label">Plazo</span>
    </li>
    <?php }?>
	<?php if($configuracion['listar_descuentos']){?>
    <li class="left" style="height:50px;width:10%;">
            <?php $descuentos=json_decode($configuracion['descuentos']);
                foreach($descuentos as $descuento):
                    $descuentos_pedido[$descuento]=$descuento;
                endforeach;
            ?>
            <?php echo form_dropdown('descuento_pedido',$descuentos_pedido);?>
        <span class="sub_label">Descuento</span>
    </li>
    <?php }?>
    <?php $precios=array('1024'=>1,'1025'=>2,'1026'=>3,'1027'=>4);?>
	<?php if($configuracion['listar_listas']){?>
	<li class="left" style="height:50px;width:10%;"><?php  $precio=1; if(isset($precios[$cliente->precio_lista])){$precio=$precios[$cliente->precio_lista];} echo form_dropdown('lista',array('1'=>'1','2'=>'2','3'=>'3'),$precio);?><span class="sub_label">Lista</span></li>
    <?php }else{
        if(isset($precios[$cliente->precio_lista])){$precio=$precios[$cliente->precio_lista];}else{ $precio=1;}
        echo form_hidden('lista_cliente',$precio);        
    }
    ?>
	<?php if($configuracion['listar_retenciones']){?>
    <li class="left" style="height:50px;width:35%;"><?php
echo form_dropdown('retencion',array(''=>'','1'=>'No retiene','2'=>'Retiene sobre la base','3'=>'Retiene sobre cualquier valor','3'=>'Autorretenedor y retiene sobre la base','4'=>'Autorretenedor y retiene sobre cualquier valor'));
?><span class="sub_label">Retención</span></li>
    <?php }?>
    <li class="left" style="height:50px;width:22%;"><?php
echo form_input(array('name'=>'direccion_envio','value'=>'','maxlength'=>'25','size'=>25));?><span class="sub_label">Dirección de envío</span></li>
    <div class="clear"></div>
</ul>
<div style="padding:0px 5px">
    <?php echo anchor('pedidos/productos_mas_pedidos_cliente/'.$cliente->id_cliente,'Ver productos mas pedidos por el cliente','style="margin:10px 0px;" class="boton" target="_blank"');?>
</div>
<?php if($cartera){?>
<div class="seccion"><h2>Cartera del cliente</h2><?php echo anchor('','Mostrar cartera','class="boton" rel="mostrar_ocultar"');?></div>
<div id="cartera_cliente" style="display:none;">
<table class="tabla">
<thead>
<tr><th style="height:30px;" title="Tipo de documento F: Factura NC:Nota crédito ND:Nota débito R:Recibo de caja">Tipo</th><th>Documento</th><th>Fecha</th><th>Vence</th><th title="Dias de vencido">Dias</th><th title="Número del vendedor">Vd</th><th>Valor Inicial</th><th>Saldo Anterior</th><th>Débitos Mes</th><th>Créditos Mes</th><th>Saldo Actual</th></tr>
</thead>
<tbody>
<?php 
$valores_iniciales=0;
$saldos_anteriores=0;
$debitos=0;
$creditos=0;
$saldos_actuales=0;
foreach($cartera as $dato):
$clase='';
$valores_iniciales=$valores_iniciales+$dato->valor_inicial;
$saldos_anteriores=$saldos_anteriores+$dato->saldo_ant;
$debitos=$debitos+$dato->cargo;
$creditos=$creditos+$dato->abono;
$saldos_actuales=$saldos_actuales+$dato->saldo;
if($dato->dias>0){
	$clase='vencido';
}
?>
<tr class="<?php echo $clase;?>">
<td><?php echo $dato->tipo;?></td>
<td><?php echo $dato->id_documento;?></td>
<td style="text-align:center;"><?php echo $dato->fecha;?></td>
<td style="text-align:center;"><?php echo $dato->fecha_vence;?></td>
<td title="Dias de vencido" style="text-align:center;"><?php echo $dato->dias;?></td>
<td style="text-align:center;"><?php echo $dato->id_vendedor;?></td>
<td style="text-align:right;"><?php echo number_format($dato->valor_inicial,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->saldo_ant,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->cargo,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->abono,0,'.',',');?></td>
<td style="text-align:right;"><?php echo number_format($dato->saldo,0,'.',',');?></td></tr>
<?php endforeach;?>
</tbody>
<tfoot style="font-weight:bold;">
<tr style="background:#FFC"><td colspan="6">Totales</td><td style="text-align:right;"><?php echo number_format($valores_iniciales,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_anteriores,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($debitos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($creditos,0,'.',',');?></td><td style="text-align:right;"><?php echo number_format($saldos_actuales,0,'.',',');?></td></tr>
</tfoot>
</table>
</div>
<?php }?>