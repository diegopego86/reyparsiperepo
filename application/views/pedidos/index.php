<h1>Pedidos</h1>
<div id="pedidos">
    <?php echo anchor('pedidos/nuevo','Nuevo pedido','class="boton"');?>
    <?php echo anchor('pedidos/imprimir_rango','Imprimir rango','class="boton" rel="imprimir_rango"  onclick="return false;"');?>
</div>
<div id="lista_borradores">
    
</div>
<div id="lista_pedidos" class="">
    <?php echo $pedidos;?>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var enviando=false;
    $('body').append('<div id="imprimir_rango_pedidos" title="Imprimir rango de pedidos"></div>');
    $('body').append('<div id="enviarMail" title="Enviar Mail"></div>');
    $('#enviarMail').html('<div class="form"><?php echo form_open('');?><ul><li style="text-align:left;"><?php echo form_label('(C.C.) Enviar copias del pedido a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','style'=>'width:100%;'));?></li><li><?php echo form_label('Mensaje adicional :','mensaje'); ?></li><li><?php echo form_textarea(array('name'=>'mensaje','style'=>'width:100%;'));?></li><li><?php echo form_submit('enviar','Enviar');?></li></ul><?php echo form_close();?></div>');
    $("#enviarMail").dialog({
        autoOpen: false,
        resizable: false,
        width: 800,
        height:480,
        modal: true,
        buttons: {
            'Cerrar': function() {
                $(this).dialog('close');
            }                
        }
    });
    $('#enviarMail form').live('submit',function(){
        var url=$(this).attr('action');
        var email=$('#enviarMail input[name=email_copias]').val();
        var mensaje=$('#enviarMail textarea[name=mensaje]').val();
        var error=false;
        mensaje=mensaje.replace(/\n/g,'<br/>');
        if(!enviando){
            enviando=true;
                if(email){
                    mostrar_mensaje('Enviando copia del pedido. Por favor espere...','alerta',0);
                        $('#enviarMail input[name=email_copias]').removeClass('error');
                        var correos = $('#enviarMail input[name=email_copias]').val().split(',');
                        for (var n=0;n< correos.length;n++){
                                if(!es_mail(correos[n])){
                                        error=true;
                                   $('#enviarMail input[name=email_copias]').addClass('error');
                                }
                        }
                }else{
                        $('#enviarMail input[name=email_copias]').addClass('error');
                        error=true;
                }
                $('#enviarMail input[name=email_copias]').next('span').remove();
                if(error){
                        $('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
                }else{
                        var form_data={
                                email:email,
                                mensaje:mensaje,
                                ajax:1
                        };
                        $.ajax({
                           type: "POST",
                           url: url,
                           data: form_data,
                           dataType:'json',
                           success: function(msg){
                               enviando=false;
                                   if(msg['estado']=='correcto'){
                                       mostrar_mensaje(msg['mensaje'],'correcto',500);
                                       $('#enviarMail').dialog('close');
                                   }else{
                                       mostrar_mensaje(msg['mensaje'],'error');										   
                                   }
                           },
                           error: function(x,e){
                               enviando=false;
                                  mostrar_error(x,e);
                           }
                         });					
                }
        }
        return false;
    });
    $('#enviarMail input[name=email_copias]').live('change',function(){
        $('#enviarMail input[name=email_copias]').removeClass('error');
        var error=false;
        var correos = $('#enviarMail input[name=email_copias]').val().split(',');
        $('#enviarMail input[name=email_copias]').next('span').remove();
        if($('#enviarMail input[name=email_copias]').val()!=""){
            for (var n=0;n< correos.length;n++){
                if(!es_mail(correos[n])){
                        error=true;
                   $('#enviarMail input[name=email_copias]').addClass('error');
                }
            }
            if(error){
               $('#enviarMail input[name=email_copias]').after('<span class="error">* Existen email no válidos</span>');
            }
        }else{
           $('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
        }
    });
    $('a[rel=enviar_mail]').click(function(){
        var url=$(this).attr('href');
        $('#enviarMail input[name=email_copias]').val('');
        $('#enviarMail textarea[name=mensaje]').val('');
        $('#enviarMail form').attr('action',url);
        $('#enviarMail').dialog({'title':'Enviar copia del pedido '+$(this).attr('numero')});
        $('#enviarMail').dialog('open');
        return false;
    });
	function es_mail(correo){
		var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		if(correo.match(expresion)){
			return true;
		}else{
			return false;
		}
	}
	var buscar=$('input[name=buscar]').val();
	if(buscar!=''){
		$('table tbody td[title=numero]').highlight(buscar);
		$('table tbody td[title=nit]').highlight(buscar);
		$('table tbody td[title=nombre]').highlight(buscar);
	}
        $('#imprimir_rango_pedidos').dialog({
            autoOpen: false,
            show:'fold',
            hide:'fold',
            height: 250,
            width: 600,
            resizable:false,
            modal: true            
        });
        $('a[rel=imprimir_rango]').click(function(){
            var url=$(this).attr('href');
            var form_data={
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       $('#imprimir_rango_pedidos').html(msg);
                       $('#imprimir_rango_pedidos').dialog('open');                       
               },
               error: function(x,e){
                      mostrar_error(x,e);
               }
           });
           return false; 
        });
        $('#imprimir_rango_pedidos form').live('submit',function(){
            if($('#imprimir_rango_pedidos input[name=rango]').val()==''){
                return false;
            }
           /* var url=$(this).attr('action');
            var form_data={
                    rango:$('#imprimir_rango_pedidos input[name=rango]').val(),
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       $('#cargando').removeClass('correcto error');
                       $('#imprimir_rango_pedidos').html(msg);
                       $('#imprimir_rango_pedidos').dialog('open');
                       if(msg){
                               $('#cargando').html('Enviado correctamente');
                       }else{
                               $('#cargando').html('Se presentaron errores. Inténtelo nuevamente.');
                               $('#cargando').addClass('error');
                               alert("error");
                       }
               },
               error: function(x,e){
                      mostrar_error(x,e);
               }
            });
            return false;*/
            $('#imprimir_rango_pedidos').dialog('close');
        });
});
</script>
