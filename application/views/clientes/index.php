<div class="form grid16">
    <h1>Clientes</h1>
    <?php
    echo form_open('clientes');
    echo form_label('Cliente: ','cliente');
    if(!$buscar){
        $buscar='Digite el nit o nombre del cliente';
    }
    echo form_input(array('name'=>'cliente','value'=>set_value('cliente',$buscar),'style'=>'width:400px;'));
    if(count($vendedores)>1){
        if($this->session->userdata('rol')==1){
            $lista=array(0=>'Buscar en todos los vendedores');
        }else{
            $lista=array(0=>'Buscar en todos mis números de vendedor');
        }
    }else{
        $lista=array();
    }
    foreach($vendedores as $vd){
        $lista[$vd]=$vd;
    }
    echo form_dropdown('vendedor',$lista,$vendedor);
    echo form_submit('buscar','Buscar');
    echo form_close();
    //echo anchor('pedidos/buscar_producto','Agregar/Buscar','class="boton" id="boton_buscar_producto"');
    ?>
    <div id="buscarCliente">
		<?php if($clientes){?>
        <div class="resultados"><?php echo $this->pagination->resultados();?></div>
        <div id="paginacion_clientes" class="paginacion"><?php echo $this->pagination->create_links();?></div>
            <table id="clientes_buscar_clientes" class="tabla">
                <thead>
                    <tr>
                        <th style="width:10%;"><?php echo $this->pagination->ordenar('id_cliente','Nit','title="Nit del cliente"');?></th>
                        <th style="width:20%;"><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
                        <th style="width:20%;"><?php echo $this->pagination->ordenar('contacto','Contacto');?></th>
                        <th style="width:10%;"><?php echo $this->pagination->ordenar('id_vendedor','Vd');?></th>
                        <th style="width:10%;"><?php echo $this->pagination->ordenar('ciudad','Ciudad');?></th>
                        <th style="width:10%;"><?php echo $this->pagination->ordenar('direccion','Direccion');?></th>
                        <th style="width:10%;"><?php echo $this->pagination->ordenar('telefono1','Teléfono');?></th>
                        <th style="width:20%;"><?php echo $this->pagination->ordenar('email','E-mail');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($clientes as $cliente):?>
                    <tr>
                        <td><?php echo $cliente->id_cliente;?></td>
                        <td><?php echo $cliente->nombre;?></td>
                        <td><?php echo $cliente->contacto;?></td>
                        <td><?php echo $cliente->id_vendedor;?></td>
                        <td><?php echo $cliente->ciudad;?></td>
                        <td><?php echo $cliente->direccion;?></td>
                        <td><?php echo $cliente->telefono1;?></td>
                        <td><?php echo $cliente->email;?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php }else{?>
            <table class="tabla">
                <thead>
                    <tr><th style="height:30px;">Nit</th><th>Nombre</th><th>Contacto</th></tr>
                </thead>
                <tbody>
                    <tr><td colspan="3"><span class="alerta"></span> No existen clientes con los datos de busqueda.</td></tr>
                </tbody>
            </table>
            <div id="paginacion_clientes" class="paginacion"><?php echo $this->pagination->create_links();?></div>
        <?php }?>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	/*$('#resultados_buscar_productos th a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
			$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	/*$('#paginacion_productos a').click(function(){
		$('#cargando').show();
		var url=$(this).attr('href');
		var form_data={
			ajax:'1'
		};
		$.ajax({
		   type: "POST",
		   url: url,
		   data: form_data,
		   success: function(msg){
				$('#cargando').hide();
			   if(msg){				   
				$('#buscarProducto').html(msg);
			   }else{
				   alert("error");
			   }
		   }
		 });
		return false;
	});*/
	$('input[name=cliente]').focus(function(){
		var valor=$(this).val();
		if(valor=='Digite el nit o nombre del cliente'){
			$(this).val('');
		}
	});
	$('input[name=cliente]').focusout(function(){
		var valor=$(this).val();
		if(valor==''){
			$(this).val('Digite el nit o nombre del cliente');
		}
	});	
	var buscar=$('input[name=cliente]').val();
	if(buscar!=''){
		$('#buscarCliente').highlight(buscar);
	}	
});
</script>