<h1>Administrar - Cartera - Configurar</h1>
<div id="configuraciones">
    
    <div class="grid16 alpha omega">
        <h2>Configuracion general de cartera</h2>
        <table class="configuracion" width="100%">
            <tr>
                <td>Dias de vencimientos (A partir de estos dias desde la fecha del documento se resaltan en rojo)</td>
                <td class="opcion">
                <?php echo form_input(array('name'=>'dias_vencimiento','size'=>3,'value'=>$configuracion_cartera->dias_vencimiento)); ?>
                </td>
            </tr>
        </table>        
    </div>
<div class="clear"></div>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="cuadro_accion"></div>');
    $("#cuadro_accion").dialog({
        autoOpen:false,
        resizable: true,
        width: 600,
        height:300,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                
        },
        buttons: {
                'Cerrar': function() {
                        $('body').css('overflow','auto');
                        $(this).dialog('close');
                }
        }
   });
   $('input[name=dias_vencimiento]').change(function(){
       var url='<?php echo site_url('administrar/cartera/modificar_configuracion/');?>/'+$(this).attr('name');
       var valor=$(this).val();
       url=url+'/'+valor;
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });   
});
</script>