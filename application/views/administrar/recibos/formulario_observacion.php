<div class="form">
    <?php 
    $detalle='';
    $mostrar=1;
    $orden=0;
    $id='';
    if(isset($observacion)){
        $id=$observacion->id;
        $detalle=$observacion->detalle;
        $mostrar=$observacion->visible;        
    }
    echo form_open('administrar/recibos/crear_editar_observacion', array('onsubmit'=>'return false;','id'=>'observaciones'));
    echo form_hidden('id',$id);
    ?>
    <div>
    <?php    
    echo form_label('Detalle:','detalle');
    echo form_input(array('name'=>'detalle','value'=>$detalle,'size'=>100,'maxlength'=>150));
    ?>
    </div>
    <div style="margin-top: 10px;">
    <?php
    echo form_label('Mostrar:','visible');
    echo form_dropdown('visible',array('1'=>'Si','0'=>'No'),$mostrar);
    ?>
    </div>
    <div style="margin-top: 10px;">
    <?php
    if($id){
        echo form_submit('editar','Editar');
    }else{
        echo form_submit('crear','Crear');
    }
    ?>
    </div>
    <?php
    echo form_close();
    ?>    
</div>