<div class="form">
    <?php 
    $id='';
    $descripcion='';
    $correo_texto='';
    if(isset($correo)){
        $id=$correo->id;
        $descripcion=$correo->descripcion;
        $correo_texto=$correo->correo;        
    }
    echo form_open('administrar/recibos/crear_editar_correo', array('onsubmit'=>'return false;','id'=>'correos'));
    echo form_hidden('id',$id);
    ?>
    <div>
    <?php    
    echo form_label('Descripción:','descripcion');
    echo form_input(array('name'=>'descripcion','value'=>$descripcion,'size'=>100,'maxlength'=>140));
    ?>
    </div>
    <div style="margin-top: 10px;">
    <?php
    echo form_label('Correo:','correo');
    echo form_input(array('name'=>'correo','value'=>$correo_texto,'size'=>100,'maxlength'=>140));
    ?>
    </div>
    <div style="margin-top: 10px;">
    <?php
    if($id){
        echo form_submit('editar','Editar');
    }else{
        echo form_submit('crear','Crear');
    }
    ?>
    </div>
    <?php
    echo form_close();
    ?>    
</div>