<h1>Administrar - Recibos - Configurar</h1>
<div id="configuraciones">
    
    <div class="grid16 alpha omega">
        <h2>Correos:</h2>
        <span style="color:#AAA;padding-bottom:5px;display:inline-block;">Correos donde serán enviados los recibos</span>
        <div style="margin-bottom: 10px;">
            <?php echo anchor('administrar/recibos/formulario_correo','Agregar correo','class="boton" rel="agregar_correo" onclick="return false;"');?>
        </div>
        <table class="tabla" width="100%">
            <thead>
                <tr>
                    <th>Descripción</th>
                    <th>Correo</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($correos as $correo):
                ?>
                <tr>
                    <td><?php echo $correo->descripcion;?></td>
                    <td title="Correo"><?php echo $correo->correo;?></td>
                    <td style="text-align: center;"><?php echo anchor('administrar/recibos/formulario_correo/'.$correo->id,'<span class="iconos_rojo editar"></span>','class="boton boton_gris peque" rel="editar_correo" onclick="return false;"');?> <?php echo anchor('administrar/recibos/eliminar_correo/'.$correo->id,'<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_correo" onclick="return false;"');?></td>
                </tr>
                <?php
                    endforeach;
                ?>
            </tbody>
        </table>
        <table class="configuracion" width="100%">
            <tr>
                <td>Enviar copia al cliente</td>
                <?php if($configuracion_recibo->enviar_copia_cliente==1){ $clase='si';}else{ $clase='no';}?>
                <td class="opcion"><?php echo anchor('administrar/recibos/modificar_configuracion/enviar_copia_cliente','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
            <tr>
                <td>Enviar copia al vendedor</td>
                <?php if($configuracion_recibo->enviar_copia_vendedor==1){ $clase='si';}else{ $clase='no';}?>
                <td class="opcion"><?php echo anchor('administrar/recibos/modificar_configuracion/enviar_copia_vendedor','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
        </table>
        <h2>Notificaciones</h2>
        <table class="configuracion" width="100%">
            <tr>
                <td>Enviar notificaciones de cheques posfechados por correo</td>
                <?php if($configuracion_recibo->enviar_notificacion_posfechados==1){ $clase='si';}else{ $clase='no';}?>
                <td class="opcion"><?php echo anchor('administrar/recibos/modificar_configuracion/enviar_notificacion_posfechados','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
            <tr>
                <td>Enviar notificaciones de cheques posfechados por correo a los vendedores</td>
                <?php if($configuracion_recibo->enviar_notificacion_posfechados_vendedores==1){ $clase='si';}else{ $clase='no';}?>
                <td class="opcion"><?php echo anchor('administrar/recibos/modificar_configuracion/enviar_notificacion_posfechados_vendedores','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
        </table>
        <table class="configuracion" width="100%">
            <tr>
                <td>Máximo de consignaciones por recibo</td>
                <td class="opcion"><?php echo form_input(array('name'=>'maximo_consignaciones_recibo','size'=>3,'value'=>$configuracion_recibo->maximo_consignaciones_recibo)); ?><span style="color: #900; font-size: 11px;margin-top:4px;display:block; text-align: right;">0 para ilimitado</span></td>
            </tr>
        </table>
        <h2>Observaciones predeterminadas:</h2>
        <div style="margin-bottom: 10px;">
            <?php echo anchor('administrar/recibos/formulario_observacion','Crear observación','class="boton" rel="crear_observacion" onclick="return false;"');?>
        </div>
        <table class="tabla">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Detalle</th>
                    <th width="50px">Mostrar</th>
                    <th width="60px">-</th>
                </tr>
            </thead>
            <tbody>
                <?php $n=1; foreach($observaciones_predeterminadas as $observacion):?>
                <tr>
                    <td><?php echo $n;?></td>
                    <td title="Detalle"><?php echo $observacion->detalle;?></td>
                    <td style="text-align: center;"><?php echo ($observacion->visible==1) ? "Si":"No";?></td>
                    <td><?php echo anchor('administrar/recibos/formulario_observacion/'.$observacion->id,'<span class="iconos_rojo editar"></span>','class="boton boton_gris peque" rel="editar_observacion" onclick="return false;"');?> <?php echo anchor('administrar/recibos/eliminar_observacion/'.$observacion->id,'<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_observacion" onclick="return false;"');?></td>
                </tr>
                    <?php $n++; endforeach;?>
            </tbody>
        </table>
    </div>
<div class="clear"></div>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="cuadro_accion"></div>');
    $('#configuraciones a[rel="modificar_configuracion"]').click(function(){
       mostrar_mensaje('Se está guardando la configuración','alerta',0);
       var url=$(this).attr('href');
       var nueva_clase='';
       var obj=$(this);
       if($(this).hasClass('si')){
           url=url+'/0';
           nueva_clase='no';
       }else{
           url=url+'/1';
           nueva_clase='si';
       }
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);
                   obj.removeClass('si no');
                   obj.addClass(nueva_clase);
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
    $("#cuadro_accion").dialog({
        autoOpen:false,
        resizable: true,
        width: 600,
        height:300,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                
        },
        buttons: {
                'Cerrar': function() {
                        $('body').css('overflow','auto');
                        $(this).dialog('close');
                }
        }
   });
   $('#cuadro_accion form#observaciones').live('submit',function(){
       var url=$(this).attr('action');
       var form_data={
           id: $(this).find('input[name=id]').val(),
           detalle: $(this).find('input[name=detalle]').val(),
           visible: $(this).find('select[name=visible]').val()
       };
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType:'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   $('cuadro_accion').dialog('close');
                   $('cuadro_accion').html('');
                   mostrar_mensaje(msg['mensaje'],'correcto');
                   location.href='<?php echo site_url('administrar/recibos/configurar');?>';
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('#cuadro_accion form#correos').live('submit',function(){
       var url=$(this).attr('action');
       var form_data={
           id: $(this).find('input[name=id]').val(),
           descripcion: $(this).find('input[name=descripcion]').val(),
           correo: $(this).find('input[name=correo]').val()
       };
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType:'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   $('cuadro_accion').dialog('close');
                   $('cuadro_accion').html('');
                   mostrar_mensaje(msg['mensaje'],'correcto');
                   location.href='<?php echo site_url('administrar/recibos/configurar');?>';
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=crear_observacion]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Crear observación'});
               $('#cuadro_accion').dialog("open");                                               
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=editar_observacion]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Editar observación'});
               $('#cuadro_accion').dialog("open");                                                
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=eliminar_observacion]').click(function(){
        var url=$(this).attr('href');
        var obj=$(this).parent().parent();
        var detalle=obj.children('[title=Detalle]').text();
        $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar la observación:  '+detalle+' ?</div>');
        $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                mostrar_mensaje('Se está eliminando la observación. Por favor espere...','alerta',0);
                                var form_data={
                                        ajax:1
                                };
                                $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: form_data,
                                        dataType: 'json',
                                        success: function(msg){
                                                if(msg['estado']=='correcto'){
                                                        obj.fadeOut('slow',function(){ $(this).remove();});
                                                        mostrar_mensaje(msg['mensaje'],'correcto');								
                                                }else{
                                                    mostrar_mensaje(msg['mensaje'],'error',10000);								
                                                }
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');
                                        },
                                        error: function(x,e){
                                                mostrar_error(x,e)
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');							
                                        }
                                });
                                //location.href=url;
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
        return false;
   });
   $('a[rel=agregar_correo]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Agregar correo'});
               $('#cuadro_accion').dialog("open");                                                 
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=editar_correo]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Editar correo'});
               $('#cuadro_accion').dialog("open");                                                 
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=eliminar_correo]').click(function(){
        var url=$(this).attr('href');
        var obj=$(this).parent().parent();
        var correo=obj.children('[title=Correo]').text();
        $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el correo:  '+correo+' ?</div>');
        $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                mostrar_mensaje('Se está eliminando la observación. Por favor espere...','alerta',0);
                                var form_data={
                                        ajax:1
                                };
                                $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: form_data,
                                        dataType: 'json',
                                        success: function(msg){
                                                if(msg['estado']=='correcto'){
                                                        obj.fadeOut('slow',function(){ $(this).remove();});
                                                        mostrar_mensaje(msg['mensaje'],'correcto');								
                                                }else{
                                                    mostrar_mensaje(msg['mensaje'],'error',10000);								
                                                }
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');
                                        },
                                        error: function(x,e){
                                                mostrar_error(x,e)
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');							
                                        }
                                });
                                //location.href=url;
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
        return false;
   });
   $('input[name=maximo_consignaciones_recibo]').change(function(){
       var url='<?php echo site_url('administrar/recibos/modificar_configuracion/');?>/'+$(this).attr('name');
       var valor=$(this).val();
       url=url+'/'+valor;
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
});
</script>