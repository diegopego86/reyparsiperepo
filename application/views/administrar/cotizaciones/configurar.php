<h1>Administrar - Cotizaciones - Configurar</h1>
<div id="configuraciones">
<div class="alpha grid8">
    <h2>Configuracion general del cotizacion</h2>
    <table class="configuracion" width="100%">
        <tr>
            <td>Máximo de items por cotizacion</td>
            <td class="opcion"><?php echo form_input(array('name'=>'maximo_items','size'=>3,'value'=>$configuracion_cotizacion->maximo_items)); ?><span style="color: #900; font-size: 11px;margin-top:4px;display:block; text-align: right;">0 para ilimitado</span></td>
        </tr>
    </table>
    <h2>Configuracion datos del cliente</h2>
    <table class="configuracion" width="100%">
        <tr>
            <td>Cambiar asesor</td>
            <?php if($configuracion_cotizacion->cambiar_asesor==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/cambiar_asesor','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Listar retenciones</td>
            <?php if($configuracion_cotizacion->listar_retenciones==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/listar_retenciones','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Listar listas de precio</td>
            <?php if($configuracion_cotizacion->listar_listas==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/listar_listas','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Listar plazos</td>
            <?php if($configuracion_cotizacion->listar_plazos==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/listar_plazos','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Plazos:</td>
            <td>
                <div style="margin-bottom: 10px;">
                    <?php echo anchor('administrar/cotizaciones/agregar_plazos','Agregar plazo','class="boton" rel="agregar_plazo" onclick="return false;"');?>
                </div>
                <table id="plazos" class="tabla" width="100%">
                    <thead>
                        <tr>
                            <th>Plazo</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $plazos=json_decode($configuracion_cotizacion->plazos);
                            if(is_array($plazos)){
                                foreach($plazos as $plazo):                                    
                                ?>
                                <tr>
                                    <td><?php echo form_input('plazo',$plazo);?></td>
                                    <td style="text-align: center;">
                                        <?php echo anchor('','<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_plazo" onclick="return false;"');?>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                            }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>Listar descuentos</td>
            <?php if($configuracion_cotizacion->listar_descuentos==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/listar_descuentos','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Descuentos:</td>
            <td>
                <div style="margin-bottom: 10px;">
                    <?php echo anchor('administrar/cotizaciones/agregar_descuentos','Agregar descuento','class="boton" rel="agregar_descuento" onclick="return false;"');?>
                </div>
                <table id="descuentos" class="tabla" width="100%">
                    <thead>
                        <tr>
                            <th>Descuento</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $descuentos=json_decode($configuracion_cotizacion->descuentos);
                            if(is_array($descuentos)){
                                foreach($descuentos as $descuento):                                    
                                ?>
                                <tr>
                                    <td><?php echo form_input('descuento',$descuento);?></td>
                                    <td style="text-align: center;">
                                        <?php echo anchor('','<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_descuento" onclick="return false;"');?>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                            }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <h2>Configuracion datos de los productos</h2>
    <table class="configuracion" width="100%">
        <tr>
            <td>Mostrar unidad de empaque</td>
            <?php if($configuracion_cotizacion->ver_ude==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/ver_ude','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Modificar precio</td>
            <?php if($configuracion_cotizacion->modificar_precio==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/modificar_precio','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Modificar descuento</td>
            <?php if($configuracion_cotizacion->modificar_dcto==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/modificar_dcto','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Tipo de Lista de precios por defecto</td>
            <td class="opcion">
                <?php 
                    if($configuracion_cotizacion->lista_tipo_por_defecto==1){
                        $tp_cliente=true;
                        $tp_config=false;
                    }else{
                        $tp_cliente=false;
                        $tp_config=true;
                    }
                    echo form_label(form_radio(array('name'=> 'lista_tipo_por_defecto','id'=> 'tipo_lista_por_defecto_cliente','value'=> 1,'checked'=>$tp_cliente)).' Lista del archivo de clientes','tipo_lista_por_defecto_cliente');
                    echo '<br/>';
                    echo form_label(form_radio(array('name'=> 'lista_tipo_por_defecto','id'=> 'tipo_lista_por_defecto_configuracion','value'=> 0,'checked'=>$tp_config)).' La lista seleccionada: ','tipo_lista_por_defecto_configuracion');
                ?>
            </td>
        </tr>
        <tr>
            <td>Número de Lista de precios por defecto</td>
            <td class="opcion"><?php echo form_dropdown('lista_numero_por_defecto',array('1'=>'Lista 1','2'=>'Lista 2','3'=>'Lista 3','4'=>'Lista 4'),$configuracion_cotizacion->lista_numero_por_defecto);?></td>
        </tr>                
    </table>    
</div>
<div class="omega grid8">
    <h2>Correos:</h2>
    <span style="color:#AAA;padding-bottom:5px;display:inline-block;">Correos donde serán enviados los cotizaciones</span>
    <div style="margin-bottom: 10px;">
        <?php echo anchor('administrar/cotizaciones/formulario_correo','Agregar correo','class="boton" rel="agregar_correo" onclick="return false;"');?>
    </div>
    <table class="tabla" width="100%">
        <thead>
            <tr>
                <th>Descripción</th>
                <th>Correo</th>
                <th>-</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach($correos as $correo):
            ?>
            <tr>
                <td><?php echo $correo->descripcion;?></td>
                <td title="Correo"><?php echo $correo->correo;?></td>
                <td style="text-align: center;"><?php echo anchor('administrar/cotizaciones/formulario_correo/'.$correo->id,'<span class="iconos_rojo editar"></span>','class="boton boton_gris peque" rel="editar_correo" onclick="return false;"');?> <?php echo anchor('administrar/cotizaciones/eliminar_correo/'.$correo->id,'<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_correo" onclick="return false;"');?></td>
            </tr>
            <?php
                endforeach;
            ?>
        </tbody>
    </table>
    <table class="configuracion" width="100%">
        <tr>
            <td>Enviar copia al cliente</td>
            <?php if($configuracion_cotizacion->enviar_copia_cliente==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/enviar_copia_cliente','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Enviar copia al vendedor</td>
            <?php if($configuracion_cotizacion->enviar_copia_vendedor==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/enviar_copia_vendedor','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
    </table>
    <h2>Buscador de productos del cotizacion</h2>
    <table class="configuracion" width="100%">
        <tr>
            <td>Mostrar unidad de empaque</td>
            <?php if($configuracion_cotizacion->productos_mostrar_ude==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_ude','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar precio</td>
            <?php if($configuracion_cotizacion->productos_mostrar_precio==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_precio','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar listas de precio</td>
            <?php if($configuracion_cotizacion->productos_mostrar_listas==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_listas','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar tipo</td>
            <?php if($configuracion_cotizacion->productos_mostrar_tipo==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_tipo','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar referencia</td>
            <?php if($configuracion_cotizacion->productos_mostrar_referencia==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_referencia','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar existencia</td>
            <?php if($configuracion_cotizacion->productos_mostrar_existencia==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/productos_mostrar_existencia','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Tipo de Lista de precios por defecto</td>
            <td class="opcion">
                <?php
                    if($configuracion_cotizacion->productos_lista_tipo_por_defecto==1){
                        $tp_cliente=true;
                        $tp_config=false;
                    }else{
                        $tp_cliente=false;
                        $tp_config=true;
                    }
                    echo form_label(form_radio(array('name'=> 'productos_lista_tipo_por_defecto','id'=> 'productos_tipo_lista_por_defecto_cliente','value'=> 1,'checked'=>$tp_cliente)).' Lista del archivo de clientes','productos_tipo_lista_por_defecto_cliente');
                    echo '<br/>';
                    echo form_label(form_radio(array('name'=> 'productos_lista_tipo_por_defecto','id'=> 'productos_tipo_lista_por_defecto_configuracion','value'=> 0,'checked'=>$tp_config)).' La lista seleccionada: ','productos_tipo_lista_por_defecto_configuracion');
                ?>
            </td>
        </tr>
        <tr>
            <td>Número de Lista de precios por defecto</td>
            <td class="opcion"><?php echo form_dropdown('productos_lista_numero_por_defecto',array('1'=>'Lista 1','2'=>'Lista 2','3'=>'Lista 3','4'=>'Lista 4'),$configuracion_cotizacion->productos_lista_numero_por_defecto);?></td>
        </tr>        
    </table>
    <h2>Buscador de clientes del cotizacion</h2>
    <table class="configuracion" width="100%">
        <tr>
            <td>Mostrar contacto</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_contacto==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_contacto','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar vendedor</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_vendedor==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_vendedor','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar ciudad</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_ciudad==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_ciudad','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar dirección</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_direccion==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_direccion','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar teléfono 1</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_telefono1==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_telefono1','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar teléfono 2</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_telefono2==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_telefono2','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>
        <tr>
            <td>Mostrar e-mail</td>
            <?php if($configuracion_cotizacion->clientes_mostrar_email==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/cotizaciones/modificar_configuracion/clientes_mostrar_email','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
        </tr>              
    </table>    
</div>
<div class="clear"></div>
<div class="grid16 alpha omega">
    <h2>Observaciones predeterminadas:</h2>
    <div style="margin-bottom: 10px;">
        <?php echo anchor('administrar/cotizaciones/formulario_observacion','Crear observación','class="boton" rel="crear_observacion" onclick="return false;"');?>
    </div>
    <table class="tabla">
        <thead>
            <tr>
                <th>#</th>
                <th>Detalle</th>
                <th width="50px">Mostrar</th>
                <th width="60px">-</th>
            </tr>
        </thead>
        <tbody>
            <?php $n=1; foreach($observaciones_predeterminadas as $observacion):?>
            <tr>
                <td><?php echo $n;?></td>
                <td title="Detalle"><?php echo $observacion->detalle;?></td>
                <td style="text-align: center;"><?php echo ($observacion->visible==1) ? "Si":"No";?></td>
                <td><?php echo anchor('administrar/cotizaciones/formulario_observacion/'.$observacion->id,'<span class="iconos_rojo editar"></span>','class="boton boton_gris peque" rel="editar_observacion" onclick="return false;"');?> <?php echo anchor('administrar/cotizaciones/eliminar_observacion/'.$observacion->id,'<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_observacion" onclick="return false;"');?></td>
            </tr>
                <?php $n++; endforeach;?>
        </tbody>
    </table>
</div>
<div class="clear"></div>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="cuadro_accion"></div>');
    $("#cuadro_accion").dialog({
        autoOpen:false,
        resizable: true,
        width: 600,
        height:300,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                
        },
        buttons: {
                'Cerrar': function() {
                        $('body').css('overflow','auto');
                        $(this).dialog('close');
                }
        }
   });
   $('a[rel=agregar_plazo]').click(function(){
    $('#plazos tbody').append('<tr>'
        +'<td><?php echo form_input('plazo');?></td>'
        +'<td style="text-align: center;"><?php echo anchor('','<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_plazo" onclick="return false;"');?></td>'
        +'</tr>');      
    return false;
   });
   $('input[name=plazo]').live('change',function(){
       actualizar_plazos();
   });
   $('a[rel=eliminar_plazo]').live('click',function(){
       var obj=$(this).parent().parent();
       $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el plazo?</div>');
       $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                obj.remove();
                                actualizar_plazos();
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
      return false; 
   });
   function actualizar_plazos(){
       var url='<?php echo site_url('administrar/cotizaciones/actualizar_plazos');?>';
       var plazos=[];
       $('#plazos tbody tr').each(function(){
          plazos.push($(this).find('input[name=plazo]').val());
       });
       var form_data={
           plazos:plazos,
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   }
   $('a[rel=agregar_descuento]').click(function(){
    $('#descuentos tbody').append('<tr>'
        +'<td><?php echo form_input('descuento');?></td>'
        +'<td style="text-align: center;"><?php echo anchor('','<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_descuento" onclick="return false;"');?></td>'
        +'</tr>');      
    return false;
   });
   $('input[name=descuento]').live('change',function(){
       actualizar_descuentos();
   });
   $('a[rel=eliminar_descuento]').live('click',function(){
       var obj=$(this).parent().parent();
       $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el descuento?</div>');
       $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                obj.remove();
                                actualizar_descuentos();
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
      return false; 
   });
   function actualizar_descuentos(){
       var url='<?php echo site_url('administrar/cotizaciones/actualizar_descuentos');?>';
       var descuentos=[];
       $('#descuentos tbody tr').each(function(){
          descuentos.push($(this).find('input[name=descuento]').val());
       });
       var form_data={
           descuentos:descuentos,
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   }
   $('#configuraciones a[rel="modificar_configuracion"]').click(function(){
       mostrar_mensaje('Se está guardando la configuración','alerta',0);
       var url=$(this).attr('href');
       var nueva_clase='';
       var obj=$(this);
       if($(this).hasClass('si')){
           url=url+'/0';
           nueva_clase='no';
       }else{
           url=url+'/1';
           nueva_clase='si';
       }
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);
                   obj.removeClass('si no');
                   obj.addClass(nueva_clase);
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
   $('input[name=lista_tipo_por_defecto],input[name=productos_lista_tipo_por_defecto]').change(function(){
       var url='<?php echo site_url('administrar/cotizaciones/modificar_configuracion/');?>/'+$(this).attr('name');
       var valor=$('input:radio[name='+$(this).attr('name')+']:checked').val();
       url=url+'/'+valor;
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
   $('select[name=lista_numero_por_defecto],select[name=productos_lista_numero_por_defecto],input[name=maximo_items]').change(function(){
       var url='<?php echo site_url('administrar/cotizaciones/modificar_configuracion/');?>/'+$(this).attr('name');
       var valor=$(this).val();
       url=url+'/'+valor;
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
   $('#cuadro_accion form#observaciones').live('submit',function(){
       var url=$(this).attr('action');
       var form_data={
           id: $(this).find('input[name=id]').val(),
           detalle: $(this).find('input[name=detalle]').val(),
           visible: $(this).find('select[name=visible]').val()
       };
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType:'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   $('cuadro_accion').dialog('close');
                   $('cuadro_accion').html('');
                   mostrar_mensaje(msg['mensaje'],'correcto');
                   location.href='<?php echo site_url('administrar/cotizaciones/configurar');?>';
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('#cuadro_accion form#correos').live('submit',function(){
       var url=$(this).attr('action');
       var form_data={
           id: $(this).find('input[name=id]').val(),
           descripcion: $(this).find('input[name=descripcion]').val(),
           correo: $(this).find('input[name=correo]').val()
       };
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType:'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   $('cuadro_accion').dialog('close');
                   $('cuadro_accion').html('');
                   mostrar_mensaje(msg['mensaje'],'correcto');
                   location.href='<?php echo site_url('administrar/cotizaciones/configurar');?>';
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=crear_observacion]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Crear observación'});
               $('#cuadro_accion').dialog("open");               
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=editar_observacion]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Editar observación'});
               $('#cuadro_accion').dialog("open");                                                 
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=eliminar_observacion]').click(function(){
        var url=$(this).attr('href');
        var obj=$(this).parent().parent();
        var detalle=obj.children('[title=Detalle]').text();
        $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar la observación:  '+detalle+' ?</div>');
        $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                mostrar_mensaje('Se está eliminando la observación. Por favor espere...','alerta',0);
                                var form_data={
                                        ajax:1
                                };
                                $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: form_data,
                                        dataType: 'json',
                                        success: function(msg){
                                                if(msg['estado']=='correcto'){
                                                        obj.fadeOut('slow',function(){ $(this).remove();});
                                                        mostrar_mensaje(msg['mensaje'],'correcto');								
                                                }else{
                                                    mostrar_mensaje(msg['mensaje'],'error',10000);								
                                                }
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');
                                        },
                                        error: function(x,e){
                                                mostrar_error(x,e)
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');							
                                        }
                                });
                                //location.href=url;
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
        return false;
   });
   $('a[rel=agregar_correo]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Agregar correo'});
               $('#cuadro_accion').dialog("open");                                                 
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=editar_correo]').click(function(){
       var url=$(this).attr('href');
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           success: function(msg){
               $('#cuadro_accion').html(msg);
               $('#cuadro_accion').dialog({'title':'Editar correo'});
               $('#cuadro_accion').dialog("open");                                                
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });       
   });
   $('a[rel=eliminar_correo]').click(function(){
        var url=$(this).attr('href');
        var obj=$(this).parent().parent();
        var correo=obj.children('[title=Correo]').text();
        $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el correo:  '+correo+' ?</div>');
        $("#dialog-confirm").dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                        'SI': function() {
                                mostrar_mensaje('Se está eliminando la observación. Por favor espere...','alerta',0);
                                var form_data={
                                        ajax:1
                                };
                                $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: form_data,
                                        dataType: 'json',
                                        success: function(msg){
                                                if(msg['estado']=='correcto'){
                                                        obj.fadeOut('slow',function(){ $(this).remove();});
                                                        mostrar_mensaje(msg['mensaje'],'correcto');								
                                                }else{
                                                    mostrar_mensaje(msg['mensaje'],'error',10000);								
                                                }
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');
                                        },
                                        error: function(x,e){
                                                mostrar_error(x,e)
                                                $('#dialog-confirm').dialog('destroy');
                                                $('#dialog-confirm').remove();
                                                $('#dialog-confirm').dialog('close');							
                                        }
                                });
                                //location.href=url;
                        },
                        'NO': function() {
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                        }
                }
        });
        return false;
   });
});
</script>