<div style="margin-top:20px;" class="grid16">
    <?php echo form_open('administrar/contador');
    echo form_label('Desde: ','desde');
    echo form_input(array('name'=>'desde','class'=>'fecha','size'=>10,'value'=>$desde));
    echo ' '.form_label('Hasta: ','hasta');
    echo form_input(array('name'=>'hasta','class'=>'fecha','size'=>10,'value'=>$hasta));
    echo form_submit('mostrar','Mostrar');
    echo form_close();
    ?>
</div>
<div class="grid16" style="margin-top:20px;">
    <h2>Reporte de pedidos realizados por asesor <?php if($desde){echo 'desde '.$desde;}else{ echo 'hoy';} if($hasta){ echo ' hasta '.$hasta;}?></h2>
    <table class="tabla" style="margin-top:20px;">
        <thead>
            <tr>
                <th style="height:30px;">#</th>
                <th>Asesor</th>
                <th>Cantidad de pedidos</th>
                <th>Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php if($vendedores){
                $n=1;
                $total=0;
                $total_pedidos=0;
                    foreach($vendedores as $vendedor):?>
            
            <tr>
                <td><?php echo $n;?></td>
                <td><?php echo $vendedor->nombre;?></td>
                <td style="text-align: right;"><?php echo $vendedor->cantidad;?></td>
                <td style="text-align: right;">$ <?php 
                $valor=$vendedor->valor;
                $total_pedidos=$total_pedidos+$vendedor->cantidad;
                $total=$total+$valor*1;
                $decimal=strstr($valor,'.');
                if($decimal=='.00'){
                    echo number_format($valor,0,',','.');
                }else{
                    echo number_format($valor,0,',','.');
                }
                ?></td>
            </tr>
            <?php $n++; endforeach;
            ?>
            <tfoot>
            <tr>
                <td colspan="2"><b>Totales -></b></td>
                <td style="text-align: right;"><b><?php echo $total_pedidos;?></b></td>
                <td style="text-align: right;"><b>$ <?php 
                    $decimal=strstr($total,'.');
                    if($decimal=='.00'){
                        echo number_format($total,0,',','.');
                    }else{
                        echo number_format($total,0,',','.');
                    }
                ?></b></td>
            </tr>
            </tfoot>
            <?php
            
            }else{?>
            <tr>
                <td colspan="3">No hay resultados en el rango de fechas seleccionado</td>                
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>
<div class="clear"></div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/sipe.js"></script>
<script type="text/javascript">
$(document).ready(function(){
        $('.fecha').datepicker({
            showOn:'both',
            changeYear:true,
            changeMonth:true,
            buttonImage: "<?php echo site_url('img/calendar.png');?>",
            buttonImageOnly: true
	});
});
</script>