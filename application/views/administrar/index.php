<style>
    #ui-datepicker-div{
        z-index: 10 !important;
    }
</style>
<h1>Administrar</h1>
<?php
        if ($this->session->userdata('rol')==1){?>
<div>
    <h2>Pedidos por vendedor:</h2>
    <div style="text-align: center;">Elija la fecha: <input id="fecha_calendario_pedidos" type="hidden" /></div>
    <div id='calendario_pedidos'></div>
    <div style="margin-top:10px;" id="total_pedidos_mes"><?php echo $total_pedidos_mes;?></div>
</div>
<?php }?>
<div>
    <h2>Recibos por vendedor:</h2>
    <div style="text-align: center;">Elija la fecha: <input id="fecha_calendario_recibos" type="hidden" /></div>
    <div id='calendario_recibos'></div>
    <div style="margin-top:10px;" id="total_recibos_mes"><?php echo $total_recibos_mes;?></div>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>js/jquery-ui-1.8.10.custom.min.js"></script>
<script type='text/javascript' src='<?php echo base_url();?>js/fullcalendar.js'></script>
<script	type="text/javascript" language="javascript" src="<?php echo base_url();?>js/date.format.js"></script>
<script>
    $(function(){
        <?php
        if ($this->session->userdata('rol')==1){?>
            $('#fecha_calendario_pedidos').datepicker({
               changeMonth: true,
               changeYear: true,
               dateFormat: 'mm-dd-yy',
               showOn: "button",
               buttonImage: "<?php echo base_url();?>img/calendar.png",
               buttonImageOnly: true,
               onSelect: function(dateText, inst) {
                   var fecha = new Date(dateText);
                   $('#calendario_pedidos').fullCalendar( 'gotoDate', fecha.getFullYear(),fecha.getMonth(),fecha.getDate() );
               }
            });
            $('#calendario_pedidos').fullCalendar({			
                header:{                          
                    left:'prev,right,next',                          
                    center:'title',                          
                    right:'basicDay,basicWeek,month'                        
                },         
                weekMode: 'variable',
                defaultView:'basicWeek',                        
                editable: false,                        
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],                        
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],                        
                dayNames:['Domingo', 'Lunes', 'Martes', 'Miércoles','Jueves', 'Viernes', 'Sábado'],                        
                dayNamesShort:['Dom', 'Lun', 'Mar', 'Mié','Jue', 'Vie', 'Sáb'],                        
                buttonText: {                                        
                    today: 'Hoy',                                        
                    month:    'Mes',                                        
                    week:     'Semana',                                        
                    day:      'Día'                                    
                },                       
                height:'500',                       
                //events: "http://190.248.67.66/sipred/tablero/eventos",                       
                events: function(start, end, callback) {                                
                   var start_timestamp = Math.round(start.getTime() / 1000);
                   var end_timestamp = Math.round(end.getTime() / 1000);
                   var actual=start_timestamp+((end_timestamp-start_timestamp)/2);
                   //var actual=Math.round($('#calendario_pedidos').fullCalendar('getDate').toString('yyyy-mm'));
                   //var actual=$('#calendario_pedidos').fullCalendar('getDate').getMonth();
                   $.ajax({
                        url: "<?php echo site_url('administrar/administrar/calendario_pedidos');?>/" + start_timestamp + "/" + end_timestamp+'/'+actual,
                        dataType: 'json',
                        success: function(events) {
                            if(events.total_mes){
                                $('#total_pedidos_mes').html(events.total_mes);
                            }else{
                                $('#total_pedidos_mes').html('');
                            }
                            if(events){
                                callback(events.eventos);
                            }else{
                                mostrar_mensaje('No hay eventos.','correcto',500);
                            }
                        }
                    });                    
                },                        
                eventRender: function(event, element) {
                    element.html('<div>'+event.nombre+'</div>');                    
                },                       
                eventDrop: function(event, delta) {				
                    //alert(event.title + ' was moved ' + delta + ' days\n' +	'(should probably update your database)');			
                },			
                loading: function(bool) {				
                    if (bool) mostrar_mensaje('Cargando información','alerta');
                    else mostrar_mensaje('Datos cargados','correcto',500);
                }		
            }); 
            <?php }?>
            $('#fecha_calendario_recibos').datepicker({
               changeMonth: true,
               changeYear: true,
               dateFormat: 'mm-dd-yy',
               showOn: "button",
               buttonImage: "<?php echo base_url();?>img/calendar.png",
               buttonImageOnly: true,
               onSelect: function(dateText, inst) {
                   var fecha = new Date(dateText);
                   $('#calendario_recibos').fullCalendar( 'gotoDate', fecha.getFullYear(),fecha.getMonth(),fecha.getDate() );
               }
            });
            
                $('#calendario_recibos').fullCalendar({			
                    header:{                          
                        left:'prev,right,next',                          
                        center:'title',                          
                        right:'basicDay,basicWeek,month'                        
                    },         
                    weekMode: 'variable',
                    defaultView:'basicWeek',                        
                    editable: false,                        
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],                        
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],                        
                    dayNames:['Domingo', 'Lunes', 'Martes', 'Miércoles','Jueves', 'Viernes', 'Sábado'],                        
                    dayNamesShort:['Dom', 'Lun', 'Mar', 'Mié','Jue', 'Vie', 'Sáb'],                        
                    buttonText: {                                        
                        today: 'Hoy',                                        
                        month:    'Mes',                                        
                        week:     'Semana',                                        
                        day:      'Día'                                    
                    },                       
                    height:'500',                       
                    //events: "http://190.248.67.66/sipred/tablero/eventos",                       
                    events: function(start, end, callback) {                                
                       var start_timestamp = Math.round(start.getTime() / 1000);
                       var end_timestamp = Math.round(end.getTime() / 1000);
                       //var actual=Math.round($('#calendario_recibos').fullCalendar('getDate').getTime()/ 1000);
                       var actual=start_timestamp+((end_timestamp-start_timestamp)/2);
                       $.ajax({
                            url: "<?php echo site_url('administrar/administrar/calendario_recibos');?>/" + start_timestamp + "/" + end_timestamp+'/'+actual,
                            dataType: 'json',
                            success: function(events) {
                                if(events.total_mes){
                                    $('#total_recibos_mes').html(events.total_mes);
                                }else{
                                    $('#total_recibos_mes').html('');
                                }
                                if(events){
                                    callback(events.eventos);                                    
                                }else{
                                    mostrar_mensaje('No hay eventos.','correcto',500);
                                }
                            }
                        });                        
                    },                        
                    eventRender: function(event, element) {
                            element.html('<div>'+event.nombre+'</div>');                                                
                    },                       
                    eventDrop: function(event, delta) {				
                        //alert(event.title + ' was moved ' + delta + ' days\n' +	'(should probably update your database)');			
                    },			
                    loading: function(bool) {				
                        if (bool) mostrar_mensaje('Cargando información','alerta');
                        else mostrar_mensaje('Datos cargados','correcto',500);
                    }		
                }); 
    });
</script>