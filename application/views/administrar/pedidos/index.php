<h1>Administrar - Pedidos</h1>
<div style="margin-top:10px;margin-bottom: 10px;">
    <?php echo anchor('administrar/pedidos/configurar','Configurar','class="boton"');?>
     <?php echo anchor('pedidos/imprimir_rango','Imprimir rango','class="boton" rel="imprimir_rango" onclick="return false;"');?>
</div>
<div style="margin-top:20px">
    <?php 
        echo form_open('administrar/pedidos');
        echo form_input(array('name'=>'buscar','size'=>50,'value'=>set_value('buscar',$buscar)));
        echo form_submit('enviar','Buscar');
        echo form_close();?>
</div>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<table class="tabla">
    <thead>
            <tr>
                    <th style="width: 3%;"></th>
                    <th style="width: 5%;"><?php echo $this->pagination->ordenar('id_maestro','Num','title="Número"');?></th>
                    <th style="width: 10%;"><?php echo $this->pagination->ordenar('id_cliente','Nit');?></th>
                    <th style="width: 20%;"><?php echo $this->pagination->ordenar('nombre_cliente','Nombre del cliente');?></th>
                    <th style="width: 4%;"><?php echo $this->pagination->ordenar('id_vendedor','vd','title="Id Vendedor"');?></th>
                    <th style="width: 10%;"><?php echo $this->pagination->ordenar('subtotal_sin_dcto','Subtotal','title="Subtotal sin descuento"');?></th>
                    <th style="width: 10%;"><?php echo $this->pagination->ordenar('iva_total','Iva','title="Iva"');?></th>
                    <th style="width: 10%;"><?php echo $this->pagination->ordenar('total','Total','title="Total"');?></th>
                    <th style="width: 8%;"><?php echo $this->pagination->ordenar('fecha','Fecha');?></th>
                    <th style="width: 10%;">Acciones</th>
            </tr>
    </thead>
    <tbody>
    <?php if($pedidos){
            $n=$desde;
            foreach($pedidos as $pedido):
            ?>
            <tr <?php if($pedido->anulado){ echo 'class="anulado"';}?>>
                    <td class="item"><?php echo $n?></td>
                    <td title="numero"><?php echo $pedido->id_maestro;?></td>
                    <td title="nit"><?php echo $pedido->id_cliente;?></td>
                    <td title="nombre"><?php if($pedido->nombre_cliente){echo $pedido->nombre_cliente; if($pedido->contacto){echo '<br/><span class="sub_label">'.$pedido->contacto.'</span>';}}else{echo '<span class="alerta"></span>El cliente cambió de razon social o fué eliminado del sistema.';}?></td>
                    <td style="text-align: center;"><?php echo $pedido->id_vendedor;?></td>
                    <td class="subtotal_sin_dcto" style="text-align: right;"><?php echo number_format($pedido->subtotal_sin_dcto,'0',',',".");?><?php if($pedido->descuento_total>0){ echo '<div class="descuento">-dcto '.number_format($pedido->descuento_total,'0',',',".").'</div>';}?></td>
                    <td class="iva_total" style="text-align: right;"><?php echo number_format($pedido->iva_total,'0',',',".");?></td>
                    <td class="total" style="text-align: right;"><?php echo number_format($pedido->total,'0',',',".");?></td>
                    <td style="color: #930; text-align: center; font-size: 0.7em;"><?php echo $pedido->fecha;?></td>
                    <td style="text-align: center;"><?php 
                    echo ' '.anchor('pedidos/ver/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_blanco ver"></span>','target="_blank" class="boton peque" title="Ver el pedido" rel="ver_pedido"');			 
                    echo ' '.anchor('pedidos/enviar_copia/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_blanco enviar"></span>','class="boton peque" title="Enviar copia por correo" rel="enviar_mail"');			 
                    echo ' '.anchor('pedidos/imprimir_copia/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="print"></span>','class="boton peque" target="_blank" title="Imprimir en formato de recibo"'); 
                    echo ' '.anchor('pedidos/generar_pdf/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="pdf"></span>','class="boton_gris peque" title="Generar copia en pdf"');
                    echo ' '.anchor('pedidos/generar_excel/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="xls"></span>','class="boton_gris peque" title="Generar copia en Excel"');
                    if($pedido->anulado){
                        echo ' '.anchor('administrar/pedidos/anular/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_azules eliminar"></span>','class="boton_gris peque" title="Activar pedido" rel="anular_pedido" onclick="return false;"');
                    }else{
                        echo ' '.anchor('administrar/pedidos/anular/'.$pedido->id_maestro.'/'.sha1($pedido->id_maestro.$seguridad),'<span class="iconos_blancos eliminar"></span>','class="boton_gris boton_rojo peque" title="Anular pedido" rel="anular_pedido" onclick="return false;"');
                    }
                    ?></td>
            </tr>
            <?php
            $n++;
            endforeach;
            ?>
            <?php }else{?>
            <tr>
                    <td colspan="10"><span class="alerta"></span> No se encontraron pedidos.</td>
            </tr>
            <?php }?>
    </tbody>
</table>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<div class="clear"></div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="imprimir_rango_pedidos" title="Imprimir rango de pedidos"></div>');
	$('a[rel=enviar_mail]').click(function(){
		$('#contenido').append('<div id="enviarMail" title="Enviar Mail"></div>');
		$('#enviarMail').html('<div class="form"><ul><li style="text-align:left;"><?php echo form_label('(C.C.) Enviar copias del pedido a : <span class="sub_label">Separa los correos con comas ( , ).</span>','email_copias'); echo form_input(array('name'=>'email_copias','style'=>'width:100%;'));?></li><li><?php echo form_label('Mensaje adicional :','mensaje'); ?></li><li><?php echo form_textarea(array('name'=>'mensaje','style'=>'width:100%;'));?></li></ul></div>');
		$('#enviarMail input[name=email_copias]').change(function(){
			$('#enviarMail input[name=email_copias]').removeClass('error');
			var error=false;
			var correos = $('#enviarMail input[name=email_copias]').val().split(',');
			$('#enviarMail input[name=email_copias]').next('span').remove();
			if($('#enviarMail input[name=email_copias]').val()!=""){
				for (var n=0;n< correos.length;n++){
					if(!es_mail(correos[n])){
						error=true;
					   $('#enviarMail input[name=email_copias]').addClass('error');
					}
				}
				if(error){
				   $('#enviarMail input[name=email_copias]').after('<span class="error">* Existen email no válidos</span>');
				}
			}else{
			   $('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
			}
	    });
		var url=$(this).attr('href');
	    $('#cargando').removeClass('correcto error');
		$("#enviarMail").dialog({
				resizable: false,
				width: 800,
				height:480,
				modal: true,
				close: function(event,ui){
					$('#enviarMail').dialog('destroy');
					$('#enviarMail').remove();
					$('#enviarMail').dialog('close');
				},
				buttons: {
					'Cerrar': function() {
						$('#enviarMail').dialog('destroy');
						$('#enviarMail').remove();
						$('#enviarMail').dialog('close');
					},
					'Enviar': function(){
						var email=$('#enviarMail input[name=email_copias]').val();
						var mensaje=$('#enviarMail textarea[name=mensaje]').val();
						var error=false;
						mensaje=mensaje.replace(/\n/g,'<br/>');
						if($('#cargando').text()!='Enviando copia del pedido'){
							if(email){
								$('#cargando').html('Enviando copia del pedido');
								$('#cargando').show();
								$('#enviarMail input[name=email_copias]').removeClass('error');
								var correos = $('#enviarMail input[name=email_copias]').val().split(',');
								for (var n=0;n< correos.length;n++){
									if(!es_mail(correos[n])){
										error=true;
									   $('#enviarMail input[name=email_copias]').addClass('error');
									}
								}
							}else{
								$('#enviarMail input[name=email_copias]').addClass('error');
								error=true;
							}
							$('#enviarMail input[name=email_copias]').next('span').remove();
							if(error){
								$('#enviarMail input[name=email_copias]').after('<span class="error">* El email es obligatorio</span>');
							}else{
								var form_data={
									email:email,
									mensaje:mensaje,
									ajax:1
								};
								$.ajax({
								   type: "POST",
								   url: url,
								   data: form_data,
								   success: function(msg){
									   $('#cargando').removeClass('correcto error');
									   if(msg){
										   $('#cargando').html('Enviado correctamente');
											$('#enviarMail').html(msg);
											$('#cargando').hide();
									   }else{
										   $('#cargando').html('Se presentaron errores. Inténtelo nuevamente.');
										   $('#cargando').addClass('error');
										   alert("error");
									   }
								   },
								   error: function(x,e){
									  $('#cargando').addClass('error');
										if(x.status==0){
												$('#cargando').html('Se perdió la conexión!!. Por favor verifique su conexión a internet.');
										}else if(x.status==404){
												$('#cargando').html('La url buscada no se encontró.');
										}else if(x.status==500){
												$('#cargando').html('Error interno del servidor.'+x.responseText);
										}else if(e=='parsererror'){
												$('#cargando').html('Error.\nParsing JSON Request failed.');
										}else if(e=='timeout'){
												$('#cargando').html('Se ha demorado mucho la operación, inténtelo nuevamente.');
										}else {
												$('#cargando').html('Error desconocido. '+x.responseText);
										}
										$('#cargando').delay(3000).slideUp('fast',function(){ $('#cargando').removeClass('correcto error')});
								   }
								 });					
							}
						}
					}
				}
		});
		return false;
	});
	function es_mail(correo){
		var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		if(correo.match(expresion)){
			return true;
		}else{
			return false;
		}
	}
	var buscar=$('input[name=buscar]').val();
	if(buscar!=''){
		$('table tbody td[title=numero]').highlight(buscar);
		$('table tbody td[title=nit]').highlight(buscar);
		$('table tbody td[title=nombre]').highlight(buscar);
	}
        $('#imprimir_rango_pedidos').dialog({
            autoOpen: false,
            show:'fold',
            hide:'fold',
            height: 250,
            width: 600,
            resizable:false,
            modal: true            
        });
        $('a[rel=imprimir_rango]').click(function(){
            var url=$(this).attr('href');
            var form_data={
                    ajax:1
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       $('#cargando').removeClass('correcto error');
                       $('#imprimir_rango_pedidos').html(msg);
                       $('#imprimir_rango_pedidos').dialog('open');
                       if(msg){
                               $('#cargando').html('Enviado correctamente');
                       }else{
                               $('#cargando').html('Se presentaron errores. Inténtelo nuevamente.');
                               $('#cargando').addClass('error');
                               alert("error");
                       }
               },
               error: function(x,e){
                      mostrar_error(x,e);
               }
           });
           return false; 
        });
        $('#imprimir_rango_pedidos form').live('submit',function(){
            if($('#imprimir_rango_pedidos input[name=rango]').val()==''){
                return false;
            }
            $('#imprimir_rango_pedidos').dialog('close');
        });
        $('a[rel=anular_pedido]').live('click',function(){
            var url=$(this).attr('href');
            var titulo=$(this).attr('title');
            var boton=$(this);
            $('#dialog-confirm').dialog('destroy');
            $('#dialog-confirm').remove();
            var mensaje='';
            if(titulo=='Activar pedido'){
                mensaje='Está seguro de activar el pedido';
            }else{
                mensaje='Está seguro de anular el pedido';
            }
            $('body').append('<div id="dialog-confirm" title="Anular pedido"><span class="icono"></span>'+mensaje+'?</div>');
            $("#dialog-confirm").dialog({
                    resizable: false,
                    height:140,
                    dialogClass: 'dialog_confirm',
                    modal: true,
                    buttons: {
                            'SI': function() {
                                if(titulo=='Activar pedido'){
                                    mostrar_mensaje('Activando el pedido. Por favor espere...','alerta',0);
                                }else{
                                    mostrar_mensaje('Anulando el pedido. Por favor espere...','alerta',0);
                                }
                                $.ajax({
                                   type: "POST",
                                   url: url,
                                   dataType:'json',
                                   success: function(msg){
                                           if(msg.estado=='correcto'){
                                                  mostrar_mensaje(msg.mensaje,'correcto',5000);
                                                  if(titulo=='Activar pedido'){
                                                      if(msg.descuento!=0){
                                                          boton.parent().parent().find('td.subtotal_sin_dcto').html(msg.subtotal_sin_dcto+'<div class="descuento">-dcto '+msg.descuento+'</div>');
                                                      }else{
                                                        boton.parent().parent().find('td.subtotal_sin_dcto').html(msg.subtotal_sin_dcto);
                                                      }
                                                        boton.parent().parent().removeClass('anulado');
                                                        boton.parent().parent().find('td.iva_total').text(msg.iva_total);
                                                        boton.parent().parent().find('td.total').text(msg.total);
                                                        boton.addClass('boton_rojo');
                                                        boton.attr('title','Anular pedido');
                                                        boton.find('span').removeClass('iconos_azules');
                                                        boton.find('span').addClass('iconos_blancos');
                                                    }else{
                                                        boton.parent().parent().addClass('anulado');
                                                        boton.parent().parent().find('td.subtotal_sin_dcto').text('0');
                                                        boton.parent().parent().find('td.iva_total').text('0');
                                                        boton.parent().parent().find('td.total').text('0');
                                                        boton.removeClass('boton_rojo');
                                                        boton.attr('title','Activar pedido');
                                                        boton.find('span').removeClass('iconos_blancos');
                                                        boton.find('span').addClass('iconos_azules');
                                                    }
                                           }else{
                                                  mostrar_mensaje(msg.mensaje,'error');
                                           }
                                   },
                                   error: function(x,e){
                                          mostrar_error(x,e);
                                   }
                                });                                
                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                            },
                            'NO': function() {
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
                            }
                    }
            });
            return false;
        })
});
</script>
