<h1>Administrar - Acceso</h1>
<div id="configuraciones">
    
    <div class="grid16 alpha omega">
        <h2>Activar o desactivar el SIPE</h2>
        <table class="configuracion" width="100%">
            <tr>
                <td>Activar el SIPE para todos los usuarios. (Si elije NO, solo podran acceder los usuarios Administradores).</td>
                <?php if($configuracion->activo==1){ $clase='si';}else{ $clase='no';}?>
            <td class="opcion"><?php echo anchor('administrar/acceso/modificar_configuracion/activo','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>Mensaje para mostrar en la página inicial:</div>
                    <div style="margin-top:10px;"><?php echo form_textarea(array('name'=>'mensaje','value'=>$configuracion->mensaje,'style'=>"width:100%; height:50px;"));?></div>
                    <div style="margin:10px 0px;"><?php echo anchor('administrar/acceso/guardar_mensaje','Guardar','rel="guardar_mensaje" onclick="return false;" class="boton"');?></div>
                </td>
                
            </tr>
            <tr>
                <td>Mostrar mensaje</td>
                <?php if($configuracion->mostrar_mensaje==1){ $clase='si';}else{ $clase='no';}?>
                <td class="opcion"><?php echo anchor('administrar/acceso/modificar_configuracion/mostrar_mensaje','<span class="si_no"></span>','rel="modificar_configuracion" class="'.$clase.'" onclick="return false;"');?></td>
            </tr>
            <?php /*por hcer
            <tr>
            <td style="vertical-align: top;">Permitir el ingreso a los roles:</td>
            <td>
                <div style="margin-bottom: 10px;">
                    <?php echo anchor('administrar/acceso/agregar_rol','Agregar rol','class="boton" rel="agregar_rol" onclick="return false;"');?>
                </div>
                <table id="plazos" class="tabla" width="100%">
                    <thead>
                        <tr>
                            <th>Plazo</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $roles=json_decode($configuracion->permitir_roles);
                            if(is_array($roles)){
                                foreach($roles as $rol):                                    
                                ?>
                                <tr>
                                    <td><?php echo form_input('rol_nombre',$rol->nombre);?></td>
                                    <td style="text-align: center;">
                                        <?php echo anchor('','<span class="iconos_rojo eliminar"></span>','class="boton boton_gris peque" rel="eliminar_plazo" onclick="return false;"');?>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                            }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
             */ ?>
             
        </table>        
    </div>
<div class="clear"></div>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('body').append('<div id="cuadro_accion"></div>');
    $("#cuadro_accion").dialog({
        autoOpen:false,
        resizable: true,
        width: 600,
        height:300,
        modal: true,
        close: function(event,ui){
                $('body').css('overflow','auto');                
        },
        buttons: {
                'Cerrar': function() {
                        $('body').css('overflow','auto');
                        $(this).dialog('close');
                }
        }
   });
   $('a[rel=guardar_mensaje]').click(function(){
       var url=$(this).attr('href');
       var form_data={
           mensaje:$('textarea[name=mensaje]').val(),
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data:form_data,
           dataType: 'json',
           success: function(msg){
               if(msg.estado=='correcto'){
                   mostrar_mensaje(msg.mensaje,'correcto',1000);                   
               }else{
                   mostrar_mensaje(msg.mensaje,'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });
    $('a[rel="modificar_configuracion"]').click(function(){
       mostrar_mensaje('Se está guardando la configuración','alerta',0);
       var url=$(this).attr('href');
       var nueva_clase='';
       var obj=$(this);
       if($(this).hasClass('si')){
           url=url+'/0';
           nueva_clase='no';
       }else{
           url=url+'/1';
           nueva_clase='si';
       }
       var form_data={
           ajax:1
       }
       $.ajax({
           type: "POST",
           cache: false,
           url: url,
           data: form_data,
           dataType: 'json',
           success: function(msg){
               if(msg['estado']=='correcto'){
                   mostrar_mensaje(msg['mensaje'],'correcto',1000);
                   obj.removeClass('si no');
                   obj.addClass(nueva_clase);
               }else{
                   mostrar_mensaje(msg['mensaje'],'error');
               }                   
           },
           error: function(x,e){
               mostrar_error(x,e);
           }
        });
   });   
});
</script>