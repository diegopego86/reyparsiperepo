<h1>Administrar - Usuarios</h1>
<div style="margin-top:10px;margin-bottom: 10px;">
<?php echo anchor('administrar/usuarios/crear', 'Crear Usuario', 'class="boton" style="margin-bottom:10px;"'); ?>
</div>
<div style="margin-top:20px">
    <?php 
        echo form_open('administrar/usuarios');
        echo form_input(array('name'=>'buscar','size'=>50,'value'=>set_value('buscar',$buscar)));
        echo form_submit('enviar','Buscar');
        echo form_close();?>
</div>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<table class="tabla">
    <thead>
        <tr>
            <th>#</th>
            <th><?php echo $this->pagination->ordenar('usuario', 'Usuario'); ?></th>
            <th><?php echo $this->pagination->ordenar('nombres', ' Nombres'); ?></th>
            <th><?php echo $this->pagination->ordenar('apellidos', 'Apellidos'); ?></th>
            <th><?php echo $this->pagination->ordenar('id_vendedor', 'Vendedor'); ?></th>
            <th><?php echo $this->pagination->ordenar('email', 'E-mail'); ?></th>
            <th><?php echo $this->pagination->ordenar('nombre_rol', 'Rol'); ?></th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php $n=$desde;
            foreach ($usuarios as $usuario) {?>
            <tr title="<?php echo $usuario->nombres.' '.$usuario->apellidos; ?>">
                <td><?php echo $n; ?></td>
                <td title="Usuario"><?php echo $usuario->usuario; ?></td>
                <td title="Nombres"><?php echo $usuario->nombres; ?></td>
                <td title="Apellidos"><?php echo $usuario->apellidos; ?></td>
                <td><?php echo $usuario->id_vendedor; ?></td>
                <td><?php echo $usuario->email; ?></td>
                <td><?php echo $usuario->nombre_rol; ?></td>
                <td style="text-align: center;"><?php if($usuario->rol==3){echo anchor('administrar/usuarios/asignar_nits/' . $usuario->id, '<span class="iconos_blanco nits"></span>', 'class="boton peque" title="Asignar Nits al usuario."').' '; }?><?php echo anchor('administrar/usuarios/editar/' . $usuario->id, '<span class="editar"></span>', 'class="boton peque" title="Editar usuario."'); ?> <?php if($this->session->userdata('id_usuario')!=$usuario->id){ echo anchor('administrar/usuarios/eliminar/' . $usuario->id, '<span class="iconos_blanco eliminar"></span>', 'class="boton boton_rojo peque" rel="boton_eliminar_usuario"');} ?></td>
            </tr>
        <?php $n=$n+1; } ?>
    </tbody>
</table>

<div class="clear"></div>
<div class="paginacion"><?php echo $this->pagination->create_links();?></div>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
    $(function(){
       var buscar=$('input[name=buscar]').val();
	if(buscar!=''){
        	$('table tbody td[title=Usuario]').highlight(buscar);
		$('table tbody td[title=Nombres]').highlight(buscar);
		$('table tbody td[title=Apellidos]').highlight(buscar);
	} 
        $('a[rel=boton_eliminar_usuario]').click(function(){
            var url=$(this).attr('href');
            var obj=$(this).parent().parent();
            var nombre=obj.attr('title');
            $('#contenido').append('<div id="dialog-confirm"><span class="icono"></span>Está seguro de eliminar el usuario '+nombre+' ?</div>');
            $("#dialog-confirm").dialog({
                    resizable: false,
                    height:140,
                    modal: true,
                    buttons: {
                            'SI': function() {
                                    var form_data={
                                            ajax:1
                                    };
                                    $.ajax({
                                            type: "POST",
                                            url: url,
                                            data: form_data,
                                            success: function(msg){
                                                    if(msg=='correcto'){
                                                            obj.fadeOut('slow',function(){ $(this).remove();});
                                                            $('#cargando').html('El usuario se eliminó correctamente');
                                                            $('#cargando').addClass('correcto');
                                                            $('#cargando').show();
                                                            $('#cargando').delay(1500).fadeOut('slow',function(){
                                                                    $(this).removeClass('correcto error');
                                                                    $(this).hide();
                                                            });                                                       
                                                    }else{
                                                            $('#cargando').html('<span class="icono"></span>'+msg);
                                                            $('#cargando').addClass('error');
                                                            $('#cargando').show();
                                                            $('#cargando').delay(2400).fadeOut('slow',function(){
                                                                    $(this).removeClass('correcto error');
                                                                    $(this).hide();
                                                            });
                                                    }
                                                    $('#dialog-confirm').dialog('destroy');
                                                    $('#dialog-confirm').remove();
                                                    $('#dialog-confirm').dialog('close');
                                            },
                                            error: function(x,e){
                                                    $('#cargando').addClass('error');
                                                    if(x.status==0){
                                                            $('#cargando').html('Se perdió la conexión!!. Por favor verifique su conexión a internet.');
                                                    }else if(x.status==404){
                                                            $('#cargando').html('La url buscada no se encontró.');
                                                    }else if(x.status==500){
                                                            $('#cargando').html('Error interno del servidor.');
                                                    }else if(e=='parsererror'){
                                                            $('#cargando').html('Error.\nParsing JSON Request failed.');
                                                    }else if(e=='timeout'){
                                                            $('#cargando').html('Se ha demorado mucho la operación, inténtelo nuevamente.');
                                                    }else {
                                                            $('#cargando').html('Error desconocido. '+x.responseText);
                                                    }
                                                    $('#dialog-confirm').dialog('destroy');
                                                    $('#dialog-confirm').remove();
                                                    $('#dialog-confirm').dialog('close');
                                                    $('#cargando').show();
                                                    $('#cargando').delay(1500).fadeOut('slow',function(){
                                                            $(this).removeClass('correcto error');
                                                            $(this).hide();
                                                    });
                                            }
                                    });
                                    //location.href=url;
                            },
                            'NO': function() {
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
                            }
                    }
            }); 
            return false;
        });
    });
</script>