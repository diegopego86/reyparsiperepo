<div class="grid16">
    <h1>Administrar - Usuarios - Asignar Nits</h1>
<ul class="datos_usuario">
<li class="label" style="float:left;">Cédula:</li><li style="float:left;"><?php echo $usuario->codigo;?></li>
<li class="label" style="clear:both;float:left;">Usuario:</li><li style="float:left;"><?php echo $usuario->usuario;?></li>
<li class="label" style="clear:both;float:left;">Nombre: </li><li style="float:left;"><?php echo $usuario->nombres.' '.$usuario->apellidos;?></li>
<li class="label" style="clear:both;float:left;">E-mail:</li><li style="float:left;"><?php echo $usuario->email;?></li>
<div class="clear"></div>
</ul>
    
<div style="margin-bottom:10px">
<?php echo anchor('','Buscar Nits para agregar  <span class="iconos_blancos buscar"></span>','class="boton" rel="boton_buscar_cliente"');    ?>
</div>
<?php if($clientes){?>
<div class="resultados"><?php echo $this->pagination->resultados();?></div>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
<table id="resultados_buscar_clientes" class="tabla">
<thead>
<tr>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('id_cliente','Nit','title="Nit del cliente"');?></th>
    <th style="width:25%;"><?php echo $this->pagination->ordenar('nombre','Nombre');?></th>
    <th style="width:25%;"><?php echo $this->pagination->ordenar('contacto','Contacto');?></th>
    <th style="width:3%;"><?php echo $this->pagination->ordenar('id_vendedor','Vd');?></th>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('ciudad','Ciudad');?></th>
    <th style="width:5%;"><?php echo $this->pagination->ordenar('direccion','Dirección');?></th>
    <th style="width:8%;"><?php echo $this->pagination->ordenar('telefono','Teléfono1');?></th>
    <th style="width:10%;"><?php echo $this->pagination->ordenar('email','E-mail');?></th>
    <th style="width:8%;">Acción</th>
</tr>
</thead>
<tbody>
<?php foreach($clientes as $cliente):?>
<tr>
<td><?php echo $cliente->id_cliente;?></td>
<td><?php echo $cliente->nombre;?></td>
<td><?php echo $cliente->contacto;?></td>
<td><?php echo $cliente->id_vendedor;?></td>
<td><?php echo $cliente->ciudad;?></td>
<td><?php echo $cliente->direccion;?></td>
<td><?php echo $cliente->telefono1;?></td>
<td><?php echo $cliente->email;?></td>
<td style="text-align:center;"><?php echo anchor('administrar/usuarios/eliminar_nit_cliente/'.$cliente->id_cliente,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="boton_eliminar_nit_cliente"');?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
<?php }else{?>
<table class="tabla">
<thead>
<tr><th style="height:30px;">Nit</th><th>Nombre</th><th>Contacto</th></tr>
</thead>
<tbody>
<tr><td colspan="3"><span class="alerta"></span> No existen clientes con los datos de busqueda.</td></tr>
</tbody>
</table>
<?php }?>
<div id="paginacion_clientes" class="paginacion">
<?php echo $this->pagination->create_links();?>
</div>
</div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript"	src="<?php echo base_url();?>js/highlight-plugin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('a[rel=boton_buscar_cliente]').click(function(){
    $('#contenido').append('<div id="buscarCliente" title="Buscar cliente"></div>');
        mostrar_mensaje('<span></span> Cargando listado de clientes.','alerta',0);
        var form_data={
                cliente: '',
                ajax:'1'
        };
        $.ajax({
           type: "POST",
           url: "<?php echo site_url('administrar/usuarios/buscar_cliente');?>",
           data: form_data,
           success: function(msg){
               if(msg){				   
                    mostrar_mensaje('Se cargó el listado de clientes.','correcto',200);
                    $('#buscarCliente').html(msg);						
                    acciones_buscar_cliente();
                    $('#buscarCliente input[name=cliente]').focus();
               }else{
                   mostrar_mensaje('<span></span> Se presentaron errores al cargar los clientes. Inténtelo nuevamente','error');
               }
           },
           error: function(x,e){
                mostrar_error(x,e);
           }
         });//Termina ajax.
        $("#buscarCliente").dialog({
                resizable: false,
                width: 800,
                height:480,
                modal: true,
                close: function(event,ui){
                        $('#buscarCliente').dialog('destroy');
                        $('#buscarCliente').remove();
                        $('#buscarCliente').dialog('close');
                },
                buttons: {
                        'Cerrar': function() {
                                $('#buscarCliente').dialog('destroy');
                                $('#buscarCliente').remove();
                                $('#buscarCliente').dialog('close');
                        }
                }
        });//Termina dialog.
        return false;
    });//Termina boton_buscar_cliente
    function acciones_buscar_cliente(){
            $('#buscarCliente input[name=cliente]').focus(function(){
                    var valor=$(this).val();
                    if(valor=='Digite el nit o nombre del cliente'){
                            $(this).val('');
                    }
            });
            $('#buscarCliente input[name=cliente]').focusout(function(){
                    var valor=$(this).val();
                    if(valor==''){
                            $(this).val('Digite el nit o nombre del cliente');
                    }
            });
            $('#buscarCliente input[name=cliente]').keyup(function(event){
                    if (event.keyCode == '13') {
                      $('#buscarCliente a[rel=filtrar_cliente]').click();
                    }
                    return false;
            });
            $('#buscarCliente a[rel=filtrar_cliente]').click(function(){
                    mostrar_mensaje('Se estan filtrando los clientes. Por favor espere...','alerta',0);
                    var url = $(this).attr('href');
                    var cliente=$('#buscarCliente input[name=cliente]').val();
                    if(cliente=='Digite el nit o nombre del cliente'){
                            cliente='';
                            $('#buscarCliente input[name=cliente]').val('');
                    }
                    var form_data={
                            cliente:cliente,
                            ajax:1
                    };
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: form_data,
                       success: function(msg){
                               if(msg){
                                   mostrar_mensaje('Se cargó la información correctamente.','correcto',200);
                                            $('#buscarCliente').html(msg);
                                            acciones_buscar_cliente();
                               }else{
                                   mostrar_mensaje('Se presentaron errores, intételo nuevamente','error');
                               }
                       },
                       error: function(x,e){
                          mostrar_error(x,e);
                       }
                     });
                    return false;
            });
            $('#buscarCliente th a').click(function(){
                    mostrar_mensaje('Ordenando los resultados. Por favor espere...','alerta',0);
                    var url=$(this).attr('href');
                    var form_data={
                            ajax:'1'
                    };
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: form_data,
                       success: function(msg){
                               if(msg){				   
                                    mostrar_mensaje('Se ordenaron correctamente.','correcto',200);
                                    $('#buscarCliente').html(msg);
                                    acciones_buscar_cliente();
                               }else{
                                   mostrar_mensaje('Se presentaron errores, intételo nuevamente','error');                                       
                               }
                       },
                       error: function(x,e){
                          mostrar_error(x,e);
                       }
                     });
                    return false;
            });
            $('#buscarCliente div.paginacion a').click(function(){
                    mostrar_mensaje('Cargando los resultados de la busqueda','alerta',0);
                    var url=$(this).attr('href');
                    var form_data={
                            ajax:'1'
                    };
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: form_data,
                       success: function(msg){
                               if(msg){
                                    mostrar_mensaje('Se cargaron los datos correctamente','correcto',200);				   
                                    $('#buscarCliente').html(msg);
                                    acciones_buscar_cliente();
                               }else{
                                   mostrar_mensaje('Se presentaron errores, intételo nuevamente','error');                                       
                               }
                       },
                        error: function(x,e){
                            mostrar_error(x,e);
                            }
                    });
                    return false;
            });
            $('#buscarCliente a[rel=boton_seleccionar_cliente]').click(function(){
                    mostrar_mensaje('Cargando datos del cliente seleccionado','alerta',0);
                    $('#buscarCliente').dialog('close');
                    $('#buscarCliente').dialog('destroy');
                    $('#buscarCliente').remove();
                    var url=$(this).attr('href');
                    var form_data={
                            cliente:'<?php echo $usuario->id;?>',
                            ajax:'1'
                    };
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: form_data,
                       success: function(msg){
                               if(msg=="correcto"){
                                    mostrar_mensaje('Se cargaron los datos correctamente','correcto',200);				   				   
                                    location.href="<?php echo site_url('administrar/usuarios/asignar_nits/'.$usuario->id);?>";		
                               }else{
                                       mostrar_mensaje('Se presentaron errores: '+msg,'error');                                       
                               }
                       },
                        error: function(x,e){
                            mostrar_error(x,e);
                        }
                     });
                    return false;
            });
            var buscar=$('#buscarCliente input[name=cliente]').val();
            if(buscar!=''){
                    $('#buscarCliente table').highlight(buscar);
            }
    }
    $('a[rel=boton_eliminar_nit_cliente]').click(function(){
            mostrar_mensaje('Eliminando al cliente. Por favor espere...','alerta',0);
            var url=$(this).attr('href');
            var form_data={
                    cliente:'<?php echo $usuario->id;?>',
                    ajax:'1'
            };
            $.ajax({
               type: "POST",
               url: url,
               data: form_data,
               success: function(msg){
                       if(msg=="correcto"){				   
                           mostrar_mensaje('Se ha eliminado al cliente correctamente','correcto',200);
                           location.href="<?php echo site_url('administrar/usuarios/asignar_nits/'.$usuario->id);?>";		
                       }else{
                           mostrar_mensaje('Se presentaron errores: '+msg,'error');                               
                       }
               },
        error: function(x,e){
            mostrar_error(x,e);
            }
        });
        return false;
    });
});
</script>