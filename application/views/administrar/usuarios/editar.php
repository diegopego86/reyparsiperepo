<div class="grid16">
<h1>Administrar - Usuarios - Editar</h1>
    <div class="form">
        <?php
        echo form_open('administrar/usuarios/editar');
        ?>
        <ul id="formulario"><li><?php
        echo form_label('Cédula/Nit','codigo');
        echo form_input(array('name'=>'codigo','value'=>set_value('codigo',$usuario->codigo)));
        echo form_hidden('id',$id);
        ?></li>
        <li class="left"><?php
        echo form_label('Nombres','nombres');
        echo form_input(array('name'=>'nombres','value'=>set_value('nombres',$usuario->nombres)));
        ?>
        </li>
        <li class="left"><?php
        echo form_label('Apellidos','apellidos');
        echo form_input(array('name'=>'apellidos','value'=>set_value('apellidos',$usuario->apellidos)));
        ?></li><li class="clear_left"><?php
        echo form_label('Nombre de usuario','usuario');
        echo form_input(array('name'=>'usuario','value'=>set_value('usuario',$usuario->usuario)));
        echo form_hidden('usuario_ant',set_value('usuario_ant',$usuario->usuario));
        ?></li><li><?php
        echo form_label('E-mail','email');
        echo form_input(array('name'=>'email','value'=>set_value('email',$usuario->email)));
        ?></li><li><?php
        echo form_label('Rol','rol');
        echo form_dropdown('rol',$roles,set_value('rol',$usuario->rol));
        ?></li><li class="clear_left"><?php
        echo form_label('Vendedor(es)','id_vendedor');
        echo form_dropdown('id_vendedor',$vendedores,set_value('id_vendedor',$usuario->id_vendedor));
        echo anchor('','Agregar','class="boton" rel="agregar_vendedor" onclick="return false;"');
        ?></li></ul>
        <table id="vendedores" class="tabla">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($vendedores_usuario as $vendedor):?>
                <tr>
                    <td class="vendedor"><?php echo $vendedor->id_vendedor;?>
                    <td class="nombre"><?php echo $vendedor->nombre.' '.$vendedor->apellido;?></td>
                    <td style="text-align:center;"><?php echo anchor('','<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="eliminar_vendedor" onclick="return false;"');?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <div style="margin-bottom: 10px;margin-top:20px;">Si no desea modificar la clave, deje los siguientes campos vacíos</div>
        <ul id="formulario"><li class="left"><?php
        echo form_label('Clave','clave');
        echo form_password(array('name'=>'clave'));
        ?></li><li><?php
        echo form_label('Repetir Clave','repetir_clave');
        echo form_password(array('name'=>'repetir_clave'));
        ?></li></ul>
        <?php
        echo form_submit('editar','Guardar');
        echo form_close();
        ?>
    </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
$(document).ready(function(){
    $('form').submit(function(){
        mostrar_mensaje('Verificando los datos del usuario.','alerta',0);
        var codigo=$('input[name=codigo]').val();
        var nombres=$('input[name=nombres]').val();
        var apellidos=$('input[name=apellidos]').val();
        var usuario=$('input[name=usuario]').val();
        var id=$('input[name=id]').val();
        var usuario_ant=$('input[name=usuario]').val();
        var email=$('input[name=email]').val();
        var clave=$('input[name=clave]').val();
        var repetir_clave=$('input[name=repetir_clave]').val();
        var id_vendedor=$('select[name=id_vendedor]').val();
        var rol=$('select[name=rol]').val();
        var vendedores=[];
        var mensaje='';

        var error=false;
        
        $('table#vendedores tbody > tr').each(function(){
            var vendedor=$(this).find('.vendedor').text();
            vendedores.push(vendedor);
        });
        
        if(rol==2 && vendedores.length==0){
            error=true;
            mensaje=mensaje+'<br/>Debe agregar vendedores al usuario.';
        }
        $('input[name=nombres]').next('span.error_label').remove();
        $('input[name=nombres]').removeClass('error');
        if(!nombres){
            error=true;
            if($('input[name=nombres]').val()==''){
                $('input[name=nombres]').addClass('error');
                $('input[name=nombres]').after('<span class="sub_label error_label">Este campo es obligatorio</span>')
            }
        }
        $('input[name=apellidos]').next('span.error_label').remove();
        $('input[name=apellidos]').removeClass('error');
        if(!apellidos){
            error=true;
            if($('input[name=apellidos]').val()==''){
                $('input[name=apellidos]').addClass('error');
                $('input[name=apellidos]').after('<span class="sub_label error_label">Este campo es obligatorio</span>')
            }
        }
        $('input[name=usuario]').next('span.error_label').remove();
        $('input[name=usuario]').removeClass('error');
        if(usuario.length<5){
            error=true;
            $('input[name=usuario]').addClass('error');
            $('input[name=usuario]').after('<span class="sub_label error_label">* Este campo es obligatorio y debe ser mayor a 4 caracteres</span>')
        }else{
            if($('input[name=usuario]').val()!=$('input[name=usuario_ant]').val()){
                if(!$('input[name=usuario]').next('span').hasClass('correcto_label')){
                    error=true;
                    $('input[name=usuario]').addClass('error');
                    $('input[name=usuario]').after('<span class="sub_label error_label">* El nombre de usuario ya existe.</span>')
                }
            }
        }
        $('input[name=email]').next('span.error_label').remove();
        $('input[name=email]').removeClass('error');
        if(email!='' && !es_mail(email)){
            error=true;
            $('input[name=email]').addClass('error');
            $('input[name=email]').after('<span class="sub_label error_label">Este campo es obligatorio y debe ser un formato de correo válido.</span>')
        }
        $('input[name=clave]').next('span.error_label').remove();
        $('input[name=clave]').removeClass('error');
        if(clave!="" && clave.length<4){
            error=true;
            $('input[name=clave]').addClass('error');
            $('input[name=clave]').after('<span class="sub_label error_label">Este campo es obligatorio y debe ser mayor a 4 caracteres</span>')
        }
        $('input[name=repetir_clave]').next('span.error_label').remove();
        $('input[name=repetir_clave]').removeClass('error');
        if(clave != repetir_clave){
            error=true;
            $('input[name=repetir_clave]').addClass('error');
            $('input[name=repetir_clave]').after('<span class="sub_label error_label">No coincide con la clave</span>')
        }
        /*$('select[name=id_vendedor]').next('span.error_label').remove();
        $('select[name=id_vendedor]').removeClass('error');
        if(rol==2 && id_vendedor==''){
            error=true;
            if($('select[name=id_vendedor]').val()==''){
                $('select[name=id_vendedor]').addClass('error');
                $('select[name=id_vendedor]').after('<span class="sub_label error_label">Este campo es obligatorio</span>')
            }
        }*/
        $('select[name=rol]').next('span.error_label').remove();
        $('select[name=rol]').removeClass('error');
        if(rol==''){
            error=true;
            if($('select[name=rol]').val()==''){
                $('select[name=rol]').addClass('error');
                $('select[name=rol]').after('<span class="sub_label error_label">Este campo es obligatorio</span>')
            }
        }
        if(!error){
            var url=$(this).attr('action')+"/<?php echo $id;?>";
            var form_data={
                id:id,
                codigo:codigo,
                nombres:nombres,
                apellidos:apellidos,
                id_vendedor:id_vendedor,
                vendedores:vendedores,
                rol:rol,
                email:email,
                usuario:usuario,
                clave:clave,
                ajax:1
            }
            $.ajax({
                type: "POST",
                url: url,
                data: form_data,
                dataType: 'json',
                success: function(msg){
                    if(msg['estado']=="correcto"){
                       mostrar_mensaje(msg['mensaje'],'correcto');                       
                    }else{
                       mostrar_mensaje(msg['mensaje'],'error');            
                    }                    
                },
                error: function(x,e){
                  mostrar_error(x,3);
                }
            });
        }else{
            mostrar_mensaje('Verifique los campos marcados con error.'+mensaje,'error');            
        }
        return false;
    });
    function es_mail(correo){
        var expresion= /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if(correo.match(expresion)){
            return true;
        }else{
            return false;
        }
    }
    $('input[name=usuario]').change(function(){
        if($(this).val()==$('input[name=usuario_ant]').val()){
            $('input[name=usuario]').removeClass('error');
            $('input[name=usuario]').next('span.error_label').remove();
            $('input[name=usuario]').next('span.correcto_label').remove();
        }else{
            mostrar_mensaje('Verificando disponibilidad del nombre de usuario.','alerta',0);
            var url="<?php echo site_url('administrar/usuarios/usuario_disponible');?>";
            var form_data={
                usuario:$(this).val()
            }
            $.ajax({
                type: "POST",
                url: url,
                data: form_data,
                dataType:'json',
                success: function(msg){
                    $('input[name=usuario]').next('span.error_label').remove();
                    $('input[name=usuario]').next('span.correcto_label').remove();
                    $('input[name=usuario]').removeClass('error');
                    if(msg['estado']=="correcto"){
                        mostrar_mensaje(msg['mensaje'],'correcto');
                        $('input[name=usuario]').after('<span class="sub_label correcto_label">El nombre de usuario se encuentra disponible.</span>')
                    }else{
                        mostrar_mensaje(msg['mensaje'],'error');
                        $('input[name=usuario]').addClass('error');
                        $('input[name=usuario]').after('<span class="sub_label error_label">* El nombre de usuario ya existe.</span>')
                    }                    
                },
                error: function(x,e){
                  mostrar_error(x,e);
                }
            });
        }
    });
    $('a[rel=agregar_vendedor]').live('click',function(){
        var valor=$("select[name=id_vendedor]").val();
        if(valor){
            var texto=$("select[name=id_vendedor] option[value='"+valor+"']").text();
            texto=texto.replace(valor+' - ','');
            $("select[name=id_vendedor] option[value='"+valor+"']").remove();
            $('table#vendedores tbody').append('<tr><td class="vendedor">'+valor+'</td><td class="nombre">'+texto+'</td><td style="text-align:center;"><?php echo anchor('','<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="eliminar_vendedor" onclick="return false;"');?></td></tr>');
        }
       return false; 
    });
    $('a[rel=eliminar_vendedor]').live('click',function(){
       var fila=$(this).parent().parent();
       var vendedor=fila.find('.vendedor').text();
       var texto=fila.find('.nombre').text();
       $("select[name=id_vendedor]").append('<option value="'+vendedor+'">'+vendedor+' - '+texto+'</option>');
       $(this).parent().parent().remove();
       return false; 
    });
});
</script>