<div style="margin-top:20px;" class="grid8">
    <h2>Cuentas bancarias de la empresa</h2>
    <div style="margin:10px 0px;"><?php echo anchor('administrar/bancos/agregar_cuenta','Agregar cuenta','class="boton" rel="agregar_cuenta"');?></div>
    <table class="tabla" id="lista_cuentas">
        <?php echo $lista_cuentas;?>
    </table>
</div>
<div style="margin-top:20px;" class="grid8">
    <h2>Listado de bancos</h2>
    <div style="margin:10px 0px;"><?php echo anchor('administrar/bancos/agregar_banco','Agregar banco','class="boton" rel="agregar_banco"');?></div>
    <table class="tabla" id="lista_bancos">
        <?php echo $lista_bancos;?>
    </table>
</div>
<div class="clear"></div>
<script	type="text/javascript" language="javascript" src="<?php echo base_url()?>/js/jquery-ui-1.8.custom.min.js"></script>
<script>
    $(function(){
       $('a[rel=agregar_cuenta]').live('click',function(){
          if($('#agregar_cuenta').length==0){
              $('body').append('<div id="agregar_cuenta" title="Agregar cuenta"></div>');
              $('#agregar_cuenta').dialog({
                    autoOpen:true,
                    resizable: true,
                    width: 978,
                    height:540,
                    modal: true                                        
                });              
          }else{
            $('#agregar_cuenta').dialog('open');
          }
          var url=$(this).attr('href');
          $.ajax({
               type: "POST",
               url: url,
               success: function(msg){
                    $('#agregar_cuenta').html(msg);                        
               },
               error: function(x,e){
                       mostrar_error(x,e);			   
               }
            });
          return false; 
       });
       $('a[rel=agregar_banco]').live('click',function(){
            if($('#agregar_banco').length==0){
              $('body').append('<div id="agregar_banco" title="Agregar banco"></div>');
              $('#agregar_banco').dialog({
                    autoOpen:true,
                    resizable: true,
                    width: 978,
                    height:540,
                    modal: true                                        
                });
              var url=$(this).attr('href');
              $.ajax({
		   type: "POST",
		   url: url,
		   success: function(msg){
                        $('#agregar_banco').html(msg);                        
		   },
		   error: function(x,e){
			   mostrar_error(x,e);			   
		   }
		});
		return false;  
          }else{
            $('#agregar_banco').dialog('open');
          }
          return false; 
       });
       $('#agregar_cuenta form').live('submit',function(){
          var banco=$('#agregar_cuenta select[name=banco]').val();
          var cuenta=$('#agregar_cuenta input[name=cuenta]').val();
          var tipo=$('#agregar_cuenta select[name=tipo]').val();
          $('#agregar_cuenta select[name=banco]').removeClass('error');
          $('#agregar_cuenta input[name=cuenta]').removeClass('error');
          $('#agregar_cuenta select[name=tipo]').removeClass('error');
          var error=false;
          if(!banco){
              $('#agregar_cuenta select[name=banco]').addClass('error');
              error=true;
          }
          if(!cuenta){
              $('#agregar_cuenta input[name=cuenta]').addClass('error');
              error=true;
          }
          if(!tipo){
              $('#agregar_cuenta select[name=tipo]').addClass('error');
              error=true;
          }
          if(!error){
              var url=$(this).attr('action');
              var form_data={
                  banco:banco,
                  cuenta:cuenta,
                  tipo:tipo
              };
              $.ajax({
		   type: "POST",
		   url: url,
                   data:form_data,
                   dataType:'json',
		   success: function(msg){
                        if(msg['estado']=='correcto'){
                            $('#lista_cuentas').html(msg['vista']);
                            $('#agregar_cuenta').dialog('close');
                        }else{
                            alert(msg['mensaje']);
                        }
		   },
		   error: function(x,e){
			   mostrar_error(x,e);			   
		   }
               });
          }
          return false; 
       });
       $('#agregar_banco form').live('submit',function(){
          var codigo=$('#agregar_banco input[name=codigo]').val();
          var nombre=$('#agregar_banco input[name=nombre]').val();
          var error=false;
          $('#agregar_banco input[name=codigo]').removeClass('error');
          $('#agregar_banco input[name=nombre]').removeClass('error');
          if(!codigo){
              $('#agregar_banco input[name=codigo]').addClass('error');
              error=true;
          }
          if(!nombre){
              $('#agregar_banco input[name=nombre]').addClass('error');
              error=true;
          }
          if(!error){
              var url=$(this).attr('action');
              var form_data={
                  codigo:codigo,
                  nombre:nombre
              };
              $.ajax({
		   type: "POST",
		   url: url,
                   data:form_data,
                   dataType:'json',
		   success: function(msg){
                        if(msg['estado']=='correcto'){
                            $('#lista_bancos').html(msg['vista']);
                            $('#agregar_banco').dialog('close');
                        }else{
                            alert(msg['mensaje']);
                        }
		   },
		   error: function(x,e){
			   mostrar_error(x,e);			   
		   }
               });
          }
          return false; 
       });
       $('a[rel=eliminar],a[rel=eliminar_banco]').live('click',function(){
            var url=$(this).attr('href');
            var tipo=$(this).attr('rel');
            $('#dialog-confirm').dialog('destroy');
            $('#dialog-confirm').remove();
            if(tipo=='eliminar_banco'){
                $('body').append('<div id="dialog-confirm" title="Eliminar banco"><span class="icono"></span>Está seguro de eliminar el banco seleccionado?</div>');
            }else{
                $('body').append('<div id="dialog-confirm" title="Eliminar cuenta"><span class="icono"></span>Está seguro de eliminar la cuenta seleccionada?</div>');
            }
            $("#dialog-confirm").dialog({
                    resizable: false,
                    height:140,
                    dialogClass: 'dialog_confirm',
                    modal: true,
                    buttons: {
                            'SI': function() {
                                $('#cargando').html('Eliminando item.');
                                $('#cargando').show();

                                  $.ajax({
                                       type: "POST",
                                       url: url,
                                       dataType:'json',
                                       success: function(msg){
                                            if(msg['estado']=='correcto'){
                                                if(tipo=='eliminar_banco'){
                                                    $('#lista_bancos').html(msg['vista']);
                                                }else{
                                                    $('#lista_cuentas').html(msg['vista']);
                                                }                                                
                                                $('#cargando').hide();
                                            }else{
                                                $('#cargando').addClass('error');
                                                $('#cargando').html(msg['mensaje']);
                                            }
                                       },
                                       error: function(x,e){
                                               mostrar_error(x,e);			   
                                       }
                                   });

                                $('#dialog-confirm').dialog('destroy');
                                $('#dialog-confirm').remove();
                                $('#dialog-confirm').dialog('close');
                            },
                            'NO': function() {
                                    $('#dialog-confirm').dialog('destroy');
                                    $('#dialog-confirm').remove();
                                    $('#dialog-confirm').dialog('close');
                            }
                    }
            });              
            return false;
       })
    });
</script>