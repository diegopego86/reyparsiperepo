<thead>
    <tr>
        <th style="height:30px;">#</th>
        <th>Cod. Banco</th>
        <th>Nombre del banco</th>
        <th>Cuenta</th>
        <th>Tipo de cuenta</th>
        <th>-</th>
    </tr>
    </thead>
    <tbody>
    <?php if($cuentas){
        $n=1;
        foreach($cuentas as $cuenta):
                ?>
        <tr>
            <td><?php echo $n;?></td>
            <td><?php echo $cuenta->codigo;?></td>
            <td><?php echo $cuenta->nombre;?></td>
            <td><?php echo $cuenta->cuenta;?></td>
            <td><?php echo $tipos[$cuenta->tipo];?></td>
            <td><?php echo anchor('administrar/bancos/eliminar_cuenta/'.$cuenta->id,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="eliminar"');?></td>
        </tr>
    <?php
          $n++; endforeach;
        }else{
    ?>
        <tr><td colspan="6">No ha agregado cuentas.</td></tr>
        <?php }?>
    </tbody>