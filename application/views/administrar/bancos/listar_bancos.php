<thead>
    <tr>
        <th style="height:30px;">#</th>
        <th>Cod. Banco</th>
        <th>Nombre del banco</th>
        <th>-</th>
    </tr>
    </thead>
    <tbody>
<?php if($bancos){
    $n=1;
    foreach($bancos as $banco):
            ?>
    <tr>
        <td><?php echo $n;?></td>
        <td><?php echo $banco->codigo;?></td>
        <td><?php echo $banco->nombre;?></td>
        <td><?php echo anchor('administrar/bancos/eliminar_banco/'.$banco->id,'<span class="iconos_blanco eliminar"></span>','class="boton boton_rojo peque" rel="eliminar_banco"');?></td>
    </tr>
<?php
      $n++; endforeach;
    }else{
?>
    <tr><td colspan="3">No ha agregado bancos.</td></tr>
    <?php }?>
</tbody>