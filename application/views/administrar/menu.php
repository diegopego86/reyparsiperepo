<?php 
if(!isset($seleccionado)){
	$seleccionado='';
}
?>
<div id="sub-menu-container">
    <div class="container16">
        <ul id="sub-menu">
        <?php
        if ($this->session->userdata('rol')==1){?>
            <li <?php if($seleccionado=='acceso'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/acceso','Acceso');?></li>    
            <li <?php if($seleccionado=='usuarios'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/usuarios','Usuarios');?></li>
            <li <?php if($seleccionado=='pedidos'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/pedidos','Pedidos');?></li>
            <li <?php if($seleccionado=='cotizaciones'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/cotizaciones','Cotizaciones');?></li>
            <?php }   ?>
            <?php
        if ($this->session->userdata('rol')==1 || $this->session->userdata('rol')==4){?>
            <li <?php if($seleccionado=='recibos'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/recibos','Recibos');?></li>
            <li <?php if($seleccionado=='cartera'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/cartera','Cartera');?></li>
            <li <?php if($seleccionado=='bancos'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/bancos','Bancos');?></li>    
            <?php }   ?>
            <?php
        if ($this->session->userdata('rol')==1){?>
            <li <?php if($seleccionado=='contador'){ echo 'class="seleccionado"';}?>><?php echo anchor('administrar/contador','Contador de pedidos');?></li>                
            <?php }   ?>
        </ul>
        <div class="clear"></div>
    </div>
</div>