<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Permisos {

    var $CI;                        // Instancia CI
    var $where = array();
    var $set = array();
    var $required = array();

    function Permisos()
    {
        $this->CI =& get_instance();
        // Se obitene el rol del usuario actual
        $this->rol = ($this->CI->session->userdata('rolid')) ? $this->CI->session->userdata('rolid') : 0;
    }
    //Permisos del rol, devuelve un arreglo con los controladores a los que tiene acceso.
    function get_permisos_rol($rol)
    {
		$this->db->select('DISTINCT(controladores.controlador)');		
        $this->CI->db->join('controladores', 'controladores.id = permisos.controlador');
        $this->CI->db->where('rol', $rol);
		$query=$this->CI->db->get('permisos');
        // set permisos array and return
        if ($query->num_rows())
        {
            foreach ($query->result() as $row)
            {
                $permisos[] = $row->controlador;
            }
            return $permisos;
        }
        else
        {
            return false;
        }
    }
    //Se obtiene los permisos por controlador y devuelve un arreglo con las funciones a las que se tiene acceso dentro del controlador
    function get_permisos_controlador($controlador = '')
    {
        // where
        if ($controlador)
        {
            $this->CI->db->where('controlador', $controlador);
        }    

        // return
        $query = $this->CI->db->get('controladores');

        if ($query->num_rows())
        {    
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    //Agregar permisos a rol
    function agregar_permisos($rol)
    {
        // delete all permisos on this groupID first
        $this->CI->db->where('rol', $rol);
        $this->CI->db->delete('permisos');

        // get post
        $post = $this->CI->easysite->get_post();
        foreach ($post as $key => $value)
        {
            if (preg_match('/^perm([0-9]+)/i', $key, $matches))
            {
                $this->CI->db->set('rol', $rol);
                $this->CI->db->set('controlador', $matches[1]);
                $this->CI->db->insert('permisos');
            }
        }
        return true;
    }
	function verificarPermisosFuncion($controlador,$accion){
		if(!$controlador || !$accion){
			return 0;			
		}else{
			$this->CI->db->select('count(*) as cantidad');
			$this->CI->db->join('controladores','controladores.id=permisos.controlador','left');
			$this->CI->db->where('rol',$this->rol);
			$this->CI->db->where('controladores.controlador',$controlador);
			$this->CI->db->where('controladores.accion',$accion);
			$query = $this->CI->db->get('permisos')->row();
			if($query->cantidad>0){
				return true;
			}else{
				return false;
			}
		}
	}
	function nuevo(){
		return false;
	}
}
?>