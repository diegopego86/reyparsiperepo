function mostrar_error(x,e){
    var mensaje='';
    if(x.status==0){
        mensaje='Se perdió la conexión!!. Por favor verifique su conexión a internet.';
    }else if(x.status==404){
        mensaje='La url buscada no se encontró.';
    }else if(x.status==500){
        mensaje='Error interno del servidor.'+x.responseText;
    }else if(e=='parsererror'){
        mensaje='Error.\nParsing JSON Request failed.'+x.responseText;
    }else if(e=='timeout'){
        mensaje='Se ha demorado mucho la operación, inténtelo nuevamente.';
    }else {
        mensaje='Error desconocido. '+x.responseText;
    }
    mostrar_mensaje(mensaje,'error',10000);    
}
var timeout;
function mostrar_mensaje(mensaje, tipo, tiempo){
   $('#mensajes').removeClass('error alerta correcto');
   $('#mensajes').html('<span class="icono"></span> '+mensaje+'<span id="cerrar_mensaje"></span>');
   $('#mensajes').addClass(tipo);
   $('#mensajes').show();
   if(timeout){
        window.clearTimeout(timeout);
   }
   if(tiempo==0){
       if(timeout){
            window.clearTimeout(timeout);
       }
   }else{
       if(!tiempo){
           tiempo=5000;
       }
   }
   if(tiempo>0){
        timeout = window.setTimeout(function(){
            $('#mensajes').slideUp('fast');
        }, tiempo);
   }
}
$(function(){    
   $('#mensajes').click(function(){
       if(timeout){
            window.clearTimeout(timeout);
       }
        $(this).fadeOut('fast');
    });
});